import requests
from bs4 import BeautifulSoup
import random
import string

def CreateUser(queue, url, username, proxy, data_sitekey):
    session = requests.session()
    cookies = {}
    header ={}
    resp =''
    proxies={}
    # Read account register page
    got_page = False
    get_counter = 0
    while not got_page and get_counter < 6:
        try:
            selected_proxy = random.choice(proxy)
            proxies = {'https': 'https://{}:{}'.format(selected_proxy['ip'], selected_proxy['port'])}
            resp = session.get(url, headers=header, proxies=proxies, timeout=(15,20))
            if resp.status_code == 200:
                got_page = True
                break
            else:
                get_counter +=1
        except Exception as e:
            get_counter += 1
            print(e)
    if not got_page:
        print('Process failed')
        return False

    # get Tokens needed for send from the page
    header['Referer'] = resp.request.url
    cookies['csrftoken'] = resp.cookies.get('csrftoken')
    soup = BeautifulSoup(resp.content, features='lxml')
    csrf_form_token = soup.find("input", {'name':'csrfmiddlewaretoken'})['value']
    try:
        recapture  = soup.find(id="id_prove_you_are_human")['data-sitekey']
    except:
        recapture = data_sitekey

    # random sets on other params
    password = "".join([random.choice(string.ascii_letters+ string.digits) for x in range(12)])
    gender = random.choice(["m", "f", "c", "s"])
    birth_month = str(random.randint(1,12))
    birth_day = str(random.randint(1,27))
    birth_year = str(random.randint(1960, 1990))

    #create parm dict
    params = {'username': username.lower(),
              'password': password.lower(),
              'gender': gender,
              'birthday_month':birth_month,
              'birthday_day': birth_day,
              'birthday_year': birth_year,
              'csrfmiddlewaretoken': csrf_form_token,
              'init_tokens':'1000',
              'terms': True,
              'privacy_policy':True,
              'g-recaptcha-response': recapture}

    #send form
    resp = session.post(url, params, headers=header, proxies=proxies, timeout=(25,35))
    # if it works send back to main code
    if resp.status_code == 200:
        if 'id_password' not in str(resp.content):
            print('worked ok')
            queue.put_nowait(params)
        else:
            print('Rejected')
