(function() {
    var k, aa = "function" == typeof Object.defineProperties ? Object.defineProperty : function(a, b, c) {
        a != Array.prototype && a != Object.prototype && (a[b] = c.value)
    }
    , ba = "undefined" != typeof window && window === this ? this : "undefined" != typeof global && null != global ? global : this;
    function da() {
        da = function() {}
        ;
        ba.Symbol || (ba.Symbol = ea)
    }
    var ea = function() {
        var a = 0;
        return function(b) {
            return "jscomp_symbol_" + (b || "") + a++
        }
    }();
    function fa() {
        da();
        var a = ba.Symbol.iterator;
        a || (a = ba.Symbol.iterator = ba.Symbol("iterator"));
        "function" != typeof Array.prototype[a] && aa(Array.prototype, a, {
            configurable: !0,
            writable: !0,
            value: function() {
                return ha(this)
            }
        });
        fa = function() {}
    }
    function ha(a) {
        var b = 0;
        return ia(function() {
            return b < a.length ? {
                done: !1,
                value: a[b++]
            } : {
                done: !0
            }
        })
    }
    function ia(a) {
        fa();
        a = {
            next: a
        };
        a[ba.Symbol.iterator] = function() {
            return this
        }
        ;
        return a
    }
    function l(a) {
        fa();
        da();
        fa();
        var b = a[Symbol.iterator];
        return b ? b.call(a) : ha(a)
    }
    var ja = "function" == typeof Object.create ? Object.create : function(a) {
        function b() {}
        b.prototype = a;
        return new b
    }
    , ka;
    if ("function" == typeof Object.setPrototypeOf)
        ka = Object.setPrototypeOf;
    else {
        var la;
        a: {
            var ma = {
                Da: !0
            }
              , na = {};
            try {
                na.__proto__ = ma;
                la = na.Da;
                break a
            } catch (a) {}
            la = !1
        }
        ka = la ? function(a, b) {
            a.__proto__ = b;
            if (a.__proto__ !== b)
                throw new TypeError(a + " is not extensible");
            return a
        }
        : null
    }
    var oa = ka;
    function m(a, b) {
        a.prototype = ja(b.prototype);
        a.prototype.constructor = a;
        if (oa)
            oa(a, b);
        else
            for (var c in b)
                if ("prototype" != c)
                    if (Object.defineProperties) {
                        var d = Object.getOwnPropertyDescriptor(b, c);
                        d && Object.defineProperty(a, c, d)
                    } else
                        a[c] = b[c];
        a.hs = b.prototype
    }
    function pa(a, b) {
        return Object.prototype.hasOwnProperty.call(a, b)
    }
    function qa(a, b) {
        if (b) {
            for (var c = ba, d = a.split("."), e = 0; e < d.length - 1; e++) {
                var f = d[e];
                f in c || (c[f] = {});
                c = c[f]
            }
            d = d[d.length - 1];
            e = c[d];
            f = b(e);
            f != e && null != f && aa(c, d, {
                configurable: !0,
                writable: !0,
                value: f
            })
        }
    }
    qa("Object.assign", function(a) {
        return a ? a : function(a, c) {
            for (var b = 1; b < arguments.length; b++) {
                var e = arguments[b];
                if (e)
                    for (var f in e)
                        pa(e, f) && (a[f] = e[f])
            }
            return a
        }
    });
    qa("Number.isNaN", function(a) {
        return a ? a : function(a) {
            return "number" === typeof a && isNaN(a)
        }
    });
    function ra(a, b) {
        fa();
        a instanceof String && (a += "");
        var c = 0
          , d = {
            next: function() {
                if (c < a.length) {
                    var e = c++;
                    return {
                        value: b(e, a[e]),
                        done: !1
                    }
                }
                d.next = function() {
                    return {
                        done: !0,
                        value: void 0
                    }
                }
                ;
                return d.next()
            }
        };
        d[Symbol.iterator] = function() {
            return d
        }
        ;
        return d
    }
    qa("Array.prototype.keys", function(a) {
        return a ? a : function() {
            return ra(this, function(a) {
                return a
            })
        }
    });
    qa("Object.is", function(a) {
        return a ? a : function(a, c) {
            return a === c ? 0 !== a || 1 / a === 1 / c : a !== a && c !== c
        }
    });
    qa("Array.prototype.includes", function(a) {
        return a ? a : function(a, c) {
            var b = this;
            b instanceof String && (b = String(b));
            for (var e = b.length, f = c || 0; f < e; f++)
                if (b[f] == a || Object.is(b[f], a))
                    return !0;
            return !1
        }
    });
    function sa(a, b, c) {
        if (null == a)
            throw new TypeError("The 'this' value for String.prototype." + c + " must not be null or undefined");
        if (b instanceof RegExp)
            throw new TypeError("First argument to String.prototype." + c + " must not be a regular expression");
        return a + ""
    }
    qa("String.prototype.includes", function(a) {
        return a ? a : function(a, c) {
            return -1 !== sa(this, a, "includes").indexOf(a, c || 0)
        }
    });
    qa("Promise", function(a) {
        function b(a) {
            this.ii = 0;
            this.am = void 0;
            this.Vh = [];
            var b = this.Uk();
            try {
                a(b.resolve, b.reject)
            } catch (p) {
                b.reject(p)
            }
        }
        function c() {
            this.Ce = null
        }
        function d(a) {
            return a instanceof b ? a : new b(function(b) {
                b(a)
            }
            )
        }
        if (a)
            return a;
        c.prototype.Vm = function(a) {
            null == this.Ce && (this.Ce = [],
            this.fq());
            this.Ce.push(a)
        }
        ;
        c.prototype.fq = function() {
            var a = this;
            this.Wm(function() {
                a.yq()
            })
        }
        ;
        var e = ba.setTimeout;
        c.prototype.Wm = function(a) {
            e(a, 0)
        }
        ;
        c.prototype.yq = function() {
            for (; this.Ce && this.Ce.length; ) {
                var a = this.Ce;
                this.Ce = [];
                for (var b = 0; b < a.length; ++b) {
                    var c = a[b];
                    delete a[b];
                    try {
                        c()
                    } catch (t) {
                        this.gq(t)
                    }
                }
            }
            this.Ce = null
        }
        ;
        c.prototype.gq = function(a) {
            this.Wm(function() {
                throw a;
            })
        }
        ;
        b.prototype.Uk = function() {
            function a(a) {
                return function(d) {
                    c || (c = !0,
                    a.call(b, d))
                }
            }
            var b = this
              , c = !1;
            return {
                resolve: a(this.gr),
                reject: a(this.Xl)
            }
        }
        ;
        b.prototype.gr = function(a) {
            if (a === this)
                this.Xl(new TypeError("A Promise cannot resolve to itself"));
            else if (a instanceof b)
                this.vr(a);
            else {
                a: switch (typeof a) {
                case "object":
                    var c = null != a;
                    break a;
                case "function":
                    c = !0;
                    break a;
                default:
                    c = !1
                }
                c ? this.fr(a) : this.Fn(a)
            }
        }
        ;
        b.prototype.fr = function(a) {
            var b = void 0;
            try {
                b = a.then
            } catch (p) {
                this.Xl(p);
                return
            }
            "function" == typeof b ? this.wr(b, a) : this.Fn(a)
        }
        ;
        b.prototype.Xl = function(a) {
            this.pp(2, a)
        }
        ;
        b.prototype.Fn = function(a) {
            this.pp(1, a)
        }
        ;
        b.prototype.pp = function(a, b) {
            if (0 != this.ii)
                throw Error("Cannot settle(" + a + ", " + b | "): Promise already settled in state" + this.ii);
            this.ii = a;
            this.am = b;
            this.zq()
        }
        ;
        b.prototype.zq = function() {
            if (null != this.Vh) {
                for (var a = this.Vh, b = 0; b < a.length; ++b)
                    a[b].call(),
                    a[b] = null;
                this.Vh = null
            }
        }
        ;
        var f = new c;
        b.prototype.vr = function(a) {
            var b = this.Uk();
            a.Li(b.resolve, b.reject)
        }
        ;
        b.prototype.wr = function(a, b) {
            var c = this.Uk();
            try {
                a.call(b, c.resolve, c.reject)
            } catch (t) {
                c.reject(t)
            }
        }
        ;
        b.prototype.then = function(a, c) {
            function d(a, b) {
                return "function" == typeof a ? function(b) {
                    try {
                        e(a(b))
                    } catch (ca) {
                        f(ca)
                    }
                }
                : b
            }
            var e, f, g = new b(function(a, b) {
                e = a;
                f = b
            }
            );
            this.Li(d(a, e), d(c, f));
            return g
        }
        ;
        b.prototype["catch"] = function(a) {
            return this.then(void 0, a)
        }
        ;
        b.prototype.Li = function(a, b) {
            function c() {
                switch (d.ii) {
                case 1:
                    a(d.am);
                    break;
                case 2:
                    b(d.am);
                    break;
                default:
                    throw Error("Unexpected state: " + d.ii);
                }
            }
            var d = this;
            null == this.Vh ? f.Vm(c) : this.Vh.push(function() {
                f.Vm(c)
            })
        }
        ;
        b.resolve = d;
        b.reject = function(a) {
            return new b(function(b, c) {
                c(a)
            }
            )
        }
        ;
        b.race = function(a) {
            return new b(function(b, c) {
                for (var e = l(a), f = e.next(); !f.done; f = e.next())
                    d(f.value).Li(b, c)
            }
            )
        }
        ;
        b.all = function(a) {
            var c = l(a)
              , e = c.next();
            return e.done ? d([]) : new b(function(a, b) {
                function f(b) {
                    return function(c) {
                        g[b] = c;
                        h--;
                        0 == h && a(g)
                    }
                }
                var g = []
                  , h = 0;
                do
                    g.push(void 0),
                    h++,
                    d(e.value).Li(f(g.length - 1), b),
                    e = c.next();
                while (!e.done)
            }
            )
        }
        ;
        return b
    });
    qa("WeakMap", function(a) {
        function b(a) {
            this.Hh = (f += Math.random() + 1).toString();
            if (a) {
                da();
                fa();
                a = l(a);
                for (var b; !(b = a.next()).done; )
                    b = b.value,
                    this.set(b[0], b[1])
            }
        }
        function c(a) {
            pa(a, e) || aa(a, e, {
                value: {}
            })
        }
        function d(a) {
            var b = Object[a];
            b && (Object[a] = function(a) {
                c(a);
                return b(a)
            }
            )
        }
        if (function() {
            if (!a || !Object.seal)
                return !1;
            try {
                var b = Object.seal({})
                  , c = Object.seal({})
                  , d = new a([[b, 2], [c, 3]]);
                if (2 != d.get(b) || 3 != d.get(c))
                    return !1;
                d["delete"](b);
                d.set(c, 4);
                return !d.has(b) && 4 == d.get(c)
            } catch (t) {
                return !1
            }
        }())
            return a;
        var e = "$jscomp_hidden_" + Math.random().toString().substring(2);
        d("freeze");
        d("preventExtensions");
        d("seal");
        var f = 0;
        b.prototype.set = function(a, b) {
            c(a);
            if (!pa(a, e))
                throw Error("WeakMap key fail: " + a);
            a[e][this.Hh] = b;
            return this
        }
        ;
        b.prototype.get = function(a) {
            return pa(a, e) ? a[e][this.Hh] : void 0
        }
        ;
        b.prototype.has = function(a) {
            return pa(a, e) && pa(a[e], this.Hh)
        }
        ;
        b.prototype["delete"] = function(a) {
            return pa(a, e) && pa(a[e], this.Hh) ? delete a[e][this.Hh] : !1
        }
        ;
        return b
    });
    qa("Map", function(a) {
        function b() {
            var a = {};
            return a.se = a.next = a.head = a
        }
        function c(a, b) {
            var c = a.ke;
            return ia(function() {
                if (c) {
                    for (; c.head != a.ke; )
                        c = c.se;
                    for (; c.next != c.head; )
                        return c = c.next,
                        {
                            done: !1,
                            value: b(c)
                        };
                    c = null
                }
                return {
                    done: !0,
                    value: void 0
                }
            })
        }
        function d(a, b) {
            var c = b && typeof b;
            "object" == c || "function" == c ? f.has(b) ? c = f.get(b) : (c = "" + ++h,
            f.set(b, c)) : c = "p_" + b;
            var d = a.wh[c];
            if (d && pa(a.wh, c))
                for (var e = 0; e < d.length; e++) {
                    var g = d[e];
                    if (b !== b && g.key !== g.key || b === g.key)
                        return {
                            id: c,
                            list: d,
                            index: e,
                            Yb: g
                        }
                }
            return {
                id: c,
                list: d,
                index: -1,
                Yb: void 0
            }
        }
        function e(a) {
            this.wh = {};
            this.ke = b();
            this.size = 0;
            if (a) {
                a = l(a);
                for (var c; !(c = a.next()).done; )
                    c = c.value,
                    this.set(c[0], c[1])
            }
        }
        if (function() {
            if (!a || !a.prototype.entries || "function" != typeof Object.seal)
                return !1;
            try {
                var b = Object.seal({
                    x: 4
                })
                  , c = new a(l([[b, "s"]]));
                if ("s" != c.get(b) || 1 != c.size || c.get({
                    x: 4
                }) || c.set({
                    x: 4
                }, "t") != c || 2 != c.size)
                    return !1;
                var d = c.entries()
                  , e = d.next();
                if (e.done || e.value[0] != b || "s" != e.value[1])
                    return !1;
                e = d.next();
                return e.done || 4 != e.value[0].x || "t" != e.value[1] || !d.next().done ? !1 : !0
            } catch (B) {
                return !1
            }
        }())
            return a;
        da();
        fa();
        var f = new WeakMap;
        e.prototype.set = function(a, b) {
            var c = d(this, a);
            c.list || (c.list = this.wh[c.id] = []);
            c.Yb ? c.Yb.value = b : (c.Yb = {
                next: this.ke,
                se: this.ke.se,
                head: this.ke,
                key: a,
                value: b
            },
            c.list.push(c.Yb),
            this.ke.se.next = c.Yb,
            this.ke.se = c.Yb,
            this.size++);
            return this
        }
        ;
        e.prototype["delete"] = function(a) {
            a = d(this, a);
            return a.Yb && a.list ? (a.list.splice(a.index, 1),
            a.list.length || delete this.wh[a.id],
            a.Yb.se.next = a.Yb.next,
            a.Yb.next.se = a.Yb.se,
            a.Yb.head = null,
            this.size--,
            !0) : !1
        }
        ;
        e.prototype.clear = function() {
            this.wh = {};
            this.ke = this.ke.se = b();
            this.size = 0
        }
        ;
        e.prototype.has = function(a) {
            return !!d(this, a).Yb
        }
        ;
        e.prototype.get = function(a) {
            return (a = d(this, a).Yb) && a.value
        }
        ;
        e.prototype.entries = function() {
            return c(this, function(a) {
                return [a.key, a.value]
            })
        }
        ;
        e.prototype.keys = function() {
            return c(this, function(a) {
                return a.key
            })
        }
        ;
        e.prototype.values = function() {
            return c(this, function(a) {
                return a.value
            })
        }
        ;
        e.prototype.forEach = function(a, b) {
            for (var c = this.entries(), d; !(d = c.next()).done; )
                d = d.value,
                a.call(b, d[1], d[0], this)
        }
        ;
        e.prototype[Symbol.iterator] = e.prototype.entries;
        var h = 0;
        return e
    });
    qa("Set", function(a) {
        function b(a) {
            this.Vd = new Map;
            if (a) {
                a = l(a);
                for (var b; !(b = a.next()).done; )
                    this.add(b.value)
            }
            this.size = this.Vd.size
        }
        if (function() {
            if (!a || !a.prototype.entries || "function" != typeof Object.seal)
                return !1;
            try {
                var b = Object.seal({
                    x: 4
                })
                  , d = new a(l([b]));
                if (!d.has(b) || 1 != d.size || d.add(b) != d || 1 != d.size || d.add({
                    x: 4
                }) != d || 2 != d.size)
                    return !1;
                var e = d.entries()
                  , f = e.next();
                if (f.done || f.value[0] != b || f.value[1] != b)
                    return !1;
                f = e.next();
                return f.done || f.value[0] == b || 4 != f.value[0].x || f.value[1] != f.value[0] ? !1 : e.next().done
            } catch (h) {
                return !1
            }
        }())
            return a;
        da();
        fa();
        b.prototype.add = function(a) {
            this.Vd.set(a, a);
            this.size = this.Vd.size;
            return this
        }
        ;
        b.prototype["delete"] = function(a) {
            a = this.Vd["delete"](a);
            this.size = this.Vd.size;
            return a
        }
        ;
        b.prototype.clear = function() {
            this.Vd.clear();
            this.size = 0
        }
        ;
        b.prototype.has = function(a) {
            return this.Vd.has(a)
        }
        ;
        b.prototype.entries = function() {
            return this.Vd.entries()
        }
        ;
        b.prototype.values = function() {
            return this.Vd.values()
        }
        ;
        b.prototype.keys = b.prototype.values;
        b.prototype[Symbol.iterator] = b.prototype.values;
        b.prototype.forEach = function(a, b) {
            var c = this;
            this.Vd.forEach(function(d) {
                return a.call(b, d, d, c)
            })
        }
        ;
        return b
    });
    qa("String.prototype.startsWith", function(a) {
        return a ? a : function(a, c) {
            var b = sa(this, a, "startsWith");
            a += "";
            for (var e = b.length, f = a.length, h = Math.max(0, Math.min(c | 0, b.length)), g = 0; g < f && h < e; )
                if (b[h++] != a[g++])
                    return !1;
            return g >= f
        }
    });
    qa("Array.prototype.values", function(a) {
        return a ? a : function() {
            return ra(this, function(a, c) {
                return c
            })
        }
    });
    var ta = window.Raven
      , ua = {};
    function va(a, b) {
        void 0 !== ta && (ua[a] = b,
        ta.setTagsContext(ua))
    }
    ;function wa() {
        this.Xd = !0;
        this.sd = this.yj = 10
    }
    function ya(a, b) {
        this.ir = a;
        this.listener = b
    }
    ya.prototype.removeListener = function() {
        this.ir.removeListener(this.listener)
    }
    ;
    function za(a, b) {
        b.add(a)
    }
    function n(a, b) {
        this.qn = a;
        this.Rf = [];
        this.history = [];
        this.options = Object.assign(new wa, b)
    }
    function q(a, b, c) {
        c = void 0 === c ? !0 : c;
        a.Rf.push(b);
        if (c) {
            c = l(a.history);
            for (var d = c.next(); !d.done; d = c.next())
                b(d.value)
        }
        a.Rf.length > a.options.sd && r('EventRouter "' + a.qn + '" has too many listeners', {
            listeners: a.Rf.length,
            "max-listeners": a.options.sd
        });
        return new ya(a,b)
    }
    n.prototype.once = function(a, b) {
        function c(b) {
            a(b);
            d.removeListener(c)
        }
        var d = this;
        return q(this, c, void 0 === b ? !0 : b)
    }
    ;
    function u(a, b) {
        a.history.push(b) > a.options.yj && a.history.shift();
        for (var c = l(a.Rf), d = c.next(); !d.done; d = c.next()) {
            d = d.value;
            try {
                d(b)
            } catch (e) {
                d = {
                    Wr: {
                        "event:name": a.qn,
                        "event:listeners": a.Rf.length
                    }
                },
                void 0 !== ta && ta.captureException(e, d)
            }
        }
    }
    n.prototype.removeListener = function(a) {
        a = this.Rf.indexOf(a);
        0 <= a && this.Rf.splice(a, 1)
    }
    ;
    function Aa() {
        this.Ik = []
    }
    Aa.prototype.add = function(a) {
        this.Ik.push(a)
    }
    ;
    function Da(a) {
        for (var b = l(a.Ik), c = b.next(); !c.done; c = b.next())
            c.value.removeListener();
        a.Ik = []
    }
    ;function v() {
        this.sg = [];
        this.Vi = new n("didReposition",{
            Xd: !1
        });
        this.a = document.createElement("div");
        this.a.style.height = "100%";
        this.a.style.width = "100%";
        this.a.style.position = "absolute";
        this.a.style.overflow = "hidden";
        this.a.style.webkitTapHighlightColor = "transparent";
        this.a.id = this.constructor.name
    }
    k = v.prototype;
    k.C = function() {}
    ;
    k.Y = function() {
        this.C();
        for (var a = l(this.children()), b = a.next(); !b.done; b = a.next())
            b.value.Y();
        u(this.Vi, {})
    }
    ;
    k.Tm = function() {}
    ;
    k.oh = function() {
        this.Tm();
        for (var a = l(this.children()), b = a.next(); !b.done; b = a.next())
            b.value.oh()
    }
    ;
    k.N = function(a, b) {
        void 0 === b && (b = this.a);
        b.appendChild(a.a);
        this.sg.push(a);
        a.parent = this;
        return a
    }
    ;
    function Ea(a, b) {
        if (void 0 === c)
            var c = a.a;
        c.insertBefore(b.a, a.a.firstChild);
        a.sg.unshift(b);
        b.parent = a;
        return b
    }
    k.removeChild = function(a) {
        var b = this.sg.indexOf(a);
        if (isNaN(b))
            w("tried removing component that doesn't exist");
        else {
            this.sg.splice(b, 1);
            a.parent === this && (a.parent = void 0);
            Ga(a);
            b = l(a.children());
            for (var c = b.next(); !c.done; c = b.next())
                c.value.Bi();
            a.Bi()
        }
    }
    ;
    function Ga(a) {
        var b = a.a.parentNode;
        null !== b ? b.removeChild(a.a) : w("couldn't find parent element to use for node removal")
    }
    function Ha(a) {
        for (var b = l(a.children()), c = b.next(); !c.done; c = b.next())
            c = c.value,
            c.parent === a && (c.parent = void 0),
            Ga(c);
        a.sg = []
    }
    k.children = function() {
        return this.sg
    }
    ;
    k.Bi = function() {}
    ;
    var Ia = window.Hls, Ja;
    function x() {
        if (void 0 === Ja) {
            var a = !0;
            try {
                window.localStorage.setItem("cbModernizr", "cbModernizr"),
                window.localStorage.removeItem("cbModernizr")
            } catch (b) {
                a = !1
            }
            Ja = a
        }
        return Ja
    }
    function Ka() {
        var a = document.createElement("a").style;
        a.cssText = "background-color:rgba(150,255,150,.5)";
        return -1 < ("" + a.backgroundColor).indexOf("rgba")
    }
    function La() {
        for (var a = "", b = l(["-webkit-", "-moz-", "-o-", "-ms-"]), c = b.next(); !c.done; c = b.next())
            a += "background-image:" + c.value + "linear-gradient(left top, #9f9, white);\n";
        b = document.createElement("a").style;
        b.cssText = a;
        return -1 < ("" + b.backgroundImage).indexOf("gradient")
    }
    function Ma() {
        try {
            return !!document.createElement("audio").canPlayType
        } catch (a) {
            return !1
        }
    }
    var Na;
    function Oa() {
        if (void 0 !== Na)
            return Na;
        Na = !1;
        try {
            if ("webkitAudioContext"in window)
                var a = new webkitAudioContext;
            else if ("AudioContext"in window)
                a = new AudioContext;
            else
                return Na;
            if (null === a)
                return Na;
            Na = "function" === typeof a.createGain && "function" === typeof a.decodeAudioData && "function" === typeof a.createBufferSource && "function" === typeof a.createBufferSource().start;
            "function" === typeof a.close && a.close()
        } catch (b) {
            w("Error checking for audio context: " + b)
        }
        return Na
    }
    var Pa;
    function Sa() {
        if (void 0 !== Pa)
            return Pa;
        var a = document.createElement("audio");
        return Pa = "function" === typeof a.canPlayType && "" !== a.canPlayType("audio/mpeg;codecs=mp3")
    }
    Object.prototype.hasOwnProperty.call(window, "onorientationchange");
    function Ta() {
        var a = document.createElement("video").canPlayType("application/vnd.apple.mpegurl");
        return "probably" === a || "maybe" === a
    }
    function Ua() {
        return window.hasOwnProperty("requestAnimationFrame") ? window.requestAnimationFrame : window.hasOwnProperty("webkitRequestAnimationFrame") ? window.webkitRequestAnimationFrame : window.hasOwnProperty("mozRequestAnimationFrame") ? window.mozRequestAnimationFrame : function(a) {
            setTimeout(function() {
                a(0)
            }, 1E3 / 7);
            return Date.now()
        }
    }
    var Va;
    function Wa() {
        if (void 0 !== Va)
            return Va;
        try {
            var a = Object.defineProperty({}, "passive", {
                get: function() {
                    Va = !0
                }
            });
            window.addEventListener("testPassive", function() {}, a);
            window.removeEventListener("testPassive", function() {}, a);
            void 0 === Va && (Va = !1)
        } catch (b) {
            Va = !1
        }
        return Va
    }
    var Xa;
    function Ya() {
        if (void 0 !== Xa)
            return Xa;
        var a = document.documentElement.scrollTop;
        document.documentElement.style.height = window.innerHeight + 1 + "px";
        document.documentElement.scrollTop = a + 1;
        Xa = document.documentElement.scrollTop !== a ? document.documentElement : document.body;
        document.documentElement.style.height = "";
        document.documentElement.scrollTop = a;
        return Xa
    }
    function Za() {
        return void 0 !== window.performance && void 0 !== window.performance.now
    }
    ;var $a = {}
      , ab = {};
    function bb(a) {
        return Number.isNaN(Number(a)) ? 0 : Number(a)
    }
    function y(a, b, c, d, e) {
        d = void 0 === d ? !1 : d;
        e = void 0 === e ? !1 : e;
        if (b === window)
            var f = $a[a] = bb($a[a]) + 1;
        else if (b === document)
            f = ab[a] = bb(ab[a]) + 1;
        else {
            var h = "data-listener-count-" + a;
            f = bb(b.getAttribute(h)) + 1;
            b.setAttribute(h, "" + f)
        }
        42 < f && r('event listener "' + a + '" has too many listeners. Element: ' + (b === document ? "document" : b === window ? "window" : b.outerHTML.split(">")[0] + ">"), {
            listeners: f,
            "max-listeners": 42
        });
        void 0 !== window.addEventListener ? (d = Wa() ? {
            capture: d,
            passive: e
        } : d,
        b.addEventListener(a, c, d)) : void 0 !== window.attachEvent ? b.attachEvent("on" + a, c) : w("Could not resolve addEventListenerPoly( " + a + " ) to " + b)
    }
    function cb(a, b, c, d) {
        d = void 0 === d ? !1 : d;
        var e = void 0 === e ? !1 : e;
        if (b === window)
            var f = $a[a] = bb($a[a]) - 1;
        else if (b === document)
            f = ab[a] = bb(ab[a]) - 1;
        else {
            var h = "data-listener-count-" + a;
            f = bb(b.getAttribute(h)) - 1;
            b.setAttribute(h, "" + f)
        }
        0 > f && w('event listener "' + a + '" removed without being added', {
            listeners: f
        });
        void 0 !== window.removeEventListener ? (d = Wa() ? {
            capture: d,
            passive: e
        } : d,
        b.removeEventListener(a, c, d)) : void 0 !== window.detachEvent ? b.detachEvent("on" + a, c) : w("Could not resolve removeEventListenerPoly( " + a + " ) from " + b)
    }
    ;function db(a) {
        for (var b = "", c = l(Object.keys(a)), d = c.next(); !d.done; d = c.next()) {
            d = d.value;
            "" !== b && (b += "&");
            var e = a[d];
            void 0 === e && (e = "");
            b += encodeURIComponent(d) + "=" + encodeURIComponent(e)
        }
        return b
    }
    function eb() {
        var a = window.location.search;
        0 < a.length && "?" === a[0] && (a = a.slice(1));
        var b = {};
        a = l(a.split("&"));
        for (var c = a.next(); !c.done; c = a.next())
            c = c.value,
            c.includes("=") && (c = c.split("="),
            0 < c[0].length && (b[decodeURIComponent(c[0])] = 0 < c[1].length ? decodeURIComponent(c[1]) : void 0));
        return b
    }
    ;function fb() {
        return document.cookie.split(";").map(function(a) {
            return a.trim()
        }).filter(function(a) {
            return "csrftoken=" === a.substring(0, 10)
        }).map(function(a) {
            return decodeURIComponent(a.substring(10))
        })[0]
    }
    function gb(a) {
        var b = a.length + 1;
        return document.cookie.split(";").map(function(a) {
            return a.trim()
        }).filter(function(c) {
            return c.substring(0, b) === a + "="
        }).map(function(a) {
            return decodeURIComponent(a.substring(b))
        })[0]
    }
    function hb(a, b, c) {
        var d = new Date;
        d.setTime(d.getTime() + 864E5 * c);
        document.cookie = a + "=" + b + "; expires=" + d.toUTCString() + "; path=/"
    }
    ;function ib(a) {
        switch (a.readyState) {
        case 0:
            return "request not sent";
        case 4:
            switch (a.status) {
            case 0:
                return "network error";
            case 400:
                return "bad request";
            case 401:
                return "unauthorized";
            case 403:
                return "access denied";
            case 404:
                return "not found";
            case 429:
                return "request throttled";
            case 500:
                return "server error";
            default:
                return "unknown status " + a.status
            }
        default:
            return "unknown ready state " + a.readyState
        }
    }
    function jb(a) {
        var b = Error.call(this, ib(a));
        this.message = b.message;
        "stack"in b && (this.stack = b.stack);
        var c = this;
        this.fd = a;
        this.toString = function() {
            return "XhrError: " + c.message
        }
    }
    m(jb, Error);
    function kb() {
        new Promise(function(a) {
            a()
        }
        )
    }
    function lb(a) {
        return 200 <= a && 299 >= a || 1223 === a
    }
    function z(a) {
        return mb(a, 6E4)[1]
    }
    function mb(a, b) {
        b = void 0 === b ? 6E4 : b;
        var c = new XMLHttpRequest;
        return [c, new Promise(function(d, e) {
            c.onload = function() {
                lb(c.status) ? d(c) : e(new jb(c))
            }
            ;
            c.onerror = function() {
                e(new jb(c))
            }
            ;
            c.open("GET", "/" + a);
            c.withCredentials = !0;
            c.timeout = b;
            c.send()
        }
        )]
    }
    function A(a, b) {
        return new Promise(function(c, d) {
            var e = new XMLHttpRequest;
            e.open("POST", "/" + a, !0);
            e.withCredentials = !0;
            e.onerror = function() {
                d(new jb(e))
            }
            ;
            e.onload = function() {
                lb(e.status) ? c(e) : d(new jb(e))
            }
            ;
            e.setRequestHeader("content-type", "application/x-www-form-urlencoded");
            e.setRequestHeader("X-Requested-With", "XMLHttpRequest");
            var f = Object.assign({}, b)
              , h = b.csrfmiddlewaretoken
              , g = fb();
            void 0 === g ? void 0 !== h ? f.csrfmiddlewaretoken = h : w("CSRF token is undefined", {
                cookies: document.cookie
            }) : f.csrfmiddlewaretoken = g;
            e.send(db(f))
        }
        )
    }
    ;function D(a) {
        this.Hd = a;
        this.Sp = {};
        try {
            this.parsed = JSON.parse(a)
        } catch (b) {
            w("Cannot JSON parse", {
                message: a
            }),
            this.parsed = {}
        }
    }
    D.prototype.Qc = function(a) {
        this.Sp[a] = !0
    }
    ;
    function E(a, b) {
        a.Qc(b);
        if (null !== a.parsed[b])
            return a.parsed[b]
    }
    D.prototype.getAsString = function(a) {
        return "" + E(this, a)
    }
    ;
    function G(a, b, c) {
        c = void 0 === c ? !0 : c;
        var d = E(a, b);
        if ("string" !== typeof d)
            c && void 0 !== d && r("getStringOrUndefined(" + b + "): " + d + " is wrong type", {
                message: a.Hd
            });
        else
            return d
    }
    function H(a, b, c) {
        c = void 0 === c ? !0 : c;
        var d = E(a, b);
        return "string" !== typeof d ? (c && r("getString(" + b + "): " + d + " is wrong type", {
            message: a.Hd
        }),
        "") : d
    }
    function I(a, b, c) {
        c = void 0 === c ? !0 : c;
        var d = E(a, b);
        return "string" !== typeof d && "number" !== typeof d ? (c && r("getStringWithNumbers(" + b + "): " + d + " is wrong type", {
            message: a.Hd
        }),
        "") : "" + d
    }
    function nb(a) {
        for (var b = new Map, c = l(["from_username", "user", "username"]), d = c.next(); !d.done; d = c.next())
            d = d.value,
            void 0 !== G(a, d, !1) && b.set(d, G(a, d, !1));
        return b
    }
    function J(a, b, c, d) {
        c = void 0 === c ? !1 : c;
        d = void 0 === d ? !0 : d;
        var e = E(a, b);
        return "boolean" !== typeof e ? (d && r("getBoolean(" + b + "): " + e + " is wrong type", {
            message: a.Hd
        }),
        c) : e
    }
    function K(a, b, c) {
        c = void 0 === c ? !0 : c;
        var d = E(a, b);
        return "number" !== typeof d ? (c && r("getNumber(" + b + "): " + d + " is wrong type", {
            message: a.Hd
        }),
        NaN) : d
    }
    function ob(a, b) {
        var c = pb(a, b);
        return "" === c ? new D("{}") : new D(c)
    }
    function pb(a, b, c) {
        c = void 0 === c ? !0 : c;
        var d = E(a, b);
        return "object" !== typeof d ? (c && r("getObjectString(" + b + "): " + d + " is wrong type", {
            message: a.Hd
        }),
        "") : JSON.stringify(d)
    }
    function qb(a, b) {
        var c = void 0 === c ? !0 : c;
        var d = E(a, b);
        return "object" !== typeof d ? (c && r("getObject(" + b + "): " + d + " is wrong type", {
            message: a.Hd
        }),
        {}) : d
    }
    function L(a) {
        var b = [], c;
        for (c in a.parsed)
            !0 !== a.Sp[c] && b.push(c);
        b = l(b);
        for (c = b.next(); !c.done; c = b.next())
            c = c.value,
            "object" === typeof a.parsed[c] && JSON.stringify(a.parsed[c])
    }
    ;var rb = window.newrelic;
    function N(a, b, c) {
        b = void 0 === b ? {} : b;
        try {
            var d = {}, e;
            for (e in b)
                d["attributes." + e] = b[e];
            if (void 0 !== c && c && Za()) {
                var f = window.performance.now();
                d["attributes.timeSinceLoad"] = f / 1E3;
                d["attributes.timeSinceLoadMS"] = f
            }
            eb();
            void 0 !== rb && (d === {} ? rb.addPageAction(a) : rb.addPageAction(a, d))
        } catch (h) {
            w("New Relic Error in addPageAction: " + h)
        }
    }
    function sb() {
        cb("load", window, sb);
        for (var a = l(document.getElementsByTagName("script")), b = a.next(); !b.done; b = a.next())
            b = b.value,
            null !== b.src.match(/https:\/\/js-agent\.newrelic\.com\/.*/) && y("error", b, function() {
                w("Ad blocker stopped analytics collection");
                A("api/adblocker/", {}).then(function() {})
            })
    }
    y("load", window, sb);
    function tb(a) {
        a = a.toLowerCase().split(" ");
        for (var b = 0; b < a.length; b += 1)
            a[b] = a[b].charAt(0).toUpperCase() + a[b].slice(1);
        return a.join(" ")
    }
    function ub(a) {
        a = a.toLowerCase().split("_");
        for (var b = 0; b < a.length; b += 1)
            a[b] = a[b].charAt(0).toUpperCase() + a[b].slice(1);
        return a.join("_")
    }
    ;var P = window.gettext;
    void 0 === P && (r("gettext is undefined."),
    P = function(a) {
        return a
    }
    );
    var vb = window.ngettext;
    void 0 === vb && (r("ngettext is undefined."),
    vb = function(a, b, c) {
        return 1 < c ? b : a
    }
    );
    var Q = window.interpolate;
    void 0 === Q && (r("interpolate is undefined."),
    Q = function(a) {
        return a
    }
    );
    function zb(a) {
        return Q(P("Chat with %(username)s in a Live Adult Video Chat Room Now"), {
            username: ub(a)
        }, !0)
    }
    function Ab(a, b) {
        return b ? vb("Token", "Tokens", a) : vb("token", "tokens", a)
    }
    function Bb(a) {
        return Q(P("Confirm tip of %(tokens)s tokens"), {
            tokens: a
        }, !0)
    }
    function Cb(a) {
        return Q(P('room subject changed to "%(subject)s"'), {
            subject: a
        }, !0)
    }
    function Db(a) {
        return Q(P("%(username_title)s's Cam"), {
            username_title: ub(a)
        }, !0)
    }
    function Eb(a) {
        return Q(P("Join %(modelname)s's Fan Club"), {
            modelname: tb(a)
        }, !0)
    }
    function Fb(a, b) {
        return Q(P("%(users_waiting)s of %(users_required)s users are ready to start a group show"), {
            users_waiting: a,
            users_required: b
        }, !0)
    }
    function Gb(a, b) {
        return Q(P("%(username)s changed Group Show price to %(price)s tokens per minute"), {
            username: a,
            price: b
        }, !0)
    }
    function Hb(a, b) {
        return Q(P("%(username)s changed Private Show price to %(price)s tokens per minute"), {
            username: a,
            price: b
        }, !0)
    }
    function Ib(a, b) {
        return Q(P("%(username)s changed Spy on Private Show price to %(price)s tokens per minute"), {
            username: a,
            price: b
        }, !0)
    }
    function Jb(a) {
        return Q(P("%(price)s tokens per minute"), {
            price: a
        }, !0)
    }
    function Kb(a, b, c, d) {
        a = Q(P("Enter private chat with %(username)s?\n%(price)s tokens per minute, minimum %(minMinutes)s minute(s)\n\n"), {
            username: a,
            price: b,
            minMinutes: c
        }, !0);
        return a = d ? a + P("This broadcaster allows private show recordings, so you will receive a recorded video of this show in your collection.") : a + P("This broadcaster does not allow private show recordings, so you will not receive a recorded video of this show in your collection.")
    }
    function Lb(a, b) {
        return Q(P("Enter group show with %(room)s? (%(price)s tokens per minute)"), {
            room: a,
            price: b
        }, !0)
    }
    function Mb(a) {
        return Q(P("Broadcaster %(username)s is running these apps:"), {
            username: a
        }, !0)
    }
    function Nb(a) {
        return Q(P("PM conversation with %(username)s &lt;ctrl-l to close&gt;"), {
            username: a
        }, !0)
    }
    function Ob(a) {
        return Q(P("Your confidential vote regarding %(room)s has been recorded. You may change your vote at any time today. Thank you for your feedback."), {
            room: a
        }, !0)
    }
    function Pb() {
        var a = R.rm;
        return Q(P("Caution: %(siteName)s staff will NEVER contact you via chat or ask for your password."), {
            siteName: a
        }, !0)
    }
    function Qb(a) {
        return Q(P("Are you sure you want to rate %(room)s thumbs down?"), {
            room: tb(a)
        }, !0)
    }
    var Vb = P("Chat Color")
      , Wb = P("Font Family")
      , Xb = P("Font Size")
      , Yb = P("Show Emoticons")
      , Zb = P("Emoticon Autocomplete Delay")
      , $b = P("Highest Token Color")
      , ac = P("Tip Volume")
      , bc = P("View/Edit Ignored Users")
      , cc = P("THE ACT OF MASTURBATING WHILE CHATTING ONLINE")
      , dc = P("CHAT ROOMS")
      , ec = P("TUBE");
    P("BETA");
    var fc = P("TAGS")
      , gc = P("EARN FREE TOKENS")
      , hc = P("MY COLLECTION")
      , ic = P("My Collection")
      , jc = P("Login")
      , kc = P("login")
      , lc = P("LOGIN")
      , mc = P("SIGN UP")
      , nc = P("Sign Up")
      , oc = P("Include an optional message:")
      , pc = P("Send Tip")
      , qc = P("SEND TIP")
      , rc = P("Send a tip")
      , sc = P("Send tip to broadcaster");
    P("Include optional message");
    P("Tip note...");
    P("Tap to chat...");
    var tc = P("This broadcaster doesn't accept tips.")
      , uc = P("Purchase Tokens")
      , vc = P("Upgrade")
      , wc = P("Supporter")
      , xc = P("Basic Member");
    P("MY PROFILE");
    P("LOG OUT");
    var yc = P("My Profile")
      , zc = P("Log Out")
      , Ac = P("Status")
      , Bc = P("You have:")
      , Cc = P("Get More")
      , Dc = P("Get more tokens")
      , Ec = P("Get More Tokens")
      , Fc = P("connection established")
      , Gc = P("Chat disconnected. The broadcaster has set a new password on this room.")
      , Hc = P("Private & Groups")
      , Ic = P("Start Private Show")
      , Jc = P("Spy on Private Show")
      , Kc = P("Private show has started.")
      , Lc = P("Private show has finished.")
      , Mc = P("Private show has been declined.")
      , Nc = P("Cancel Private Request")
      , Oc = P("Cannot cancel private show. Please try again in a few seconds.")
      , Pc = P("Leave Private Show")
      , Qc = P("Private show request has been sent. Waiting on broadcaster to approve.")
      , Rc = P("In group show")
      , Sc = P("Cancel Group Request")
      , Tc = P("Waiting for group")
      , Uc = P("Join Group Show")
      , Vc = P("join group show")
      , Wc = P("Leave Group Show")
      , Xc = P("You are already requesting to join this group show.")
      , Yc = P("Group Shows")
      , Zc = P("You left the group show.")
      , $c = P("Group show has started.")
      , ad = P("Group show has finished.")
      , bd = P("Group show has been declined.")
      , cd = P("Broadcaster has returned from away mode.")
      , dd = P("You were kicked because you have joined this room again.")
      , ed = P("The broadcaster has kicked you from the room.")
      , fd = P("You were kicked from the room.")
      , gd = P("signup to chat")
      , hd = P("About Me")
      , id = P("Wish List");
    P("PURCHASED");
    P("TOKENS");
    var jd = P("Photos and Videos")
      , kd = P("Real Name")
      , ld = P("Followers")
      , md = P("Sex")
      , nd = P("Birthday")
      , od = P("Birth Date")
      , pd = P("Age")
      , qd = P("Body Type")
      , rd = P("Body Decorations")
      , sd = P("Interested In")
      , td = P("Location")
      , ud = P("Language(s)")
      , vd = P("Last Broadcast")
      , wd = P("Languages")
      , xd = P("Smoke Drink")
      , yd = P("Smoke / Drink")
      , zd = P("User has no available BIO")
      , Ad = P("Bio and Free Webcam")
      , Bd = P("Loading")
      , Cd = P("loading")
      , Dd = P("Chat settings saved")
      , Ed = P("CHAT")
      , Fd = P("SEND")
      , Gd = P("TIP")
      , Hd = P("Users")
      , Id = P("USERS")
      , Jd = P("Report Abuse")
      , Kd = P("REPORT ABUSE")
      , Ld = P("More Rooms")
      , Md = P("spy private show")
      , Nd = P("Private Message with:");
    P("Unfollow");
    P("Follow");
    var Od = P("Refresh Rooms")
      , Pd = P("Female")
      , Qd = P("FEMALE")
      , Rd = P("Male")
      , Sd = P("MALE")
      , Td = P("Trans")
      , Ud = P("TRANS")
      , Wd = P("Couple")
      , Xd = P("COUPLE");
    P("Followed");
    var Yd = P("FOLLOWED")
      , Zd = P("SPY SHOWS")
      , $d = P("SAVE")
      , ae = P("Standard Emoticons")
      , be = P("Upload Emoticons")
      , ce = P("Your current balance:");
    P("Balance:");
    var de = P("You currently have: ")
      , ee = P("WARNING: This room has a low satisfaction rating.")
      , fe = P("TIP AT YOUR OWN RISK!")
      , ge = P("Enter tip amount:")
      , he = P("Invalid tip amount!");
    P("Amount:");
    var ie = P("Toggle this window with CTRL-S")
      , je = P("Leave open after tipping")
      , ke = P("Select One")
      , le = P("Loading user list")
      , me = P("refresh userlist")
      , ne = P("View Profile")
      , oe = P("Send private message")
      , pe = P("HIDE ALL ADS NOW")
      , qe = P("Powered by ExoticAds<br>Buy/Sell Traffic")
      , re = P("Show More")
      , se = P("Pics & Videos")
      , te = P("Unique Registered Viewers Watching")
      , ue = P("Points")
      , ve = P("Current Hour's Top Cams")
      , we = P("Every hour, the cam with the most points wins a $10 prize. The cam in 2nd place wins a $5 prize.")
      , xe = P("Free Cams")
      , ye = P("Featured Cams")
      , ze = P("Female Cams")
      , Ae = P("Male Cams")
      , Be = P("Couple Cams")
      , Ce = P("Trans Cams")
      , De = P("Free Cams by Age")
      , Ee = P("Teen Cams (18+)")
      , Fe = P("18 to 21 Cams")
      , Ge = P("20 to 30 Cams")
      , He = P("30 to 50 Cams")
      , Ie = P("Mature Cams (50+)")
      , Je = P("Free Cams by Region")
      , Ke = P("North American Cams")
      , Le = P("Other Region Cams")
      , Me = P("Euro Russian Cams")
      , Ne = P("Asian Cams")
      , Oe = P("South American Cams")
      , Pe = P("Free Cams by Status")
      , Qe = P("Exhibitionist Cams")
      , Re = P("HD Cams")
      , Se = P("Private Shows")
      , Te = P("New Cams")
      , Ue = P("6 Tokens per Minute")
      , Ve = P("12 Tokens per Minute")
      , We = P("18 Tokens per Minute")
      , Xe = P("30 Tokens per Minute")
      , Ye = P("60 Tokens per Minute")
      , Ze = P("90 Tokens per Minute")
      , $e = P("Terms & Conditions")
      , af = P("Privacy Policy")
      , bf = P("Support")
      , cf = P("Feedback")
      , df = P("Security Center")
      , ef = P("Law Enforcement")
      , ff = P("Billing")
      , gf = P("Disable Account")
      , hf = P("Apps")
      , jf = P("Contest")
      , kf = P("Affiliates")
      , lf = P("Affiliate Program")
      , mf = P("Buy Traffic")
      , nf = P("Jobs")
      , of = P("Sitemap")
      , pf = P("dismiss this message")
      , qf = P("dismiss")
      , rf = P("Register later")
      , sf = P("Already have an account?")
      , tf = P("Login here")
      , uf = P("Please sign in using the form below")
      , vf = P("Username")
      , wf = P("Password")
      , xf = P("Keep me logged in")
      , yf = P("Forgot password?")
      , zf = P("Create Free Account")
      , Af = P("We've improved Chaturbate's mobile site by adding these feature(s):")
      , Bf = P("Satisfaction Voting")
      , Cf = P("Visit the ")
      , Df = P("mobile site")
      , Ef = P("survey")
      , Ff = P(" again, or tell us why you're not satisfied in this ");
    P("Or ");
    var Gf = P("or");
    P("tell us why you left");
    P("the mobile site");
    var Hf = P("FOLLOWER BROWSER NOTIFICATIONS")
      , If = P("Would you like to receive browser notifications when a broadcaster you follow comes online?")
      , Jf = P("Subscription failure. Please try again from the Settings & Privacy page on your profile.")
      , Kf = P("Yes")
      , Lf = P("Not now")
      , Mf = P("This room requires a password.")
      , Nf = P("Login to room")
      , Of = P("Private conversation with")
      , Pf = P("Remove Ignored Users")
      , Qf = P("Unignore this user")
      , Rf = P("Unignored Users")
      , Sf = P("Ignore this user")
      , Tf = P("Ignored Users")
      , Uf = P("Click on a user to unignore them.")
      , Vf = P("Room is currently offline")
      , Wf = P("Help us make the new Chaturbate player page better, ")
      , Xf = P("Help us make the new player page better, ")
      , Yf = P("share your thoughts")
      , Zf = P("Switch to the legacy page")
      , $f = P("Follow broadcasters to receive instant notification of when they come online.<br><br>Note: Your email address must be verified in order to receive notifications.")
      , ag = P("- UNFOLLOW")
      , bg = P("+ FOLLOW")
      , cg = P("Share")
      , dg = P("Bio")
      , eg = P("Contest Stats")
      , fg = P("Submit feedback to broadcaster")
      , gg = P("Optional comment for broadcaster:")
      , hg = P("Submitted")
      , ig = P("SATISFIED?");
    P("Satisfied?");
    var jg = P("SCAN CAMS")
      , kg = P("NEXT CAM")
      , lg = P("SKIP CAM")
      , mg = P("EXIT SCANNING")
      , ng = P("Token Linkcodes")
      , og = P("Earn up to 10 tokens for every registered user and 500 tokens for users who broadcast (broadcasters must earn $20.00 before they qualify).")
      , pg = P("Please send to chaturbate using one of the link codes below.")
      , qg = P("Embed Chaturbate's Top Cam on Your Webpage")
      , rg = P("the affiliate program stats")
      , sg = P("See details about tokens earned in ")
      , tg = P('Add an extra layer of protection to your account by setting up 2-Step Verification at the <a href="/security/">Security Center</a>')
      , ug = P("Silence for 6 hours")
      , vg = P("Theater Mode")
      , wg = P("Full Screen")
      , xg = P("Exit Full Screen")
      , yg = P("Default View")
      , zg = P("unknown")
      , Ag = P("Show All")
      , Bg = P(" to follow your favorite broadcasters and see when they are live.")
      , Cg = P("Follow your favorite broadcasters to see when they are live.");
    P("Cancel");
    P("SUBMIT");
    P("You have unsaved changes, are you sure you want to leave this page?");
    P("Choose a category");
    var Dg = P("Abuse Report");
    P("Broadcaster is underage");
    P("Broadcaster is advertising");
    P("Broadcaster is abusive");
    P("Broadcaster is intoxicated");
    P("Using a toy that is too large");
    P("Asking for offline payments");
    P("Broadcasting in public");
    P("Broadcasting in service uniform");
    P("Broadcaster is sleeping");
    P("Broadcaster is wrong gender");
    P("Other");
    P("Please choose a category");
    P("Additional comments:");
    P("Please add a description");
    P("REPORT");
    P("Your abuse report has been submitted.");
    var Eg = P("There was an error requesting your private show. Please try again.")
      , Fg = P("There was an error requesting your spy show. Please try again.")
      , Gg = P("There was an error requesting your group show. Please try again.")
      , Hg = P("Private & Group Shows")
      , Ig = P("Are you sure?")
      , Jg = P("This broadcaster allows private show recordings, so you will receive a recorded video of this show in your collection.");
    P("This broadcaster does not allow private or group shows");
    P("You may continue chatting while you wait for the broadcaster to return.");
    P("Performer Is Away");
    var Kg = P("Group Show")
      , Lg = P("LEAVE GROUP SHOW")
      , Mg = P("JOIN GROUP SHOW")
      , Ng = P("CANCEL GROUP REQUEST")
      , Og = P("LEAVE PRIVATE SHOW")
      , Pg = P("CANCEL PRIVATE REQUEST")
      , Qg = P("START PRIVATE SHOW");
    P("SPY PRIVATE SHOW");
    var Rg = P("LEAVE SPY SHOW")
      , Sg = P("Private Show");
    P("Private Show in Progress");
    var Tg = P("This feature is not currently enabled on your device.")
      , Ug = P('You must be logged in to enter a private show. Click "OK" to login.')
      , Vg = P('You must be logged in to spy on a private show. Click "OK" to login.')
      , Wg = P('You must be logged in to enter a group show. Click "OK" to login.')
      , Xg = P("Spy has been disabled by the broadcaster")
      , Yg = P("More Rooms Like This")
      , Zg = P("Here are some trending rooms instead:")
      , $g = P("RECOMMENDED")
      , ah = P("OFFLINE")
      , bh = P("EXHIBITIONIST")
      , ch = P("NEW")
      , dh = P("HD")
      , eh = P("HD+")
      , fh = P("IN PRIVATE")
      , gh = P("GROUP SHOW")
      , hh = P("CHATURBATING")
      , ih = P("HIDDEN - STAFF ONLY");
    var jh = new n("bioClicked")
      , kh = new n("userListRequest")
      , lh = new n("openTipCalloutRequest")
      , mh = new n("loadRoomRequest")
      , nh = new n("reportAbuseRequest")
      , oh = new n("privateGroupWindowRequest")
      , ph = new n("chatWindowRequest");
    var qh = new n("switchedToHLS")
      , rh = new n("openDefaultTipCalloutRequest")
      , sh = new n("loginOverlayRequest")
      , th = new n("userChatSettingsUpdate",{
        sd: 50
    })
      , uh = new n("windowReSized");
    var S = new n("roomLoaded",{
        sd: 100,
        yj: 1
    })
      , vh = new n("roomCleanup",{
        sd: 20
    })
      , wh = new n("userViewedPm");
    function xh(a, b) {
        a.style.webkitTransition = b;
        a.style.transition = b;
        void 0 !== a.style.setProperty && a.style.setProperty("moz-transition", b)
    }
    function yh(a) {
        a.style.Fr = "text";
        a.style.webkitUserSelect = "text";
        a.style.bs = "text";
        void 0 !== a.style.setProperty && a.style.setProperty("moz-user-select", "text")
    }
    ;function zh() {
        this.ld = []
    }
    zh.prototype.hd = function(a) {
        this.remove(a);
        this.ld.unshift(a)
    }
    ;
    function Ah(a, b) {
        a.remove(b);
        a.ld.push(b)
    }
    zh.prototype.remove = function(a) {
        a = this.ld.indexOf(a);
        -1 !== a && this.ld.splice(a, 1)
    }
    ;
    function Bh(a) {
        return new Promise(function(b, c) {
            z("api/biocontext/" + a + "/").then(function(c) {
                c = new D(c.responseText);
                var d = {
                    username: a,
                    Xr: K(c, "fan_club_cost", !1),
                    eo: J(c, "fan_club_is_member"),
                    Pn: J(c, "performer_has_fanclub"),
                    Aq: H(c, "fan_club_join_url"),
                    location: H(c, "location"),
                    sj: pb(c, "interested_in"),
                    Oj: H(c, "real_name"),
                    $j: tb(H(c, "sex")),
                    $i: K(c, "follower_count"),
                    he: K(c, "display_age", !1),
                    Ii: H(c, "body_decorations"),
                    Xi: H(c, "display_birthday", !1),
                    Mh: G(c, "time_since_last_broadcast"),
                    ck: H(c, "smoke_drink"),
                    Ji: H(c, "body_type"),
                    languages: H(c, "languages"),
                    Xa: H(c, "room_status")
                }
                  , f = H(c, "about_me")
                  , h = H(c, "wish_list")
                  , g = pb(c, "photo_sets")
                  , p = [];
                g = l(JSON.parse(g));
                for (var t = g.next(); !t.done; t = g.next())
                    t = new D(JSON.stringify(t.value)),
                    p.push({
                        id: K(t, "id"),
                        name: H(t, "name"),
                        hn: H(t, "cover_url"),
                        Sb: K(t, "tokens"),
                        fo: J(t, "is_video"),
                        rk: J(t, "user_can_access"),
                        Yr: J(t, "fan_club_only"),
                        vj: H(t, "label_text"),
                        ho: H(t, "label_color")
                    }),
                    L(t);
                d = {
                    Nf: d,
                    xi: f,
                    yk: h,
                    Nj: p
                };
                L(c);
                b(d)
            })["catch"](function(a) {
                c(a)
            })
        }
        )
    }
    ;var Ch = {
        fullscreenEnabled: 0,
        fullscreenElement: 1,
        requestFullscreen: 2,
        exitFullscreen: 3,
        fullscreenchange: 4,
        fullscreenerror: 5
    }, Dh = "webkitFullscreenEnabled webkitFullscreenElement webkitRequestFullscreen webkitExitFullscreen webkitfullscreenchange webkitfullscreenerror".split(" "), Eh = "mozFullScreenEnabled mozFullScreenElement mozRequestFullScreen mozCancelFullScreen mozfullscreenchange mozfullscreenerror".split(" "), Fh = "msFullscreenEnabled msFullscreenElement msRequestFullscreen msExitFullscreen MSFullscreenChange MSFullscreenError".split(" "), Gh;
    "fullscreenEnabled"in document ? Gh = Object.keys(Ch) : Dh[0]in document ? Gh = Dh : Eh[0]in document ? Gh = Eh : Fh[0]in document && (Gh = Fh);
    function Hh(a) {
        if (void 0 !== Gh)
            a[Gh[Ch.requestFullscreen]]()
    }
    function Ih() {
        void 0 !== Gh && document[Gh[Ch.exitFullscreen]].bind(document)()
    }
    function Jh() {
        return void 0 === Gh ? !1 : !!document[Gh[Ch.fullscreenEnabled]]
    }
    function T() {
        if (void 0 !== Gh)
            return document[Gh[Ch.fullscreenElement]]
    }
    function Kh() {
        return void 0 === Gh ? "" : Gh[Ch.fullscreenchange]
    }
    function Lh() {
        return void 0 === Gh ? "" : ("on" + Gh[Ch.fullscreenerror]).toLowerCase()
    }
    ;function Mh(a, b, c) {
        T() ? (Ih(),
        setTimeout(function() {
            var d = window.open(a, b, c, void 0);
            null !== d && setTimeout(function() {
                d.focus()
            }, 200)
        }, 500)) : window.open(a, b, c, void 0)
    }
    ;function Nh(a, b) {
        var c = document.createElement("div");
        c.innerHTML = a;
        var d = document.createElement("div");
        d.style.paddingTop = "5px";
        var e = document.createElement("span");
        e.innerText = b;
        e.style.color = "#0c6a93";
        e.style.padding = "5px";
        d.appendChild(e);
        e = document.createElement("div");
        e.style.padding = "10px";
        for (var f = l(c.getElementsByTagName("*")), h = f.next(); !h.done; h = f.next()) {
            var g = h = h.value;
            "fixed" === g.style.position && (g.style.position = "absolute");
            "IMG" === g.tagName && (h.className = "bioImage");
            null !== g.style.top && 0 > parseInt(g.style.top, 10) && (g.style.top = "0");
            null !== g.style.marginTop && 0 > parseInt(g.style.marginTop, 10) && (g.style.marginTop = "0");
            null !== g.style.left && 0 > parseInt(g.style.left, 10) && (g.style.left = "0");
            null !== g.style.marginLeft && 0 > parseInt(g.style.marginLeft, 10) && (g.style.marginLeft = "0")
        }
        c.style.position = "relative";
        e.appendChild(c);
        d.appendChild(e);
        return d
    }
    function Oh(a, b) {
        function c(a) {
            var c = document.createElement("div");
            c.style.padding = "10px";
            c.style.position = "relative";
            c.style.display = "inline-block";
            c.style.height = "100px";
            c.title = a.name;
            c.onmouseenter = function() {
                c.style.cursor = "pointer";
                e.style.textDecoration = "underline"
            }
            ;
            c.onmouseleave = function() {
                c.style.cursor = "default";
                e.style.textDecoration = "none"
            }
            ;
            c.onclick = function() {
                var c = "resizable,dependent,scrollbars,height=520,width=803,top=" + (screen.height / 2 - 260).toString() + ",left=" + (screen.width / 2 - 401.5).toString()
                  , d = a.rk ? "/photo_videos/photoset/detail/" + b + "/" + a.id : "/photo_videos/purchase/" + a.id + "/";
                -1 !== window.location.search.indexOf("from_tube") && (d = d + "?from_tube=" + b);
                Mh(d, a.name, c);
                return !1
            }
            ;
            var d = document.createElement("img");
            d.src = a.hn;
            d.width = 150;
            d.height = 100;
            d.className = "userUpload";
            c.appendChild(d);
            a.rk || (d = document.createElement("img"),
            d.src = "https://ssl-ccstatic.highwebmedia.com/images/locked_rectangle4.png",
            d.width = 150,
            d.height = 100,
            d.style.top = "10px",
            d.style.left = "10px",
            d.style.position = "absolute",
            d.style.border = "none",
            c.appendChild(d));
            a.fo && (d = document.createElement("img"),
            d.src = "https://ssl-ccstatic.highwebmedia.com/images/play3.png",
            d.height = 20,
            d.style.position = "absolute",
            d.style.top = "10px",
            d.style.left = "10px",
            c.appendChild(d));
            "" !== a.vj && (d = document.createElement("span"),
            d.innerText = a.vj,
            d.style.backgroundColor = a.ho,
            d.style.position = "absolute",
            d.style.top = "92px",
            d.style.right = "15px",
            d.style.fontSize = "10px",
            d.style.padding = "2px",
            c.appendChild(d));
            var e = document.createElement("div");
            e.innerText = a.name;
            e.style.fontSize = "12px";
            e.style.position = "absolute";
            e.style.top = "111px";
            e.style.left = "0px";
            e.style.paddingLeft = "13px";
            e.style.color = "#0c6a93";
            e.style.maxWidth = "145px";
            e.style.whiteSpace = "nowrap";
            e.style.textOverflow = "ellipsis";
            e.style.overflow = "hidden";
            c.appendChild(e);
            return c
        }
        var d = document.createElement("div");
        d.style.position = "relative";
        d.style.paddingTop = "5px";
        d.style.paddingBottom = "25px";
        var e = document.createElement("span");
        e.innerText = jd + ":";
        e.style.color = "#0c6a93";
        e.style.padding = "5px";
        d.appendChild(e);
        e = document.createElement("div");
        e.style.paddingLeft = "5px";
        for (var f = l(a), h = f.next(); !h.done; h = f.next())
            e.appendChild(c(h.value));
        d.appendChild(e);
        return d
    }
    function Ph(a) {
        function b(a, b) {
            var c = document.createElement("div")
              , d = document.createElement("div");
            d.style.position = "relative";
            d.style.paddingLeft = "5px";
            d.style.display = "inline";
            var e = document.createElement("span");
            e.style.color = "#0c6a93";
            e.innerText = a;
            d.appendChild(e);
            e = document.createElement("div");
            e.style.position = "absolute";
            e.style.paddingRight = "5px";
            e.style.display = "inline";
            e.style.left = "150px";
            e.style.overflow = "hidden";
            e.style.textOverflow = "ellipsis";
            e.style.whiteSpace = "nowrap";
            var f = document.createElement("span");
            f.style.whiteSpace = "nowrap";
            f.innerText = b;
            e.appendChild(f);
            c.style.paddingTop = "5px";
            c.appendChild(d);
            c.appendChild(e);
            return c
        }
        var c = document.createElement("div")
          , d = a.username.charAt(0).toUpperCase() + a.username.slice(1);
        if ("public" === a.Xa) {
            var e = document.createElement("a");
            e.href = "./?" + a.username;
            e.text = "View " + d + "'s Cam";
            e.style.position = "relative";
            e.style.padding = "5px 5px 5px 0";
            e.style.textAlign = "right";
            e.style.display = "inline-block";
            e.style.width = "100%";
            e.style.fontSize = "14px";
            e.style.textDecoration = "none";
            e.onmouseenter = function() {
                e.style.textDecoration = "underline"
            }
            ;
            e.onmouseleave = function() {
                e.style.textDecoration = "none"
            }
            ;
            y("click", e, function(b) {
                b.preventDefault();
                u(mh, a.username)
            });
            c.appendChild(e)
        }
        if (!1 === a.eo && !0 === a.Pn) {
            var f = document.createElement("a");
            f.href = "#";
            f.text = "Join Fanclub";
            f.style.position = "relative";
            f.style.padding = "0 5px 5px 0";
            f.style.color = "#0a5a83";
            f.style.width = "100%";
            f.style.display = "inline-block";
            f.style.textAlign = "right";
            f.style.fontSize = "14px";
            f.style.textDecoration = "none";
            f.onclick = function() {
                Mh("" + a.Aq, "Join Fanclub", "resizable,dependent,scrollbars,height=583,width=1000,top=" + (screen.height / 2 - 291.5).toString() + ",left=" + (screen.width / 2 - 500).toString());
                return !1
            }
            ;
            f.onmouseenter = function() {
                f.style.textDecoration = "underline"
            }
            ;
            f.onmouseleave = function() {
                f.style.textDecoration = "none"
            }
            ;
            c.appendChild(f)
        }
        "" !== a.Oj && c.appendChild(b(kd + ":", a.Oj));
        !1 === isNaN(a.$i) && c.appendChild(b(ld + ":", a.$i.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",")));
        "" !== a.$j && c.appendChild(b(md + ":", "" + a.$j.charAt(0).toUpperCase() + a.$j.substring(1)));
        "" !== a.Xi && c.appendChild(b(nd + ":", a.Xi));
        !1 === isNaN(a.he) && c.appendChild(b(pd + ":", a.he.toString()));
        "" !== a.Ji && c.appendChild(b(qd + ":", a.Ji));
        "" !== a.Ii && c.appendChild(b(rd + ":", a.Ii));
        "" !== a.sj && (d = JSON.parse(a.sj).join(", "),
        c.appendChild(b(sd + ":", d)));
        "" !== a.location && c.appendChild(b(td + ":", a.location));
        void 0 !== a.Mh && c.appendChild(b(vd + ":", a.Mh));
        "" !== a.languages && c.appendChild(b(wd + ":", a.languages));
        "" !== a.ck && c.appendChild(b(xd + ":", a.ck));
        return c
    }
    function Qh(a) {
        v.call(this);
        var b = this;
        this.td(Bh(a));
        this.gp = function(a) {
            b.td(Bh(a.h.i))
        }
        ;
        q(S, this.gp, !1)
    }
    m(Qh, v);
    Qh.prototype.td = function(a) {
        var b = this;
        void 0 !== this.content && this.a.removeChild(this.content.pe);
        var c = document.createElement("span");
        c.innerText = Bd + "...";
        this.a.appendChild(c);
        this.a.style.display = "block";
        a.then(function(a) {
            var d = document.createElement("div");
            d.style.fontSize = "14px";
            var f = Ph(a.Nf);
            d.appendChild(f);
            var h = void 0;
            0 < a.Nj.length && (h = Oh(a.Nj, a.Nf.username),
            d.appendChild(h));
            var g = void 0;
            "" !== a.xi && (g = Nh(a.xi, hd + ":"),
            d.appendChild(g));
            var p = void 0;
            "" !== a.yk && (p = Nh(a.yk, id + ":"),
            d.appendChild(p));
            b.content = {
                pe: d,
                Nf: f,
                ds: h,
                ns: g,
                os: p
            };
            b.content.pe.style.position = "relative";
            b.content.pe.style.paddingRight = "3px";
            b.content.pe.style.cursor = "default";
            b.content.pe.style.overflow = "scroll";
            b.content.pe.style.height = "100%";
            b.a.removeChild(c);
            b.a.appendChild(b.content.pe);
            b.Y()
        })["catch"](function(a) {
            w("Bio Content error: " + a);
            a = document.createElement("div");
            a.style.position = "absolute";
            a.style.height = "100%";
            a.style.width = "100%";
            a.style.top = "0";
            a.style.left = "0";
            a.style.display = "block";
            var d = document.createElement("span");
            d.innerText = zd;
            d.style.display = "block";
            d.style.padding = "10px";
            a.appendChild(d);
            b.content = {
                pe: a
            };
            b.a.removeChild(c);
            b.a.appendChild(b.content.pe)
        })
    }
    ;
    Qh.prototype.C = function() {
        if (void 0 !== this.content && void 0 !== this.content.Nf)
            for (var a = l(this.content.Nf.childNodes), b = a.next(); !b.done; b = a.next())
                b = b.value.childNodes[1],
                void 0 !== b && (b.style.maxWidth = this.a.offsetWidth - 150 - 5 + "px")
    }
    ;
    Qh.prototype.Bi = function() {
        S.removeListener(this.gp)
    }
    ;
    var Rh = {
        enabled: !1,
        move: function() {},
        end: function() {}
    };
    function Sh(a, b) {
        Th(a, b, function(a) {
            return {
                valid: !0,
                x: a.clientX,
                y: a.clientY
            }
        }, "mousedown", "mousemove", "mouseup");
        Th(a, b, function(a) {
            return 1 === a.touches.length && (a = a.touches.item(0),
            null !== a) ? {
                valid: !0,
                x: a.clientX,
                y: a.clientY
            } : {
                valid: !1,
                x: 0,
                y: 0
            }
        }, "touchstart", "touchmove", "touchend")
    }
    function Th(a, b, c, d, e, f) {
        y(d, a, function(a) {
            var d = c(a);
            if (d.valid) {
                var h = b(a, d.x, d.y);
                if (h.enabled) {
                    h.move(d.x, d.y);
                    a.preventDefault();
                    var t = function(a) {
                        a = c(a);
                        a.valid && h.move(a.x, a.y)
                    }
                      , C = function(a) {
                        a.preventDefault();
                        void 0 !== a.buttons && 1 === a.buttons || void 0 !== a.touches && 1 === a.touches.length ? t(a) : B(a, !1)
                    };
                    y(e, document, C, !0);
                    var B = function(a, b) {
                        (void 0 === b || b) && t(a);
                        cb(e, document, C, !0);
                        cb(f, document, B, !0);
                        h.end()
                    };
                    y(f, document, B, !0)
                }
            }
        })
    }
    ;function Uh() {
        var a = document.createElement("div");
        a.style.visibility = "hidden";
        a.style.width = "100px";
        a.style.as = "scrollbar";
        document.body.appendChild(a);
        var b = a.offsetWidth;
        a.style.overflow = "scroll";
        var c = document.createElement("div");
        c.style.width = "100%";
        a.appendChild(c);
        c = c.offsetWidth;
        document.body.removeChild(a);
        return b - c
    }
    ;function Vh(a, b, c) {
        this.Za = a;
        this.F = c;
        this.I = {
            Sc: NaN,
            dd: NaN,
            Nc: NaN,
            Xc: NaN,
            width: NaN,
            height: NaN
        };
        this.oo = "window__" + b;
        a = {
            Sc: NaN,
            dd: NaN,
            Nc: NaN,
            Xc: NaN,
            width: NaN,
            height: NaN
        };
        if (x() && (b = window.localStorage.getItem(this.oo),
        null !== b)) {
            a = JSON.parse(b);
            for (var d in a)
                null === a[d] && (a[d] = NaN);
            isNaN(a.width) && (a.width = this.F.Rd);
            isNaN(a.height) && (a.height = this.F.Qd)
        }
        this.I = a;
        isNaN(this.I.width) && (this.I.width = this.F.Rd);
        isNaN(this.I.height) && (this.I.height = this.F.Qd);
        isNaN(this.I.Sc) && isNaN(this.I.Xc) && (0 <= this.F.Pd.indexOf(1) ? this.I.Xc = Uh() : this.I.Sc = 0 <= this.F.Pd.indexOf(4) ? window.innerWidth / 2 - this.F.Rd / 2 : 0);
        isNaN(this.I.dd) && isNaN(this.I.Nc) && (0 <= this.F.Pd.indexOf(3) ? this.I.Nc = Uh() : this.I.dd = 0 <= this.F.Pd.indexOf(4) ? window.innerHeight / 2 - this.F.Qd / 2 : 0)
    }
    Vh.prototype.save = function() {
        var a = this.Za.a.parentElement;
        if (null === a)
            w("no parent element");
        else {
            var b = this.Za.a.offsetLeft
              , c = this.Za.a.offsetTop
              , d = a.clientHeight - (this.Za.a.offsetHeight + c);
            a = a.clientWidth - (this.Za.a.offsetWidth + b);
            this.I.height = this.Za.a.offsetHeight;
            this.I.width = this.Za.a.offsetWidth;
            b <= a ? (this.I.Sc = b,
            this.I.Xc = NaN) : (this.I.Sc = NaN,
            this.I.Xc = a);
            c <= d ? (this.I.dd = c,
            this.I.Nc = NaN) : (this.I.dd = NaN,
            this.I.Nc = d)
        }
        x() && window.localStorage.setItem(this.oo, JSON.stringify(this.I))
    }
    ;
    Vh.prototype.apply = function() {
        var a = this.Za.a.parentElement;
        if (null === a)
            w("no parent element");
        else {
            void 0 !== this.F.Gi && this.F.Gi ? (this.Za.ra.style.position = "relative",
            this.Za.a.style.width = "auto",
            this.Za.a.style.height = "auto",
            this.I.height = this.Za.a.offsetHeight,
            this.I.width = this.Za.a.offsetWidth) : (this.F.af ? (this.I.width = Math.min(this.I.width, a.clientWidth),
            this.I.height = Math.min(this.I.height, a.clientHeight),
            this.I.width = Math.max(this.I.width, this.F.minWidth),
            this.I.height = Math.max(this.I.height, this.F.minHeight)) : (this.I.width = this.F.Rd,
            this.I.height = this.F.Qd + Wh(this.Za)),
            this.Za.a.style.width = this.I.width + "px",
            this.Za.a.style.height = this.I.height + "px");
            var b = !isNaN(this.I.Sc) && !isNaN(this.I.Xc) || isNaN(this.I.Sc) && isNaN(this.I.Xc);
            !isNaN(this.I.dd) && !isNaN(this.I.Nc) || isNaN(this.I.dd) && isNaN(this.I.Nc) || b ? Xh("unexpected bounds: top:" + this.I.dd + " bottom:" + this.I.Nc + " left:" + this.I.Sc + " right: " + this.I.Xc) : (b = a.clientHeight,
            isNaN(this.I.dd) ? isNaN(this.I.Nc) || (this.I.Nc + this.I.height > b && (Xh("set bottomBound to container height - bounds height to make fit"),
            this.I.Nc = b - this.I.height),
            this.Za.a.style.top = b - this.I.height - this.I.Nc + "px") : (this.I.dd + this.I.height > b && (Xh("set topBound to container height - bounds height to make fit"),
            this.I.dd = b - this.I.height),
            this.Za.a.style.top = this.I.dd + "px"),
            a = a.clientWidth,
            isNaN(this.I.Sc) ? isNaN(this.I.Xc) || (this.I.Xc + this.I.width > a && (Xh("set rightBound to 0 to make fit"),
            this.I.Xc = 0),
            this.Za.a.style.left = a - this.I.width - this.I.Xc + "px") : (this.I.Sc + this.I.width > a && (Xh("set leftBound to 0 to make fit"),
            this.I.Sc = 0),
            this.Za.a.style.left = this.I.Sc + "px"))
        }
    }
    ;
    function Yh(a) {
        return {
            left: a.offsetLeft,
            top: a.offsetTop,
            width: a.offsetWidth,
            height: a.offsetHeight
        }
    }
    function Zh(a, b, c) {
        var d = Object.assign({}, a);
        d.top = a.top + b;
        d.height = a.height - b;
        0 > d.top ? (d.height += d.top,
        d.top = 0) : d.height < c && (d.top -= c - d.height,
        d.height = c);
        return d
    }
    function $h(a, b, c, d) {
        var e = Object.assign({}, a);
        e.height = a.height + b;
        e.top + e.height > d ? e.height = d - e.top : e.height < c && (e.height = c);
        return e
    }
    function ai(a, b, c) {
        var d = Object.assign({}, a);
        d.left = a.left + b;
        d.width = a.width - b;
        0 > d.left ? (d.width += d.left,
        d.left = 0) : d.width < c && (d.left -= c - d.width,
        d.width = c);
        return d
    }
    function bi(a, b, c, d) {
        var e = Object.assign({}, a);
        e.width = a.width + b;
        e.left + e.width > d ? e.width = d - e.left : e.width < c && (e.width = c);
        return e
    }
    ;function ci(a, b) {
        this.Hn = a;
        this.F = b;
        this.Kk = this.Tl = !1
    }
    function di(a) {
        function b() {
            a.Kk && !0 === a.F.Rl ? (a.Kk = !1,
            a.Hn(),
            setTimeout(b, a.F.ph)) : a.Tl = !1
        }
        a.Tl ? a.Kk = !0 : (a.Tl = !0,
        a.Hn(),
        setTimeout(b, a.F.ph))
    }
    ;function ei(a) {
        v.call(this);
        var b = this;
        this.a.style.height = "0";
        this.a.style.width = "0";
        this.a.style.overflow = "visible";
        this.a.style.position = "fixed";
        this.a.style.top = "0";
        this.a.style.left = "0";
        this.Vb = a.Vb;
        this.listener = function() {
            b.Y()
        }
        ;
        q(this.Vb.Vi, this.listener);
        this.N(new fi(0,a));
        this.N(new gi(0,a));
        this.N(new hi(0,a));
        this.N(new ii(0,a));
        this.N(new ji(0,a));
        this.N(new ki(0,a));
        this.N(new li(0,a));
        this.N(new mi(0,a))
    }
    m(ei, v);
    ei.prototype.C = function() {}
    ;
    function ni(a, b, c) {
        v.call(this);
        var d = this;
        this.options = b;
        this.cursor = c;
        this.zb = 8;
        this.a.style.height = this.zb + "px";
        this.a.style.width = this.zb + "px";
        this.a.style.cursor = this.cursor;
        Sh(this.a, function(a, c, h) {
            a.stopPropagation();
            void 0 !== b.Io && b.Io();
            var e = Object.freeze(Yh(b.Vb.a))
              , f = new ci(function() {
                b.Vb.Y()
            }
            ,{
                ph: 10
            });
            return {
                enabled: !0,
                move: function(a, g) {
                    var p = d.We(e, {
                        x: a - c,
                        y: g - h
                    })
                      , t = b.Vb.a;
                    t.style.left = p.left + "px";
                    t.style.top = p.top + "px";
                    t.style.width = p.width + "px";
                    t.style.height = p.height + "px";
                    di(f)
                },
                end: function() {
                    void 0 !== b.Jo && b.Jo()
                }
            }
        })
    }
    m(ni, v);
    function fi(a, b) {
        ni.call(this, 0, b, "ns-resize")
    }
    m(fi, ni);
    fi.prototype.C = function() {
        var a = this.options.Vb.a.getBoundingClientRect();
        this.a.style.top = a.top - this.zb / 2 + "px";
        this.a.style.left = a.left + "px";
        this.a.style.width = a.right - a.left + "px"
    }
    ;
    fi.prototype.We = function(a, b) {
        return Zh(a, b.y, this.options.minHeight)
    }
    ;
    function gi(a, b) {
        ni.call(this, 0, b, "ns-resize")
    }
    m(gi, ni);
    gi.prototype.C = function() {
        var a = this.options.Vb.a.getBoundingClientRect();
        this.a.style.top = a.top + a.height - this.zb / 2 + "px";
        this.a.style.left = a.left + "px";
        this.a.style.width = a.width + "px"
    }
    ;
    gi.prototype.We = function(a, b) {
        return $h(a, b.y, this.options.minHeight, this.options.maxHeight())
    }
    ;
    function hi(a, b) {
        ni.call(this, 0, b, "ew-resize")
    }
    m(hi, ni);
    hi.prototype.C = function() {
        var a = this.options.Vb.a.getBoundingClientRect();
        this.a.style.top = a.top + "px";
        this.a.style.left = a.left - this.zb / 2 + "px";
        this.a.style.height = a.bottom - a.top + "px"
    }
    ;
    hi.prototype.We = function(a, b) {
        return ai(a, b.x, this.options.minWidth)
    }
    ;
    function ii(a, b) {
        ni.call(this, 0, b, "ew-resize")
    }
    m(ii, ni);
    ii.prototype.C = function() {
        var a = this.options.Vb.a.getBoundingClientRect();
        this.a.style.top = a.top + "px";
        this.a.style.left = a.right - this.zb / 2 + "px";
        this.a.style.height = a.bottom - a.top + "px"
    }
    ;
    ii.prototype.We = function(a, b) {
        return bi(a, b.x, this.options.minWidth, this.options.maxWidth())
    }
    ;
    function ji(a, b) {
        ni.call(this, 0, b, "nwse-resize")
    }
    m(ji, ni);
    ji.prototype.C = function() {
        var a = this.options.Vb.a.getBoundingClientRect();
        this.a.style.top = a.top - this.zb + this.zb / 2 + "px";
        this.a.style.left = a.left - this.zb + this.zb / 2 + "px"
    }
    ;
    ji.prototype.We = function(a, b) {
        var c = Zh(a, b.y, this.options.minHeight)
          , d = ai(a, b.x, this.options.minWidth);
        return {
            top: c.top,
            height: c.height,
            left: d.left,
            width: d.width
        }
    }
    ;
    function ki(a, b) {
        ni.call(this, 0, b, "nesw-resize")
    }
    m(ki, ni);
    ki.prototype.C = function() {
        var a = this.options.Vb.a.getBoundingClientRect();
        this.a.style.top = a.top - this.zb + this.zb / 2 + "px";
        this.a.style.left = a.right - this.zb / 2 + "px"
    }
    ;
    ki.prototype.We = function(a, b) {
        var c = Zh(a, b.y, this.options.minHeight)
          , d = bi(a, b.x, this.options.minWidth, this.options.maxWidth());
        return {
            top: c.top,
            height: c.height,
            left: d.left,
            width: d.width
        }
    }
    ;
    function li(a, b) {
        ni.call(this, 0, b, "nesw-resize")
    }
    m(li, ni);
    li.prototype.C = function() {
        var a = this.options.Vb.a.getBoundingClientRect();
        this.a.style.top = a.bottom - this.zb / 2 + "px";
        this.a.style.left = a.left - this.zb + this.zb / 2 + "px"
    }
    ;
    li.prototype.We = function(a, b) {
        var c = $h(a, b.y, this.options.minHeight, this.options.maxHeight())
          , d = ai(a, b.x, this.options.minWidth);
        return {
            top: c.top,
            height: c.height,
            left: d.left,
            width: d.width
        }
    }
    ;
    function mi(a, b) {
        ni.call(this, 0, b, "nwse-resize")
    }
    m(mi, ni);
    mi.prototype.C = function() {
        var a = this.options.Vb.a.getBoundingClientRect();
        this.a.style.top = a.bottom - this.zb / 2 + "px";
        this.a.style.left = a.right - this.zb / 2 + "px"
    }
    ;
    mi.prototype.We = function(a, b) {
        var c = $h(a, b.y, this.options.minHeight, this.options.maxHeight())
          , d = bi(a, b.x, this.options.minWidth, this.options.maxWidth());
        return {
            top: c.top,
            height: c.height,
            left: d.left,
            width: d.width
        }
    }
    ;
    function oi(a, b, c) {
        v.call(this);
        var d = this;
        this.J = a;
        this.content = b;
        this.F = c;
        this.co = !1;
        this.nn = new n("draggingOrResizingChanged");
        this.rb = !1;
        this.a.style.overflow = "visible";
        this.ra = document.createElement("div");
        this.ra.style.height = "100%";
        this.ra.style.width = "100%";
        this.ra.style.position = "absolute";
        this.ra.style.overflow = "visible";
        this.ra.style.boxSizing = "border-box";
        this.ra.style.backgroundColor = "rgb(255, 255, 255)";
        this.ra.style.border = "2px solid";
        this.ra.style.borderColor = "#0d5d81";
        this.pq = void 0 !== c.title;
        var e = this.Ra = document.createElement("div");
        this.Ra.id = (this.F.ed + "-close-btn").toLowerCase();
        this.Ra.style.position = "absolute";
        this.Ra.style.right = "3px";
        this.Ra.style.height = "10px";
        this.Ra.style.width = "10px";
        this.Ra.style.cursor = "pointer";
        this.Ra.style.border = "1px solid";
        this.Ra.style.borderColor = "#999999";
        this.Ra.style.borderRadius = "2px";
        y("mouseenter", this.Ra, function() {
            e.style.backgroundImage = "url(https://ssl-ccstatic.highwebmedia.com/theatermodeassets/close.svg)";
            e.style.backgroundSize = "10px";
            e.style.backgroundColor = "#999999"
        });
        y("mouseleave", this.Ra, function() {
            e.style.backgroundImage = "";
            e.style.backgroundColor = ""
        });
        void 0 !== c.title ? (this.gc = document.createElement("div"),
        this.gc.style.position = "relative",
        this.gc.style.backgroundColor = "#cccccc",
        this.gc.style.color = "#0b5d81",
        this.gc.style.fontSize = "13px",
        this.gc.style.fontWeight = "bold",
        this.gc.style.padding = "3px 3px 3px 5px",
        this.gc.style.minHeight = "10px",
        this.ce = document.createElement("div"),
        this.ce.style.height = "100%",
        this.ce.style.whiteSpace = "nowrap",
        this.ce.style.overflow = "hidden",
        this.ce.style.textOverflow = "ellipsis",
        this.gc.appendChild(this.ce),
        this.pk = document.createElement("span"),
        this.pk.innerText = c.title,
        this.ce.appendChild(this.pk),
        this.Xe = c.Xe,
        this.gc.appendChild(this.Ra),
        this.ra.appendChild(this.gc),
        y("click", this.Ra, function(a) {
            a.stopPropagation();
            d.J.removeChild(d)
        })) : (this.Ra.style.top = "5px",
        this.Ra.style.zIndex = "1000",
        this.a.appendChild(this.Ra),
        y("click", this.Ra, function(a) {
            a.stopPropagation();
            d.a.style.display = "none";
            d.rb = !1
        }));
        this.N(this.content, this.ra);
        this.Ol = new Vh(this,this.F.ed,this.F);
        this.Ap = function() {
            d.Ol.save();
            d.Y()
        }
        ;
        void 0 === c.hl && (c.hl = void 0 !== this.ce ? [this.ce] : [this.a]);
        pi(this.a, {
            ln: function() {
                qi(d.J, d);
                ri(d, !0)
            },
            mn: function() {
                ri(d, !1);
                d.Ap();
                N("MoveDraggableWindow", {
                    window: d.F.ed,
                    left: d.a.offsetLeft,
                    top: d.a.offsetTop,
                    windowWidth: window.innerWidth,
                    windowHeight: window.innerHeight
                })
            },
            cn: function() {
                return !d.co
            }
        }, c.hl);
        !0 === c.af && si(this);
        this.a.appendChild(this.ra)
    }
    m(oi, v);
    function ti(a, b) {
        void 0 !== a.pk && (a.pk.innerText = b)
    }
    function si(a) {
        void 0 === a.Sg && (a.Sg = a.N(new ei({
            minWidth: a.F.minWidth,
            minHeight: a.F.minHeight,
            maxWidth: function() {
                return a.J.a.clientWidth
            },
            maxHeight: function() {
                return a.J.a.clientHeight
            },
            Vb: a,
            Io: function() {
                qi(a.J, a);
                ri(a, !0)
            },
            Jo: function() {
                ri(a, !1);
                a.Ap();
                N("ResizeDraggableWindow", {
                    window: a.F.ed,
                    width: a.a.clientWidth,
                    height: a.a.clientHeight,
                    windowWidth: window.innerWidth,
                    windowHeight: window.innerHeight
                })
            }
        })),
        a.Sg.Y())
    }
    function ri(a, b) {
        a.co = b;
        u(a.nn, b)
    }
    function ui(a) {
        if (void 0 !== a.Sg) {
            var b = a.Sg;
            b.Vb.Vi.removeListener(b.listener);
            a.removeChild(a.Sg);
            a.Sg = void 0
        }
    }
    oi.prototype.C = function() {
        this.content.a.style.height = void 0 !== this.F.Gi && this.F.Gi ? "auto" : this.a.clientHeight - Wh(this) - 4 + "px";
        void 0 !== this.gc && (void 0 === this.Ra ? w("expected close button to be defined") : void 0 === this.ce ? w("expected title bar div inner to be defined") : (this.ce.style.maxWidth = this.gc.clientWidth - this.Ra.offsetWidth - 5 - 6 + "px",
        this.Ra.style.top = (this.gc.offsetHeight - this.Ra.offsetHeight) / 2 + "px"))
    }
    ;
    oi.prototype.show = function() {
        this.a.style.opacity = "1"
    }
    ;
    oi.prototype.A = function() {
        this.a.style.opacity = "0"
    }
    ;
    function Wh(a) {
        return void 0 === a.gc ? 0 : a.gc.offsetHeight
    }
    oi.prototype.blur = function() {
        this.J.Lb === this && qi(this.J, void 0)
    }
    ;
    function vi(a, b) {
        return void 0 !== a.F.Kl ? a.F.Kl(b) : !1
    }
    function pi(a, b, c) {
        c = l(c);
        for (var d = c.next(); !d.done; d = c.next())
            d = d.value,
            d.style.cursor = "move",
            Sh(d, function(c, d, h) {
                if (void 0 !== b.cn && !1 === b.cn())
                    return Rh;
                var e = a.offsetLeft
                  , f = a.offsetTop
                  , t = a.offsetHeight
                  , C = a.offsetWidth;
                if (null === a.parentElement)
                    return w("draggable without parent element"),
                    Rh;
                var B = a.parentElement.clientHeight
                  , M = a.parentElement.clientWidth
                  , F = d
                  , O = h;
                void 0 !== b.ln && b.ln();
                return {
                    enabled: !0,
                    move: function(b, c) {
                        F = b;
                        O = c;
                        var g = f + (O - h)
                          , p = e + (F - d);
                        0 > g ? g = 0 : g + t > B && (g = B - t);
                        0 > p ? p = 0 : p + C > M && (p = M - C);
                        a.style.top = g + "px";
                        a.style.left = p + "px"
                    },
                    end: function() {
                        void 0 !== b.mn && b.mn()
                    }
                }
            })
    }
    ;var wi;
    q(S, function(a) {
        wi = a.h.Yp
    });
    function xi(a, b) {
        this.cq = "https://roomlister.stream.highwebmedia.com/session/";
        this.wd = {
            categories: a.join(",").trim(),
            num: b.toString()
        }
    }
    function yi(a, b) {
        var c = a.cq + b;
        return new Promise(function(b, e) {
            function d(c) {
                var d = [];
                c = new D(c);
                a.wd.key = H(c, "key");
                var e = JSON.parse(pb(c, "rooms"));
                e = l(e);
                for (var f = e.next(); !f.done; f = e.next())
                    f = f.value,
                    d.push({
                        username: f.username,
                        Bg: f.gender
                    });
                L(c);
                b(d)
            }
            void 0 !== wi && (a.wd.clientGender = wi);
            var h = new XMLHttpRequest;
            if (void 0 !== h.withCredentials)
                h.onload = function() {
                    206 === h.status && "Session Not Found" === h.responseText.trim() ? e("Session Not Found") : lb(h.status) ? d(h.responseText) : e(h.status)
                }
                ,
                h.onerror = function() {
                    e(h.status)
                }
                ,
                h.open("POST", c, !0),
                h.setRequestHeader("content-type", "application/x-www-form-urlencoded"),
                h.send(db(a.wd));
            else {
                var g = new XDomainRequest;
                g.onerror = function() {
                    e("error")
                }
                ;
                g.onload = function() {
                    d(g.responseText)
                }
                ;
                g.onprogress = function() {}
                ;
                g.ontimeout = function() {
                    e("error")
                }
                ;
                g.open("POST", c, !0);
                g.send(db(a.wd).toString())
            }
        }
        )
    }
    xi.prototype.start = function(a) {
        0 < a.length && (this.wd.alreadyShownRooms = a);
        return yi(this, "start/")
    }
    ;
    xi.prototype.next = function() {
        return yi(this, "next/")
    }
    ;
    function zi(a, b) {
        this.fa = a;
        this.Lq = b;
        this.ki = !1;
        Ai(this);
        var c = navigator.userAgent.match(/Puffin\/(\d)/);
        this.Ar = null !== c && 8 > parseInt(c[1]) ? 1500 : 100
    }
    function Ai(a) {
        var b = a.ki ? "https://cbjpeg.stream.highwebmedia.com/minifap/" + a.fa + ".jpg?f=" + Math.random() : "https://roomimg.stream.highwebmedia.com/ri/" + a.fa + ".jpg?" + Math.floor((new Date).getTime() / 3E4);
        return new Promise(function(c, d) {
            var e = new Image;
            e.onload = function() {
                a.Lq.src = e.src;
                c()
            }
            ;
            e.onerror = function() {
                d("Error downloading image " + b)
            }
            ;
            e.src = b
        }
        )
    }
    function Bi(a) {
        if (!a.ki) {
            a.ki = !0;
            var b = function() {
                a.ki && Ai(a).then(function() {
                    setTimeout(function() {
                        b()
                    }, a.Ar)
                })["catch"](b)
            };
            b()
        }
    }
    ;var Ci;
    q(S, function(a) {
        Ci = a.h.i
    });
    function Di(a) {
        v.call(this);
        var b = this;
        this.fa = a;
        this.a.style.position = "relative";
        this.a.style.padding = "4px";
        this.bi = document.createElement("a");
        this.bi.href = "/" + a + "/";
        this.a.appendChild(this.bi);
        y("click", this.bi, function(a) {
            N("LoadRoomFromRoomList", {
                location: "draggable_room_list",
                from_username: Ci,
                username: b.fa
            });
            a.metaKey || a.ctrlKey || (u(mh, b.fa),
            a.preventDefault())
        });
        this.ai = document.createElement("img");
        this.ai.className = "moreRooms";
        this.ai.style.width = "100%";
        this.ai.style.height = "100%";
        this.bi.appendChild(this.ai);
        this.bb = document.createElement("div");
        this.bb.innerText = a;
        this.bb.style.position = "absolute";
        this.bb.style.bottom = "7px";
        this.bb.style.color = "#ffffff";
        this.bb.style.fontSize = "19px";
        this.bb.style.textShadow = "1px 1px 1px #000, -1px -1px 1px #000, 1px -1px 1px #000, -1px 1px 1px #000";
        this.bi.appendChild(this.bb);
        this.um = new zi(this.fa,this.ai);
        this.a.onmouseenter = function() {
            Bi(b.um)
        }
        ;
        this.a.onmouseleave = function() {
            b.um.ki = !1
        }
    }
    m(Di, v);
    Di.prototype.C = function() {
        void 0 !== this.parent ? this.bb.style.left = this.parent.a.offsetWidth / 2 - this.bb.offsetWidth / 2 + "px" : w("Room does not have a parent")
    }
    ;
    function Ei(a) {
        switch (a) {
        case "male":
            return "m";
        case "female":
            return "f";
        case "trans":
            return "t";
        case "couple":
            return "c";
        case "followed":
            return "o";
        default:
            return w("Cannot convert gender " + a),
            ""
        }
    }
    ;function Fi() {
        this.On = []
    }
    function Gi(a, b) {
        a.On.push(b)
    }
    function Hi(a, b) {
        for (var c = l(a.On), d = c.next(); !d.done; d = c.next())
            if (d = d.value,
            b.which === d.keyCode && (!d.eg || b.ctrlKey || b.metaKey))
                return d.handle(b),
                !0;
        return !1
    }
    ;function Ii() {
        var a = this;
        this.message = document.createElement("div");
        this.ka = document.createElement("div");
        this.Cb = document.createElement("div");
        this.Wb = document.createElement("div");
        this.accept = document.createElement("div");
        this.li = function() {}
        ;
        this.Uo = [];
        this.ka.style.display = "none";
        this.ka.style.position = "fixed";
        this.ka.style.overflow = "auto";
        this.ka.style.zIndex = "11";
        this.ka.style.top = "0";
        this.ka.style.left = "0";
        this.ka.style.width = "100%";
        this.ka.style.height = "100%";
        this.ka.style.zIndex = "2000";
        this.ka.style.color = "#000000";
        Ka() ? this.ka.style.backgroundColor = "rgba(0, 0, 0, 0.4)" : this.ka.style.backgroundColor = "rgb(0, 0, 0)";
        this.Cb.style.width = "440px";
        this.Cb.style.position = "relative";
        this.Cb.style.backgroundColor = "white";
        this.Cb.style.display = "inline-block";
        this.Cb.style.borderRadius = "10px";
        this.Cb.style.textAlign = "left";
        this.Cb.style.overflow = "hidden";
        this.Cb.style.zIndex = "2001";
        this.Cb.style.fontFamily = "Helvetica, Arial, sans-serif";
        this.message.style.display = "inline-block";
        this.message.style.wordWrap = "break-word";
        this.message.style.padding = "20px";
        this.message.style.textAlign = "left";
        this.Cb.appendChild(this.message);
        var b = document.createElement("div");
        b.style.borderTop = "1px solid rgb(204, 204, 204)";
        b.style.textAlign = "center";
        b.style.height = "40px";
        b.style.color = "#0268ff";
        b.style.background = "#f9f9f9";
        this.accept.style.width = "70px";
        this.accept.style.padding = "3px";
        this.accept.style.display = "inline-block";
        this.accept.style.cssFloat = "right";
        this.accept.style.position = "relative";
        this.accept.style.top = "7px";
        this.accept.style.right = "10px";
        this.accept.style.boxSizing = "border-box";
        this.accept.innerText = "OK";
        this.accept.style.cursor = "pointer";
        this.accept.style.color = "#fff";
        this.accept.style.background = "#006ffc";
        this.accept.style.border = "1px solid #006ffc";
        this.accept.onclick = function(b) {
            b.stopPropagation();
            void 0 !== a.rg && a.rg();
            Ji.li()
        }
        ;
        b.appendChild(this.accept);
        this.Wb.style.width = "70px";
        this.Wb.style.padding = "3px";
        this.Wb.style.display = "inline-block";
        this.Wb.style.cssFloat = "right";
        this.Wb.style.position = "relative";
        this.Wb.style.top = "7px";
        this.Wb.style.right = "25px";
        this.Wb.innerText = "Cancel";
        this.Wb.style.cursor = "pointer";
        this.Wb.style.background = "#fff";
        this.Wb.style.border = "1px solid rgb(204, 204, 204)";
        this.Wb.onclick = function(b) {
            b.stopPropagation();
            void 0 !== a.Uh ? a.Uh() : void 0 !== a.rg && a.rg();
            Ji.li()
        }
        ;
        b.appendChild(this.Wb);
        this.Cb.appendChild(b);
        this.ka.appendChild(this.Cb)
    }
    Ii.prototype.resize = function() {
        this.Cb.style.width = Math.min(440, document.documentElement.clientWidth - 40) + "px";
        this.Cb.style.left = Math.max(0, (this.ka.offsetWidth - this.Cb.offsetWidth) / 2) + "px";
        this.Cb.style.top = Math.max(0, (this.ka.offsetHeight - 80 - this.Cb.offsetHeight) / 2) + "px"
    }
    ;
    Ii.prototype.display = function(a, b, c) {
        var d = this;
        if (this.active)
            this.Uo.push({
                Sq: a,
                rg: b,
                Uh: c
            });
        else {
            this.message.innerText = a;
            this.ka.style.display = "block";
            this.rg = b;
            void 0 === c ? Ji.Wb.style.display = "none" : (this.Uh = c,
            Ji.Wb.style.display = "");
            var e = new Fi
              , f = function(a) {
                a.stopPropagation();
                a.preventDefault();
                Hi(e, a)
            };
            Gi(e, {
                keyCode: 13,
                eg: !1,
                handle: function() {
                    b();
                    d.li()
                }
            });
            Gi(e, {
                keyCode: 27,
                eg: !1,
                handle: function() {
                    void 0 !== c && c();
                    d.li()
                }
            });
            this.active = !0;
            var h = function() {
                d.resize()
            };
            y("keydown", document, f, !0);
            y("resize", window, h);
            this.li = function() {
                try {
                    cb("keydown", document, f, !0)
                } catch (p) {}
                try {
                    cb("resize", window, h)
                } catch (p) {}
                d.ka.style.display = "none";
                d.active = !1;
                d.ka.parentElement === document.body && document.body.removeChild(d.ka);
                d.rg = void 0;
                d.Uh = void 0;
                var a = d.Uo.shift();
                void 0 !== a && d.display(a.Sq, a.rg, a.Uh)
            }
            ;
            a = T();
            void 0 !== a && a ? a.appendChild(this.ka) : document.body.appendChild(this.ka);
            this.resize()
        }
    }
    ;
    var Ji = new Ii;
    function U(a) {
        var b;
        void 0 === b && (b = function() {}
        );
        Ji.display(a, b)
    }
    function Ki(a, b, c) {
        void 0 === b && (b = function() {}
        );
        void 0 === c && (c = function() {}
        );
        Ji.display(a, b, c)
    }
    ;var Li = !0;
    q(S, function(a) {
        Li = a.j.ob
    });
    var Mi;
    function Ni(a) {
        a = void 0 === a ? "Login required" : a;
        return Li ? (void 0 !== Mi ? Mi() : Ki(a, function() {
            window.top.location.href = "/auth/login/?next=" + window.location.pathname + window.location.search
        }),
        !0) : !1
    }
    ;function Oi(a) {
        var b = Pi;
        Ni('You must be logged in to follow a broadcaster. Click "OK" to login.') || A("follow/follow/" + b + "/", {
            from_tube: -1 !== window.location.search.indexOf("from_tube") ? "True" : ""
        }).then(function() {
            u(a, {
                fa: b,
                If: !0
            });
            void 0 !== window._followedTabUpdate && window._followedTabUpdate(b)
        })["catch"](function(a) {
            w("Unable to follow " + b + ": " + a)
        })
    }
    function Qi(a) {
        var b = Pi;
        Ni('You must be logged in to follow a broadcaster. Click "OK" to login.') || A("follow/unfollow/" + b + "/", {
            from_tube: -1 !== window.location.search.indexOf("from_tube") ? "True" : ""
        }).then(function() {
            u(a, {
                fa: b,
                If: !1
            });
            void 0 !== window._followedTabUpdate && window._followedTabUpdate(b)
        })["catch"](function(a) {
            w("Unable to unfollow " + b + ": " + a)
        })
    }
    function Ri(a) {
        var b = Si;
        return new Promise(function(c) {
            function d() {
                c(a.map(function(a) {
                    return {
                        fa: a,
                        If: !1
                    }
                }))
            }
            b ? d() : A("follow/is_following_batch/", {
                users: a.join(",")
            }).then(function(b) {
                b = new D(b.responseText);
                var d = ob(b, "following");
                L(b);
                c(a.map(function(a) {
                    return {
                        fa: a,
                        If: J(d, a)
                    }
                }))
            })["catch"](function(a) {
                w("Error calling is_following_batch (" + a + ")");
                d()
            })
        }
        )
    }
    function Ti() {
        return new Promise(function(a) {
            z("follow/online_following").then(function(b) {
                b = new D(b.responseText);
                var c = E(b, "following");
                L(b);
                a(c)
            })["catch"](function(b) {
                401 !== b.fd.status && w("getOnlineFollowedRooms failed (" + b + ")");
                a([])
            })
        }
        )
    }
    ;var Ui = {
        wi: "chaturbate.com",
        zk: "",
        Fp: cc,
        ym: "#0b5d81",
        rm: "Chaturbate",
        bgColor: "#ffffff",
        Dj: "#0c6a93",
        yo: "#ffffff",
        Fl: "#f47321",
        wo: "#e2e2e2",
        xo: "url(https://ssl-ccstatic.highwebmedia.com/theatermodeassets/gender_tab_bg.gif) repeat-x",
        Hq: "#004B5E",
        Kd: "#494949",
        Ba: "#494949",
        Xb: "#222222",
        sk: "#222222",
        lc: "#0A5B83",
        cd: "#8bb3da",
        bd: "#ffffff",
        mg: "#dde9f5",
        lb: "#dc5500",
        ec: "#5e81a4",
        Dp: "#6D85B5",
        Ep: "#FFF",
        gk: "#2472B4",
        Gp: "#4F4F4F",
        zm: "#B60A42",
        Si: "#FFFFFF",
        Ym: "#F0F1F1",
        Zm: "#ACACAC",
        $m: "#000",
        an: "#0A5A83",
        Mk: "#575757",
        Nk: "#0A5A83",
        Cm: "#e45900",
        cl: "#676767",
        Bn: "#494949",
        fn: "#494949",
        qo: "https://ssl-ccstatic.highwebmedia.com/theatermodeassets/logo.svg",
        qm: "#f47321",
        yl: void 0,
        dl: "",
        Qn: "#ffffff url(https://ssl-ccstatic.highwebmedia.com/theatermodeassets/header_bg.gif) repeat-x",
        yn: "#ffffff url(https://ssl-ccstatic.highwebmedia.com/theatermodeassets/footer_bg.gif) repeat-x",
        Jm: "#e9e9e9",
        Vp: "#ffffff",
        zo: "#FCEADB",
        xn: "#494949",
        zn: "#0A5A83",
        An: "#dc5500",
        Qk: "#4C4C4C",
        Yk: "",
        oe: !1,
        mm: !1,
        rp: !1,
        xr: !1,
        qp: !1,
        up: !0,
        vp: !0,
        wp: !0,
        sp: !0,
        tp: !0,
        om: !0,
        hp: "#494949",
        po: "#000000",
        Zk: "#494949",
        Tp: "#3C87BA",
        Km: "#F47321",
        Lm: "#ffffff"
    }
      , Vi = Ui;
    if (void 0 !== window.siteSettings) {
        var V = window.siteSettings
          , Wi = V.navigation_font_color
          , Xi = V.text_color
          , Yi = V.tab_inactive_color
          , Zi = V.tab_inactive_font_color
          , $i = V.content_bgcolor
          , aj = V.footer_href_color
          , bj = V.footer_text_color
          , cj = V.cam_href_color;
        Vi = {
            wi: V.chaturbate_alias,
            zk: V.dbwl_domain,
            Fp: V.tagline,
            ym: V.tagline_color,
            rm: V.site_name,
            bgColor: V.bgcolor,
            Dj: V.navigation_bgcolor,
            yo: Wi,
            Fl: V.navigation_alt_bgcolor,
            wo: V.navigation_alt_2_bgcolor,
            xo: "",
            Hq: V.h1_color,
            Kd: Xi,
            Ba: Ui.Ba,
            Xb: Ui.Xb,
            sk: Xi,
            lc: V.href_color,
            cd: V.tab_border_color,
            bd: V.tab_active_color,
            mg: Yi,
            lb: V.tab_active_font_color,
            ec: Zi,
            Dp: V.tag_exhibitionist_color,
            Ep: V.tag_font_color,
            gk: V.tag_hd_color,
            Gp: V.tag_offline_color,
            zm: V.tag_private_group_color,
            Si: $i,
            Ym: V.cam_background_color,
            Zm: V.cam_border_color,
            $m: V.cam_text_color,
            an: "#0C6A93",
            Mk: V.cam_text_alt_color,
            Nk: cj,
            Cm: cj,
            cl: aj,
            Bn: bj,
            fn: bj,
            qo: V.logo_image_name,
            qm: V.signup_bg_color,
            yl: V._logo_width,
            dl: V.footer_html,
            Qn: "",
            yn: "",
            Jm: $i,
            Vp: Wi,
            zo: Yi,
            xn: Zi,
            zn: aj,
            An: aj,
            Qk: "#0C6A93",
            Yk: V.default_campaign_slug,
            oe: !0,
            mm: !0,
            rp: !0,
            xr: !0,
            qp: !0,
            up: !1,
            vp: !1,
            wp: !1,
            sp: !1,
            tp: !1,
            om: !1,
            hp: "#222222",
            po: Xi,
            Zk: Xi,
            Xo: V.whitelabel_hit_count_referring_url,
            Tp: V.tab_inactive_color,
            Km: V.tab_active_color,
            Lm: V.tab_active_font_color
        }
    }
    void 0 !== Vi.Xo && A("whitelabels/record_hit/", {
        referer: Vi.Xo
    })["catch"](function(a) {
        w("Error recording whitelabel hit: " + a)
    });
    var R = Vi;
    var dj, Si = !0;
    q(S, function(a) {
        dj = Ei(a.h.Ad);
        Si = a.j.ob
    });
    function ej() {
        v.call(this);
        var a = this;
        this.In = [];
        this.aj = [];
        this.Fi = !0;
        this.wl = !1;
        this.xl = "genders";
        this.Zc = [];
        this.Ci = [];
        this.Qf = !1;
        this.a.style.fontSize = "12px";
        this.a.style.overflowY = "scroll";
        y("scroll", this.a, function() {
            fj(a)
        });
        if (x()) {
            var b = window.localStorage.getItem(this.xl);
            null !== b && (this.Zc = JSON.parse(b),
            Si && (b = this.Zc.indexOf(Ei("followed")),
            0 <= b && this.Zc.splice(b, 1)))
        }
        this.pd = document.createElement("div");
        this.pd.style.margin = "2px 2px 0 2px";
        this.pd.style.whiteSpace = "nowrap";
        this.pd.style.textAlign = "center";
        this.a.appendChild(this.pd);
        gj(this, "Female", Pd);
        gj(this, "Male", Rd);
        gj(this, "Couple", Wd);
        gj(this, "Trans", Td);
        this.Qh = document.createElement("div");
        this.Qh.innerText = Bd + "...";
        this.Qh.style.color = R.Xb;
        this.Qh.style.margin = "4px";
        hj(this);
        b = document.createElement("p");
        b.style.color = R.Xb;
        b.style.cursor = "pointer";
        b.style.padding = "0";
        b.style.margin = "0";
        b.onclick = function() {
            hj(a)
        }
        ;
        b.innerText = Od;
        b.style.textAlign = "left";
        this.pd.appendChild(b);
        this.Mq = setInterval(function() {
            for (var b = l(a.children()), d = b.next(); !d.done; d = b.next())
                Ai(d.value.um)
        }, 3E4)
    }
    m(ej, v);
    ej.prototype.close = function() {
        clearInterval(this.Mq)
    }
    ;
    ej.prototype.children = function() {
        return v.prototype.children.call(this)
    }
    ;
    function gj(a, b, c) {
        function d() {
            return "Followed" === b && Ni('You must be logged in to view your followed cams. Click "OK" to login.') ? !0 : !1
        }
        var e = document.createElement("input");
        e.setAttribute("type", "checkbox");
        e.name = b.toLowerCase();
        e.title = b;
        e.checked = -1 < a.Zc.indexOf(Ei(b.toLowerCase()));
        0 < a.aj.length && (e.style.marginLeft = "12px");
        e.onchange = function() {
            d() ? e.checked = !1 : ij(a)
        }
        ;
        a.pd.appendChild(e);
        a.In.push(e);
        var f = document.createElement("img");
        f.src = "https://ssl-ccstatic.highwebmedia.com/theatermodeassets/icon" + b + ".png";
        f.style.position = "relative";
        f.style.top = "2px";
        f.style.cursor = "pointer";
        f.style.width = "16px";
        f.style.height = "16px";
        f.title = c;
        f.onclick = function() {
            d() || (e.checked = !e.checked,
            ij(a))
        }
        ;
        a.pd.appendChild(f);
        c = document.createElement("span");
        c.innerText = b;
        c.style.position = "relative";
        c.style.color = R.Xb;
        c.style.top = "1px";
        c.style.marginLeft = "4px";
        c.style.cursor = "pointer";
        c.onclick = function() {
            d() || (e.checked = !e.checked,
            ij(a))
        }
        ;
        a.pd.appendChild(c);
        a.aj.push(c)
    }
    function ij(a) {
        a.Zc = [];
        for (var b = l(a.In), c = b.next(); !c.done; c = b.next())
            c = c.value,
            c.checked && a.Zc.push(Ei(c.name));
        N("ChangeRoomListGender", {
            selectedGenders: a.Zc
        });
        x() && (0 === a.Zc.length ? window.localStorage.removeItem(a.xl) : window.localStorage.setItem(a.xl, JSON.stringify(a.Zc)));
        hj(a)
    }
    ej.prototype.C = function() {
        v.prototype.C.call(this);
        for (var a = this.a.clientWidth - 8, b = l(this.children()), c = b.next(); !c.done; c = b.next())
            c = c.value,
            c.a.style.width = a + "px",
            c.a.style.height = .84375 * c.a.offsetWidth + "px";
        this.pd.style.width = a + "px";
        if (!this.Fi && this.pd.scrollWidth <= a) {
            c = l(this.aj);
            for (b = c.next(); !b.done; b = c.next())
                b.value.style.display = "inline-block";
            this.Fi = !0
        }
        if (this.Fi && this.pd.scrollWidth > a) {
            a = l(this.aj);
            for (b = a.next(); !b.done; b = a.next())
                b.value.style.display = "none";
            this.Fi = !1
        }
        fj(this)
    }
    ;
    function hj(a) {
        Ha(a);
        a.Ci = [];
        a.wl = !0;
        a.a.appendChild(a.Qh);
        !Si && -1 < a.Zc.indexOf(Ei("followed")) ? (a.Qf = !0,
        Ti().then(function(b) {
            a.Qf = !1;
            0 < b.length && jj(a, b.map(function(a) {
                return {
                    fa: a,
                    If: !0
                }
            }));
            kj(a);
            a.Y()
        })) : (kj(a),
        a.Y())
    }
    function lj(a) {
        return 0 === a.Zc.length ? void 0 === dj ? window.default_gender.split("") : [dj] : a.Zc.filter(function(a) {
            return a !== Ei("followed")
        })
    }
    function kj(a) {
        a.bf = new xi(lj(a),10);
        a.bf.start(mj(a)).then(function(b) {
            nj(a, b.map(function(a) {
                return a.username
            }))
        })["catch"](function(b) {
            a.Qf = !1;
            w("getNewIterator: " + b)
        })
    }
    function oj(a) {
        a.Qf || (a.Qf = !0,
        "key"in a.bf.wd && a.bf.next().then(function(b) {
            try {
                0 === b.length ? (a.Ci = [],
                kj(a)) : nj(a, b.map(function(a) {
                    return a.username
                }))
            } catch (c) {
                w(c + ": Could not load more rooms from the roomIterator")
            }
        })["catch"](function(b) {
            a.Qf = !1;
            "Session Not Found" === b ? (Xh("roomlist session not-found. recreating..."),
            kj(a)) : w("loadMoreRooms: " + b)
        }))
    }
    function nj(a, b) {
        Ri(b).then(function(b) {
            jj(a, b);
            fj(a);
            a.Y()
        })
    }
    function jj(a, b) {
        a.Qf = !1;
        for (var c = .84375 * (a.a.clientWidth - 8), d = a.a.scrollTop, e = [], f = l(a.children()), h = f.next(); !h.done; h = f.next())
            if (h = h.value,
            h.a.offsetTop - d < -15 * c)
                e.push(h);
            else
                break;
        c = l(e);
        for (h = c.next(); !h.done; h = c.next())
            e = h.value,
            d -= e.a.offsetHeight,
            a.removeChild(e);
        a.a.scrollTop = d;
        a.wl && (a.a.removeChild(a.Qh),
        a.wl = !1);
        d = l(b);
        for (c = d.next(); !c.done; c = d.next())
            a.Ci.push(a.N(new Di(c.value.fa)))
    }
    function fj(a) {
        a.a.scrollTop + 4.21875 * (a.a.clientWidth - 8) >= a.a.scrollHeight - a.a.offsetHeight && oj(a)
    }
    function mj(a) {
        var b = [];
        a = l(a.Ci);
        for (var c = a.next(); !c.done; c = a.next())
            b.push(c.value.fa);
        return b
    }
    ;var pj = new n("tipsInPast24HoursUpdate")
      , qj = new n("tokenBalanceUpdate");
    function rj(a) {
        return new Promise(function(b, c) {
            z("tipping/current_tokens/?room=" + a).then(function(a) {
                a = new D(a.responseText);
                var c = {
                    sa: K(a, "token_balance")
                }
                  , d = G(a, "tip_options");
                if (void 0 !== d) {
                    d = new D(d);
                    var h = void 0 === h ? !0 : h;
                    var g = E(d, "tip_options");
                    "object" !== typeof g ? (h && void 0 !== g && r("getObjectStringOrUndefined(tip_options): " + g + " is wrong type", {
                        message: d.Hd
                    }),
                    d = void 0) : d = JSON.stringify(g);
                    if (void 0 !== d) {
                        d = new D(d);
                        h = d.getAsString("label");
                        var p = pb(d, "options");
                        g = [];
                        p = l(JSON.parse(p));
                        for (var t = p.next(); !t.done; t = p.next())
                            t = new D(JSON.stringify(t.value)),
                            g.push({
                                label: t.getAsString("label")
                            }),
                            L(t);
                        h = {
                            label: h,
                            options: g
                        };
                        L(d);
                        d = h
                    } else
                        d = void 0;
                    c.dh = d
                }
                L(a);
                u(qj, {
                    Sb: c.sa
                });
                b(c)
            })["catch"](function(a) {
                c(a.status)
            })
        }
        )
    }
    var sj = !1;
    function tj(a) {
        return new Promise(function(b, c) {
            sj || (sj = !0,
            A("tipping/send_tip/" + a.fa + "/", {
                tip_amount: a.Lp,
                message: a.message,
                source: a.source,
                tip_room_type: a.Mp
            }).then(function(a) {
                sj = !1;
                a = new D(a.responseText);
                var c = {
                    Id: J(a, "success")
                }
                  , d = G(a, "error", !1);
                void 0 !== d ? c.error = d : (c.sa = K(a, "token_balance"),
                c.Lc = K(a, "tipped_performer_last_24hrs"));
                L(a);
                void 0 !== c.sa && u(qj, {
                    Sb: c.sa
                });
                b(c)
            })["catch"](function(a) {
                sj = !1;
                c(a.status)
            }))
        }
        )
    }
    ;function uj() {
        var a = document.createElement("input");
        y("keypress", a, function(a) {
            a.metaKey || 8 === a.charCode || 0 === a.charCode || 13 === a.charCode || 48 <= a.charCode && 57 >= a.charCode || a.preventDefault()
        });
        return a
    }
    ;function vj(a) {
        v.call(this);
        var b = this;
        this.Am = a;
        this.ig = "public";
        this.Pe = !1;
        this.jf = new n("tipSent");
        this.Ao = new n("notifyAttemptSendTip");
        this.tm = !1;
        this.a.style.minWidth = "400px";
        this.a.style.color = R.Xb;
        a = document.createElement("div");
        a.style.display = "inline-block";
        var c = document.createElement("span");
        c.innerText = ce;
        c.style.display = "inline-block";
        c.style.fontSize = "13px";
        c.style.fontWeight = "bold";
        c.style.padding = "6px";
        a.appendChild(c);
        this.Jb = document.createElement("span");
        this.Jb.innerText = Cd + "...";
        this.Jb.style.display = "inline-block";
        this.Jb.style.color = "green";
        this.Jb.style.fontSize = "13px";
        this.Jb.style.fontWeight = "bold";
        this.Jb.style.padding = "6px 6px 6px 0";
        a.appendChild(this.Jb);
        q(S, function(a) {
            b.fa = a.j.i();
            q(a.j.event.kb, function(a) {
                switch (a.Aa) {
                case "privatewatching":
                case "groupwatching":
                    b.ig = "private";
                    break;
                default:
                    b.ig = "public"
                }
            })
        });
        q(qj, function(a) {
            b.rf(a.Sb)
        });
        var d = document.createElement("a");
        d.innerText = Dc;
        d.href = "/tipping/purchase_tokens/";
        d.style.display = "inline-block";
        d.style.color = "#0a5a83";
        d.style.position = "absolute";
        d.style.right = "0";
        d.style.fontSize = "13px";
        d.style.padding = "6px";
        d.onclick = function() {
            Mh("/tipping/purchase_tokens/", "_blank", "height=615, width=850");
            return !1
        }
        ;
        d.onmouseenter = function() {
            d.style.textDecoration = "underline"
        }
        ;
        d.onmouseleave = function() {
            d.style.textDecoration = "none"
        }
        ;
        a.appendChild(d);
        this.a.appendChild(a);
        this.Bm = a;
        this.Na = document.createElement("div");
        this.Na.style.display = "none";
        this.Na.style.color = "#ff0000";
        this.Na.style.fontSize = "13px";
        this.Na.style.fontWeight = "bold";
        this.Na.style.padding = "6px";
        this.Na.style.textAlign = "center";
        this.Na.style.position = "absolute";
        this.Na.style.left = "50%";
        a = document.createElement("div");
        a.innerText = ee;
        this.Na.appendChild(a);
        a = document.createElement("div");
        a.innerText = fe;
        this.Na.appendChild(a);
        this.a.appendChild(this.Na);
        this.ad = document.createElement("form");
        c = document.createElement("label");
        a = document.createElement("span");
        a.innerText = ge;
        a.style.display = "inline-block";
        a.style.fontSize = "13px";
        a.style.padding = "6px";
        c.appendChild(a);
        this.ba = uj();
        this.ba.value = "25";
        this.ba.min = "1";
        this.ba.style.width = "5em";
        this.ba.style.display = "inline-block";
        this.ba.style.fontSize = "13px";
        this.ba.style.padding = "4px";
        this.ba.style.border = "1px solid #b4b4b4";
        this.ba.style.borderRadius = "4px";
        this.ba.style.marginTop = "12px";
        this.ba.onclick = function() {
            wj(b)
        }
        ;
        this.Cc = document.createElement("div");
        this.Cc.innerText = he;
        this.Cc.style.color = "red";
        this.Cc.style.display = "none";
        this.Cc.style.paddingLeft = "5px";
        a = document.createElement("div");
        this.ba.oninput = function() {
            xj(b)
        }
        ;
        c.appendChild(this.ba);
        c.appendChild(this.Cc);
        this.ad.appendChild(c);
        c = document.createElement("label");
        this.yc = document.createElement("div");
        this.yc.innerText = oc;
        this.yc.style.fontSize = "13px";
        this.yc.style.padding = "6px 6px 0 6px";
        c.appendChild(this.yc);
        this.Jc = document.createElement("div");
        this.ea = document.createElement("textarea");
        this.ea.maxLength = 255;
        this.ea.style.width = "100%";
        this.ea.style.resize = "none";
        this.ea.style.fontSize = "13px";
        this.ea.style.margin = "6px";
        this.ea.style.padding = "4px";
        this.ea.style.border = "1px solid #4b4c4b";
        this.ea.style.borderRadius = "4px";
        this.ea.style.boxSizing = "border-box";
        this.Jc.appendChild(this.ea);
        this.yc.onclick = function() {
            b.ea.select()
        }
        ;
        c.appendChild(this.Jc);
        this.ad.appendChild(c);
        a.style.textAlign = "right";
        a.style.position = "relative";
        c = document.createElement("div");
        c.style.textAlign = "left";
        this.eb = document.createElement("div");
        this.eb.innerText = ie;
        c.style.position = "absolute";
        this.eb.style.display = "none";
        c.style.fontSize = "10px";
        c.style.cssFloat = "left";
        c.style.lineHeight = "1.3em";
        c.style.padding = "6px";
        c.style.textAlign = "left";
        c.style.left = "0px";
        a.appendChild(c);
        c.appendChild(this.eb);
        this.D = document.createElement("button");
        this.D.innerHTML = pc + " &#9656;";
        this.D.setAttribute("type", "submit");
        this.D.style.display = "inline-block";
        this.D.style.fontSize = "14px";
        this.D.style.fontFamily = "UbuntuMedium, Helvetica, Arial, sans-serif";
        this.D.style.margin = "6px";
        this.D.style.padding = "6px 18px";
        this.D.style.color = "#ffffff";
        this.D.style.backgroundColor = "#f47321";
        this.D.style.border = "1px solid #b1b1b1";
        this.D.style.borderRadius = "4px";
        this.D.style.cursor = "pointer";
        a.appendChild(this.D);
        this.ji = document.createElement("div");
        this.ji.style.fontSize = "10px";
        var e = document.createElement("label")
          , f = document.createElement("span");
        f.innerText = je;
        f.style.cursor = "pointer";
        var h = document.createElement("input");
        h.type = "checkbox";
        y("click", h, function() {
            b.tm = !b.tm
        });
        e.appendChild(f);
        e.appendChild(h);
        this.ji.appendChild(e);
        c.appendChild(this.ji);
        this.ad.appendChild(a);
        y("keydown", this.a, function(a) {
            9 === a.keyCode && (a.preventDefault(),
            a.shiftKey ? document.activeElement === b.D ? h.focus() : document.activeElement === h ? b.ea.focus() : document.activeElement === b.ea ? b.ba.focus() : b.D.focus() : document.activeElement === b.D ? b.ba.focus() : document.activeElement === b.ba ? b.ea.focus() : document.activeElement === b.ea ? h.focus() : b.D.focus())
        });
        this.a.appendChild(this.ad);
        this.C();
        var g;
        y("submit", this.ad, function(a) {
            a.preventDefault();
            var c = parseInt(b.ba.value);
            isNaN(c) || b.D.disabled || (N("SendTipClicked", {
                amount: c
            }),
            100 < c && !b.Pe ? (b.D.innerText = Bb(c),
            b.Pe = !0,
            b.C()) : (b.ba.blur(),
            void 0 === g && (tj({
                fa: b.fa,
                Lp: b.ba.value,
                message: "" + (void 0 !== b.ua ? b.ua.value : b.ea.value),
                source: b.Am,
                Mp: b.ig
            }).then(function(a) {
                a.Id ? N("SendTipSuccess", {
                    amount: c
                }) : void 0 !== a.error ? U(a.error) : w("unknown send tip error");
                b.Sj();
                b.ea.value = "";
                void 0 !== a.Lc && u(pj, a.Lc);
                u(b.jf, {
                    La: c,
                    Id: a.Id
                })
            })["catch"](function(a) {
                w("Error sending tip (" + a + ")");
                u(b.jf, {
                    La: c,
                    Id: !1
                })
            }),
            u(b.Ao, {
                La: c
            }),
            g = setTimeout(function() {
                return g = void 0
            }, 250))))
        })
    }
    m(vj, v);
    k = vj.prototype;
    k.C = function() {
        this.Jc.style.width = Math.max(0, this.a.clientWidth - 12) + "px";
        this.eb.style.maxWidth = Math.max(0, this.a.offsetWidth - this.D.offsetWidth - 15) + "px"
    }
    ;
    k.Sj = function() {
        this.D.innerText = pc;
        this.Pe = !1
    }
    ;
    function xj(a) {
        /^[1-9]\d*$/.test(a.ba.value) ? (a.D.disabled = !1,
        a.D.style.backgroundColor = "#f47321",
        a.D.style.color = "#ffffff",
        a.Cc.style.display = "none") : (a.D.disabled = !0,
        a.D.style.backgroundColor = "#888",
        a.D.style.color = "#ccc",
        a.Cc.style.display = "inline-block");
        a.Pe && a.Sj()
    }
    k.show = function(a) {
        var b = this;
        void 0 !== a.La && (this.ba.value = a.La.toString(),
        xj(this));
        this.ea.value = void 0 !== a.message ? a.message : "";
        void 0 !== a.si && a.si ? (this.eb.style.display = "none",
        this.ji.style.lineHeight = "3em") : (this.eb.style.display = "block",
        this.ji.style.lineHeight = "");
        rj(this.fa).then(function(a) {
            b.ba.max = a.sa.toString();
            if (void 0 !== a.dh) {
                b.yc.innerText = a.dh.label;
                void 0 !== b.ua && b.Jc.removeChild(b.ua);
                b.ua = document.createElement("select");
                b.ua.style.width = "100%";
                b.ua.style.fontSize = ".8125em";
                b.ua.style.margin = "6px";
                b.ua.style.border = "1px solid #4b4c4b";
                b.ua.style.boxSizing = "border-box";
                b.Jc.appendChild(b.ua);
                var c = document.createElement("option");
                c.innerText = "-- " + ke + " --";
                b.ua.appendChild(c);
                a = l(a.dh.options);
                for (c = a.next(); !c.done; c = a.next()) {
                    var e = c.value;
                    c = document.createElement("option");
                    c.innerText = e.label;
                    c.value = e.label;
                    b.ua.appendChild(c)
                }
                b.ea.style.display = "none"
            } else
                void 0 !== b.ua && (b.yc.innerText = oc,
                b.ea.value = "",
                b.ea.style.display = "block",
                b.Jc.removeChild(b.ua),
                b.ua = void 0);
            b.C();
            wj(b)
        })["catch"](function(a) {
            w("Error getting token balance (" + a + ")")
        })
    }
    ;
    function wj(a) {
        document.activeElement !== a.ba && (a.ba.focus(),
        a.ba.select())
    }
    k.rf = function(a) {
        this.Jb.innerText = a + " " + Ab(a, !1)
    }
    ;
    k.Kn = function() {
        this.a.style.visibility = "hidden";
        document.body.appendChild(this.a);
        this.a.style.width = this.Bm.offsetWidth + "px";
        this.a.style.height = "auto";
        var a = [this.a.offsetWidth, this.a.offsetHeight];
        document.body.removeChild(this.a);
        this.a.style.visibility = "";
        return a
    }
    ;
    function yj(a) {
        return a.Oe ? "#DC5500" : a.Dc ? "#DC0000" : a.Bc ? "#090" : a.mf ? "#804baa" : a.kf ? "#be6aff" : a.lf ? "#009" : a.je ? "#69A" : "#494949"
    }
    function zj(a) {
        a = a.toLowerCase();
        if (-1 < ["m", "male"].indexOf(a))
            return "https://ssl-ccstatic.highwebmedia.com/theatermodeassets/gendericons/male.svg";
        if (-1 < ["f", "female"].indexOf(a))
            return "https://ssl-ccstatic.highwebmedia.com/theatermodeassets/gendericons/female.svg";
        if (-1 < ["t", "s", "trans"].indexOf(a))
            return "https://ssl-ccstatic.highwebmedia.com/theatermodeassets/gendericons/trans.svg";
        if (-1 < ["c", "couple"].indexOf(a))
            return "https://ssl-ccstatic.highwebmedia.com/theatermodeassets/gendericons/couple.svg";
        w("Could not set menu gender icon. Defaulting to male.");
        return "https://ssl-ccstatic.highwebmedia.com/theatermodeassets/gendericons/male.svg"
    }
    ;var Aj;
    q(S, function(a) {
        Aj = a.j.i()
    });
    function Cj(a) {
        return new Promise(function(b, c) {
            z("accounts/api/usermenu/" + a + "/" + Aj + "/").then(function(a) {
                a = new D(a.responseText);
                var c = J(a, "online")
                  , d = H(a, "username")
                  , h = K(a, "display_age", !1)
                  , g = H(a, "gender")
                  , p = H(a, "image_url")
                  , t = J(a, "can_silence")
                  , C = J(a, "can_access");
                L(a);
                b({
                    username: d,
                    he: h,
                    Bg: g,
                    oj: p,
                    Ye: c,
                    xg: t,
                    bn: C
                })
            })["catch"](function(a) {
                c(a)
            })
        }
        )
    }
    ;var Dj = new n("roomListRequest")
      , Ej = new n("playerForceMuted")
      , Fj = new n("standardEmoticonRequest")
      , Gj = new n("userMenuPmClicked")
      , Hj = new n("closePmSession");
    var Ij, Jj, Kj;
    q(S, function(a) {
        Ij = a.j.i();
        Jj = a.j.username();
        Kj = a.j
    });
    function Lj(a, b) {
        var c = document.createElement("span");
        c.style.color = (void 0 === b ? 0 : b) ? yj(a) : a.Oe ? "#ff6200" : a.Dc ? "#DC0000" : a.Bc ? "#00ff00" : a.mf ? "#ad62e1" : a.kf ? "#d4a0ff" : a.lf ? "#8a98ff" : a.je ? "#84c6dc" : "#b3b3b3";
        c.style.fontWeight = "bold";
        c.innerText = a.username;
        c.onmouseenter = function() {
            c.style.textDecoration = "underline";
            c.style.cursor = "pointer"
        }
        ;
        c.onmouseleave = function() {
            c.style.textDecoration = "none";
            c.style.cursor = "default"
        }
        ;
        y("click", c, function() {
            null === c.parentElement ? w("Username span has no parent") : (N("OpenUserContextMenu", {
                username: a.username
            }),
            Mj(a.username, c.parentElement))
        });
        return c
    }
    function Nj() {
        var a = document.createElement("hr");
        a.style.margin = "6px";
        return a
    }
    function Mj(a, b) {
        2 === Oj && (Oj = 0,
        Cj(a).then(function(c) {
            var d = new Pj(a,c,b);
            b.style.cursor = "default";
            d.position();
            d.xg(c.xg, a)
        })["catch"](function(a) {
            Oj = 2;
            w("Could not process user menu request: " + a)
        }))
    }
    function Pj(a, b, c) {
        var d = this;
        Oj = 0;
        this.d = b;
        this.ka = c;
        this.T = document.createElement("div");
        this.T.style.border = "1px solid #6B6B6B";
        this.T.style.position = "absolute";
        this.T.style.background = "#e1e1e1";
        this.T.style.borderRadius = "4px";
        this.T.style.display = "none";
        this.T.style.zIndex = "999";
        this.T.style.fontWeight = "normal";
        this.T.style.fontSize = "12px";
        this.T.onmouseenter = function() {
            d.T.style.cursor = "default"
        }
        ;
        this.O = document.createElement("div");
        this.O.style.position = "fixed";
        this.O.style.top = "0";
        this.O.style.left = "0";
        this.O.style.height = "100%";
        this.O.style.width = "100%";
        this.O.style.display = "block";
        this.ka.appendChild(this.O);
        this.O.onclick = function() {
            d.Jd()
        }
        ;
        b = document.createElement("div");
        c = document.createElement("img");
        var e = document.createElement("span");
        c.src = "https://ssl-ccstatic.highwebmedia.com/theatermodeassets/red-arrow-down.gif";
        e.innerText = a;
        b.style.margin = "6px";
        b.style.color = "#000";
        c.style.paddingRight = "6px";
        b.appendChild(c);
        b.appendChild(e);
        this.T.appendChild(b);
        this.T.appendChild(Nj());
        b = document.createElement("div");
        b.style.margin = "0 6px 0 6px";
        b.style.display = "block";
        c = document.createElement("div");
        c.style.display = "inline-block";
        c.style.margin = "4px";
        c.style.verticalAlign = "top";
        b.appendChild(c);
        this.d.bn && (e = document.createElement("span"),
        e.innerText = isNaN(this.d.he) ? "" : "" + this.d.he,
        e.style.display = "inline-block",
        e.style.color = "#000",
        e.style.marginRight = "2px",
        c.appendChild(e),
        e = document.createElement("img"),
        e.src = zj(this.d.Bg),
        e.style.display = "inline-block",
        e.height = 16,
        e.width = 16,
        c.appendChild(e));
        e = document.createElement("div");
        var f = document.createElement("a");
        e.appendChild(f);
        c.appendChild(e);
        f.href = "/" + a + "/";
        f.target = "_blank";
        f.innerText = ne;
        f.style.textDecoration = "none";
        f.style.color = "black";
        f.onmouseenter = function() {
            f.style.textDecoration = "underline";
            f.style.cursor = "pointer"
        }
        ;
        f.onmouseleave = function() {
            f.style.textDecoration = "none";
            f.style.cursor = "default"
        }
        ;
        y("click", e, function(b) {
            N("ViewProfile", {
                username: a
            });
            b.stopPropagation();
            d.Jd()
        });
        this.d.Ye && (e = document.createElement("img"),
        e.src = this.d.oj,
        e.height = 41,
        e.width = 49,
        e.style.display = "inline-block",
        b.insertBefore(e, c));
        this.T.appendChild(b);
        this.T.appendChild(Nj());
        if (Jj !== a) {
            if (void 0 === Kj) {
                w("ignoreText: Chatconnection should be defined");
                return
            }
            b = document.createElement("div");
            var h = document.createElement("div")
              , g = document.createElement("div");
            c = document.createElement("img");
            c.src = "https://ssl-ccstatic.highwebmedia.com/theatermodeassets/icon-email.gif";
            var p = document.createElement("span");
            p.innerText = oe;
            var t = document.createElement("span");
            Kj.ne(a) ? (t.style.paddingLeft = "13px",
            t.innerText = Qf) : (t.style.paddingLeft = "7px",
            t.innerText = Sf,
            e = document.createElement("img"),
            e.src = "https://ssl-ccstatic.highwebmedia.com/theatermodeassets/icon-ignore.gif",
            g.appendChild(e));
            g.appendChild(t);
            c.style.paddingRight = "4px";
            h.style.padding = "3px 9px 3px 9px";
            h.style.whiteSpace = "nowrap";
            g.style.padding = "3px 9px 3px 9px";
            b.style.marginBottom = "7px";
            b.style.color = "#000";
            h.appendChild(c);
            h.appendChild(p);
            b.appendChild(h);
            b.appendChild(g);
            g.onmouseenter = function() {
                g.style.background = "#6B6B6B";
                g.style.cursor = "pointer";
                t.style.color = "#e1e1e1"
            }
            ;
            g.onmouseleave = function() {
                g.style.background = "#e1e1e1";
                g.style.cursor = "default";
                t.style.color = "black"
            }
            ;
            g.onclick = function() {
                N("IgnoreUser", {
                    username: a
                });
                void 0 === Kj ? w("no chatConn defined in ignoreLink.onclick") : (Kj.ne(a) ? Kj.qk(a) : Kj.Qc(a),
                d.Jd())
            }
            ;
            h.onmouseenter = function() {
                h.style.background = "#6B6B6B";
                h.style.cursor = "pointer";
                p.style.color = "#e1e1e1"
            }
            ;
            h.onmouseleave = function() {
                h.style.background = "#e1e1e1";
                h.style.cursor = "default";
                p.style.color = "black"
            }
            ;
            y("click", h, function(b) {
                d.Jd();
                Ni('You must be logged in to send a private message. Click "OK" to login.') || (N("StartPrivateMessage", {
                    username: a
                }),
                u(Gj, a),
                b.stopPropagation())
            });
            this.T.appendChild(b)
        }
        this.T.style.display = "block";
        Oj = 1
    }
    Pj.prototype.xg = function(a, b) {
        var c = this;
        if (a) {
            this.we = document.createElement("div");
            var d = document.createElement("div")
              , e = document.createElement("span");
            e.innerText = "Silence for 6 hours";
            e.style.paddingLeft = "3px";
            d.style.padding = "3px 9px 3px 9px";
            this.we.style.marginBottom = "7px";
            this.we.style.color = "#000";
            d.appendChild(e);
            this.we.appendChild(d);
            d.onmouseenter = function() {
                d.style.background = "#6B6B6B";
                d.style.cursor = "pointer";
                d.style.color = "#e1e1e1"
            }
            ;
            d.onmouseleave = function() {
                d.style.background = "#e1e1e1";
                d.style.cursor = "default";
                d.style.color = "black"
            }
            ;
            d.onclick = function() {
                Ki("Silence " + b + "?", function() {
                    N("SilenceUser", {
                        username: b
                    });
                    A("roomsilence/" + b + "/" + Ij + "/", {}).then(function() {})["catch"](function(a) {
                        w("Unable to silence user (" + a + ")", {
                            room: Ij,
                            username: b
                        });
                        U("Error silencing user " + b)
                    })
                });
                c.Jd()
            }
            ;
            this.T.appendChild(Nj());
            this.T.appendChild(this.we)
        }
    }
    ;
    Pj.prototype.position = function() {
        this.ka.appendChild(this.T);
        var a = this.ka.getBoundingClientRect()
          , b = this.ka.offsetParent.getBoundingClientRect();
        this.T.style.top = a.top - b.top - 3 + "px";
        this.T.offsetTop + this.T.offsetHeight > window.document.documentElement.clientHeight - 8 && (this.T.style.top = window.document.documentElement.clientHeight - this.T.offsetHeight - 8 + "px");
        a = this.T.getBoundingClientRect();
        a.left + this.T.offsetWidth > window.document.documentElement.clientWidth - 8 && (this.T.style.left = window.document.documentElement.clientWidth - a.left - this.T.offsetWidth + "px")
    }
    ;
    Pj.prototype.Jd = function() {
        this.ka.removeChild(this.T);
        this.ka.removeChild(this.O);
        Oj = 2
    }
    ;
    var Oj = 2;
    var Qj;
    q(S, function(a) {
        Qj = a.j
    });
    function Rj() {
        v.call(this);
        this.Im = new n("UserListWindow:userCountUpdate");
        this.ti = [];
        this.a.id = "users-list";
        this.a.style.padding = "0 6px";
        this.a.style.boxSizing = "border-box";
        this.a.style.position = "";
        this.Af();
        this.refresh(!1)
    }
    m(Rj, v);
    k = Rj.prototype;
    k.dg = function() {
        for (; null !== this.a.firstChild; )
            this.a.removeChild(this.a.firstChild)
    }
    ;
    k.Af = function() {
        var a = document.createElement("div");
        a.innerText = le + "...";
        a.style.color = R.Xb;
        a.style.margin = "6px 0";
        this.a.style.fontSize = "12px";
        this.a.appendChild(a)
    }
    ;
    k.Tk = function() {
        var a = this
          , b = document.createElement("div");
        b.style.color = R.Xb;
        b.style.margin = "6px 0";
        var c = document.createElement("span");
        c.innerText = me;
        c.id = "refresh-userlist";
        c.style.cursor = "pointer";
        y("click", c, function(b) {
            a.dg();
            a.Af();
            a.refresh(!1);
            b.preventDefault()
        });
        b.appendChild(c);
        this.a.appendChild(b)
    }
    ;
    k.clear = function(a) {
        u(this.Im, a);
        this.dg();
        this.Af();
        this.C()
    }
    ;
    k.refresh = function(a) {
        var b = this;
        a = void 0 === a ? !0 : a;
        Qj.al().then(function(c) {
            u(b.Im, c.nf);
            if (!a) {
                b.dg();
                b.Tk();
                b.ti = c.ye;
                for (var d = l(b.ti), e = d.next(); !e.done; e = d.next()) {
                    e = e.value;
                    var f = document.createElement("div");
                    f.style.width = "100%";
                    f.style.margin = "6px 0";
                    f.appendChild(Lj(e));
                    b.a.appendChild(f)
                }
                d = document.createElement("div");
                d.style.color = R.Xb;
                d.innerText = "+" + c.fe + " anonymous user" + (1 === c.fe ? "" : "s");
                d.style.width = "100%";
                d.style.margin = "6px 0";
                d.style.whiteSpace = "nowrap";
                b.a.appendChild(d);
                b.C()
            }
        })["catch"](function(a) {
            w("Error retrieving user list: " + a)
        })
    }
    ;
    k.C = function() {
        v.prototype.C.call(this)
    }
    ;
    function Sj() {
        v.call(this);
        this.xm = [];
        this.Vn = "#0d5d81";
        this.xc = document.createElement("div");
        this.xc.id = "tab-row";
        this.xc.style.width = "100%";
        this.xc.style.background = "#cccccc";
        this.a.appendChild(this.xc);
        this.xc.style.borderBottom = "2px solid #ccc";
        this.window = document.createElement("div");
        this.window.style.width = "100%";
        this.window.style.position = "absolute";
        this.a.appendChild(this.window)
    }
    m(Sj, v);
    k = Sj.prototype;
    k.C = function() {
        this.window.style.height = this.a.clientHeight - this.xc.offsetHeight + "px"
    }
    ;
    k.N = function() {
        throw Error("addChild not implemented");
    }
    ;
    function Tj(a, b) {
        b.a.style.width = "100%";
        b.a.style.height = "100%";
        b.a.style.display = "none";
        a.xm.push(a.Ek(b));
        v.prototype.N.call(a, b, a.window);
        return b
    }
    k.Ek = function(a) {
        var b = document.createElement("div");
        b.style.position = "relative";
        b.style.fontSize = "10px";
        b.style.padding = "5px";
        b.style.margin = "2px 0 0 2px";
        b.style.borderRadius = "4px 4px 0 0";
        b.style.backgroundRepeat = "no-repeat";
        b.style.backgroundPosition = "center";
        b.style.minWidth = "16px";
        Uj(a, b);
        y("click", b, function(c) {
            a.bh(b, c)
        });
        y("touchstart", b, function(c) {
            a.bh(b, c)
        });
        this.xc.appendChild(b);
        return b
    }
    ;
    function Vj(a, b, c) {
        var d = a.xm[b];
        if (c)
            d.style.color = a.Vn,
            d.style.backgroundColor = "rgb(255, 255, 255)",
            d.style.cursor = "default",
            d.onmouseenter = function() {}
            ,
            d.onmouseleave = function() {}
            ;
        else {
            var e = function() {
                d.style.color = R.Qk;
                d.style.backgroundColor = "rgb(201, 201, 201)"
            };
            e();
            d.style.cursor = "pointer";
            d.onmouseenter = function() {
                d.style.color = R.Qk;
                d.style.backgroundColor = "rgb(255, 255, 255)"
            }
            ;
            d.onmouseleave = function() {
                e()
            }
        }
    }
    k.children = function() {
        return v.prototype.children.call(this)
    }
    ;
    k.qh = function(a) {
        a >= this.children().length ? w("invalid tab index") : (this.Ie = this.children()[a],
        Wj(this))
    }
    ;
    function Xj(a, b) {
        var c = a.children().indexOf(b);
        isNaN(c) ? w("tab not found") : a.qh(c)
    }
    k.$a = function() {
        return this.Ie
    }
    ;
    function Wj(a) {
        for (var b = 0; b < a.children().length; b += 1) {
            var c = a.children()[b];
            c !== a.Ie ? (c.A(),
            Vj(a, b, !1)) : a.uq !== a.Ie && (Vj(a, b, !0),
            c.show(),
            a.uq = c)
        }
        a.Y()
    }
    k.qc = function() {
        for (var a = 0; a < this.children().length; a += 1) {
            var b = this.children()[a];
            Uj(b, this.xm[a])
        }
    }
    ;
    function Uj(a, b) {
        b.innerHTML = a.Kf();
        b.id = a.Me();
        b.style.backgroundImage = a.fl();
        b.style.display = a.Jg() ? "none" : "inline-block"
    }
    function W() {
        v.call(this);
        this.a.style.position = "relative"
    }
    m(W, v);
    k = W.prototype;
    k.bh = function() {
        N("FocusChatTab");
        Yj(this)
    }
    ;
    k.Jg = function() {
        return !1
    }
    ;
    k.fl = function() {
        return ""
    }
    ;
    k.Me = function() {
        return ""
    }
    ;
    function Yj(a) {
        void 0 === a.parent ? w("no parent") : Xj(a.parent, a)
    }
    k.show = function() {
        this.a.style.display = "block";
        this.Y()
    }
    ;
    k.A = function() {
        this.a.style.display = "none"
    }
    ;
    function Zj(a) {
        return void 0 === a.parent ? (w("no parent"),
        !1) : a.parent.$a() === a
    }
    k.qc = function() {
        void 0 === this.parent ? w("no parent") : this.parent.qc()
    }
    ;
    var ak = {
        aqua: "#00ffff",
        black: "#000000",
        blue: "#0000ff",
        fuchsia: "#ff00ff",
        gray: "#808080",
        green: "#008000",
        lime: "#00ff00",
        maroon: "#800000",
        navy: "#000080",
        olive: "#808000",
        orange: "#ffa500",
        purple: "#800080",
        red: "#ff0000",
        silver: "#c0c0c0",
        teal: "#008080",
        white: "#ffffff",
        yellow: "#ffff00"
    };
    function bk(a) {
        this.defaultValue = a = void 0 === a ? {
            red: 200,
            green: 200,
            blue: 200
        } : a
    }
    bk.prototype.parse = function(a) {
        var b = a.toLowerCase();
        if (/^[a-z]+$/.test(b)) {
            var c = ak[b];
            void 0 !== c && (b = c)
        }
        if (/^#([a-f0-9]{3}){1,2}$/.test(b))
            return 4 === b.length && (b = "#" + b[1] + b[1] + b[2] + b[2] + b[3] + b[3]),
            a = Number("0x" + b.substring(1)),
            {
                red: a >> 16 & 255,
                green: a >> 8 & 255,
                blue: a & 255
            };
        if (0 === b.indexOf("rgb")) {
            a = b.match(/\d+(\.\d+)?%?/g);
            if (null === a || 3 > a.length || 4 < a.length)
                a = this.defaultValue;
            else {
                a = [a[0], a[1], a[2]];
                b = [];
                for (c = 0; 3 > c; c += 1)
                    b[c] = -1 !== a[c].indexOf("%") ? Math.round(2.55 * parseFloat(a[c])) : Number(a[c]),
                    0 > b[c] ? b[c] = 0 : 255 < b[c] && (b[c] = 255);
                a = {
                    red: b[0],
                    green: b[1],
                    blue: b[2]
                }
            }
            return a
        }
        Xh('Cannot parse color: "' + a + '"');
        return this.defaultValue
    }
    ;
    function ck(a, b, c) {
        this.red = a;
        this.green = b;
        this.blue = c;
        this.Hi = this.bj = this.Qj = 10
    }
    function dk(a, b) {
        var c = void 0 === c ? !1 : c;
        for (b = Math.min(255, b); b > a.brightness() && (!c || 255 !== a.red && 255 !== a.green && 255 !== a.blue); )
            a.red = Math.min(a.red + Math.max(1, .05 * a.red), 255),
            a.green = Math.min(a.green + Math.max(1, .05 * a.green), 255),
            a.blue = Math.min(a.blue + Math.max(1, .05 * a.blue), 255);
        a.red = Math.round(a.red);
        a.green = Math.round(a.green);
        a.blue = Math.round(a.blue)
    }
    ck.prototype.brightness = function() {
        return Math.round(this.Qj / (this.Qj + this.bj + this.Hi) * this.red + this.bj / (this.Qj + this.bj + this.Hi) * this.green + this.Hi / (this.Qj + this.bj + this.Hi) * this.blue)
    }
    ;
    var ek = {
        height: 0,
        name: "",
        me: "",
        width: 0,
        Rm: ""
    };
    function fk(a) {
        var b = []
          , c = []
          , d = [];
        if (0 > a.indexOf("%%%[emoticon "))
            a = {
                Ib: [a],
                da: []
            };
        else {
            for (var e = 0; e < a.length; e += 1)
                if ("%" === a[e])
                    if ("%%%[emoticon " === a.slice(e, e + 13)) {
                        var f = e + a.slice(e, a.length).indexOf("]%%%");
                        if ("]%%%" === a.slice(f, f + 4)) {
                            d.push(b.join(""));
                            b = [];
                            b: {
                                e = a.slice(e, f + 4);
                                e = e.replace("%%%[emoticon ", "").replace("]%%%", "");
                                e = e.split(/\|/);
                                if (6 === e.length) {
                                    if (e = {
                                        name: e[0],
                                        hk: e[1],
                                        width: gk(e[2]) ? Number(e[2]) : 80,
                                        height: gk(e[3]) ? Number(e[3]) : 80,
                                        me: e[4],
                                        Rm: e[5]
                                    },
                                    void 0 !== e.hk && hk(e.hk))
                                        break b
                                } else if (5 === e.length && (e = {
                                    name: e[0],
                                    me: e[1],
                                    width: gk(e[2]) ? Number(e[2]) : 80,
                                    height: gk(e[3]) ? Number(e[3]) : 80,
                                    Rm: e[4]
                                },
                                hk(e.me)))
                                    break b;
                                e = void 0
                            }
                            void 0 !== e ? c.push(e) : (w("Cannot parse emoticon: " + e),
                            c.push(ek));
                            e = f + 4 - 1
                        }
                    } else
                        b.push(a[e]);
                else
                    b.push(a[e]);
            d.push(b.join(""));
            a = {
                Ib: d,
                da: c
            }
        }
        this.Qm = a
    }
    fk.prototype.Ib = function() {
        return this.Qm.Ib
    }
    ;
    fk.prototype.da = function() {
        return this.Qm.da
    }
    ;
    function hk(a) {
        return null === a.match(/(http|https):\/\/(\w+:{0,1}\w*@)?(\S+)(:[0-9]+)?(\/|\/([\w#!:.?+=&%@!\-\/]))?/g) ? (w("Invalid emoticon URL"),
        !1) : !0
    }
    function gk(a) {
        return isNaN(parseInt(a, 10)) ? (w("Invalid dimension argument"),
        !1) : !0
    }
    ;function ik(a) {
        var b = "tipping/private_show_tokens_per_minute/" + a + "/";
        return new Promise(function(a, d) {
            z(b).then(function(b) {
                b = new D(b.responseText);
                a({
                    uo: I(b, "private_show_minimum_minutes"),
                    Ro: K(b, "price"),
                    cr: J(b, "recordings_allowed"),
                    bq: J(b, "allowed", !0, !1)
                });
                L(b)
            })["catch"](function(a) {
                d(a)
            })
        }
        )
    }
    function jk(a) {
        Ni(Ug) || (N("RequestPrivateShow"),
        ik(a.i()).then(function(b) {
            if (b.bq) {
                var c = Kb(a.i(), b.Ro, b.uo, b.cr);
                Ki(c, function() {
                    a.bp(b.Ro, b.uo).then(function(a) {
                        "" !== a && U(a)
                    })["catch"](function(b) {
                        "privatewatching" !== a.status && (U(Eg),
                        w("Error entering private show: " + b))
                    });
                    N("StartPrivateShow")
                })
            } else
                U(Tg)
        }))
    }
    function kk(a) {
        if (!Ni(Vg)) {
            N("RequestSpyShow");
            var b = "tipping/spy_on_private_show_tokens_per_minute/" + a.i() + "/";
            z(b).then(function(b) {
                var c = Number(b.responseText);
                isNaN(c) ? w("isNaN on " + b.responseText) : 0 === c ? U(Xg) : Ki(Q(P("Spy on private show for (%(price)s tokens per minute)"), {
                    price: c
                }, !0), function() {
                    a.cp().then(function(a) {
                        "" !== a && U(a)
                    })["catch"](function(b) {
                        "privatespying" !== a.status && (U(Fg),
                        w("Error entering spy show: " + b))
                    });
                    N("StartSpyShow")
                })
            })
        }
    }
    function lk(a) {
        Ni(Wg) || (N("RequestGroupShow"),
        z("tipping/group_show_tokens_per_minute/" + a.i() + "/").then(function(b) {
            var c = Number(b.responseText);
            isNaN(c) ? w("isNaN on " + b.responseText) : Ki(Lb(a.i(), c), function() {
                a.ap(c).then(function(a) {
                    "" !== a && U(a)
                })["catch"](function(b) {
                    "groupnotwatching" !== a.status && (U(Gg),
                    w("Error entering group show: " + b))
                });
                N("StartGroupShow")
            })
        }))
    }
    function mk(a) {
        Ki(Ig, function() {
            N("LeaveGroupShow");
            a.Oh()
        })
    }
    ;var nk = new n("modalReposition");
    y("resize", window, function() {
        u(nk, void 0)
    });
    var pk = new n("modalFullscreenChange");
    y(Kh(), document, function() {
        u(pk, void 0)
    });
    var qk = new n("modalEscape");
    y("keydown", document, function(a) {
        27 === a.keyCode && u(qk, a)
    });
    function rk(a) {
        a = void 0 === a ? !0 : a;
        v.call(this);
        var b = this;
        this.bc = new n("overlayClick");
        this.Fa = new Aa;
        this.Pf = !1;
        this.O = document.createElement("div");
        this.O.style.position = "fixed";
        this.O.style.display = "none";
        this.O.style.left = "0";
        this.O.style.top = "0";
        this.O.style.right = "0";
        this.O.style.bottom = "0";
        this.O.style.zIndex = "1000";
        this.O.onclick = function(a) {
            a.stopPropagation();
            b.$b();
            u(b.bc, void 0)
        }
        ;
        this.a.style.zIndex = "1001";
        this.a.onclick = function(a) {
            a.stopPropagation()
        }
        ;
        this.dr = function() {
            b.Pf && b.Y()
        }
        ;
        this.Dq = function() {
            b.Pf && b.tc()
        }
        ;
        a && q(qk, function(a) {
            b.Pf && (a.preventDefault(),
            b.$b())
        })
    }
    m(rk, v);
    rk.prototype.tc = function() {
        var a = T();
        T() && void 0 !== a ? (a.appendChild(this.O),
        a.appendChild(this.a)) : (document.body.appendChild(this.O),
        document.body.appendChild(this.a));
        this.Pf || (a = q(nk, this.dr),
        this.Fa.add(a),
        a = q(pk, this.Dq),
        this.Fa.add(a),
        this.Pf = !0);
        this.O.style.display = "block"
    }
    ;
    rk.prototype.$b = function() {
        this.O.style.display = "none";
        null !== this.a.parentElement && this.a.parentElement.removeChild(this.a);
        null !== this.O.parentElement && this.O.parentElement.removeChild(this.O);
        this.Pf && (Da(this.Fa),
        this.Pf = !1)
    }
    ;
    var sk = [];
    q(S, function(a) {
        sk = a.h.nj
    });
    var tk = new n("previewEmoticon");
    function uk() {
        rk.call(this);
        var a = this;
        this.a.style.visibility = "hidden";
        this.a.style.position = "fixed";
        this.a.style.width = "auto";
        this.a.style.height = "auto";
        this.a.style.backgroundColor = "#ffffff";
        this.a.style.borderRadius = "6px";
        this.a.style.padding = "10px";
        this.a.style.fontSize = "12px";
        this.a.style.fontFamily = "UbuntuRegular, Helvetica, Arial, sans-serif";
        this.a.style.zIndex = "1003";
        this.a.style.textAlign = "center";
        Ka() && (this.a.style.boxShadow = "0 0 18px rgba(0, 0, 0, 0.4)");
        this.O.style.backgroundColor = "#000000";
        this.O.style.opacity = "0.4";
        this.O.style.zIndex = "1002";
        var b = document.createElement("img");
        b.src = "https://ssl-ccstatic.highwebmedia.com/theatermodeassets/close.svg";
        b.style.position = "absolute";
        b.style.width = "10px";
        b.style.top = "9px";
        b.style.right = "9px";
        b.style.cursor = "pointer";
        b.style.opacity = "0.5";
        b.onclick = function() {
            a.A()
        }
        ;
        b.onmouseenter = function() {
            b.style.opacity = "1"
        }
        ;
        b.onmouseleave = function() {
            b.style.opacity = "0.5"
        }
        ;
        this.a.appendChild(b);
        this.oc = document.createElement("img");
        this.oc.style.verticalAlign = "middle";
        this.oc.style.padding = "10px";
        y("load", this.oc, function() {
            a.a.style.visibility = "visible";
            a.tc();
            a.C()
        });
        this.a.appendChild(this.oc);
        this.rc = document.createElement("div");
        this.rc.innerText = "REPORT EMOTICON";
        this.rc.style.color = R.lc;
        this.rc.style.cursor = "pointer";
        this.rc.onclick = function() {
            a.rc.style.display = "none";
            a.Yd.style.display = "block";
            a.C()
        }
        ;
        this.rc.onmouseenter = function() {
            a.rc.style.textDecoration = "underline"
        }
        ;
        this.rc.onmouseleave = function() {
            a.rc.style.textDecoration = "none"
        }
        ;
        this.a.appendChild(this.rc);
        this.Yd = document.createElement("div");
        this.Yd.style.display = "none";
        this.a.appendChild(this.Yd);
        var c = document.createElement("span");
        c.innerText = "Choose a category:";
        c.style.marginRight = "6px";
        this.Yd.appendChild(c);
        var d = document.createElement("select");
        c = l(vk);
        for (var e = c.next(); !e.done; e = c.next()) {
            e = e.value;
            var f = document.createElement("option");
            f.innerText = e.label;
            f.value = e.value;
            d.appendChild(f)
        }
        this.Yd.appendChild(d);
        c = document.createElement("span");
        c.innerText = "CANCEL";
        c.style.fontFamily = "UbuntuMedium, Helvetica, Arial, sans-serif";
        c.style.fontSize = "12px";
        c.style.textShadow = "#585858 1px 1px 0";
        c.style.color = "#ffffff";
        c.style.padding = "2px 6px";
        c.style.backgroundColor = "#898989";
        La() && (c.style.background = "linear-gradient(180deg, #7b7b7b 0%, #898989 50%, #585858 60%, #616161 100%)");
        c.style.borderRadius = "4px";
        c.style.boxSizing = "border-box";
        c.style.cursor = "pointer";
        c.style.display = "inline-block";
        c.style.marginLeft = "6px";
        c.onclick = function() {
            a.rc.style.display = "block";
            a.Yd.style.display = "none";
            a.C()
        }
        ;
        this.Yd.appendChild(c);
        c = document.createElement("span");
        c.innerText = "REPORT";
        c.style.fontFamily = "UbuntuMedium, Helvetica, Arial, sans-serif";
        c.style.fontSize = "12px";
        c.style.textShadow = "#bc2b1d 1px 1px 0";
        c.style.color = "#ffffff";
        c.style.padding = "2px 6px";
        c.style.backgroundColor = "#ef6352";
        La() && (c.style.background = "linear-gradient(180deg, #ff4d30 0%, #ef6352 50%, #f2240f 60%, #e63125 100%)");
        c.style.borderRadius = "4px";
        c.style.boxSizing = "border-box";
        c.style.cursor = "pointer";
        c.style.display = "inline-block";
        c.style.marginLeft = "6px";
        c.onclick = function() {
            A("emoticon_report_abuse/" + a.zh.vc + "/", {
                category: d.value
            }).then(function() {
                sk.push(a.zh.vc);
                a.Yd.style.display = "none";
                a.pf.style.display = "block";
                a.C()
            })["catch"](function(a) {
                w("Error reporting emoticon: " + a)
            })
        }
        ;
        this.Yd.appendChild(c);
        this.pf = document.createElement("div");
        this.pf.style.display = "none";
        this.a.appendChild(this.pf);
        c = document.createElement("span");
        c.innerText = "EMOTICON REPORTED - ";
        this.pf.appendChild(c);
        var h = document.createElement("span");
        h.innerText = "UNDO";
        h.style.color = R.lc;
        h.style.cursor = "pointer";
        h.onclick = function() {
            A("emoticon_report_abuse/" + a.zh.vc + "/", {
                category: d.value
            }).then(function() {
                var b = sk.indexOf(a.zh.vc, 0);
                0 <= b && sk.splice(b, 1);
                a.pf.style.display = "none";
                a.rc.style.display = "block";
                a.C()
            })["catch"](function(a) {
                w("Error reporting emoticon: " + a)
            })
        }
        ;
        h.onmouseenter = function() {
            h.style.textDecoration = "underline"
        }
        ;
        h.onmouseleave = function() {
            h.style.textDecoration = "none"
        }
        ;
        this.pf.appendChild(h);
        q(this.bc, function() {
            a.A()
        })
    }
    m(uk, rk);
    uk.prototype.show = function(a) {
        this.zh = a;
        this.oc.src = this.zh.url;
        this.rc.style.display = "none";
        this.Yd.style.display = "none";
        this.pf.style.display = "none";
        0 <= sk.indexOf(a.vc) ? this.pf.style.display = "block" : this.rc.style.display = "block";
        this.C()
    }
    ;
    uk.prototype.C = function() {
        this.a.style.left = (document.documentElement.clientWidth - this.a.offsetWidth) / 2 + "px";
        this.a.style.top = (document.documentElement.clientHeight - this.a.offsetHeight) / 2 + "px"
    }
    ;
    uk.prototype.A = function() {
        this.$b();
        this.a.style.visibility = "hidden"
    }
    ;
    var vk = [{
        label: "Just ignore this emoticon",
        value: "ignore"
    }, {
        label: "Offensive",
        value: "offensive"
    }, {
        label: "Disgusting",
        value: "disgusting"
    }, {
        label: "Fake Tip",
        value: "fake_tip"
    }, {
        label: "Advertising",
        value: "advertising"
    }];
    var wk = new bk, xk = !1, yk, zk, Ak;
    q(S, function(a) {
        xk = a.h.qa.Zg;
        yk = a.h.Ad;
        zk = a.j;
        Ak = a.h.nj
    });
    q(th, function(a) {
        xk = a.Zg
    });
    function Bk(a) {
        a = wk.parse(a);
        a = new ck(a.red,a.green,a.blue);
        var b = a.brightness();
        dk(a, 255 - b / 3);
        return "rgb(" + a.red + ", " + a.green + ", " + a.blue + ")"
    }
    function Ck(a) {
        var b = xk
          , c = new fk(a);
        a = document.createElement("span");
        a.className = "emoticonImage";
        a.innerText = c.Ib()[0];
        for (var d = {
            ha: 1
        }; d.ha < c.Ib().length; d = {
            ha: d.ha
        },
        d.ha += 1) {
            if (!b || 0 <= Ak.indexOf(c.da()[d.ha - 1].name)) {
                var e = document.createElement("span");
                e.style.color = "lightskyblue";
                e.innerText = ":" + c.da()[d.ha - 1].name;
                e.style.cursor = "pointer";
                e.onclick = function(a) {
                    return function() {
                        var b = {
                            vc: c.da()[a.ha - 1].name,
                            url: c.da()[a.ha - 1].me
                        };
                        u(tk, b)
                    }
                }(d)
            } else {
                e = document.createElement("img");
                var f = c.da()[d.ha - 1].hk;
                e.src = void 0 !== f ? f : c.da()[d.ha - 1].me;
                e.title = ":" + c.da()[d.ha - 1].name;
                e.style.cursor = "pointer";
                e.onclick = function(a) {
                    return function() {
                        var b = {
                            vc: c.da()[a.ha - 1].name,
                            url: c.da()[a.ha - 1].me
                        };
                        u(tk, b)
                    }
                }(d);
                e.name = c.da()[d.ha - 1].name;
                e.height = c.da()[d.ha - 1].height;
                e.width = c.da()[d.ha - 1].width
            }
            a.appendChild(e);
            "" !== c.Ib()[d.ha] && (e = document.createElement("span"),
            e.innerText = c.Ib()[d.ha],
            a.appendChild(e))
        }
        return a
    }
    function Dk(a) {
        var b = Ek(a.fb.username);
        b.style.fontSize = "";
        var c = Lj(a.fb);
        c.style.marginRight = "4px";
        b.appendChild(c);
        var d = Ck(a.message);
        d.style.color = Bk(void 0 !== a.Kd ? a.Kd : "#FFFFFF");
        void 0 !== a.font && (c.style.fontFamily = a.font,
        d.style.fontFamily = a.font);
        b.appendChild(d);
        return b
    }
    function Fk(a) {
        var b = Ek();
        b.style.padding = "1px 5px";
        var c = document.createElement("div");
        c.style.display = "inline-block";
        void 0 !== a.background ? (c.style.background = a.background,
        c.style.color = void 0 !== a.oa ? a.oa : "#000000",
        c.style.padding = "2px",
        c.style.textShadow = "none") : c.style.color = Bk(void 0 !== a.oa ? a.oa : "#aaaaaa");
        c.style.fontWeight = void 0 !== a.weight ? a.weight : "normal";
        b.appendChild(Gk(c, a.U, void 0 !== a.background));
        return b
    }
    function Gk(a, b, c) {
        b = l(b);
        for (var d = b.next(); !d.done; d = b.next()) {
            var e = document.createElement("div");
            d = l(d.value);
            for (var f = d.next(); !f.done; f = d.next())
                switch (f = f.value,
                f.cc) {
                case 1:
                    e.appendChild(Hk(f));
                    break;
                case 0:
                    e.appendChild(Lj(f.hc, c));
                    break;
                case 2:
                    e.appendChild(Ik(f.message));
                    break;
                case 4:
                    e.appendChild(Jk(Vc, function() {
                        "grouprequesting" === zk.status ? U(Xc) : lk(zk)
                    }));
                    break;
                case 3:
                    e.appendChild(Jk(Md, function() {
                        "privatespying" !== zk.status && kk(zk)
                    }));
                    break;
                default:
                    w("Unknown roomNotice type for: " + f)
                }
            a.appendChild(e)
        }
        return a
    }
    function Jk(a, b) {
        var c = document.createElement("a");
        c.style.color = "#85FFFF";
        c.style.textDecoration = "underline";
        c.innerText = a;
        c.onclick = b;
        c.style.cursor = "pointer";
        return c
    }
    function Ik(a) {
        var b = document.createElement("a");
        b.href = "/tag/" + a + "/" + yk;
        b.style.color = "#85FFFF";
        b.style.textDecoration = "none";
        b.innerText = "#" + a;
        return b
    }
    function Kk(a) {
        var b = Ek();
        b.innerText = a;
        b.style.color = Bk("#aaaaaa");
        return b
    }
    function Lk(a, b) {
        var c = Ek()
          , d = document.createElement("span");
        d.innerText = Mb(a);
        c.appendChild(d);
        for (d = 0; d < b.length; d += 1) {
            var e = document.createElement("a");
            e.href = b[d].Um;
            e.target = "_blank";
            e.innerText = b[d].Dk;
            e.style.color = Bk("#1c8fc8");
            e.style.textDecoration = "none";
            c.appendChild(e);
            d < b.length - 1 && (e = document.createElement("span"),
            e.innerText = ", ",
            c.appendChild(e))
        }
        c.style.color = Bk("#aaaaaa");
        return c
    }
    function Ek(a) {
        a = void 0 === a ? "" : a;
        var b = document.createElement("div");
        b.style.fontFamily = "Tahoma,Arial,Helvetica,sans-serif";
        b.style.boxSizing = "border-box";
        b.style.paddingTop = "3px";
        b.style.paddingBottom = "3px";
        b.style.paddingLeft = "5px";
        b.style.paddingRight = "5px";
        0 < a.length && b.setAttribute("data-nick", a);
        return b
    }
    function Hk(a) {
        var b = document.createElement("span");
        !1 !== a.Ml ? b = Ck(a.message) : b.innerText = a.message;
        b.style.color = void 0 !== a.oa ? a.oa : "";
        b.style.background = void 0 !== a.background ? a.background : "";
        b.style.fontWeight = void 0 !== a.weight ? a.weight : "";
        return b
    }
    ;function Mk(a) {
        a = a.match(/(\S+)/g);
        if (null !== a && "/tip" === a[0]) {
            if (2 > a.length)
                return {};
            a.shift();
            if (2 > a.length) {
                var b = Number(a[0]);
                return isNaN(b) ? {
                    message: a[0]
                } : {
                    La: b
                }
            }
            b = Number(a[0]);
            a.shift();
            return {
                La: b,
                message: a.join(" ")
            }
        }
    }
    ;function X() {
        v.call(this);
        var a = this;
        this.bc = new n("overlayClick");
        this.O = document.createElement("div");
        this.O.style.position = "fixed";
        this.O.style.display = "none";
        this.O.style.left = "0";
        this.O.style.top = "0";
        this.O.style.right = "0";
        this.O.style.bottom = "0";
        this.O.style.zIndex = "1000";
        this.O.onclick = function(b) {
            b.stopPropagation();
            a.$b();
            u(a.bc, void 0)
        }
        ;
        this.a.style.zIndex = "1001";
        this.a.onclick = function(a) {
            a.stopPropagation()
        }
        ;
        setTimeout(function() {
            a.oh()
        }, 0)
    }
    m(X, v);
    X.prototype.oh = function() {
        v.prototype.oh.call(this);
        null !== this.a.parentElement && this.a.parentElement.insertBefore(this.O, this.a)
    }
    ;
    X.prototype.tc = function() {
        this.O.style.display = "block"
    }
    ;
    X.prototype.$b = function() {
        this.O.style.display = "none"
    }
    ;
    var Nk = new n("changeVideoMode",{
        sd: 15
    });
    var Ok, Pk = [40, 37, 39, 38, 13, 27, 32, 9], Qk = new n("videoModeChanged",{
        sd: 20
    });
    q(Nk, function() {
        u(Qk, void 0)
    });
    function Rk(a) {
        X.call(this);
        var b = this;
        this.F = a;
        this.sb = !1;
        this.cache = new Map;
        this.Fa = new Aa;
        this.a.style.visibility = "hidden";
        this.a.style.width = "auto";
        this.a.style.height = "auto";
        this.a.style.backgroundColor = "#ffffff";
        this.a.style.border = "1px solid #acacac";
        this.a.style.borderBottom = "none";
        this.a.style.fontFamily = "Helvetica, Arial, sans-serif";
        this.a.style.cursor = "pointer";
        q(this.bc, function() {
            b.A()
        });
        this.Ve = document.createElement("span");
        this.Ve.style.position = "fixed";
        this.Ve.style.width = "auto";
        this.Ve.style.fontSize = "12px";
        this.Ve.style.fontFamily = "Helvetica, Arial, sans-serif";
        document.body.appendChild(this.Ve);
        this.Uc = document.createElement("div");
        this.Uc.style.display = "none";
        this.Uc.style.width = "260px";
        this.Uc.style.height = "110px";
        this.Uc.style.lineHeight = "110px";
        this.Uc.style.borderBottom = "1px solid #acacac";
        this.Uc.style.textAlign = "center";
        this.Uc.onclick = function() {
            void 0 !== b.selectedIndex && u(tk, b.da[b.selectedIndex])
        }
        ;
        this.a.appendChild(this.Uc);
        var c = document.createElement("div");
        c.innerText = "VIEW / REPORT EMOTICON";
        c.style.color = R.lc;
        c.style.fontSize = "10px";
        c.style.position = "relative";
        c.style.top = "102px";
        c.style.lineHeight = "0";
        c.onmouseenter = function() {
            c.style.textDecoration = "underline"
        }
        ;
        c.onmouseleave = function() {
            c.style.textDecoration = "none"
        }
        ;
        this.Uc.appendChild(c);
        this.oc = document.createElement("img");
        this.oc.src = "https://ssl-ccstatic.highwebmedia.com/theatermodeassets/loading.png";
        this.oc.style.maxWidth = "240px";
        this.oc.style.maxHeight = "80px";
        this.oc.style.verticalAlign = "middle";
        this.oc.style.padding = "10px 10px 16px 10px";
        this.Uc.appendChild(this.oc);
        this.list = document.createElement("div");
        this.list.style.width = "260px";
        this.list.style.height = "180px";
        this.list.style.overflowY = "scroll";
        this.a.appendChild(this.list);
        y("keyup", this.F.Nb, function(a) {
            Sk(b, a)
        });
        y("keydown", this.F.Nb, function(a) {
            if (b.sb) {
                13 === a.keyCode && b.A();
                38 === a.keyCode && (a.preventDefault(),
                Tk(b, !0));
                40 === a.keyCode && (a.preventDefault(),
                Tk(b, !1));
                if (9 === a.keyCode || 39 === a.keyCode || 32 === a.keyCode)
                    a.preventDefault(),
                    b.A(),
                    b.F.Nb.value += " ";
                if (27 === a.keyCode || 37 === a.keyCode)
                    a.preventDefault(),
                    b.A(),
                    null !== b.F.Nb.selectionStart && (b.F.Nb.value = b.F.Nb.value.substring(0, b.F.Nb.selectionStart))
            }
        });
        this.Fa.add(q(S, function(a) {
            b.Ui = parseInt(a.h.qa.Yi);
            Ok = a.h.nj
        }));
        this.Fa.add(q(th, function(a) {
            b.Ui = parseInt(a.Yi)
        }));
        this.Fa.add(q(Qk, function() {
            b.A()
        }))
    }
    m(Rk, X);
    Rk.prototype.zg = function() {
        Da(this.Fa)
    }
    ;
    Rk.prototype.C = function() {
        this.a.style.left = Math.min(this.Ve.offsetWidth + this.F.lo, this.F.Nb.clientWidth - this.F.fp) + "px"
    }
    ;
    function Sk(a, b) {
        if (0 <= a.Ui && -1 === Pk.indexOf(b.keyCode)) {
            var c = a.F.Nb.value.match(/(^|.+\s):([\w-]+)$/);
            null !== c ? (a.prefix = c[2],
            void 0 !== a.cache[a.prefix] ? (a.da = a.cache[a.prefix],
            Uk(a, c[1])) : (clearTimeout(a.vq),
            a.vq = setTimeout(function() {
                Vk(a.prefix).then(function(b) {
                    b = new D(b.responseText);
                    H(b, "slug");
                    var d = qb(b, "emoticons")
                      , f = [];
                    d = l(d);
                    for (var h = d.next(); !h.done; h = d.next())
                        h = h.value,
                        f.push({
                            vc: h.slug,
                            url: h.url,
                            element: Wk
                        });
                    L(b);
                    a.da = f;
                    a.cache[a.prefix] = a.da;
                    Uk(a, c[1])
                })["catch"](function(a) {
                    w("Error loading emoticon autocomplete: " + a)
                })
            }, 100))) : a.A()
        }
    }
    function Xk(a) {
        for (; null !== a.list.firstChild; )
            a.list.removeChild(a.list.firstChild);
        a.selectedIndex = void 0;
        a.Uc.style.display = "none"
    }
    function Uk(a, b) {
        Xk(a);
        for (var c = 0; c < a.da.length; c += 1)
            Yk(a, a.da[c], c);
        a.list.scrollTop = a.list.clientHeight;
        a.sb || (a.Ve.innerText = b + ":",
        a.show())
    }
    function Yk(a, b, c) {
        var d = document.createElement("div");
        d.innerText = b.vc;
        d.style.padding = "2px 8px";
        0 <= Ok.indexOf(b.vc) && (d.style.display = "none");
        d.onclick = function() {
            a.Po = void 0 === a.selectedIndex ? a.prefix : a.da[a.selectedIndex].vc;
            a.selectedIndex = c;
            Zk(a, !1);
            a.F.Nb.focus()
        }
        ;
        a.list.appendChild(d);
        b.element = d
    }
    function $k(a) {
        a = l(a.da);
        for (var b = a.next(); !b.done; b = a.next())
            b.value.element.style.backgroundColor = "#ffffff"
    }
    function Tk(a, b) {
        a.Po = void 0 === a.selectedIndex ? a.prefix : a.da[a.selectedIndex].vc;
        $k(a);
        a.selectedIndex = void 0 === a.selectedIndex ? b ? a.da.length - 1 : 0 : a.selectedIndex + (b ? -1 : 1);
        0 > a.selectedIndex ? a.selectedIndex = a.da.length - 1 : a.selectedIndex >= a.da.length && (a.selectedIndex = 0);
        Zk(a, !0)
    }
    function Zk(a, b) {
        void 0 !== a.selectedIndex && (a.oc.src = "https://ssl-ccstatic.highwebmedia.com/theatermodeassets/loading.png",
        a.oc.src = a.da[a.selectedIndex].url,
        a.Uc.style.display = "block",
        $k(a),
        a.da[a.selectedIndex].element.style.backgroundColor = "#cccccc",
        b && (a.list.scrollTop = 18 * a.selectedIndex - 90),
        a.F.Nb.value = a.F.Nb.value.replace(new RegExp(a.Po + "$"), a.da[a.selectedIndex].vc),
        a.F.Nb.setSelectionRange(a.Ve.innerText.length + a.prefix.length, a.F.Nb.value.length))
    }
    Rk.prototype.show = function() {
        var a = this;
        0 <= this.Ui && setTimeout(function() {
            a.a.style.visibility = "visible";
            a.tc();
            a.Y();
            a.sb = !0
        }, this.Ui)
    }
    ;
    Rk.prototype.A = function() {
        this.$b();
        this.a.style.visibility = "hidden";
        this.sb = !1;
        Xk(this)
    }
    ;
    function Vk(a) {
        var b = new XMLHttpRequest;
        return new Promise(function(c, d) {
            b.onload = function() {
                lb(b.status) ? c(b) : d(new jb(b))
            }
            ;
            b.onerror = function() {
                d(new jb(b))
            }
            ;
            b.open("GET", "https://emote.highwebmedia.com/autocomplete?slug=" + a + "&_=" + Date.now());
            b.withCredentials = !1;
            b.timeout = 6E4;
            b.send()
        }
        )
    }
    var Wk = document.createElement("div");
    function al() {
        var a = document.createElement("div");
        a.style.width = "100%";
        a.style.boxSizing = "border-box";
        a.style.overflow = "auto";
        return a
    }
    function bl() {
        var a = document.createElement("div");
        a.style.width = "100%";
        yh(a);
        a.style.cursor = "text";
        return a
    }
    function cl() {
        var a = document.createElement("form");
        a.style.display = "inline-block";
        a.style.boxSizing = "border-box";
        return a
    }
    function dl() {
        var a = document.createElement("div");
        a.style.height = "27px";
        a.style.backgroundColor = "#ffffff";
        a.style.boxSizing = "border-box";
        a.style.position = "absolute";
        a.style.bottom = "0";
        a.style.fontSize = "12px";
        return a
    }
    function el() {
        var a = document.createElement("input");
        a.style.outline = "none";
        a.style.border = "none";
        a.style.margin = "6px";
        a.style.boxSizing = "border-box";
        a.style.fontFamily = "Helvetica, Arial, sans-serif";
        a.autocomplete = "off";
        return a
    }
    function fl() {
        var a = document.createElement("span");
        a.innerText = Fd;
        a.style.fontSize = "12px";
        a.style.color = "#ffffff";
        a.style.padding = "5px 8px 4px";
        a.style.marginRight = "6px";
        a.style.backgroundColor = "#eb3404";
        La() && (a.style.background = "linear-gradient(#f66d51, #eb3404)");
        a.style.borderRadius = "4px";
        a.style.boxSizing = "border-box";
        a.style.cursor = "pointer";
        a.style.display = "inline-block";
        return a
    }
    function gl(a) {
        u(lh, {});
        a.stopPropagation();
        a.preventDefault()
    }
    function hl() {
        var a = document.createElement("span");
        a.innerText = Gd;
        a.style.fontSize = "12px";
        a.style.color = "#ffffff";
        a.style.padding = "5px 8px 4px";
        a.style.marginRight = "2px";
        a.style.backgroundColor = "#53843a";
        La() && (a.style.background = "linear-gradient(#87c667, #53843a)");
        a.style.borderRadius = "4px";
        a.style.boxSizing = "border-box";
        a.style.cursor = "pointer";
        a.style.display = "inline-block";
        y("mousedown", a, function(a) {
            a.stopPropagation()
        });
        y("click", a, gl);
        q(S, function(b) {
            a.style.display = !0 === b.h.hb ? "inline-block" : "none"
        });
        return a
    }
    function il() {
        var a = document.createElement("img");
        a.width = 15;
        a.src = "https://ssl-ccstatic.highwebmedia.com/theatermodeassets/ico-smile.gif";
        a.style.cursor = "pointer";
        a.style.display = "inline-block";
        a.style.marginRight = "6px";
        a.style.verticalAlign = "text-bottom";
        return a
    }
    function jl(a) {
        v.call(this);
        var b = this;
        this.$c = a;
        this.nh = new n("addMessageHtml",{
            Xd: !1
        });
        this.Zl = new n("removeMessageHtml",{
            Xd: !1
        });
        this.xa = bl();
        this.Oa = al();
        this.Dn = new n("formSubmitted");
        this.ud = 0;
        this.Ti = new Map;
        this.Fa = new Aa;
        this.a.style.fontSize = "12px";
        this.a.style.overflow = "";
        this.Oa.appendChild(this.xa);
        this.a.appendChild(this.Oa);
        var c = cl();
        this.aa = dl();
        this.K = el();
        this.Ia = il();
        this.jb = fl();
        this.hf = hl();
        c.appendChild(this.K);
        this.aa.appendChild(c);
        this.aa.appendChild(this.Ia);
        this.aa.appendChild(this.jb);
        this.Ia.onload = function() {
            b.Y()
        }
        ;
        y("mousedown", this.aa, function(a) {
            if (a.target === b.aa || a.target === c)
                b.K.focus(),
                a.preventDefault()
        });
        y("submit", c, function(a) {
            a.preventDefault();
            b.di()
        });
        y("click", this.jb, function(a) {
            a.stopPropagation();
            b.di()
        });
        y("click", this.Ia, function(a) {
            a.stopPropagation();
            u(Fj, b.Ia)
        });
        this.aa.appendChild(this.hf);
        this.a.appendChild(this.aa);
        this.Fa.add(q(S, function(a) {
            b.a.style.fontSize = a.h.qa.fontSize
        }));
        this.Fa.add(q(th, function(a) {
            b.a.style.fontSize = a.fontSize
        }));
        this.Ah = this.N(new Rk({
            Nb: this.K,
            lo: 2,
            fp: 167
        }))
    }
    m(jl, v);
    k = jl.prototype;
    k.zg = function() {
        Da(this.Fa);
        this.Ah.zg()
    }
    ;
    k.di = function() {
        if (!Ni('You must be logged in to send a message. Click "OK" to login.')) {
            this.pa();
            var a = this.K.value;
            this.K.value = "";
            if ("" !== a.trim()) {
                var b = Mk(a);
                void 0 !== b ? u(lh, b) : this.$c(a)
            }
            u(this.Dn, a)
        }
    }
    ;
    k.C = function() {
        var a = kl(this);
        ll(this);
        this.aa.style.width = this.a.clientWidth + "px";
        var b = this.aa.clientWidth - 18 - 6 - Math.ceil(this.hf.offsetWidth + this.jb.offsetWidth + this.Ia.offsetWidth) - 4;
        b = Math.max(0, b);
        this.K.style.width = b + "px";
        a || this.pa();
        this.Ah.a.style.bottom = this.aa.offsetHeight + "px"
    }
    ;
    function ll(a) {
        a.Oa.style.height = Math.max(0, a.a.clientHeight - 27) + "px"
    }
    function kl(a) {
        return a.Oa.scrollTop <= a.Oa.scrollHeight - (a.Oa.offsetHeight + 20)
    }
    k.pa = function() {
        this.Oa.scrollTop = this.Oa.scrollHeight;
        this.Oa.scrollLeft = 0
    }
    ;
    k.ta = function(a, b) {
        var c = this.Oa.scrollTop
          , d = kl(this);
        this.xa.appendChild(a);
        if (d)
            ll(this),
            this.Oa.scrollTop = c;
        else {
            for (c = this.xa.childElementCount - 1E3; 0 < c; --c)
                d = this.xa.firstElementChild,
                null !== d && this.xa.removeChild(d);
            ll(this);
            this.pa()
        }
        void 0 !== b && this.Ti.set(a, b);
        u(this.nh, {
            zl: function() {
                return void 0 !== b ? b() : a.cloneNode(!0)
            }
        });
        this.ud += 1;
        return a
    }
    ;
    k.Uj = function(a) {
        this.Ti["delete"](a);
        this.xa.removeChild(a)
    }
    ;
    k.Bh = function() {
        return this.ud
    }
    ;
    k.Aj = function(a) {
        return this.ud - a
    }
    ;
    function ml(a) {
        for (var b = {}, c = l(a.xa.childNodes), d = c.next(); !d.done; b = {
            Ki: b.Ki
        },
        d = c.next())
            b.Ki = d.value,
            u(a.nh, {
                zl: function(b) {
                    return function() {
                        var c = a.Ti.get(b.Ki);
                        return void 0 !== c ? c() : b.Ki.cloneNode(!0)
                    }
                }(b),
                nr: !1
            })
    }
    k.Zb = function() {
        this.K.focus()
    }
    ;
    k.Yl = function(a) {
        u(this.Zl, a)
    }
    ;
    k.A = function() {
        this.xa.style.opacity = "0"
    }
    ;
    k.show = function() {
        this.xa.style.opacity = "1"
    }
    ;
    k.clear = function() {
        for (this.ud = 0; null !== this.xa.firstChild; )
            this.xa.removeChild(this.xa.firstChild);
        this.Ti.clear()
    }
    ;
    function nl(a) {
        if (void 0 === a)
            return ol();
        a = {
            name: H(a, "name", !1),
            Cr: H(a, "template", !1),
            tn: I(a, "row1_label", !1),
            mp: I(a, "row2_label", !1),
            Jp: I(a, "row3_label", !1),
            un: I(a, "row1_value", !1),
            np: I(a, "row2_value", !1),
            Kp: I(a, "row3_value", !1)
        };
        return ol(a)
    }
    function ol(a) {
        var b = document.createElement("tr")
          , c = document.createElement("tr")
          , d = document.createElement("tr");
        if (void 0 !== a) {
            var e = a.Cr
              , f = []
              , h = [];
            if ("" !== a.tn) {
                var g = document.createElement("th");
                g.style.padding = "2px";
                g.innerText = a.tn;
                f.push(g);
                b.appendChild(g);
                g = document.createElement("td");
                g.innerText = a.un;
                b.appendChild(g);
                h.push(g);
                g.style.width = "50%";
                g.style.paddingLeft = "10px"
            } else
                g = document.createElement("td"),
                g.innerText = a.un,
                g.colSpan = 2,
                h.push(g),
                b.appendChild(g),
                g.style.textAlign = "center";
            "" !== a.mp ? (g = document.createElement("th"),
            g.style.padding = "2px",
            g.style.fontWeight = "bold",
            g.innerText = a.mp,
            f.push(g),
            c.appendChild(g),
            g = document.createElement("td"),
            g.innerText = a.np,
            h.push(g),
            c.appendChild(g),
            g.style.width = "50%",
            g.style.paddingLeft = "10px") : (g = document.createElement("td"),
            g.innerText = a.np,
            g.colSpan = 2,
            h.push(g),
            c.appendChild(g),
            g.style.textAlign = "center");
            "" !== a.Jp ? (g = document.createElement("th"),
            g.style.padding = "2px",
            g.innerText = a.Jp,
            f.push(g),
            d.appendChild(g),
            g = document.createElement("td"),
            g.innerText = a.Kp,
            h.push(g),
            d.appendChild(g),
            g.style.width = "50%",
            g.style.paddingLeft = "10px") : (g = document.createElement("td"),
            g.innerText = a.Kp,
            g.colSpan = 2,
            h.push(g),
            d.appendChild(g),
            g.style.textAlign = "center");
            a = l(f);
            for (g = a.next(); !g.done; g = a.next())
                g = g.value,
                g.style.overflow = "hidden",
                g.style.display = "block",
                g.style.cssFloat = "right",
                g.style.color = "#494949";
            a = l(h);
            for (g = a.next(); !g.done; g = a.next())
                g.value.style.color = "#494949";
            if ("3_rows_of_labels" === e)
                f[0].style.display = "block",
                f[0].style.color = "#057205",
                f[0].style.margin = "0px auto",
                f[0].style.height = "16px",
                f[0].style.fontWeight = "bold";
            else if ("3_rows_11_21_31" === e || "3_rows_11_22_32" === e)
                h[0].style.color = "#0b5d81",
                h[0].style.fontWeight = "bold"
        } else
            e = document.createElement("td"),
            b.appendChild(e.cloneNode()),
            c.appendChild(e.cloneNode()),
            d.appendChild(e.cloneNode());
        b.style.backgroundColor = "#d5ebf8";
        b.style.height = "33.3333%";
        c.style.backgroundColor = "#f2f9fd";
        c.style.height = "33.3333%";
        d.style.backgroundColor = "#d5ebf8";
        d.style.height = "33.3333%";
        e = document.createElement("table");
        e.style.width = "100%";
        e.style.height = "100%";
        e.style.borderCollapse = "collapse";
        e.style.margin = "0 auto";
        e.style.color = "#000000";
        e.appendChild(b);
        e.appendChild(c);
        e.appendChild(d);
        return e
    }
    ;function pl(a) {
        a = new D(a);
        return {
            color: I(a, "color", !1),
            filename: I(a, "filename", !1),
            fontFamily: I(a, "font-family", !1),
            fontSize: I(a, "font-size", !1),
            fontStyle: I(a, "font-style", !1),
            fontWeight: I(a, "font-weight", !1),
            height: I(a, "height", !1),
            oj: I(a, "image_url", !1),
            ko: I(a, "type", !1),
            left: I(a, "left", !1),
            maxWidth: I(a, "max-width", !1),
            opacity: I(a, "opacity", !1),
            text: I(a, "text", !1),
            textAlign: I(a, "text-align", !1),
            top: I(a, "top", !1),
            width: I(a, "width", !1)
        }
    }
    function ql(a) {
        var b = new D(a);
        a = {
            backgroundColor: I(b, "background-color", !1),
            color: I(b, "color", !1),
            textAlign: I(b, "text-align", !1),
            fontStyle: I(b, "font-style", !1),
            fontWeight: I(b, "font-weight", !1)
        };
        var c = pb(b, "col_1", !1);
        "" !== c && (a.Z = rl(c));
        b = pb(b, "col_2", !1);
        "" !== b && (a.data = rl(b));
        return a
    }
    function rl(a) {
        a = new D(a);
        return {
            backgroundColor: I(a, "background-color", !1),
            color: I(a, "color", !1),
            left: I(a, "left", !1),
            textAlign: I(a, "text-align", !1),
            fontStyle: I(a, "font-style", !1),
            fontWeight: I(a, "font-weight", !1),
            value: I(a, "value", !1),
            width: I(a, "width", !1)
        }
    }
    function sl(a) {
        var b = document.createElement("span");
        b.style.position = "absolute";
        b.style.whiteSpace = "nowrap";
        b.style.overflow = "hidden";
        b.style.textOverflow = "ellipsis";
        b.style.lineHeight = "normal";
        b.style.color = a.color;
        b.style.fontFamily = a.fontFamily;
        b.style.fontSize = a.fontSize;
        b.style.fontStyle = a.fontStyle;
        b.style.fontWeight = a.fontWeight;
        b.style.left = a.left;
        b.style.maxWidth = a.maxWidth;
        b.innerText = a.text;
        b.style.textAlign = a.textAlign;
        b.style.top = a.top;
        b.style.width = a.width;
        return b
    }
    function tl(a) {
        var b = document.createElement("img");
        b.style.position = "absolute";
        b.alt = a.filename;
        b.style.height = a.height;
        b.src = a.oj;
        b.style.left = a.left;
        b.style.top = a.top;
        b.style.width = a.width;
        return b
    }
    function ul(a) {
        var b = document.createElement("div");
        b.style.height = "23px";
        b.style.lineHeight = "23px";
        b.style.width = "270px";
        if (void 0 !== a) {
            b.style.backgroundColor = a.backgroundColor;
            b.style.color = a.color;
            b.style.fontStyle = a.fontStyle;
            b.style.fontWeight = a.fontWeight;
            b.style.textAlign = a.textAlign;
            if (void 0 !== a.Z) {
                var c = vl(a.Z);
                b.appendChild(c)
            }
            void 0 !== a.data && (a = vl(a.data),
            b.appendChild(a))
        }
        return b
    }
    function vl(a) {
        var b = document.createElement("div");
        b.style.position = "absolute";
        b.style.whiteSpace = "nowrap";
        b.style.overflow = "hidden";
        b.style.textOverflow = "ellipsis";
        b.style.position = "absolute";
        b.style.backgroundColor = a.backgroundColor;
        b.style.color = a.color;
        b.style.fontStyle = a.fontStyle;
        b.style.fontWeight = a.fontWeight;
        b.style.textAlign = a.textAlign;
        b.innerText = a.value;
        b.style.width = a.width;
        b.style.left = a.left;
        return b
    }
    ;function wl() {
        v.call(this);
        var a = this;
        this.Mj = new n("panelUpdated");
        this.Ne = !1;
        this.a.style.display = "inline-block";
        this.a.style.paddingLeft = "5px";
        this.a.style.paddingTop = "3px";
        this.a.style.paddingRight = "5px";
        this.a.style.paddingBottom = "3px";
        this.a.style.position = "";
        this.a.style.width = "270px";
        this.a.style.height = "69px";
        this.a.style.textShadow = "none";
        this.a.style.fontSize = "11px";
        this.a.style.lineHeight = "1.7em";
        q(S, function(b) {
            xl(a, b.j.i());
            q(b.j.event.Rj, function() {
                xl(a, b.j.i())
            });
            q(b.j.event.Qi, function() {
                xl(a, b.j.i())
            })
        });
        this.wf = nl();
        this.a.appendChild(this.wf)
    }
    m(wl, v);
    function xl(a, b) {
        z("api/panel_context/" + b + "/").then(function(b) {
            0 < a.a.children.length && a.a.removeChild(a.wf);
            if ("" === b.responseText)
                a.Ne = !1,
                a.wf = nl();
            else {
                a.Ne = !0;
                b = new D(b.responseText);
                var c = H(b, "template", !1);
                switch (c) {
                case "image_template":
                    H(b, "name", !1);
                    H(b, "template", !1);
                    c = E(b, "layers");
                    if (void 0 !== c && c instanceof Array && 0 !== c.length) {
                        var e = [];
                        c = l(c);
                        for (var f = c.next(); !f.done; f = c.next())
                            e.push(pl(JSON.stringify(f.value)))
                    }
                    c = pb(b, "table", !1);
                    if (0 !== c.length) {
                        c = new D(c);
                        var h = {
                            backgroundColor: I(c, "background-color", !1),
                            color: I(c, "color", !1),
                            textAlign: I(c, "text-align", !1),
                            fontStyle: I(c, "font-style", !1),
                            fontWeight: I(c, "font-weight", !1)
                        };
                        f = pb(c, "row_1", !1);
                        "" !== f && (h.jr = ql(f));
                        f = pb(c, "row_2", !1);
                        "" !== f && (h.lr = ql(f));
                        c = pb(c, "row_3", !1);
                        "" !== c && (h.kr = ql(c))
                    }
                    c = document.createElement("div");
                    c.style.position = "absolute";
                    c.style.height = "69px";
                    c.style.width = "270px";
                    c.style.backgroundColor = "white";
                    c.style.overflow = "hidden";
                    c.style.font = "1em 'UbuntuRegular', Arial, Helvetica, sans-serif";
                    if (void 0 !== e)
                        for (e = l(e),
                        f = e.next(); !f.done; f = e.next())
                            f = f.value,
                            "text" === f.ko ? c.appendChild(sl(f)) : "image" === f.ko && c.appendChild(tl(f));
                    if (void 0 !== h) {
                        e = h;
                        h = document.createElement("div");
                        h.style.height = "69px";
                        h.style.width = "270px";
                        h.style.backgroundColor = e.backgroundColor;
                        h.style.color = e.color;
                        h.style.fontStyle = e.fontStyle;
                        h.style.fontWeight = e.fontWeight;
                        h.style.textAlign = e.textAlign;
                        e = l([e.jr, e.lr, e.kr]);
                        for (f = e.next(); !f.done; f = e.next())
                            f = f.value,
                            f = void 0 !== f ? ul(f) : ul(),
                            h.appendChild(f);
                        c.appendChild(h)
                    }
                    a.wf = c;
                    break;
                case "3_rows_11_21_31":
                case "3_rows_11_22_32":
                case "3_rows_12_21_31":
                case "3_rows_12_22_31":
                case "3_rows_of_labels":
                    a.wf = nl(b);
                    break;
                default:
                    a.Ne = !1,
                    a.wf = nl(),
                    r("template of type (" + c + "): is not supported")
                }
                L(b)
            }
            a.a.appendChild(a.wf);
            u(a.Mj, void 0)
        })["catch"](function(a) {
            w("Error fetching app panel (" + a + ")")
        })
    }
    ;function yl(a) {
        var b = this;
        this.F = a;
        this.Bb = new wl;
        this.Bb.a.style.width = "270px";
        this.Bb.a.style.position = "relative";
        this.Ok = this.Ne = !1;
        q(S, function() {
            b.Ne = !1;
            b.Ok = !1;
            b.Ei = void 0;
            b.Rc = void 0;
            setTimeout(function() {
                b.Ok = !0;
                b.Ne && zl(b)
            }, 2E3)
        });
        q(this.Bb.Mj, function() {
            b.Ne = !0;
            b.Ok && zl(b)
        })
    }
    function zl(a) {
        if (a.Bb.Ne) {
            if (void 0 !== a.Rc) {
                for (var b = 0; b < a.Rc.children.length; b += 1)
                    a.Rc.removeChild(a.Rc.children.item(b));
                a.Rc.appendChild(a.Bb.a.cloneNode(!0))
            }
            void 0 === a.Ei ? (a.F.ta(a.Bb.a, function() {
                return Al(a)
            }),
            a.Ei = a.F.Bh()) : (b = a.Bb.a.offsetTop - a.F.Fq(),
            (0 > b || b > a.F.Eq() - 30) && 20 < a.F.Aj(a.Ei) && (a.F.Uj(a.Bb.a),
            a.F.ta(a.Bb.a, function() {
                return Al(a)
            }),
            a.Ei = a.F.Bh()))
        }
    }
    function Al(a) {
        void 0 !== a.Rc && null !== a.Rc.parentNode && a.Rc.parentNode.removeChild(a.Rc);
        a.Rc = document.createElement("div");
        a.Rc.appendChild(a.Bb.a.cloneNode(!0));
        return a.Rc
    }
    ;function Bl() {
        W.call(this);
        var a = this;
        this.W = 0;
        this.a.id = "chat-box";
        this.a.style.overflow = "";
        this.w = new jl(function(b) {
            a.j.im(b)
        }
        );
        this.N(this.w);
        this.Bk = new yl({
            ta: function(b, c) {
                return a.w.ta(b, c)
            },
            Uj: function(b) {
                a.w.Uj(b)
            },
            Bh: function() {
                return a.w.Bh()
            },
            Aj: function(b) {
                return a.w.Aj(b)
            },
            Fq: function() {
                return a.w.Oa.scrollTop
            },
            Eq: function() {
                return a.w.a.offsetHeight
            }
        });
        q(S, function(b) {
            a.j = b.j;
            a.w.ta(Kk("Rules: No spamming. Do not insist the cam hosts to do as you please. Do not announce other rooms or websites that would conflict with this room. Avoid any argumentative and/or rude posts related to the cam viewing. Do not attempt to post your e-mail address in the public chat."));
            a.w.ta(Kk('To send a tip, press CTRL+S or type "/tip 25".'));
            0 < b.h.ug.length && a.w.ta(Lk(b.h.i, b.h.ug))
        });
        q(vh, function() {
            a.w.clear()
        })
    }
    m(Bl, W);
    k = Bl.prototype;
    k.C = function() {
        W.prototype.C.call(this)
    }
    ;
    k.pa = function() {
        this.w.pa()
    }
    ;
    k.Kf = function() {
        return 99 < this.W ? Ed + " (99+)" : 0 < this.W ? Ed + " (" + this.W + ")" : Ed
    }
    ;
    k.Me = function() {
        return "chat-tab-fvm"
    }
    ;
    k.Zb = function() {
        this.w.Zb()
    }
    ;
    k.Od = function() {}
    ;
    k.Qe = function() {
        return document.activeElement === this.w.K
    }
    ;
    function Cl(a) {
        Zj(a) || (a.W += 1);
        a.qc()
    }
    k.show = function() {
        W.prototype.show.call(this);
        this.W = 0;
        this.qc();
        this.w.pa();
        this.w.K.focus()
    }
    ;
    k.dj = function(a) {
        for (var b = [], c = l(this.w.xa.childNodes), d = c.next(); !d.done; d = c.next())
            d = d.value,
            d.getAttribute("data-nick") === a && b.push(d);
        a = l(b);
        for (b = a.next(); !b.done; b = a.next())
            this.w.xa.removeChild(b.value)
    }
    ;
    k.Jf = function() {
        return this.w.K.value
    }
    ;
    k.kg = function(a) {
        this.w.K.value = a
    }
    ;
    k.Le = function() {
        return this.w.K
    }
    ;
    function Dl() {
        this.Df = new zh;
        this.Bf = new zh
    }
    Dl.prototype.hd = function(a) {
        this.Bf.remove(a);
        this.Df.hd(a)
    }
    ;
    function El(a) {
        var b = a.Df.ld.shift();
        void 0 !== a.Ma && a.Bf.hd(a.Ma);
        return a.Ma = b
    }
    function Fl(a) {
        for (; ; ) {
            var b = a.Bf.ld.pop();
            if (void 0 === b)
                break;
            Ah(a.Df, b)
        }
        void 0 !== a.Ma && (Ah(a.Df, a.Ma),
        a.Ma = void 0)
    }
    Dl.prototype.remove = function(a) {
        this.Bf.remove(a);
        this.Df.remove(a);
        this.Ma === a && (this.Ma = void 0)
    }
    ;
    function Gl(a, b) {
        a.hd(b);
        El(a)
    }
    ;var Hl = new bk;
    function Il() {
        var a = void 0 === a ? "" : a;
        var b = document.createElement("div");
        b.style.fontFamily = "Tahoma,Arial,Helvetica,sans-serif";
        b.style.boxSizing = "border-box";
        b.style.width = "100%";
        b.style.paddingTop = "3px";
        b.style.paddingBottom = "3px";
        b.style.paddingLeft = "5px";
        b.style.paddingRight = "5px";
        b.style.fontSize = "12px";
        b.style.overflowX = "hidden";
        0 < a.length && b.setAttribute("data-nick", a);
        return b
    }
    function Jl(a, b) {
        return b.username === a ? "Broadcaster " : b.Dc ? "Moderator " : b.Bc ? "Fan club member " : ""
    }
    function Kl(a) {
        a = Hl.parse(a);
        a = new ck(a.red,a.green,a.blue);
        var b = a.brightness();
        dk(a, 255 - b / 3);
        return "rgb(" + a.red + ", " + a.green + ", " + a.blue + ")"
    }
    ;function Ll(a) {
        var b = document.createElement("div")
          , c = document.createElement("div")
          , d = document.createElement("div")
          , e = document.createElement("img");
        e.src = "https://ssl-ccstatic.highwebmedia.com/theatermodeassets/icon-close-off.png";
        e.height = 16;
        e.width = 16;
        d.style.padding = "5px 5px 0 10px";
        d.style.verticalAlign = "top";
        d.style.display = "inline-block";
        d.style.overflow = "hidden";
        c.innerText = a.jg.hc + " " + (0 === a.jg.W ? "" : "(" + a.jg.W + ")");
        c.style.whiteSpace = "nowrap";
        c.style.textOverflow = "ellipsis";
        c.style.overflow = "hidden";
        c.style.padding = "5px 5px 5px 15px";
        c.style.display = "inline-block";
        c.style.width = a.size - 16 - 5 - 10 - 20 + "px";
        b.style.color = "#423DB0";
        b.style.width = a.size + "px";
        b.onmouseenter = function() {
            e.src = "https://ssl-ccstatic.highwebmedia.com/theatermodeassets/icon-close-on.png";
            b.style.background = "#7E7E7E";
            b.style.cursor = "pointer";
            b.style.color = "#e1e1e1"
        }
        ;
        b.onmouseleave = function() {
            e.src = "https://ssl-ccstatic.highwebmedia.com/theatermodeassets/icon-close-off.png";
            b.style.background = "#e1e1e1";
            b.style.cursor = "default";
            b.style.color = "#423DB0"
        }
        ;
        y("click", c, function() {
            a.Il()
        });
        y("click", e, function() {
            a.Jl()
        });
        b.appendChild(c);
        d.appendChild(e);
        b.appendChild(d);
        return b
    }
    function Ml(a, b) {
        W.call(this);
        var c = this;
        this.tl = b;
        this.W = 0;
        this.Yh = new n("incomingPm");
        this.Ca = new Map;
        this.va = new Dl;
        this.a.style.overflow = "";
        this.Pi = a;
        q(Gj, function(a) {
            c.Pc();
            void 0 === c.Ca.get(a) && c.Jh(a, !1);
            Gl(c.va, a);
            c.qc();
            c.ve();
            Yj(c)
        });
        q(S, function(a) {
            c.j = a.j
        });
        q(vh, function() {
            for (var a = l(c.Ca.keys()), b = a.next(); !b.done; b = a.next())
                c.nd(b.value)
        });
        q(Hj, function(a) {
            c.nd(a)
        })
    }
    m(Ml, W);
    k = Ml.prototype;
    k.pa = function() {
        var a = Y(this);
        void 0 !== a && a.w.pa()
    }
    ;
    k.Kf = function() {
        return 99 < this.W ? "PM (99+)" : 0 < this.W ? "PM (" + this.W + ")" : "PM"
    }
    ;
    k.bh = function(a) {
        var b = this;
        N("FocusPMTab", {
            unread: this.W
        });
        if (1 === this.Ca.size)
            Yj(this),
            this.Pc(),
            Gl(this.va, this.Ca.keys().next().value),
            this.ve();
        else {
            var c = document.createElement("div")
              , d = document.createElement("div")
              , e = function() {
                b.Pi.a.removeChild(c);
                b.Pi.a.removeChild(d);
                S.removeListener(e)
            };
            q(S, e, !1);
            var f = document.createElement("div")
              , h = document.createElement("span")
              , g = document.createElement("img");
            g.src = "https://ssl-ccstatic.highwebmedia.com/theatermodeassets/red-arrow-down.gif";
            g.height = 6;
            g.style.padding = "0 0 2px 4px";
            h.innerText = Nd;
            h.style.padding = "0 5px 0 5px";
            h.style.whiteSpace = "nowrap";
            h.style.color = R.Ba;
            f.style.minWidth = "150px";
            f.appendChild(g);
            f.appendChild(h);
            h = document.createElement("hr");
            h.style.margin = "4px";
            c.appendChild(f);
            c.appendChild(h);
            c.style.border = "1px solid #6B6B6B";
            c.style.fontSize = "12px";
            c.style.position = "relative";
            c.style.background = "#e1e1e1";
            c.style.borderRadius = "4px";
            c.style.display = "none";
            c.style.padding = "6px 0 6px 0";
            c.style.maxHeight = "300px";
            c.style.overflow = "auto";
            f = [];
            h = l(this.Ca.keys());
            for (g = h.next(); !g.done; g = h.next())
                f.push(g.value);
            h = f.sort();
            f = {};
            h = l(h);
            for (g = h.next(); !g.done; f = {
                Ed: f.Ed
            },
            g = h.next())
                f.Ed = g.value,
                g = this.Ca.get(f.Ed),
                void 0 === g ? w("can't find pmsession") : c.appendChild(Ll({
                    jg: g,
                    Jl: function(a) {
                        return function() {
                            b.nd(a.Ed);
                            e()
                        }
                    }(f),
                    Il: function(a) {
                        return function() {
                            Yj(b);
                            b.Pc();
                            Gl(b.va, a.Ed);
                            b.ve();
                            e()
                        }
                    }(f),
                    size: 250
                }));
            a = a.getBoundingClientRect();
            c.style.top = "-24px";
            c.style.left = "40px";
            c.style.width = "252px";
            c.style.zIndex = "999";
            this.Pi.a.appendChild(c);
            c.style.display = "block";
            d.style.position = "fixed";
            d.style.height = "100%";
            d.style.width = "100%";
            d.style.top = "0";
            d.style.left = "0";
            d.style.zIndex = "998";
            d.onclick = e;
            this.Pi.a.insertBefore(d, c);
            window.document.documentElement.clientHeight < c.offsetTop + c.offsetHeight && (c.style.height = window.document.documentElement.clientHeight - a.top - 18 + "px");
            window.document.documentElement.clientWidth < c.offsetLeft + c.offsetWidth + 8 && (c.style.left = window.document.documentElement.clientWidth - c.offsetWidth - 8 + "px")
        }
    }
    ;
    k.Jg = function() {
        return 0 === this.Ca.size
    }
    ;
    k.Me = function() {
        return "pm-tab-fvm"
    }
    ;
    k.Zb = function() {
        var a = Y(this);
        void 0 === a ? w("session undefined") : a.w.Zb()
    }
    ;
    k.Od = function() {}
    ;
    k.Qe = function() {
        var a = Y(this);
        return void 0 === a ? (w("session undefined"),
        !1) : document.activeElement === a.w.K
    }
    ;
    k.C = function() {
        W.prototype.C.call(this);
        var a = Y(this);
        void 0 !== a && a.w.pa()
    }
    ;
    k.Jh = function(a, b) {
        var c = this
          , d = this.Ca.get(a);
        if (void 0 === d) {
            var e = Il();
            e.innerHTML = Nb(a);
            e.style.textAlign = "center";
            e.style.width = "auto";
            e.style.borderBottom = "1px solid #acacac";
            e.style.margin = "0 10px 5px 5px";
            e.style.display = "none";
            e.style.color = "#cfcfcf";
            d = {
                w: new jl(function(b) {
                    N("SendPrivateMessage", {
                        username: a
                    });
                    c.va.hd(a);
                    c.j.hm(b, a)
                }
                ),
                W: 0,
                hc: a,
                th: e
            };
            this.Ca.set(a, d);
            d.w.ta(Kk("Private conversation with " + a));
            d.w.ta(Kk(Pb()));
            d.w.a.style.display = "none";
            this.N(d.w);
            e = !0
        } else
            e = !1,
            d.th.style.display = "";
        !0 === b && u(this.Yh, {
            username: a,
            tj: e
        });
        return d
    }
    ;
    k.ta = function(a, b, c) {
        this.va.hd(c);
        var d = this.Jh(c, b !== this.j.username());
        this.j.username() === b || Zj(this) && this.va.Ma === c ? Gl(this.va, c) : (this.W += 1,
        d.W += 1,
        this.qc());
        return a = d.w.ta(a)
    }
    ;
    k.Pl = function(a) {
        if (Zj(this)) {
            var b = Y(this);
            void 0 !== b && b.w.ta(a)
        }
    }
    ;
    function Y(a) {
        if (void 0 !== a.va.Ma)
            return a.Ca.get(a.va.Ma)
    }
    function Nl(a) {
        a.Pc();
        if (void 0 === El(a.va))
            return !1;
        a.ve();
        return !0
    }
    function Ol(a) {
        a.Pc();
        var b = a.va
          , c = b.Bf.ld.shift();
        void 0 !== b.Ma && b.Df.hd(b.Ma);
        b.Ma = c;
        if (void 0 === c)
            return !1;
        a.ve();
        return !0
    }
    k.nd = function(a) {
        var b = this.Ca.get(a);
        if (void 0 !== b) {
            var c = Zj(this) && this.va.Ma === a;
            this.va.remove(a);
            b.w.zg();
            this.removeChild(b.w);
            this.Ca["delete"](a);
            c && (Nl(this) || this.tl());
            this.W -= b.W;
            this.qc();
            u(Hj, b.hc)
        }
    }
    ;
    k.zf = function() {
        var a = Y(this);
        void 0 === a ? w("cannot close current tab") : this.nd(a.hc)
    }
    ;
    k.ve = function() {
        if (void 0 === this.va.Ma)
            w("no currentpmsession");
        else {
            var a = this.Ca.get(this.va.Ma);
            void 0 === a ? w("no pm session") : (a.w.a.style.display = "block",
            a.w.Y(),
            a.w.K.focus(),
            0 < a.W && (this.W -= a.W,
            a.W = 0,
            this.qc()),
            u(wh, a.hc),
            a.w.ta(a.th))
        }
    }
    ;
    k.Pc = function() {
        var a = Y(this);
        void 0 !== a && (a.w.a.style.display = "none")
    }
    ;
    k.Jf = function() {
        var a = Y(this);
        return void 0 === a ? "" : a.w.K.value
    }
    ;
    k.kg = function(a) {
        var b = Y(this);
        void 0 !== b && (b.w.K.value = a)
    }
    ;
    k.Le = function() {
        var a = Y(this);
        if (void 0 !== a)
            return a.w.K
    }
    ;
    var Pl = new Set;
    function Ql() {
        for (var a = [], b = l(Pl), c = b.next(); !c.done; c = b.next())
            a.push(c.value);
        x() && window.localStorage.setItem("ignorelist", JSON.stringify({
            ye: a
        }))
    }
    function Rl(a) {
        try {
            Pl = new Set(JSON.parse(a).users)
        } catch (b) {
            w("Cannot parse ignore list: " + b)
        }
    }
    function Sl() {
        Tl();
        (0 === Pl.size || Ul()) && z("api/ignored_user_list/").then(function(a) {
            x() && (window.localStorage.setItem("ignorelist", a.responseText),
            window.localStorage.setItem("ignorelistlastfetch", Date.now().toString()));
            Rl(a.responseText)
        })["catch"](function(a) {
            w("Network error retrieving ignore list: " + a)
        })
    }
    function Ul() {
        if (!x())
            return !0;
        var a = window.localStorage.getItem("ignorelistlastfetch");
        return null === a ? !0 : parseInt(a, 10) + 3E5 < Date.now()
    }
    function Tl() {
        if (x()) {
            var a = window.localStorage.getItem("ignorelist");
            null !== a && Rl(a)
        }
    }
    ;var Vl;
    q(S, function(a) {
        Vl = a.j
    });
    function Wl() {
        rk.call(this);
        var a = this;
        this.a.style.visibility = "hidden";
        this.a.style.position = "fixed";
        this.a.style.width = "410px";
        this.a.style.height = "307px";
        this.a.style.backgroundColor = "#ffffff";
        this.a.style.borderRadius = "6px";
        this.a.style.padding = "10px";
        this.a.style.fontSize = "12px";
        Ka() && (this.a.style.boxShadow = "0 0 18px rgba(0, 0, 0, 0.4)");
        this.O.style.backgroundColor = "#000000";
        this.O.style.opacity = "0.4";
        var b = document.createElement("img");
        b.src = "https://ssl-ccstatic.highwebmedia.com/theatermodeassets/close.svg";
        b.style.position = "absolute";
        b.style.width = "10px";
        b.style.top = "9px";
        b.style.right = "9px";
        b.style.cursor = "pointer";
        b.style.opacity = "0.5";
        b.onclick = function() {
            a.A()
        }
        ;
        b.onmouseenter = function() {
            b.style.opacity = "1"
        }
        ;
        b.onmouseleave = function() {
            b.style.opacity = "0.5"
        }
        ;
        this.a.appendChild(b);
        var c = document.createElement("div");
        c.innerText = Pf;
        c.style.color = "#0C6A93";
        c.style.fontSize = "15px";
        c.style.fontFamily = "UbuntuMedium, Helvetica, Arial, sans-serif";
        this.a.appendChild(c);
        c = document.createElement("div");
        c.innerText = Uf;
        c.style.color = R.Kd;
        c.style.margin = "10px 0";
        this.a.appendChild(c);
        c = document.createElement("div");
        c.style.display = "inline-block";
        c.style.marginRight = "10px";
        var d = document.createElement("div");
        d.innerText = Tf;
        d.style.color = "#0C6A93";
        d.style.fontFamily = "UbuntuRegular, Helvetica, Arial, sans-serif";
        d.style.fontWeight = "bold";
        c.appendChild(d);
        this.Db = document.createElement("select");
        this.Db.multiple = !0;
        this.Db.style.border = "1px solid #b1b1b1";
        this.Db.style.borderRadius = "4px";
        this.Db.style.marginTop = "4px";
        this.Db.style.width = "200px";
        this.Db.style.height = "200px";
        this.Db.style.padding = "2px";
        this.Db.onchange = function() {
            try {
                var b = a.Db.selectedOptions[0]
            } catch (f) {
                b = a.Db.options[a.Db.selectedIndex]
            }
            b.selected = !1;
            a.Kb.appendChild(b)
        }
        ;
        c.appendChild(this.Db);
        this.a.appendChild(c);
        c = document.createElement("div");
        c.style.display = "inline-block";
        d = document.createElement("div");
        d.innerText = Rf;
        d.style.color = "#0C6A93";
        d.style.fontFamily = "UbuntuRegular, Helvetica, Arial, sans-serif";
        d.style.fontWeight = "bold";
        c.appendChild(d);
        this.Kb = document.createElement("select");
        this.Kb.multiple = !0;
        this.Kb.style.border = "1px solid #b1b1b1";
        this.Kb.style.borderRadius = "4px";
        this.Kb.style.marginTop = "4px";
        this.Kb.style.width = "200px";
        this.Kb.style.height = "200px";
        this.Kb.style.padding = "2px";
        this.Kb.onchange = function() {
            try {
                var b = a.Kb.selectedOptions[0]
            } catch (f) {
                b = a.Kb.options[a.Kb.selectedIndex]
            }
            b.selected = !1;
            a.Db.appendChild(b)
        }
        ;
        c.appendChild(this.Kb);
        this.a.appendChild(c);
        c = document.createElement("div");
        c.style.textAlign = "right";
        d = document.createElement("span");
        d.innerText = $d;
        d.style.display = "inline-block";
        d.style.color = "#ffffff";
        d.style.backgroundColor = "#ff7002";
        La() && (d.style.background = "linear-gradient(180deg, #ff9735 0%, #ff9e36 50%, #ff7002 60%)");
        d.style.fontFamily = "UbuntuMedium, Helvetica, Arial, sans-serif";
        d.style.fontSize = "14px";
        d.style.textShadow = "#f18112 1px 1px 0px";
        d.style.padding = "5px 15px 5px 15px";
        d.style.marginTop = "10px";
        d.style.borderRadius = "4px";
        d.style.cursor = "pointer";
        d.onclick = function() {
            for (var b = l(a.Kb.options), c = b.next(); !c.done; c = b.next())
                Vl.qk(c.value.value);
            a.A()
        }
        ;
        c.appendChild(d);
        this.a.appendChild(c);
        q(this.bc, function() {
            a.A()
        })
    }
    m(Wl, rk);
    Wl.prototype.C = function() {
        this.a.style.left = (document.documentElement.clientWidth - this.a.offsetWidth) / 2 + "px";
        this.a.style.top = (document.documentElement.clientHeight - this.a.offsetHeight) / 2 + "px"
    }
    ;
    Wl.prototype.show = function() {
        for (; null !== this.Db.firstChild; )
            this.Db.removeChild(this.Db.firstChild);
        for (; null !== this.Kb.firstChild; )
            this.Kb.removeChild(this.Kb.firstChild);
        for (var a = l(Pl), b = a.next(); !b.done; b = a.next()) {
            b = b.value;
            var c = document.createElement("option");
            c.innerText = b;
            c.value = b;
            this.Db.appendChild(c)
        }
        this.a.style.visibility = "visible";
        this.tc();
        this.Y()
    }
    ;
    Wl.prototype.A = function() {
        this.$b();
        this.a.style.visibility = "hidden"
    }
    ;
    function Xl(a, b, c) {
        X.call(this);
        var d = this;
        this.wm = a;
        this.Mo = !1;
        this.No = new n("pickerImgLoaded");
        this.a.style.visibility = "hidden";
        this.a.style.width = "auto";
        this.a.style.height = "auto";
        y("keydown", this.a, function(a) {
            27 === a.keyCode && d.A()
        });
        q(this.bc, function() {
            d.A()
        });
        var e = document.createElement("canvas");
        e.style.cursor = "crosshair";
        e.style.border = "1px solid #000001";
        this.a.appendChild(e);
        this.Ua = document.createElement("input");
        this.Ua.type = "text";
        this.Ua.style.display = "block";
        this.Ua.style.border = "1px solid #000000";
        this.Ua.style.padding = "2px 4px";
        this.Ua.style.position = "relative";
        this.Ua.style.top = "-3px";
        this.Ua.style.fontSize = "12px";
        this.Ua.oninput = function() {
            c(d.Ua.value)
        }
        ;
        y("keydown", this.Ua, function(a) {
            13 === a.keyCode && (b(d.Ua.value),
            d.A())
        });
        this.a.appendChild(this.Ua);
        var f = document.createElement("img");
        "https://ssl-ccstatic.highwebmedia.com/theatermodeassets/colorPicker.png".startsWith("/") || f.setAttribute("crossorigin", "anonymous");
        f.src = "https://ssl-ccstatic.highwebmedia.com/theatermodeassets/colorPicker.png";
        f.onload = function() {
            e.width = f.width;
            e.height = f.height;
            e.onclick = function() {
                d.pj = d.Ua.value;
                b(d.Ua.value);
                d.A()
            }
            ;
            e.onmousemove = function(a) {
                var b = e.getContext("2d");
                if (null !== b && 0 < a.offsetX && 0 < a.offsetY && a.offsetX < e.clientWidth && a.offsetY < e.clientHeight) {
                    var f = b.getImageData(a.offsetX, a.offsetY, 1, 1).data;
                    a = f[0];
                    b = f[1];
                    f = f[2];
                    (255 < a || 255 < b || 255 < f || 0 > a || 0 > b || 0 > f) && w("Invalid rgb color value: r: " + a + ", g: " + b + ", b: " + f);
                    d.Ua.value = "#" + ("000000" + (a << 16 | b << 8 | f).toString(16)).slice(-6)
                } else
                    d.Ua.value = d.pj;
                c(d.Ua.value)
            }
            ;
            e.onmouseleave = function() {
                d.Ua.value = d.pj;
                c(d.Ua.value)
            }
            ;
            var a = e.getContext("2d");
            null !== a && a.drawImage(f, 0, 0, f.width, f.height);
            d.Ua.style.width = e.width - 8 + "px";
            d.Mo = !0;
            u(d.No, void 0)
        }
    }
    m(Xl, X);
    Xl.prototype.C = function() {
        this.a.style.left = this.wm.offsetLeft + "px";
        this.a.style.top = this.wm.offsetTop + this.wm.offsetHeight + 2 + "px"
    }
    ;
    Xl.prototype.show = function(a) {
        function b() {
            c.Ua.value = c.pj;
            c.a.style.visibility = "visible";
            c.tc();
            c.Y()
        }
        var c = this;
        this.pj = a;
        if (this.Mo)
            b();
        else
            this.No.once(b)
    }
    ;
    Xl.prototype.A = function() {
        this.$b();
        this.a.style.visibility = "hidden"
    }
    ;
    function Yl(a, b) {
        try {
            return new MouseEvent(a,b)
        } catch (d) {
            var c = document.createEvent("MouseEvent");
            c.initMouseEvent(a, void 0 !== b.bubbles ? b.bubbles : !1, void 0 !== b.cancelable ? b.cancelable : !1, window, 0, void 0 !== b.screenX ? b.screenX : 0, void 0 !== b.screenY ? b.screenY : 0, void 0 !== b.clientX ? b.clientX : 0, void 0 !== b.clientY ? b.clientY : 0, void 0 !== b.ctrlKey ? b.ctrlKey : !1, void 0 !== b.altKey ? b.altKey : !1, void 0 !== b.shiftKey ? b.shiftKey : !1, void 0 !== b.metaKey ? b.metaKey : !1, void 0 !== b.button ? b.button : 0, void 0 !== b.relatedTarget ? b.relatedTarget : null);
            return c
        }
    }
    ;function Zl(a) {
        v.call(this);
        var b = this;
        this.F = a;
        this.value = 0;
        this.wk = new n("valueChanged");
        this.vk = new n("valueChangeStart");
        this.uk = new n("valueChangeEnd");
        this.a.style.position = "relative";
        this.a.style.overflow = "visible";
        this.a.style.cursor = "pointer";
        this.a.style.width = void 0 !== a.Nd ? a.Nd + a.gb + "px" : "90%";
        this.a.style.height = a.gb + "px";
        this.xf = void 0 !== a.xf ? a.xf : 2 * Math.round(.25 * a.gb / 2);
        void 0 !== a.Gk ? (this.zc = document.createElement("img"),
        this.zc.src = a.Gk,
        this.zc.style.width = "100%",
        this.zc.style.height = "100%") : (this.zc = document.createElement("div"),
        this.zc.style.position = "absolute",
        this.zc.style.boxSizing = "border-box",
        void 0 !== a.pn ? this.zc.style.backgroundColor = a.pn : (this.zc.style.backgroundColor = "#333333",
        Ka() && (this.zc.style.backgroundColor = "rgba(255, 255, 255, 0.2)")),
        this.zc.style.top = .5 * (a.gb - this.xf) + "px",
        this.zc.style.height = this.xf + "px",
        this.zc.style.width = "100%");
        this.a.appendChild(this.zc);
        void 0 !== a.Gk ? (this.Sd = document.createElement("div"),
        this.Sd.style.visibility = "none") : (this.Sd = document.createElement("div"),
        this.Sd.style.position = "absolute",
        this.Sd.style.boxSizing = "border-box",
        this.Sd.style.backgroundColor = void 0 !== a.sn ? a.sn : "#ffffff",
        this.Sd.style.top = .5 * (a.gb - this.xf) + "px",
        this.Sd.style.height = this.xf + "px",
        this.a.appendChild(this.Sd));
        void 0 !== a.Nn ? (this.handle = document.createElement("img"),
        this.handle.style.position = "absolute",
        this.handle.style.top = "0",
        this.handle.src = a.Nn,
        this.handle.style.height = a.gb + "px",
        this.handle.style.width = a.gb + "px",
        this.handle.style.cursor = "grab",
        this.handle.style.left = "0px") : (this.handle = document.createElement("div"),
        this.handle.style.position = "absolute",
        this.handle.style.top = "0",
        this.handle.style.backgroundColor = void 0 !== a.Mn ? a.Mn : "#ffffff",
        this.handle.style.borderRadius = "50%",
        this.handle.style.height = a.gb + "px",
        this.handle.style.height = a.gb + "px",
        this.handle.style.width = a.gb + "px",
        this.handle.style.cursor = "grab");
        this.a.appendChild(this.handle);
        this.Gr = new ci(function() {
            u(b.wk, b.value)
        }
        ,{
            ph: 100,
            Rl: !0
        });
        $l(this, function(a) {
            am(b, a, a.clientX)
        }, "mousedown", "mousemove", "mouseup");
        $l(this, function(a) {
            if (1 === a.touches.length) {
                var c = a.touches.item(0);
                null !== c && am(b, a, c.clientX)
            }
        }, "touchstart", "touchmove", "touchend");
        y("mousedown", this.a, function(a) {
            am(b, a, a.clientX);
            b.handle.dispatchEvent(Yl(a.type, a))
        })
    }
    m(Zl, v);
    function $l(a, b, c, d, e) {
        y(c, a.handle, function(c) {
            function f(c) {
                u(a.uk, a.value);
                a.handle.style.cursor = "grab";
                document.body.style.cursor = "";
                c.preventDefault();
                cb(d, document, b, !0);
                cb(e, document, f, !0)
            }
            u(a.vk, a.value);
            a.handle.style.cursor = "";
            document.body.style.cursor = "grabbing";
            c.preventDefault();
            c.stopPropagation();
            y(d, document, b, !0);
            y(e, document, f, !0)
        })
    }
    function am(a, b, c) {
        var d = a.a.getBoundingClientRect().left;
        void 0 !== a.F.Nd ? bm(a, 100 / a.F.Nd * (Math.max(.5 * a.F.gb, Math.min(a.F.Nd + .5 * a.F.gb, c - d)) - .5 * a.F.gb)) : bm(a, 100 / a.a.offsetWidth * (Math.max(.5 * a.F.gb, Math.min(a.a.offsetWidth + .5 * a.F.gb, c - d)) - .5 * a.F.gb));
        di(a.Gr);
        b.preventDefault()
    }
    function bm(a, b) {
        void 0 !== a.F.Nd ? (a.handle.style.left = b * a.F.Nd / 100 + "px",
        a.Sd.style.width = b * a.F.Nd / 100 + "px") : (a.handle.style.left = b * (a.a.offsetWidth - a.F.gb) / 100 + "px",
        a.Sd.style.width = b * a.a.offsetWidth / 100 + "px");
        a.value = b
    }
    ;function cm(a) {
        switch (a) {
        case "orga":
            return 0;
        case "orgb":
            return 1;
        case "org":
            return 2;
        case "none":
            return 3;
        default:
            return 1
        }
    }
    function dm(a) {
        switch (a) {
        case "away":
            return "away";
        case "group":
            return "groupnotwatching";
        case "group_watching":
            return "groupwatching";
        case "group_requesting":
            return "grouprequesting";
        case "private":
            return "privatenotwatching";
        case "private_watching":
            return "privatewatching";
        case "private_spying":
            return "privatespying";
        case "private_requesting":
            return "privaterequesting";
        case "public":
            return "public";
        case "hidden":
            return "hidden";
        case "offline":
            return "offline";
        case "password protected":
            return "passwordprotected";
        default:
            return w("unknown room status -- " + a),
            "unknown"
        }
    }
    var em;
    function fm(a) {
        var b = [];
        if ("" !== a)
            try {
                JSON.parse(a).forEach(function(a) {
                    b.push({
                        Dk: a[0],
                        Um: a[1]
                    })
                })
            } catch (c) {
                w("Error parsing apps " + c.toString())
            }
        return b
    }
    function gm(a) {
        var b = [];
        if ("" !== a)
            try {
                b = JSON.parse(a).map(function(a) {
                    return {
                        text: a.text,
                        href: a.href
                    }
                })
            } catch (c) {
                w("Error parsing anchors " + c.toString())
            }
        return b
    }
    function hm(a) {
        return new Promise(function(b, c) {
            var d = new D(a);
            if (400 <= K(d, "status", !1))
                c("Bad response from server: " + a);
            else {
                var e = ob(d, "chat_settings")
                  , f = ob(d, "ads_zone_ids")
                  , h = ob(d, "satisfaction_score")
                  , g = H(d, "flash_host")
                  , p = eb();
                void 0 !== p.edge && (g = "edge" + p.edge + ".stream.highwebmedia.com");
                p = H(d, "wschat_host");
                var t = H(d, "broadcaster_username")
                  , C = H(d, "room_pass")
                  , B = H(d, "room_title")
                  , M = dm(H(d, "room_status"))
                  , F = J(d, "is_supporter")
                  , O = H(d, "broadcaster_gender")
                  , ca = H(d, "viewer_username")
                  , xa = H(d, "chat_username")
                  , Ba = encodeURI(H(d, "edge_auth"))
                  , Ca = H(d, "chat_password")
                  , wb = H(d, "viewer_gender")
                  , Qa = H(d, "hls_source")
                  , xb = J(d, "is_widescreen")
                  , Ra = fm(H(d, "apps_running"))
                  , Fa = J(d, "allow_private_shows")
                  , Rb = J(d, "allow_group_shows")
                  , yb = K(d, "private_show_price")
                  , fr = K(d, "private_min_minutes")
                  , gr = J(d, "allow_show_recordings")
                  , hr = K(d, "group_show_price")
                  , ir = K(d, "spy_private_show_price")
                  , jr = J(d, "low_satisfaction_score")
                  , kr = J(d, "is_age_verified")
                  , lr = H(d, "hidden_message")
                  , mr = J(d, "following")
                  , nr = J(d, "is_moderator")
                  , or = H(d, "recommender_hmac")
                  , pr = J(d, "broadcaster_on_new_chat")
                  , qr = K(d, "token_balance")
                  , rr = H(d, "server_name")
                  , sr = K(d, "num_followed")
                  , tr = K(d, "num_followed_online")
                  , ur = J(d, "has_studio")
                  , vr = J(d, "is_mobile")
                  , wr = qb(d, "ignored_emoticons")
                  , xr = J(d, "hide_satisfaction_score")
                  , yr = K(d, "tips_in_past_24_hours");
                var Sb = void 0 === Sb ? !0 : Sb;
                var Tb = E(d, "last_vote_in_past_24_hours");
                "number" !== typeof Tb ? (Sb && void 0 !== Tb && r("getNumberOrUndefined(last_vote_in_past_24_hours): " + Tb + " is wrong type", {
                    message: d.Hd
                }),
                Sb = void 0) : Sb = Tb;
                Tb = J(d, "last_vote_in_past_90_days_down");
                var Vd = qb(d, "dismissible_messages")
                  , Bj = [];
                Vd = l(Vd);
                for (var Ub = Vd.next(); !Ub.done; Ub = Vd.next())
                    Ub = Ub.value,
                    Bj.push({
                        id: Ub.id,
                        Rq: Ub.message_html
                    });
                e = {
                    host: p,
                    i: t,
                    em: C,
                    bb: B,
                    Xa: M,
                    Se: F,
                    Ad: O,
                    userName: ca,
                    rh: xa,
                    wq: Ba,
                    Nm: Ca,
                    Yp: wb,
                    xq: g,
                    Gh: Qa,
                    Ec: xb,
                    ug: Ra,
                    kd: Fa,
                    jd: Rb,
                    Wa: yb,
                    Ze: fr,
                    vf: gr,
                    Ta: hr,
                    wb: ir,
                    fj: jr,
                    hb: kr,
                    Fg: lr,
                    If: mr,
                    mc: nr,
                    Wo: or,
                    mq: pr,
                    sa: qr,
                    sr: rr,
                    Uq: sr,
                    Vq: tr,
                    Iq: ur,
                    Ig: vr,
                    nj: wr,
                    Tn: xr,
                    Lc: yr,
                    jo: Sb,
                    Oq: Tb,
                    yh: Bj,
                    zr: J(d, "show_mobile_site_banner_link"),
                    Bo: K(d, "num_users_required_for_group"),
                    Co: K(d, "num_users_waiting_for_group"),
                    Gj: K(d, "num_viewers"),
                    cf: {
                        qf: K(h, "up_votes", !1),
                        Je: K(h, "down_votes", !1),
                        xd: K(h, "percent", !1)
                    },
                    Dr: J(d, "tfa_enabled"),
                    Ai: {
                        header: H(f, "468x60"),
                        rightTop: H(f, "160x600,top"),
                        rightMiddle: H(f, "160x600,middle"),
                        rightBottom: H(f, "160x600,bottom"),
                        footerLeft: H(f, "300x250,left"),
                        footerMiddle: H(f, "300x250,centre"),
                        footerRight: H(f, "300x250,right")
                    },
                    qa: {
                        wn: H(e, "font_color"),
                        fontFamily: H(e, "font_family"),
                        fontSize: H(e, "font_size"),
                        Zg: J(e, "show_emoticons"),
                        Yi: H(e, "emoticon_autocomplete_delay"),
                        Un: H(e, "highest_token_color"),
                        gs: H(e, "sort_users_key"),
                        zd: cm(H(e, "room_entry_for")),
                        Bd: cm(H(e, "room_leave_for")),
                        fs: H(e, "silence_broadcasters"),
                        Zr: H(e, "ignored_users"),
                        Lr: H(e, "allowed_chat"),
                        Ae: parseInt(H(e, "v_tip_vol")),
                        Qr: parseInt(H(e, "b_tip_vol"))
                    }
                };
                f = G(d, "staff_links");
                void 0 !== f && (e.dk = {
                    sm: gm(f),
                    hq: J(d, "auto_load_admin_info"),
                    Qo: G(d, "previous_usernames"),
                    kn: G(d, "deletion_admin_notice")
                });
                L(d);
                b(e)
            }
        }
        )
    }
    function im(a) {
        void 0 !== em && (em.abort(),
        em = void 0);
        return new Promise(function(b, c) {
            var d = mb("api/chatvideocontext/" + a + "/");
            em = d[0];
            d[1].then(function(a) {
                b(hm(a.responseText))
            })["catch"](function(a) {
                404 === a.fd.status ? (U("HTTP 404 - Page Not Found\n\nIt's probably just a broken link, or perhaps a cancelled broadcaster."),
                a.fd.abort()) : c(a)
            })
        }
        )
    }
    ;function jm() {
        function a() {
            if (!Ni('You must be logged in to change chat settings. Click "OK" to login.')) {
                var a = {
                    color: B.value
                };
                A("choose_viewer_chat_color/", a)["catch"](function(a) {
                    w("Error saving user chat color: " + a)
                });
                a.font_family = decodeURIComponent(M.value);
                a.font_size = ca.value;
                a.show_emoticons = "" + xa.checked;
                a.emoticon_autocomplete_delay = Ba.value;
                a.highest_token_color = Ca.value;
                a.viewer_tip_volume = "" + Math.round(Fa.value);
                f.mc && (a.enter_notify = Qa.value,
                a.leave_notify = Ra.value,
                f.qa.zd = cm(Qa.value),
                f.qa.Bd = cm(Ra.value));
                A("api/viewerchatsettings/" + f.fa + "/", a).then(function() {
                    u(f.Rk, void 0);
                    f.qa.wn = B.value;
                    f.qa.fontFamily = decodeURIComponent(M.value);
                    f.qa.fontSize = ca.value;
                    f.qa.Zg = xa.checked;
                    f.qa.Yi = Ba.value;
                    f.qa.Un = Ca.value;
                    f.qa.Ae = Math.round(Fa.value);
                    u(th, f.qa)
                })["catch"](function(a) {
                    w("Error saving user chat settings: " + a)
                })
            }
        }
        function b(a) {
            Rb.innerText = Math.round(a) + "%"
        }
        function c() {
            Ni('You must be logged in to change chat color. Click "OK" to login.') || (f.Se ? (void 0 === C && (C = f.N(new Xl(t,function(b) {
                d(b);
                a()
            }
            ,function(a) {
                d(a)
            }
            ))),
            C.show(B.value)) : U("You must be a supporter to change chat color"))
        }
        function d(a) {
            B.value = a;
            t.style.color = a;
            t.style.backgroundColor = a
        }
        function e() {
            var a = document.createElement("div");
            a.style.marginBottom = "16px";
            h.appendChild(a);
            var b = document.createElement("label");
            a.appendChild(b);
            return b
        }
        v.call(this);
        var f = this;
        this.Rk = new n("chatSettingsSavedRequest");
        this.Rp = new n("updateTipVolume");
        this.a.style.width = "100%";
        this.a.style.overflow = "auto";
        this.a.style.boxSizing = "border-box";
        this.a.style.padding = "11px";
        var h = document.createElement("form");
        h.style.marginBottom = "40px";
        this.a.appendChild(h);
        var g = e()
          , p = document.createElement("span");
        p.innerText = Vb + ":";
        p.style.marginRight = "6px";
        g.appendChild(p);
        var t = document.createElement("span");
        t.style.display = "inline-block";
        t.style.width = "20px";
        t.style.height = "20px";
        t.style.border = "1px solid " + R.Ba;
        t.style.cursor = "pointer";
        t.style.verticalAlign = "middle";
        g.appendChild(t);
        var C;
        t.onclick = c;
        p.onclick = c;
        var B = document.createElement("input");
        B.type = "hidden";
        B.onchange = function() {
            a()
        }
        ;
        g.appendChild(B);
        g = e();
        p = document.createElement("span");
        p.innerText = Wb + ":";
        p.style.marginRight = "6px";
        g.appendChild(p);
        var M = document.createElement("select");
        M.style.border = "1px solid #b1b1b1";
        p = l(km);
        for (var F = p.next(); !F.done; F = p.next()) {
            F = F.value;
            var O = document.createElement("option");
            O.innerText = F.label;
            O.value = F.value;
            M.appendChild(O)
        }
        y("mousedown", M, function(a) {
            Ni('You must be logged in to change font family. Click "OK" to login.') ? (a.preventDefault(),
            M.blur()) : f.Se || (a.preventDefault(),
            M.blur(),
            U("You must be a supporter to change font family"))
        });
        M.onchange = function() {
            a()
        }
        ;
        g.appendChild(M);
        g = e();
        p = document.createElement("span");
        p.innerText = Xb + ":";
        p.style.marginRight = "6px";
        g.appendChild(p);
        var ca = document.createElement("select");
        ca.style.border = "1px solid #b1b1b1";
        for (p = 9; 20 >= p; p += 1)
            F = document.createElement("option"),
            F.innerText = p + "pt",
            F.value = p + "pt",
            ca.appendChild(F);
        ca.onchange = function() {
            a()
        }
        ;
        g.appendChild(ca);
        g = e();
        p = document.createElement("span");
        p.innerText = Yb + ":";
        p.style.marginRight = "6px";
        g.appendChild(p);
        var xa = document.createElement("input");
        xa.type = "checkbox";
        xa.onchange = function() {
            a()
        }
        ;
        g.appendChild(xa);
        g = e();
        p = document.createElement("span");
        p.innerText = Zb + ":";
        p.style.marginRight = "6px";
        g.appendChild(p);
        var Ba = document.createElement("select");
        Ba.style.border = "1px solid #b1b1b1";
        p = l(lm);
        for (F = p.next(); !F.done; F = p.next())
            F = F.value,
            O = document.createElement("option"),
            O.innerText = F.label,
            O.value = F.value,
            Ba.appendChild(O);
        Ba.onchange = function() {
            a()
        }
        ;
        g.appendChild(Ba);
        g = e();
        p = document.createElement("span");
        p.innerText = $b + ":";
        p.style.marginRight = "6px";
        g.appendChild(p);
        var Ca = document.createElement("select");
        Ca.style.border = "1px solid #b1b1b1";
        p = l(mm);
        for (F = p.next(); !F.done; F = p.next())
            F = F.value,
            O = document.createElement("option"),
            O.innerText = F.label,
            O.value = F.value,
            Ca.appendChild(O);
        Ca.onchange = function() {
            a()
        }
        ;
        g.appendChild(Ca);
        var wb = e();
        wb.style.display = "none";
        g = document.createElement("span");
        g.innerText = "Notify on Entry For:";
        g.style.marginRight = "6px";
        wb.appendChild(g);
        var Qa = document.createElement("select");
        Qa.style.border = "1px solid #b1b1b1";
        g = l(nm);
        for (p = g.next(); !p.done; p = g.next())
            p = p.value,
            F = document.createElement("option"),
            F.innerText = p.label,
            F.value = p.value,
            Qa.appendChild(F);
        Qa.onchange = function() {
            a()
        }
        ;
        wb.appendChild(Qa);
        var xb = e();
        xb.style.display = "none";
        g = document.createElement("span");
        g.innerText = "Notify on Leave For:";
        g.style.marginRight = "6px";
        xb.appendChild(g);
        var Ra = document.createElement("select");
        Ra.style.border = "1px solid #b1b1b1";
        g = l(nm);
        for (p = g.next(); !p.done; p = g.next())
            p = p.value,
            F = document.createElement("option"),
            F.innerText = p.label,
            F.value = p.value,
            Ra.appendChild(F);
        Ra.onchange = function() {
            a()
        }
        ;
        xb.appendChild(Ra);
        g = e();
        p = document.createElement("span");
        p.innerText = ac + ":";
        p.style.marginRight = "6px";
        p.style.marginTop = "16px";
        g.appendChild(p);
        var Fa = this.N(new Zl({
            gb: 16,
            Nd: 200,
            Mn: "#92b2d7",
            pn: "#cccccc",
            sn: "#92b2d7"
        }));
        Fa.a.style.display = "inline-block";
        Fa.a.style.verticalAlign = "top";
        g.appendChild(Fa.a);
        var Rb = document.createElement("span");
        Rb.innerText = "100%";
        Rb.style.marginLeft = "6px";
        g.appendChild(Rb);
        q(Fa.vk, function(a) {
            b(a)
        });
        q(Fa.wk, b);
        q(Fa.uk, function(c) {
            u(f.Rp, c);
            b(c);
            a();
            N("ChangeTipVolume", {
                volume: c
            })
        });
        var yb;
        g = e();
        this.Fc = document.createElement("span");
        this.Fc.innerText = "" + bc;
        this.Fc.style.marginTop = "16px";
        this.Fc.style.color = "#0a5a83";
        this.Fc.style.cursor = "pointer";
        this.Fc.onclick = function() {
            Ni('You must be logged in to edit ignored users. Click "OK" to login.') || (void 0 === yb && (yb = new Wl),
            yb.show())
        }
        ;
        this.Fc.onmouseenter = function() {
            f.Fc.style.textDecoration = "underline"
        }
        ;
        this.Fc.onmouseleave = function() {
            f.Fc.style.textDecoration = "none"
        }
        ;
        g.appendChild(this.Fc);
        q(S, function(a) {
            f.qa = a.h.qa;
            f.fa = a.h.i;
            f.mc = a.h.mc;
            f.Se = a.h.Se;
            d(a.h.qa.wn);
            var c = l(M.children);
            for (a = c.next(); !a.done; a = c.next())
                a = a.value,
                a.selected = a.value === f.qa.fontFamily;
            c = l(ca.children);
            for (a = c.next(); !a.done; a = c.next())
                a = a.value,
                a.selected = a.value === f.qa.fontSize;
            xa.checked = f.qa.Zg;
            c = l(Ba.children);
            for (a = c.next(); !a.done; a = c.next())
                a = a.value,
                a.selected = a.value === f.qa.Yi;
            c = l(Ca.children);
            for (a = c.next(); !a.done; a = c.next())
                a = a.value,
                a.selected = a.value === f.qa.Un;
            if (f.mc) {
                wb.style.display = "block";
                xb.style.display = "block";
                c = l(Qa.children);
                for (a = c.next(); !a.done; a = c.next())
                    a = a.value,
                    a.selected = a.value === om(f.qa.zd);
                c = l(Ra.children);
                for (a = c.next(); !a.done; a = c.next())
                    a = a.value,
                    a.selected = a.value === om(f.qa.Bd)
            } else
                wb.style.display = "none",
                xb.style.display = "none";
            bm(Fa, f.qa.Ae);
            b(f.qa.Ae)
        });
        this.Fh = function() {
            void 0 !== C && C.A();
            void 0 !== yb && yb.A()
        }
    }
    m(jm, v);
    var km = [{
        label: "Default",
        value: "default"
    }, {
        label: "Arial",
        value: "Arial, Helvetica"
    }, {
        label: "Bookman Old Style",
        value: "Bookman Old Style"
    }, {
        label: "Comic Sans",
        value: '"Comic Sans MS", cursive'
    }, {
        label: "Courier",
        value: '"Courier New"'
    }, {
        label: "Lucida",
        value: "Lucida"
    }, {
        label: "Palantino",
        value: "Palantino"
    }, {
        label: "Tahoma",
        value: "Tahoma, Geneva"
    }, {
        label: "Times New Roman",
        value: '"Times New Roman"'
    }]
      , lm = [{
        label: "Turn Off Autocomplete",
        value: "-1"
    }, {
        label: "No Delay - 0 Seconds",
        value: "0"
    }, {
        label: "Short - 0.5 Seconds",
        value: "500"
    }, {
        label: "Medium - 1 Second",
        value: "1000"
    }, {
        label: "Long - 1.5 Seconds",
        value: "1500"
    }]
      , mm = [{
        label: "Dark Purple (Tipped 1000 recently)",
        value: "darkpurple"
    }, {
        label: "Light Purple (Tipped 250 recently)",
        value: "lightpurple"
    }, {
        label: "Dark Blue (Tipped 50 recently)",
        value: "darkblue"
    }, {
        label: "Light Blue (Owns or purchased tokens)",
        value: "lightblue"
    }]
      , nm = [{
        label: "All Registered Users",
        value: "orga"
    }, {
        label: "Mods, Fans, and Users With Tokens",
        value: "orgb"
    }, {
        label: "Mods and Fans",
        value: "org"
    }, {
        label: "No Users",
        value: "none"
    }];
    function om(a) {
        switch (a) {
        case 0:
            return "orga";
        case 1:
            return "orgb";
        case 2:
            return "org";
        case 3:
            return "none";
        default:
            return w("Invalid enter/leave setting: " + a),
            "orgb"
        }
    }
    var pm = new jm;
    function qm() {
        function a() {
            pm.Fh();
            void 0 !== b.parent && b.parent.qh(0)
        }
        W.call(this);
        var b = this;
        this.a.id = "settings-tab";
        this.a.style.color = "#ffffff";
        this.a.style.fontSize = "12px";
        this.a.style.fontFamily = "Tahoma, Arial, Helvetica, sans-serif";
        Ea(this, pm);
        pm.Fc.style.color = "#85ffff";
        this.aa = dl();
        this.aa.style.position = "absolute";
        var c = cl();
        this.K = el();
        this.K.onclick = a;
        this.K.onfocus = a;
        this.jb = fl();
        this.jb.style.fontFamily = "UbuntuRegular, Helvetica, Arial, sans-serif";
        this.hf = hl();
        this.hf.style.fontFamily = "UbuntuRegular, Helvetica, Arial, sans-serif";
        c.appendChild(this.K);
        this.aa.appendChild(c);
        this.aa.appendChild(this.jb);
        this.aa.appendChild(this.hf);
        this.a.appendChild(this.aa);
        var d = document.createElement("div");
        d.innerText = Dd;
        d.style.position = "absolute";
        d.style.top = "10px";
        d.style.right = "10px";
        d.style.fontFamily = "UbuntuRegular, Helvetica, Arial, sans-serif";
        d.style.color = "#000000";
        d.style.backgroundColor = "#d8deea";
        d.style.border = "1px solid #acacac";
        d.style.borderRadius = "4px";
        d.style.padding = "8px 12px";
        d.style.opacity = "0";
        this.a.appendChild(d);
        q(Nk, function(a) {
            b.S = a.Ya;
            "default" !== b.S && (Ea(b, pm),
            pm.Fc.style.color = "#85ffff")
        });
        var e;
        q(pm.Rk, function() {
            clearTimeout(e);
            xh(d, "250ms");
            d.style.opacity = "1";
            e = setTimeout(function() {
                xh(d, "1000ms");
                d.style.opacity = "0"
            }, 3E3)
        })
    }
    m(qm, W);
    k = qm.prototype;
    k.C = function() {
        if ("default" !== this.S) {
            this.aa.style.width = this.a.clientWidth + "px";
            var a = this.jb.getBoundingClientRect();
            a = a.right - a.left;
            var b = this.hf.getBoundingClientRect();
            a = this.aa.clientWidth - 12 - 6 - Math.ceil(b.right - b.left + a) - 4;
            a = Math.max(0, a);
            this.K.style.width = a + "px";
            pm.a.style.height = this.a.clientHeight - 27 + "px"
        }
    }
    ;
    k.Kf = function() {
        return "&nbsp;"
    }
    ;
    k.fl = function() {
        return "url(https://ssl-ccstatic.highwebmedia.com/theatermodeassets/ico-preferences.png)"
    }
    ;
    k.Me = function() {
        return "settings-tab-fvm"
    }
    ;
    k.Zb = function() {}
    ;
    k.Od = function() {
        this.K.blur()
    }
    ;
    k.Qe = function() {
        return document.activeElement === this.K
    }
    ;
    k.pa = function() {}
    ;
    k.Jf = function() {
        return this.K.value
    }
    ;
    k.kg = function(a) {
        this.K.value = a
    }
    ;
    k.Le = function() {
        return this.K
    }
    ;
    function rm() {
        var a = this;
        this.Pj = new Map;
        this.Sm = function(b) {
            a.Pj.set(b, (new Date).getTime())
        }
        ;
        this.$o = function(b) {
            a.Pj["delete"](b)
        }
        ;
        setInterval(function() {
            for (var b = (new Date).getTime(), c = l(a.Pj), d = c.next(); !d.done; d = c.next())
                d = d.value,
                5E3 < b - d[1] && a.$o(d[0])
        }, 5E3);
        q(wh, function(b) {
            a.$o(b)
        })
    }
    function sm(a, b) {
        var c = a.Pj.get(b);
        if (void 0 !== c && 5E3 > (new Date).getTime() - c)
            return a.Sm(b),
            !1;
        a.Sm(b);
        return !0
    }
    ;function tm() {
        Sj.call(this);
        var a = this;
        this.ql = !1;
        this.a.style.overflow = "";
        this.ca = Tj(this, new Bl);
        this.wa = Tj(this, new Ml(this,function() {
            Xj(a, a.ca)
        }
        ));
        Tj(this, new qm);
        Xj(this, this.ca);
        q(S, function(b) {
            q(b.j.event.hg, function(b) {
                Cl(a.ca);
                a.ca.w.ta(Dk(b))
            });
            q(b.j.event.P, function(b) {
                a.ca.w.ta(Fk(b));
                b.V && a.wa.Pl(Fk(b))
            });
            q(b.j.event.ag, function(b) {
                a.wa.ta(Dk(b), b.fb.username, b.Ll)
            });
            q(b.j.event.Rg, function(b) {
                a.ca.dj(b.username);
                a.ca.w.Yl(b.username);
                a.wa.nd(b.username)
            })
        });
        var b = new rm;
        q(this.wa.Yh, function(c) {
            (a.ql || c.tj) && sm(b, c.username) && a.ca.w.ta(Fk({
                U: [[{
                    cc: 1,
                    message: "New private " + (c.tj ? "conversation" : "message") + " from " + c.username + " "
                }, {
                    cc: 1,
                    message: "(Press TAB to cycle through your conversations)",
                    oa: "#cd0000",
                    weight: "700"
                }]],
                V: !1
            }))
        })
    }
    m(tm, Sj);
    k = tm.prototype;
    k.C = function() {
        this.window.style.height = this.a.clientHeight - this.xc.offsetHeight + "px"
    }
    ;
    k.$a = function() {
        return Sj.prototype.$a.call(this)
    }
    ;
    k.children = function() {
        return Sj.prototype.children.call(this)
    }
    ;
    k.Xk = function() {
        pm.Fh();
        if (Sj.prototype.$a.call(this) === this.ca) {
            if (!this.wa.Jg()) {
                Xj(this, this.wa);
                var a = this.wa;
                a.Pc();
                Fl(a.va);
                Nl(this.wa) || w("no pmSessions")
            }
        } else
            0 < this.ca.W ? Xj(this, this.ca) : Nl(this.wa) || Xj(this, this.ca)
    }
    ;
    function um(a) {
        pm.Fh();
        if (Sj.prototype.$a.call(a) === a.ca) {
            if (!a.wa.Jg()) {
                Xj(a, a.wa);
                var b = a.wa;
                b.Pc();
                for (b = b.va; ; ) {
                    var c = b.Df.ld.pop();
                    if (void 0 === c)
                        break;
                    Ah(b.Bf, c)
                }
                void 0 !== b.Ma && (Ah(b.Bf, b.Ma),
                b.Ma = void 0);
                Ol(a.wa) || w("no pmSessions")
            }
        } else
            Ol(a.wa) || Xj(a, a.ca)
    }
    k.zf = function() {
        Sj.prototype.$a.call(this) === this.wa && this.wa.zf()
    }
    ;
    function vm(a, b, c) {
        var d = document.createElement("div");
        d.style.position = "absolute";
        d.style.height = "0";
        d.style.width = "0";
        d.style.top = "-8px";
        d.style.left = c;
        c = document.createElement("div");
        c.style.borderLeft = "8px solid transparent";
        c.style.borderRight = "8px solid transparent";
        c.style.borderBottom = "8px solid " + a;
        d.appendChild(c);
        a = document.createElement("div");
        a.style.position = "absolute";
        a.style.top = "3px";
        a.style.left = "3px";
        a.style.borderLeft = "5px solid transparent";
        a.style.borderRight = "5px solid transparent";
        a.style.borderBottom = "5px solid " + b;
        c.appendChild(a);
        return d
    }
    function wm(a) {
        var b = document.createElement("div");
        b.style.position = "absolute";
        b.style.height = "0";
        b.style.width = "0";
        b.style.bottom = "0";
        b.style.left = a;
        a = document.createElement("div");
        a.style.borderLeft = "8px solid transparent";
        a.style.borderRight = "8px solid transparent";
        a.style.borderTop = "8px solid #0b5d81";
        b.appendChild(a);
        var c = document.createElement("div");
        c.style.position = "absolute";
        c.style.top = "0";
        c.style.left = "3px";
        c.style.borderLeft = "5px solid transparent";
        c.style.borderRight = "5px solid transparent";
        c.style.borderTop = "5px solid #ffffff";
        a.appendChild(c);
        return b
    }
    function xm() {
        var a = document.createElement("div");
        a.style.position = "absolute";
        a.style.height = "0";
        a.style.width = "0";
        a.style.left = "-8px";
        a.style.top = "30px";
        var b = document.createElement("div");
        b.style.borderTop = "8px solid transparent";
        b.style.borderBottom = "8px solid transparent";
        b.style.borderRight = "8px solid #0b5d81";
        a.appendChild(b);
        var c = document.createElement("div");
        c.style.position = "absolute";
        c.style.top = "3px";
        c.style.left = "3px";
        c.style.borderTop = "5px solid transparent";
        c.style.borderBottom = "5px solid transparent";
        c.style.borderRight = "5px solid #ffffff";
        b.appendChild(c);
        return a
    }
    ;var ym = new n("emoticonAdded");
    function zm(a) {
        function b(a) {
            var b = d.ia.$a().Le();
            ":" === a[0] && (a = a.slice(1));
            void 0 !== b && (b.value = "" === b.value ? ":" + a + " " : b.value + " :" + a + " ",
            u(ym, void 0))
        }
        function c(a, c) {
            var f = document.createElement("img");
            f.src = "https://ssl-ccstatic.highwebmedia.com/theatermodeassets/emoticons/" + a + "." + (void 0 === c ? "gif" : c);
            f.style.margin = "1px";
            f.style.cursor = "pointer";
            f.onclick = function() {
                b(a);
                d.A()
            }
            ;
            e.appendChild(f)
        }
        X.call(this);
        var d = this;
        this.ia = a;
        this.a.style.visibility = "hidden";
        this.a.style.width = "356px";
        this.a.style.height = "146px";
        this.a.style.backgroundColor = "#ffffff";
        this.a.style.border = "2px solid #0b5d81";
        this.a.style.borderRadius = "4px";
        this.a.style.top = "";
        this.a.style.right = "0";
        this.a.style.overflow = "";
        this.a.appendChild(wm("313px"));
        y("keydown", document.documentElement, function(a) {
            27 === a.keyCode && d.A()
        });
        a = document.createElement("div");
        a.innerText = ae;
        a.style.color = "#0b5d81";
        a.style.backgroundColor = "#e0e0e0";
        a.style.fontSize = "15px";
        a.style.fontFamily = "UbuntuBold, Arial, Helvetica, sans-serif";
        a.style.fontWeight = "bold";
        a.style.padding = "6px";
        this.a.appendChild(a);
        var e = document.createElement("div");
        e.style.width = "100%";
        e.style.height = "100%";
        e.style.padding = "8px";
        e.style.boxSizing = "border-box";
        this.a.appendChild(e);
        c("lmao");
        c("oo", "png");
        c("kissy", "png");
        c("gangsta", "png");
        c("yawn");
        c("innocent");
        c("help");
        c("rofl");
        c("bow");
        c("upset");
        c("what");
        c("hearts");
        c("yes");
        c("thumbsup");
        c("hello");
        c("crazy");
        c("smoke");
        c("bounce");
        c("angel");
        c("woot");
        c("wink");
        c("thumbup");
        c("thumbdown");
        c("smile");
        c("ohmy");
        c("mellow");
        c("roll");
        c("huh");
        c("drool");
        c("cry");
        c("cool");
        c("confused");
        c("curse");
        c("blush");
        var f = document.createElement("a");
        f.innerHTML = be + " &#9656;";
        f.href = "/emoticons/";
        f.style.fontSize = "13px";
        f.style.cssFloat = "right";
        f.style.color = "#dc5500";
        f.style.cursor = "pointer";
        f.style.margin = "12px 4px";
        f.onmouseenter = function() {
            f.style.textDecoration = "underline"
        }
        ;
        f.onmouseleave = function() {
            f.style.textDecoration = "none"
        }
        ;
        f.onclick = function() {
            Mh(f.href, "_blank", "height=615, width=850");
            d.A();
            return !1
        }
        ;
        e.appendChild(f);
        q(this.bc, function() {
            d.A()
        });
        window.use_uploaded_emoticon = function(a) {
            b(a)
        }
    }
    m(zm, X);
    zm.prototype.C = function() {
        if (void 0 === this.$k)
            this.A();
        else {
            var a = this.ia.$a().Le();
            void 0 !== a && (this.a.style.bottom = a.offsetHeight + 20 + "px",
            this.a.style.left = this.$k.offsetLeft + this.$k.offsetWidth - this.a.offsetWidth + 25 + "px")
        }
    }
    ;
    zm.prototype.show = function(a) {
        this.$k = a;
        this.tc();
        this.C();
        this.a.style.visibility = "visible"
    }
    ;
    zm.prototype.A = function() {
        this.$b();
        this.a.style.visibility = "hidden"
    }
    ;
    function Am(a) {
        var b = new tm;
        oi.call(this, a, b, {
            ed: "Chat",
            minWidth: 150,
            minHeight: 100,
            Rd: 400,
            Qd: 250,
            Pd: [1, 3],
            af: !0,
            Ij: function() {
                c.ia.$a().Zb()
            },
            Eo: function() {
                c.ia.$a().pa()
            },
            hl: [b.xc]
        });
        var c = this;
        this.nm = !1;
        Ka() ? this.ra.style.background = "rgba(0,0,0,0.6)" : this.ra.style.background = "#000";
        this.ra.style.borderColor = "#ccc";
        q(b.ca.w.Dn, function() {
            c.blur()
        });
        y("blur", b.ca.w.K, function() {
            if (a.lj !== c || c.nm)
                a.Lb = void 0,
                u(a.bl, void 0)
        });
        y("mouseleave", this.a, function() {
            a.lj = void 0;
            u(a.Om, void 0)
        });
        var d = !0;
        q(S, function(a) {
            d = a.j.ob
        });
        var e;
        q(Nk, function(a) {
            e = a.Ya;
            void 0 !== f && "default" === e && f.A()
        });
        var f;
        q(Fj, function(a) {
            d ? u(sh, void 0) : "default" !== e && (void 0 === f && (f = c.N(new zm(b))),
            f.show(a))
        });
        this.ia = b
    }
    m(Am, oi);
    var Bm;
    q(S, function(a) {
        Bm = a.j.i()
    });
    var Cm = {
        "---": "---",
        underage: "Broadcaster is underage",
        advertising: "Broadcaster is advertising",
        abusive: "Broadcaster is abusive",
        intoxicated: "Broadcaster is intoxicated",
        "large toy": "Broadcaster is using a toy that is too large",
        "offline payments": "Broadcaster is asking for offline payments",
        "public broadcast": "Broadcaster is broadcasting in public",
        "service uniform": "Broadcaster is broadcasting in service uniform",
        sleeping: "Broadcaster is sleeping",
        gender: "Broadcaster is wrong gender",
        other: "Other"
    };
    function Dm() {
        v.call(this);
        var a = this;
        this.Ri = new n("closeReportAbuseRequest");
        this.a.style.color = "#454545";
        this.a.style.fontFamily = "UbuntuBold, Arial, Helvetica, sans-serif";
        this.a.style.fontSize = "11px";
        var b = document.createElement("form")
          , c = document.createElement("div");
        this.De = document.createElement("label");
        this.De.style.color = R.Ba;
        this.De.style.margin = "6px";
        this.De.htmlFor = "abuse-category";
        this.De.innerText = "Choose a category:";
        c.appendChild(this.De);
        this.md = document.createElement("select");
        this.md.id = "abuse-category";
        this.md.style.margin = "6px";
        this.md.style.boxSizing = "border-box";
        for (var d in Cm) {
            var e = document.createElement("option");
            e.value = d;
            e.innerText = Cm[d];
            this.md.appendChild(e)
        }
        c.appendChild(this.md);
        b.appendChild(c);
        c = document.createElement("div");
        c.style.paddingTop = "6px";
        d = document.createElement("label");
        d.style.margin = "6px";
        d.htmlFor = "abuse-comments";
        d.innerText = "Additional Comments:";
        c.appendChild(d);
        this.Ub = document.createElement("textarea");
        this.Ub.id = "abuse-comments";
        this.Ub.maxLength = 255;
        this.Ub.style.width = "100%";
        this.Ub.style.resize = "none";
        this.Ub.style.margin = "6px";
        this.Ub.style.padding = "4px";
        this.Ub.style.border = "1px solid #4b4c4b";
        this.Ub.style.borderRadius = "4px";
        this.Ub.style.boxSizing = "border-box";
        this.Ub.style.display = "block";
        c.appendChild(this.Ub);
        b.appendChild(c);
        this.wg = document.createElement("div");
        this.wg.style.cssFloat = "right";
        this.wg.style.fontFamily = "UbuntuMedium, Arial, Helvetica, sans-serif";
        c = document.createElement("span");
        c.innerText = "REPORT";
        c.style.color = "#ffffff";
        c.style.padding = "3px 6px 4px 5px";
        c.style.marginRight = "6px";
        c.style.backgroundColor = "#eb3404";
        La() && (c.style.background = "linear-gradient(#f66d51, #eb3404)");
        c.style.borderRadius = "4px";
        c.style.boxSizing = "border-box";
        c.style.cursor = "pointer";
        c.style.display = "inline-block";
        d = document.createElement("span");
        d.innerText = "CANCEL";
        d.style.color = "#ffffff";
        d.style.padding = "3px 6px 4px 5px";
        d.style.marginRight = "6px";
        d.style.backgroundColor = "#383838";
        La() && (d.style.background = "linear-gradient(#8b8b8b, #686868)");
        d.style.borderRadius = "4px";
        d.style.boxSizing = "border-box";
        d.style.cursor = "pointer";
        d.style.display = "inline-block";
        this.wg.appendChild(d);
        this.wg.appendChild(c);
        b.appendChild(this.wg);
        y("click", c, function(b) {
            b.preventDefault();
            "other" === a.md.value && "" === a.Ub.value ? U("Please add a description of the abuse being reported.") : "---" === a.md.value ? U("Please choose a category.") : (Em(a).then(function(a) {
                a.Id ? U("Your abuse report has been submitted.") : void 0 !== a.Zn && U(a.Zn)
            })["catch"](function(a) {
                403 === a.fd.status ? U(a.fd.responseText) : U("Error sending abuse report (" + a + ")")
            }),
            u(a.Ri, void 0))
        });
        y("click", d, function() {
            u(a.Ri, void 0)
        });
        this.a.appendChild(b)
    }
    m(Dm, v);
    Dm.prototype.C = function() {
        this.md.style.width = Math.max(0, this.a.clientWidth - 12 - (this.De.offsetWidth + 2 * this.De.offsetLeft)) + "px";
        this.Ub.style.width = Math.max(0, this.a.clientWidth - 12) + "px";
        this.Ub.style.height = Math.max(0, this.a.clientHeight - (this.wg.offsetHeight + 4 * this.De.offsetHeight + 6 + 10)) + "px"
    }
    ;
    function Em(a) {
        return new Promise(function(b, c) {
            return A("abuse/report/" + Bm + "/", {
                category: a.md.value,
                additional_comments: a.Ub.value
            }).then(function(a) {
                a = new D(a.responseText);
                var c = {
                    Id: "success" === H(a, "result"),
                    Zn: G(a, "html")
                };
                L(a);
                b(c)
            })["catch"](function(a) {
                c(a)
            })
        }
        )
    }
    ;var Fm = "1" === eb().signup_notice;
    function Gm() {
        v.call(this);
        var a = this;
        this.a.style.backgroundColor = "#f7f6a8";
        this.a.style.fontSize = "12px";
        this.a.style.padding = "3px 10px";
        this.a.style.boxSizing = "border-box";
        this.a.style.display = "none";
        this.a.style.width = "auto";
        this.a.style.height = "auto";
        var b = document.createElement("a");
        b.innerText = gd;
        b.onclick = function() {
            N("Subscribe")
        }
        ;
        this.a.appendChild(b);
        this.a.appendChild(document.createTextNode(" -- totally free, no email needed"));
        Fm && q(S, function(c) {
            b.href = "/accounts/register/?next=" + encodeURIComponent(window.location.pathname) + "?b=" + c.h.i;
            a.a.style.display = c.j.ob ? "block" : "none"
        })
    }
    m(Gm, v);
    function Hm(a) {
        v.call(this);
        var b = this;
        this.Vf = a;
        this.a.style.position = "static";
        this.a.style.overflow = "auto";
        this.a.style.padding = "4px 7px";
        this.a.style.boxSizing = "border-box";
        this.a.style.fontSize = "12px";
        this.a.style.minWidth = "230px";
        this.a.style.maxWidth = "230px";
        this.a.style.height = "auto";
        a = l(this.children());
        for (var c = a.next(); !c.done; c = a.next())
            c.value.a.style.margin = "2px 0";
        q(S, function(a) {
            var c = !0 === a.h.kd
              , d = !0 === a.h.jd;
            Im(b, c, d, a.j.status);
            q(a.j.event.kb, function(a) {
                Im(b, c, d, a.Aa)
            });
            q(a.j.event.Zd, function(e) {
                c = e.kd;
                d = e.jd;
                Im(b, c, d, a.j.status)
            })
        })
    }
    m(Hm, v);
    function Im(a, b, c, d) {
        Ha(a);
        a: switch (d) {
        case "privatewatching":
        case "privatespying":
        case "groupwatching":
            var e = !0;
            break a;
        default:
            e = !1
        }
        if (e)
            void 0 === a.dn && (a.dn = new Jm),
            a.N(a.dn);
        else {
            a: switch (d) {
            case "privaterequesting":
                b = !0;
                break a;
            case "privatespying":
            case "privatewatching":
            case "privatenotwatching":
            case "groupwatching":
            case "grouprequesting":
            case "groupnotwatching":
            case "away":
                b = !1
            }
            b && (void 0 === a.To && (a.To = new Km(a.Vf)),
            a.N(a.To));
            a: switch (d) {
            case "grouprequesting":
            case "groupnotwatching":
                c = !0;
                break a;
            case "groupwatching":
            case "privatewatching":
            case "privatespying":
            case "privaterequesting":
            case "privatenotwatching":
            case "away":
                c = !1
            }
            c && (void 0 === a.Ln && (a.Ln = new Lm(a.Vf)),
            a.N(a.Ln))
        }
        a.Vf()
    }
    function Mm() {
        v.call(this);
        this.a.style.display = "inline-block";
        this.a.style.height = "auto";
        this.a.style.position = "relative";
        this.a.style.padding = "5px 5px";
        this.a.style.backgroundColor = "#F3F3F3";
        this.a.style.border = "1px solid #DEDEDE";
        this.a.style.boxSizing = "border-box";
        this.a.style.borderRadius = "3px";
        this.a.style.font = "UbuntuRegular, Arial, Helvetica, sans-serif";
        this.a.style.fontSize = "12px";
        this.a.style.lineHeight = "14px";
        this.a.style.textAlign = "center"
    }
    m(Mm, v);
    function Nm(a) {
        var b = document.createElement("div");
        b.innerText = a;
        b.style.width = "100%";
        b.style.margin = "5px 0";
        b.style.fontSize = "14px";
        b.style.fontFamily = "UbuntuMedium, Arial, Helvetica, sans-serif";
        b.style.fontWeight = "500";
        b.style.letterSpacing = "-0.05em";
        b.style.color = "#033E58";
        return b
    }
    function Om(a) {
        var b = document.createElement("div");
        La() && (b.style.background = "linear-gradient(180deg, #4083A1 30.21%, #4063A1 78.45%)");
        Ka() && (b.style.textShadow = "0px 1px 2px rgba(0, 0, 0, 0.25)");
        b.style.display = "inline-block";
        b.innerText = a;
        b.style.border = "1px solid #3C5464";
        b.style.boxSizing = "border-box";
        b.style.borderRadius = "4px";
        b.style.color = "#FFF";
        b.style.fontWeight = "bold";
        b.style.padding = "6px 10px";
        b.style.margin = "5px 0";
        b.style.cursor = "pointer";
        b.style.textDecoration = "none";
        return b
    }
    function Km(a) {
        Mm.call(this);
        var b = this;
        this.Vf = a;
        this.Z = Nm(Sg);
        this.button = Om(Qg);
        this.Ze = this.Wa = 0;
        this.vf = !1;
        this.a.appendChild(this.Z);
        this.Qg = document.createElement("div");
        this.Qg.style.margin = "5px 0";
        this.Qg.style.lineHeight = "16px";
        this.Qg.style.letterSpacing = "-0.05em";
        this.xe = document.createElement("div");
        this.xe.style.fontFamily = "UbuntuMedium, Arial, Helvetica, sans-serif";
        this.xe.style.color = "#49494F";
        this.Qg.appendChild(this.xe);
        this.Bj = document.createElement("div");
        this.Bj.style.color = "#999";
        this.Qg.appendChild(this.Bj);
        this.a.appendChild(this.Qg);
        this.a.appendChild(this.button);
        this.$f = document.createElement("div");
        this.$f.innerText = Jg;
        this.$f.style.fontSize = "9px";
        this.$f.style.color = "#49494F";
        this.$f.style.lineHeight = "125%";
        this.$f.style.margin = "5px 0";
        this.a.appendChild(this.$f);
        q(S, function(a) {
            q(a.j.event.kb, function(c) {
                "privaterequesting" === c.Aa ? (b.button.innerText = Pg,
                b.button.onclick = function() {
                    a.j.Ph().then(function(a) {
                        "" !== a && U(a)
                    })
                }
                ) : (b.button.innerText = Qg,
                b.button.onclick = function() {
                    jk(a.j)
                }
                ,
                b.update())
            });
            q(a.j.event.Zd, function(c) {
                Pm(b, c.vf, c.Wa, c.Ze);
                switch (a.j.status) {
                case "grouprequesting":
                case "privaterequesting":
                    break;
                default:
                    b.update()
                }
            });
            Pm(b, a.h.vf, a.h.Wa, a.h.Ze);
            b.update()
        })
    }
    m(Km, Mm);
    function Pm(a, b, c, d) {
        void 0 !== b && (a.vf = b);
        void 0 !== c && (a.Wa = c);
        void 0 !== d && (a.Ze = d)
    }
    Km.prototype.update = function() {
        this.xe.innerText = Jb(this.Wa);
        var a = this.Bj;
        var b = this.Ze;
        b = Q(P("(minimum %(minMinutes)s minutes)"), {
            minMinutes: b
        }, !0);
        a.innerText = b;
        this.Bj.style.display = 0 === this.Ze ? "none" : "block";
        this.$f.style.display = this.vf ? "block" : "none";
        this.Vf()
    }
    ;
    function Lm(a) {
        Mm.call(this);
        var b = this;
        this.Vf = a;
        this.xe = document.createElement("div");
        this.ui = document.createElement("div");
        this.Z = Nm(Kg);
        this.button = Om(Mg);
        this.pg = this.Ld = this.Ta = 0;
        this.a.appendChild(this.Z);
        this.xe.style.fontFamily = "UbuntuMedium, Arial, Helvetica, sans-serif";
        this.xe.style.color = "#49494F";
        this.ui.style.color = "#999";
        a = document.createElement("div");
        a.appendChild(this.xe);
        a.appendChild(this.ui);
        this.a.appendChild(a);
        this.a.appendChild(this.button);
        q(S, function(a) {
            Qm(b, a.j.status, a.h.Ta, a.h.Co, a.h.Bo);
            q(a.j.event.Eg, function(c) {
                Qm(b, a.j.status, void 0, c.Ld, c.pg)
            });
            q(a.j.event.Zd, function(c) {
                Qm(b, a.j.status, c.Ta, void 0, c.Bl)
            });
            q(a.j.event.kb, function(c) {
                "grouprequesting" === c.Aa ? (Qm(b, c.Aa),
                b.button.onclick = function() {
                    a.j.Oh()
                }
                ) : ("groupnotwatching" === c.Aa && "groupwatching" === c.Vc && --b.Ld,
                Qm(b, c.Aa),
                b.button.onclick = function() {
                    lk(a.j)
                }
                )
            });
            q(a.j.event.Tg, function(c) {
                "groupwatching" === a.j.status && (b.Ld = c - 1,
                Qm(b, a.j.status))
            })
        })
    }
    m(Lm, Mm);
    function Qm(a, b, c, d, e) {
        void 0 !== c && (a.Ta = c);
        void 0 !== d && (a.Ld = d);
        void 0 !== e && (a.pg = e);
        "grouprequesting" === b ? (a.ui.style.display = "block",
        a.button.innerText = Ng) : (a.button.innerText = Mg + " ( " + a.Ld + " / " + a.pg + " )",
        a.ui.style.display = "none");
        a.ui.innerText = Tc + " ( " + a.Ld + " / " + a.pg + " )";
        a.xe.innerText = Jb(a.Ta);
        a.Vf()
    }
    function Jm() {
        v.call(this);
        var a = this;
        this.fh = document.createElement("div");
        this.a.style.display = "inline-block";
        this.a.style.height = "auto";
        this.a.style.position = "relative";
        this.a.style.boxSizing = "border-box";
        this.a.style.borderRadius = "3px";
        this.a.style.font = "UbuntuRegular, Arial, Helvetica, sans-serif";
        this.a.style.fontSize = "12px";
        this.a.style.lineHeight = "14px";
        this.a.style.minWidth = "200px";
        this.a.style.textAlign = "center";
        this.fh.style.display = "block";
        this.fh.style.fontSize = "16px";
        this.fh.style.lineHeight = "18px";
        this.fh.style.margin = "5px 0";
        this.a.appendChild(this.fh);
        var b = document.createElement("div");
        Ka() && (b.style.textShadow = "0px 1px 2px rgba(0, 0, 0, 0.25)");
        La() ? b.style.background = "linear-gradient(180deg, #FD5035 16.39%, #C51B01 86.03%)" : b.style.backgroundColor = "#FD5035";
        b.style.display = "inline-block";
        b.style.boxSizing = "border-box";
        b.style.borderRadius = "4px";
        b.style.color = "#FFF";
        b.style.padding = "6px 10px";
        b.style.margin = "5px 0";
        b.style.cursor = "pointer";
        b.style.textDecoration = "none";
        this.a.appendChild(b);
        q(S, function(c) {
            Rm(a, c.h.sa);
            q(c.j.event.ik, function(b) {
                b.fb.username === c.j.username() && Rm(a, a.iq - b.La)
            });
            q(c.j.event.kb, function(a) {
                function d() {
                    Ki(Ig, function() {
                        c.j.Ph().then(function(a) {
                            "" !== a && U(a)
                        })
                    })
                }
                switch (a.Aa) {
                case "privatewatching":
                    b.innerText = Og;
                    b.onclick = d;
                    break;
                case "privatespying":
                    b.innerText = Rg;
                    b.onclick = d;
                    break;
                case "groupwatching":
                    b.innerText = Lg;
                    b.onclick = function() {
                        mk(c.j)
                    }
                    ;
                    break;
                default:
                    b.onclick = function() {}
                }
            })
        });
        q(qj, function(b) {
            Rm(a, b.Sb)
        })
    }
    m(Jm, v);
    function Rm(a, b) {
        a.iq = b;
        a.fh.innerText = b + " " + Ab(b, !1)
    }
    ;function Sm(a, b) {
        a = void 0 === a ? "fullvideo" : a;
        v.call(this);
        var c = this;
        this.Wg = b;
        this.qq = [];
        this.mh = new zh;
        this.Cq = new n("focusChanging",{
            Xd: !1
        });
        this.bl = new n("focusChanged",{
            Xd: !1
        });
        this.Kr = new n("windowOpened",{
            Xd: !1
        });
        this.Jr = new n("windowClosed",{
            Xd: !1
        });
        this.Om = new n("windowHover",{
            Xd: !1
        });
        this.a.style.overflow = "";
        this.X = Tm(this, new Am(this));
        q(Gj, function() {
            c.X.rb || (c.X.a.style.display = "",
            c.X.rb = !0);
            qi(c, c.X);
            c.X.Y()
        });
        q(kh, function() {
            if (void 0 !== c.Mm && c.Mm.rb)
                c.removeChild(c.Mm);
            else {
                Um(c, "Users");
                var a = new Rj
                  , b = ["privatewatching", "groupwatching"]
                  , d = function(c) {
                    (0 <= b.indexOf(c.Vc) || 0 <= b.indexOf(c.Aa)) && setTimeout(function() {
                        a.refresh(!1)
                    }, 2E3);
                    "notconnected" === c.Vc && (a.refresh(!1),
                    a.a.scrollTop = 0)
                }
                  , e = !0
                  , f = function(b) {
                    q(b.j.event.kb, d, !1);
                    e || void 0 === b.h.Gj || a.clear(b.h.Gj);
                    e = !1
                };
                q(S, f);
                var B = Tm(c, new oi(c,a,{
                    title: Hd,
                    af: !0,
                    ed: "Users",
                    minWidth: 100,
                    minHeight: 100,
                    Rd: 190,
                    Qd: 230,
                    Pd: [0, 2]
                }));
                a.a.style.overflowY = "auto";
                c.Y();
                q(a.Im, function(a) {
                    ti(B, Hd + " (" + a + ")")
                });
                B.Xe = function() {
                    S.removeListener(f)
                }
                ;
                c.Mm = B
            }
        });
        q(ph, function() {
            c.X.rb ? (c.X.a.style.display = "none",
            c.X.rb = !1) : (c.X.a.style.display = "",
            c.X.rb = !0,
            qi(c, c.X));
            c.Y()
        });
        q(Dj, function() {
            void 0 !== c.El && c.El.rb ? c.removeChild(c.El) : (Vm(c),
            c.Y())
        });
        q(jh, function(a) {
            function b(a) {
                ti(e, a.h.i)
            }
            Um(c, "Bio");
            var d = new Qh(a)
              , e = Tm(c, new oi(c,d,{
                title: a,
                af: !0,
                ed: "Bio",
                Qd: 400,
                Rd: 360,
                minWidth: 360,
                minHeight: 100,
                Pd: [0, 2],
                Xe: function() {
                    S.removeListener(b)
                }
            }));
            q(S, b, !1);
            c.Y()
        });
        var d;
        q(S, function(a) {
            void 0 !== a.h.hb ? d = a.h.hb : w("isAgeVerified is not defined.")
        });
        var e;
        q(lh, function(b) {
            if (void 0 !== c.fc && c.fc.rb)
                void 0 !== c.Rb && c.Rb.tm || c.removeChild(c.fc);
            else {
                N("OpenTipCallout");
                void 0 === c.Rb && (c.Rb = new vj(a),
                q(c.Rb.Ao, function(a) {
                    e = a.La;
                    void 0 !== c.fc && u(lh, {})
                }));
                void 0 === b.La && (b.La = e);
                var f = function() {
                    void 0 !== c.fc && c.removeChild(c.fc)
                };
                if (d)
                    var h = !Ni('You must be logged in to tip. Click "OK" to login.');
                else
                    U("This broadcaster doesn't accept tips."),
                    h = !1;
                h && (void 0 !== c.Wg && c.Wg.Xm(c.Rb),
                c.Rb.show(b),
                void 0 !== c.fc ? qi(c, c.fc) : (h = l(c.Rb.Kn()),
                b = h.next().value,
                h = h.next().value,
                c.fc = Tm(c, new oi(c,c.Rb,{
                    title: rc,
                    af: !1,
                    ed: "Tip",
                    minWidth: b + 4,
                    minHeight: h + 4,
                    Rd: b,
                    Qd: h,
                    Pd: [4],
                    Xe: function() {
                        S.removeListener(f);
                        c.fc = void 0
                    },
                    Ij: function() {
                        void 0 !== c.Rb && wj(c.Rb)
                    },
                    Kl: function(a) {
                        if (void 0 === c.fc)
                            return w("undefined tipwindow"),
                            !0;
                        27 === a.keyCode ? c.removeChild(c.fc) : 83 === a.keyCode && a.ctrlKey && c.removeChild(c.fc);
                        return !0
                    }
                })),
                c.fc.ra.style.boxSizing = "",
                q(S, f, !1),
                c.Y()))
            }
        });
        var f;
        q(S, function(a) {
            0 < a.h.ug.length && (f = a.h.ug[0].Dk,
            void 0 !== c.eq && ti(c.eq, "App: " + f));
            void 0 !== c.ub && c.ub.rb && c.removeChild(c.ub);
            var b = a.j.status
              , d = !0 !== a.h.hb || !0 !== a.h.kd && !0 !== a.h.jd;
            q(a.j.event.kb, function(a) {
                b = a.Aa;
                switch (a.Aa) {
                case "unknown":
                case "offline":
                case "notconnected":
                case "away":
                case "hidden":
                case "privatenotwatching":
                case "passwordprotected":
                    void 0 !== c.ub && c.ub.rb && c.removeChild(c.ub);
                    break;
                case "privaterequesting":
                case "privatewatching":
                case "privatespying":
                case "grouprequesting":
                case "groupwatching":
                    break;
                default:
                    d && void 0 !== c.ub && c.ub.rb && c.removeChild(c.ub)
                }
            });
            var e = ["privaterequesting", "privatespying", "privatewatching", "grouprequesting", "groupwatching"];
            q(a.j.event.Zd, function(a) {
                (d = !a.kd && !a.jd) && -1 === e.indexOf(b) && void 0 !== c.ub && c.ub.rb && c.removeChild(c.ub)
            })
        });
        q(nh, function() {
            void 0 !== c.Md && c.Md.rb ? c.removeChild(c.Md) : (void 0 !== c.Md && c.removeChild(c.Md),
            c.de = new Dm,
            c.Md = Tm(c, new oi(c,c.de,{
                title: Jd,
                af: !0,
                ed: "Abuse",
                minWidth: 270,
                minHeight: 200,
                Rd: 270,
                Qd: 200,
                Pd: [3, 1],
                Ij: function() {
                    void 0 !== c.de && c.de.md.focus()
                },
                Kl: function() {
                    return !0
                },
                Xe: function() {
                    c.Md = void 0
                }
            })),
            c.Y(),
            q(c.de.Ri, function() {
                void 0 !== c.Md && c.removeChild(c.Md)
            }))
        });
        q(oh, function() {
            void 0 !== c.ub && c.ub.rb ? c.removeChild(c.ub) : (void 0 === c.So && (c.So = new Hm(function() {
                void 0 !== c.ub && c.ub.rb && c.ub.Ol.apply()
            }
            )),
            c.ub = Tm(c, new oi(c,c.So,{
                title: Hg,
                af: !1,
                ed: "PrivateAndGroup",
                Gi: !0,
                minWidth: 248,
                minHeight: 270,
                Rd: 248,
                Qd: 270,
                Pd: [2, 1]
            })),
            c.Y())
        });
        this.Di = new Gm;
        this.a.appendChild(this.Di.a);
        "1" === eb().room_list && Vm(this);
        qi(this, void 0)
    }
    m(Sm, v);
    function Vm(a) {
        Um(a, "Rooms");
        var b = new ej
          , c = 225 + Uh();
        a.El = Tm(a, new oi(a,b,{
            title: Ld,
            af: !0,
            ed: "Rooms",
            minWidth: c,
            minHeight: 100,
            Rd: c,
            Qd: 385,
            Pd: [1, 2],
            Xe: function() {
                b.close()
            }
        }))
    }
    Sm.prototype.C = function() {
        v.prototype.C.call(this);
        for (var a = l([].concat(this.mh.ld)), b = a.next(); !b.done; b = a.next())
            b.value.Ol.apply();
        void 0 !== this.Di && (this.Di.a.style.left = (this.a.clientWidth - this.Di.a.offsetWidth) / 2 + "px")
    }
    ;
    function qi(a, b) {
        if (!a.er && a.Lb !== b) {
            if (void 0 !== a.Lb) {
                var c = a.Lb;
                c.ra.style.opacity = void 0 !== c.F.lq ? c.F.lq : "1";
                void 0 !== c.F.Eo && c.F.Eo();
                if (document.activeElement !== document.body && void 0 !== document.activeElement && null !== document.activeElement)
                    try {
                        document.activeElement.blur()
                    } catch (f) {}
            }
            u(a.Cq, b);
            if (void 0 !== b) {
                N("FocusWindow", {
                    windowName: b.F.ed
                });
                a.mh.hd(b);
                c = a.children().length + 10;
                for (var d = l([].concat(a.mh.ld)), e = d.next(); !e.done; e = d.next())
                    e.value.a.style.zIndex = "" + c,
                    --c;
                d = l(a.qq);
                for (e = d.next(); !e.done; e = d.next())
                    e.value.a.style.zIndex = "" + c,
                    --c;
                b.ra.style.opacity = "1";
                void 0 !== b.F.Ij && b.F.Ij()
            }
            a.Lb = b;
            u(a.bl, b)
        }
    }
    Sm.prototype.N = function() {
        throw Error("addChild not supported");
    }
    ;
    function Tm(a, b) {
        q(b.nn, function(b) {
            a.er = b
        });
        y("mousedown", b.a, function(c) {
            a.Lb !== b && (qi(a, b),
            c.preventDefault())
        });
        y("mouseover", b.a, function() {
            a.lj = b;
            u(a.Om, b)
        });
        a.mh.hd(b);
        v.prototype.N.call(a, b, void 0);
        qi(a, b);
        u(a.Kr, b);
        b.rb = !0;
        return b
    }
    Sm.prototype.removeChild = function(a) {
        a.rb = !1;
        void 0 !== a.Xe && a.Xe();
        qi(this, void 0);
        this.mh.remove(a);
        v.prototype.removeChild.call(this, a);
        u(this.Jr, a)
    }
    ;
    function Um(a, b) {
        for (var c = l([].concat(a.mh.ld)), d = c.next(); !d.done; d = c.next())
            if (d = d.value,
            d.F.ed === b) {
                a.removeChild(d);
                d === a.Lb && qi(a, void 0);
                break
            }
    }
    ;function Wm(a) {
        X.call(this);
        var b = this;
        this.u = a;
        this.bg = new Map;
        this.a.style.visibility = "hidden";
        this.a.style.width = "auto";
        this.a.style.minWidth = "100px";
        this.a.style.height = "auto";
        this.a.style.position = "absolute";
        this.a.style.borderRadius = "2px";
        this.a.style.fontSize = "14px";
        this.a.style.textAlign = "center";
        Ka() ? this.a.style.backgroundColor = "rgba(0, 0, 0, 0.3)" : this.a.style.backgroundColor = "black";
        this.a.onclick = function() {
            b.A()
        }
        ;
        q(this.u.Tc, function(a) {
            b.bg.clear();
            b.clear();
            for (var c = l(a), e = c.next(); !e.done; e = c.next())
                e = e.value,
                b.bg.set(e.label, e.value),
                b.a.appendChild(b.Sk(e));
            0 === a.length ? b.u.pb.Sn() : b.u.pb.xp()
        });
        q(this.bc, function() {
            b.A()
        });
        this.A()
    }
    m(Wm, X);
    k = Wm.prototype;
    k.C = function() {
        this.a.style.right = "0px";
        this.a.style.bottom = this.u.pb.Mb() + "px"
    }
    ;
    k.Sk = function(a) {
        var b = this
          , c = document.createElement("div");
        c.innerText = a.label;
        c.style.color = "#ffffff";
        c.style.padding = "1px";
        c.style.width = "100%";
        Ka() ? c.style.border = "1px solid rgba(255, 255, 255, 0.5)" : c.style.border = "1px solid black";
        c.onmouseenter = function() {
            c.style.color = "#fc760c";
            c.style.borderColor = "#fc760c"
        }
        ;
        c.onmouseleave = function() {
            c.style.color = "#FFFFFF";
            Ka() ? c.style.borderColor = "rgba(255, 255, 255, 0.5)" : c.style.borderColor = "black"
        }
        ;
        a.eh ? Ka() ? c.style.backgroundColor = "rgba(204, 204, 204, 0.6)" : c.style.backgroundColor = "lightgrey" : c.onclick = function() {
            N("clickMenuLink:QualitySelect-" + a.label);
            b.ue(a.label)
        }
        ;
        return c
    }
    ;
    k.ue = function(a) {
        a = this.bg.get(a);
        void 0 === a ? w("Undefined quality level key", {}, "VIDEO_") : this.u.ue(a)
    }
    ;
    k.clear = function() {
        for (; null !== this.a.firstChild; )
            this.a.removeChild(this.a.firstChild)
    }
    ;
    k.show = function() {
        this.tc();
        this.a.style.visibility = "visible";
        this.C()
    }
    ;
    k.A = function() {
        this.$b();
        this.a.style.visibility = "hidden"
    }
    ;
    function Xm(a, b) {
        function c(a) {
            a !== d.state.volume && d.ih(a)
        }
        v.call(this);
        var d = this;
        this.u = a;
        this.state = {
            volume: 60,
            na: !0
        };
        this.br = b;
        this.state.na = Ym();
        this.state.volume = Zm();
        this.a.style.bottom = "0px";
        this.a.style.left = "0px";
        this.a.style.height = "32px";
        this.a.style.background = "url(https://ssl-ccstatic.highwebmedia.com/theatermodeassets/ConfigBarNoEdge.png) repeat-x";
        this.Qa = this.N(new Zl({
            gb: 11,
            Nd: 66,
            xf: 4,
            Nn: "https://ssl-ccstatic.highwebmedia.com/theatermodeassets/SliderKnob.png",
            Gk: "https://ssl-ccstatic.highwebmedia.com/theatermodeassets/SliderBar.png"
        }));
        this.Qa.a.style.position = "absolute";
        this.Qa.a.style.left = "30px";
        this.Qa.a.style.bottom = "10px";
        bm(this.Qa, this.state.volume);
        q(this.Qa.vk, function(a) {
            d.u.Fd(!1);
            d.oi();
            c(a)
        });
        q(this.Qa.wk, c);
        q(this.Qa.uk, function(a) {
            c(a);
            0 === d.state.volume && d.u.Fd(!0);
            N("ChangeVolume", {
                volume: a
            })
        });
        var e = document.createElement("div");
        e.style.width = "115px";
        e.style.height = "100%";
        e.style.cssFloat = "left";
        this.a.appendChild(e);
        this.bk = document.createElement("img");
        this.bk.src = "https://ssl-ccstatic.highwebmedia.com/theatermodeassets/ConfigBar.png";
        this.bk.style.width = "100%";
        this.bk.style.height = "100%";
        e.appendChild(this.bk);
        this.Ha = document.createElement("img");
        this.Ha.src = "https://ssl-ccstatic.highwebmedia.com/theatermodeassets/ButtonVolume0.png";
        this.Ha.style.position = "absolute";
        this.Ha.style.bottom = "6px";
        this.Ha.style.left = "5px";
        this.Ha.onclick = function() {
            N("ToggleMute", {
                newState: !d.state.na
            });
            N("ChangeVolume", {
                volume: d.state.volume
            });
            d.state.na ? d.oi() : d.Th()
        }
        ;
        e.appendChild(this.Ha);
        var f = Jh();
        document[Lh()] = function() {
            T() || (f = !1,
            d.Sa.style.display = "none",
            U("Full screen is unavailable."))
        }
        ;
        this.Sa = document.createElement("img");
        this.Sa.src = "https://ssl-ccstatic.highwebmedia.com/theatermodeassets/ButtonFullscreen.png";
        this.Sa.style.position = "relative";
        this.Sa.style.top = "1px";
        this.Sa.style.cssFloat = "right";
        Jh() || (this.Sa.style.display = "none");
        this.Sa.onclick = function() {
            f && d.u.Ef()
        }
        ;
        this.a.appendChild(this.Sa);
        this.cb = document.createElement("img");
        this.cb.src = "https://ssl-ccstatic.highwebmedia.com/theatermodeassets/ButtonQuality.png";
        this.cb.style.position = "relative";
        this.cb.style.top = "1px";
        this.cb.style.display = "none";
        this.cb.style.cssFloat = "right";
        this.cb.onclick = function() {
            d.br.show()
        }
        ;
        this.a.appendChild(this.cb);
        this.df = document.createElement("img");
        this.df.src = "https://ssl-ccstatic.highwebmedia.com/theatermodeassets/ButtonScale.png";
        this.df.style.position = "relative";
        this.df.style.top = "1px";
        this.df.style.cssFloat = "right";
        this.df.style.display = T() ? "" : "none";
        this.df.onclick = function() {
            var a = d.u;
            a.ol = !a.ol;
            $m(a)
        }
        ;
        this.a.appendChild(this.df);
        q(S, function(a) {
            void 0 === a.h.hb ? w("Dossier is missing context.") : (d.sf(),
            an(d))
        });
        q(Ej, function() {
            d.Th()
        });
        q(a.Tc, function(a) {
            if (0 === a.length)
                d.cb.style.display = "none";
            else {
                d.cb.style.display = "";
                var b = "https://ssl-ccstatic.highwebmedia.com/theatermodeassets/ButtonQuality.png";
                a = l(a);
                for (var c = a.next(); !c.done; c = a.next())
                    if (c = c.value,
                    c.label.includes("4K") || c.label.includes("HD")) {
                        if (b = "https://ssl-ccstatic.highwebmedia.com/theatermodeassets/ButtonQualityHDAvil.png",
                        !0 === c.eh) {
                            b = "https://ssl-ccstatic.highwebmedia.com/theatermodeassets/ButtonQualityHDSel.png";
                            break
                        }
                    } else
                        c.label.includes("Auto") && 720 <= parseInt(c.label.slice(6, -2)) && (b = "https://ssl-ccstatic.highwebmedia.com/theatermodeassets/ButtonQualityHDSel.png");
                d.cb.src = b
            }
        });
        an(this);
        this.A()
    }
    m(Xm, v);
    k = Xm.prototype;
    k.Dd = function() {
        x() && window.localStorage.setItem("videoControls", JSON.stringify({
            volume: this.state.volume,
            isMuted: this.state.na
        }))
    }
    ;
    function Zm() {
        if (x()) {
            var a = window.localStorage.getItem("videoControls");
            if (null !== a)
                return JSON.parse(a).volume
        }
        return 60
    }
    function Ym() {
        if (x()) {
            var a = window.localStorage.getItem("videoControls");
            if (null !== a)
                return JSON.parse(a).isMuted
        }
        return !1
    }
    k.show = function() {
        this.a.style.visibility = "visible"
    }
    ;
    k.A = function() {
        this.a.style.visibility = "hidden"
    }
    ;
    k.Th = function() {
        !0 !== this.state.na && (this.state.na = !0,
        an(this))
    }
    ;
    k.oi = function() {
        !1 !== this.state.na && (this.state.na = !1,
        0 === this.state.volume && (this.state.volume = 60),
        an(this))
    }
    ;
    k.C = function() {
        this.df.style.display = T() ? "" : "none"
    }
    ;
    function an(a) {
        a.sf();
        a.u.Yg(a.state.volume, a.state.na);
        bm(a.Qa, a.state.volume);
        a.Dd()
    }
    k.sf = function() {
        this.Ha.src = !0 === this.state.na ? "https://ssl-ccstatic.highwebmedia.com/theatermodeassets/ButtonVolume0.png" : 0 === this.state.volume ? "https://ssl-ccstatic.highwebmedia.com/theatermodeassets/ButtonVolume1.png" : 40 > this.state.volume ? "https://ssl-ccstatic.highwebmedia.com/theatermodeassets/ButtonVolume2.png" : 70 > this.state.volume ? "https://ssl-ccstatic.highwebmedia.com/theatermodeassets/ButtonVolume3.png" : "https://ssl-ccstatic.highwebmedia.com/theatermodeassets/ButtonVolume4.png"
    }
    ;
    k.ih = function(a) {
        this.state.volume !== a && (this.state.volume = a,
        0 !== a && (this.state.na = !1),
        an(this))
    }
    ;
    k.ri = function(a) {
        this.state.na = a;
        an(this)
    }
    ;
    k.Gm = function(a) {
        this.ih(a);
        this.Dd()
    }
    ;
    k.pi = function(a) {
        this.ri(a);
        this.Dd()
    }
    ;
    k.Eh = function() {
        this.A()
    }
    ;
    k.Mb = function() {
        return "hidden" === this.a.style.visibility ? 0 : 32
    }
    ;
    k.Sn = function() {
        this.cb.style.display = "none"
    }
    ;
    k.xp = function() {
        this.cb.style.display = ""
    }
    ;
    var bn = window.Hls;
    function cn(a) {
        a = void 0 === a ? {} : a;
        a.tabHidden = document.hidden;
        N("VideoMetric", a, !0)
    }
    function dn() {
        return Za() ? window.performance.now() : (new Date).getTime()
    }
    function en() {
        Za() && (this.start = dn())
    }
    en.prototype.send = function(a, b, c) {
        if (Za() && void 0 !== this.start) {
            var d = dn() - this.start - b
              , e = this.start = dn();
            a = {
                eventName: "autoPlayDuration",
                autoPlayDuration: e / 1E3,
                autoPlayDurationMS: e,
                autoPlayDurationNoDelay: d / 1E3,
                autoPlayDurationNoDelayMS: d,
                autoPlayable: a
            };
            0 !== b && (a.autoPlayHiddenDelay = b / 1E3,
            a.autoPlayHiddenDelayMS = b);
            void 0 !== c && null !== c && (a.autoPlayError = c);
            cn(a)
        }
    }
    ;
    function fn(a) {
        var b = this;
        this.dm = this.Pm = !1;
        this.te = function(a) {
            a = void 0 === a ? {} : a;
            var c = {
                playerType: b.$q
            }, e;
            for (e in a)
                c[e] = a[e];
            cn(c)
        }
        ;
        this.qr = function() {
            var a = {
                eventName: "playerCreated"
            }
              , d = eb().player;
            void 0 !== d && (a.playerOverride = d);
            b.te(a);
            b.rq = dn()
        }
        ;
        this.Go = function() {
            b.Wj = dn()
        }
        ;
        this.Ho = function() {
            b.Pm || b.Yj()
        }
        ;
        this.Kj = function() {
            b.Pm || (b.Yj(),
            b.Pm = !0,
            b.Tj())
        }
        ;
        this.$q = a;
        this.qr()
    }
    function gn(a) {
        a.Tj();
        q(S, a.Go);
        q(vh, a.Ho);
        y("beforeunload", window, a.Kj);
        y("unload", window, a.Kj);
        a.dm = !0
    }
    fn.prototype.Yj = function() {
        if (void 0 !== this.Wj && Za()) {
            this.fm = dn();
            var a = this.fm - this.Wj;
            this.te({
                eventName: "viewerDuration",
                viewerDuration: a / 1E3,
                viewerDurationMS: a
            });
            return a
        }
    }
    ;
    fn.prototype.Tj = function() {
        this.dm && (cb("beforeunload", window, this.Kj),
        cb("unload", window, this.Kj),
        S.removeListener(this.Go),
        vh.removeListener(this.Ho),
        this.dm = !1)
    }
    ;
    function hn(a) {
        fn.call(this, a);
        var b = this;
        this.jj = this.$n = this.ll = this.mj = !1;
        this.pr = function() {
            if (!b.ll) {
                var a = {
                    eventName: "videoPlaying",
                    isAjax: !1
                }
                  , d = b.tq();
                void 0 !== d && b.te({
                    eventName: "broadcastDelay",
                    broadcastDelayMS: d,
                    broadcastDelay: d / 1E3
                });
                d = dn();
                if (void 0 === b.kl)
                    r("Playing metric has triggered but htmlMediaLoadStartTime is undefined...skipping");
                else {
                    var e = d - b.kl;
                    a.timeSinceMediaLoadStart = e / 1E3;
                    a.timeSinceMediaLoadStartMS = e
                }
                void 0 !== b.jl && (e = d - b.jl,
                a.timeSinceMediaAttaching = e / 1E3,
                a.timeSinceMediaAttachingMS = e);
                if (b.$n) {
                    if (void 0 === b.Wj) {
                        r("Playing metric is for AJAX reload but roomLoadedTime is undefined...skipping");
                        return
                    }
                    e = d - b.Wj;
                    a.timeSinceLoadAjax = e / 1E3;
                    a.timeSinceLoadAjaxMS = e;
                    a.isAjax = !0;
                    void 0 === b.fm ? r("Playing metric is for AJAX reload but roomUnloadedTime is undefined") : (e = d - b.fm,
                    a.timeSinceUnloadAjax = e / 1E3,
                    a.timeSinceUnloadAjaxMS = e)
                } else
                    d -= b.rq,
                    a.timeSincePlayerCreated = d / 1E3,
                    a.timeSincePlayerCreatedMS = d;
                b.te(a);
                b.ll = !0;
                b.$n = !0
            }
        }
        ;
        this.or = function() {
            var a = b.Ga instanceof HTMLMediaElement ? b.Ga.error : b.Ga.error_;
            void 0 !== a && null !== a && b.te({
                eventName: "playerError",
                errorCode: a.code,
                errorMessage: a.message,
                errorSource: "HTMLMediaElement",
                errorReadyState: b.Ga.readyState,
                errorNetworkState: b.Ga.networkState
            })
        }
        ;
        this.Ob = function(a) {
            b.mj && ("playing" === a.type ? b.pr() : "loadstart" === a.type ? b.kl = dn() : "error" === a.type ? b.or() : "stalled" === a.type ? (a = a.target,
            void 0 !== a && null !== a && b.te({
                eventName: "playerStalled",
                stalledReadyState: a.readyState,
                stalledNetworkState: a.networkState
            })) : r("Received unknown HTMLMediaElement event: ", a))
        }
        ;
        this.jq = function(a) {
            b.Yo();
            b.le = a;
            b.le.on(bn.Events.ERROR, b.qe);
            b.le.on(bn.Events.FRAG_CHANGED, b.qe);
            Za() && (b.le.on(bn.Events.LEVEL_SWITCHING, b.qe),
            b.le.on(bn.Events.LEVEL_SWITCHED, b.qe),
            b.le.on(bn.Events.MEDIA_ATTACHING, b.qe));
            b.jj = !0
        }
        ;
        this.$p = function(a) {
            var c = a.level;
            if (void 0 === c)
                r("Received Hls.Events.LEVEL_SWITCHING without a level: ", a);
            else {
                for (var e = l(["bitrate", "height", "width"]), f = e.next(); !f.done; f = e.next())
                    if (f = f.value,
                    isNaN(a[f])) {
                        r("Level " + f + " is not a number: " + a[f] + "...skipping");
                        return
                    }
                b.Xn[c] = a;
                b.Yn.unshift({
                    level: c,
                    ts: dn()
                })
            }
        }
        ;
        this.ur = function(a) {
            var c = a.level;
            if (void 0 === c)
                r("Received Hls.Events.LEVEL_SWITCHED without a level: ", a);
            else {
                var e = b.Xn[c];
                if (void 0 === e)
                    r("Received unknown Hls.Events.LEVEL_SWITCHED level: ", a);
                else {
                    a = l(b.Yn);
                    for (var f = a.next(); !f.done; f = a.next())
                        if (f = f.value,
                        f.level === c) {
                            b.il.push(dn() - f.ts);
                            break
                        }
                    if (void 0 === b.Td || e.bitrate > b.Td.bitrate)
                        b.Td = e
                }
            }
        }
        ;
        this.tq = function() {
            if (void 0 !== b.kj) {
                var a = b.Ga.currentTime() - b.kj.start;
                if (!(0 > a || (a = (new Date).getTime() - b.kj.programDateTime + 1E3 * a,
                0 > a || 3E5 < a)))
                    return a
            }
        }
        ;
        this.qe = function(a, d) {
            if (b.jj)
                if (a === bn.Events.LEVEL_SWITCHING)
                    b.$p(d);
                else if (a === bn.Events.LEVEL_SWITCHED)
                    b.ur(d);
                else if (a === bn.Events.FRAG_CHANGED)
                    b.kj = d.frag;
                else if (a === bn.Events.MEDIA_ATTACHING)
                    b.jl = dn();
                else if (a === bn.Events.ERROR) {
                    var c = b.Ga.tech().el();
                    b.te({
                        eventName: "playerError",
                        errorCode: d.type,
                        errorMessage: d.details,
                        errorSource: "hls.js",
                        errorReadyState: c.readyState,
                        errorNetworkState: c.networkState
                    })
                } else
                    r("Received unknown Hls.Event: ", a)
        }
        ;
        this.Yo = function() {
            b.jj && (b.le.off(bn.Events.ERROR, b.qe),
            b.le.off(bn.Events.FRAG_CHANGED, b.qe),
            Za() && (b.le.off(bn.Events.LEVEL_SWITCHING, b.qe),
            b.le.off(bn.Events.LEVEL_SWITCHED, b.qe)),
            b.jj = !1)
        }
        ;
        this.Zo = function() {
            b.mj && (b.Ga instanceof HTMLMediaElement ? (Za() && (cb("playing", b.Ga, b.Ob),
            cb("loadstart", b.Ga, b.Ob)),
            cb("error", b.Ga, b.Ob),
            cb("stalled", b.Ga, b.Ob)) : (Za() && (b.Ga.off("playing", b.Ob),
            b.Ga.off("loadstart", b.Ob)),
            b.Ga.off("error", b.Ob),
            b.Ga.off("stalled", b.Ob)),
            b.mj = !1)
        }
        ;
        this.reset = function() {
            b.ll = !1;
            b.Xn = new Map;
            b.Yn = [];
            b.il = [];
            b.Td = void 0;
            b.kj = void 0;
            b.kl = void 0;
            b.jl = void 0
        }
        ;
        this.reset()
    }
    m(hn, fn);
    function jn(a, b) {
        a.Zo();
        a.Ga = b;
        a.Ga instanceof HTMLMediaElement ? (Za() && (y("playing", a.Ga, a.Ob),
        y("loadstart", a.Ga, a.Ob)),
        y("error", a.Ga, a.Ob),
        y("stalled", a.Ga, a.Ob)) : (Za() && (a.Ga.on("playing", a.Ob),
        a.Ga.on("loadstart", a.Ob)),
        a.Ga.on("error", a.Ob),
        a.Ga.on("stalled", a.Ob));
        a.mj = !0
    }
    hn.prototype.Yj = function() {
        var a = fn.prototype.Yj.call(this);
        if (void 0 !== a && 5E3 < a) {
            void 0 !== this.Td && this.te({
                eventName: "levelMax",
                levelMaxName: this.Td.name,
                levelMaxBitrate: this.Td.bitrate,
                levelMaxAudioCodec: this.Td.audioCodec,
                levelMaxVideoCodec: this.Td.videoCodec,
                levelMaxHeight: this.Td.height,
                levelMaxWidth: this.Td.width
            });
            var b = this.il.reduce(function(a, b) {
                return a + b
            }, 0) / this.il.length;
            isNaN(b) || this.te({
                eventName: "levelSwitchAvg",
                levelSwitchAvg: b / 1E3,
                levelSwitchAvgMS: b
            })
        }
        this.reset();
        return a
    }
    ;
    hn.prototype.Tj = function() {
        fn.prototype.Tj.call(this);
        this.Yo();
        this.Zo()
    }
    ;
    function kn() {
        v.call(this);
        var a = this;
        this.fk = this.ah = !0;
        this.Bp = "10.2.0";
        this.Wd = "xmovie";
        this.vo = "xmovieContainer";
        this.Lk = !1;
        this.Tc = new n("possibleQualityLevelsChanged");
        this.ze = new n("videoOfflineChange");
        this.volume = 0;
        this.muted = !0;
        this.ol = !1;
        this.oq = "https://ssl-ccstatic.highwebmedia.com/theatermodeassets/CBV_TS_v1.0.swf";
        this.wd = {
            allowScriptAccess: "always",
            allowFullScreen: "true",
            quality: "high",
            wmode: "opaque",
            id: this.Wd,
            scale: "noborder"
        };
        this.attributes = {
            id: "xmovie",
            name: this.Wd,
            align: "middle"
        };
        gn(new fn("FlashPlayer"));
        this.uj = !1;
        if (x()) {
            var b = window.localStorage.getItem("isTheaterMode");
            null !== b && JSON.parse(b).uj && (this.uj = !0)
        }
        this.rl = !0;
        this.Bq = this.N(new Wm(this));
        this.pb = this.N(new Xm(this,this.Bq));
        this.uj ? this.pb.A() : this.pb.show();
        this.a.style.backgroundColor = "#000";
        ln(this);
        this.a.className = "videoPlayerDiv";
        window.updateQualityLevels = function(b) {
            u(a.Tc, b)
        }
        ;
        window.showQualitySelect = function() {
            u(a.Tc, mn(a))
        }
        ;
        window.hideQualitySelect = function() {
            u(a.Tc, [])
        }
        ;
        window.statusUpdate = function(b) {
            "offline" === dm(b) ? u(a.ze, !0) : u(a.ze, !1)
        }
        ;
        window.callbacksRegistered = function() {
            a.Lk = !0
        }
        ;
        window.setFVMFlashMaxQ = function(a) {
            hb("cbv_maxq", a, 365)
        }
        ;
        window.getFVMFlashMaxQ = function() {
            return gb("cbv_maxq")
        }
    }
    m(kn, v);
    function ln(a) {
        null === document.getElementById(a.vo) && (a.Ka = document.createElement("div"),
        a.Ka.id = a.vo,
        a.Ka.style.height = "calc(100% - " + a.pb.Mb() + "px)",
        a.Ka.style.width = "100%",
        a.Ka.style.position = "relative",
        a.Ka.style.top = "0px",
        a.Ka.style.left = "0px",
        a.a.appendChild(a.Ka));
        a.Tf = document.createElement("div");
        a.Tf.id = a.Wd;
        a.Tf.style.margin = "0";
        a.Tf.style.padding = "0";
        a.Tf.style.width = "100%";
        a.Tf.style.height = "100%";
        a.Tf.style["object-fit"] = "cover";
        a.Ka.appendChild(a.Tf);
        a.a.style.position = "static"
    }
    k = kn.prototype;
    k.ej = function(a) {
        this.context = a;
        var b = "anonymous";
        this.context.j.ob || (b = a.h.Nm);
        this.wd.FlashVars = "pid=" + a.h.i + "&address=" + a.h.xq + "&uid=" + a.h.userName + "&pw=" + encodeURIComponent(b) + "&rpw=" + encodeURIComponent(a.h.em) + "&auth=" + a.h.wq + "&flash_debug=" + ("1" === eb().flash_debug ? "1" : "0");
        a = document.getElementById(this.Wd);
        null === a ? w("id " + this.Wd + " for div not found for flash player in " + this.constructor.name, {}, "VIDEO_") : (a.appendChild(nn(this)),
        swfobject.embedSWF(this.oq.toString(), this.Wd, "100%", "100%", this.Bp, void 0, void 0, this.wd, this.attributes),
        u(this.Tc, mn(this)),
        on(this))
    }
    ;
    function nn(a) {
        var b = document.createElement("div");
        b.style.textAlign = "center";
        b.style.marginTop = "50px";
        b.style.zIndex = "999";
        b.style.position = "relative";
        var c = document.createElement("a");
        c.href = "https://www.adobe.com/go/getflashplayer";
        c.target = "_blank";
        c.style.border = "0";
        b.appendChild(c);
        var d = document.createElement("img");
        d.alt = "Get Adobe Flash player";
        d.src = "https://ssl-ccstatic.highwebmedia.com/theatermodeassets/160x41_Get_Flash_Player.jpg";
        c.appendChild(d);
        c = document.createElement("p");
        c.appendChild(document.createTextNode("We've detected that you are missing the latest version of Flash Player."));
        c.style.margin = "0";
        b.appendChild(c);
        c = document.createElement("p");
        c.appendChild(document.createTextNode("This version of the Chaturbate Viewer requires Flash Player " + a.Bp + " or later."));
        c.style.margin = "0";
        b.appendChild(c);
        a = document.createElement("a");
        a.href = "https://www.adobe.com/go/getflashplayer";
        a.innerText = "Click here to update your Flash Player for free";
        b.appendChild(a);
        return b
    }
    function on(a) {
        var b = a.muted ? 0 : a.volume
          , c = window[a.Wd];
        void 0 !== c && (void 0 === c.SetCamVol ? setTimeout(function() {
            on(a)
        }, 100) : (a.pb.ih(a.volume),
        a.pb.ri(a.muted),
        void 0 !== a.R && (a.R.ih(a.volume),
        a.R.ri(a.muted)),
        c.SetCamVol(b)))
    }
    k.Xg = function(a) {
        this.volume !== a && (this.volume = a,
        this.muted || on(this))
    }
    ;
    k.Cg = function() {
        return this.muted ? 0 : this.volume
    }
    ;
    k.Fd = function(a) {
        this.muted !== a && (this.muted = a,
        on(this))
    }
    ;
    k.Yg = function(a, b) {
        if (this.volume !== a || this.muted !== b)
            this.volume = a,
            this.muted = b,
            on(this)
    }
    ;
    k.ue = function(a) {
        var b = window[this.Wd];
        void 0 === b ? w("Cannot find movie to set quality level.", {}, "VIDEO_") : void 0 === b.SetQualityLevel ? w("SetQualityLevel is not available on this player.", {}, "VIDEO_") : (b.SetQualityLevel(a),
        u(this.Tc, mn(this)))
    }
    ;
    function mn(a) {
        var b = window[a.Wd];
        if (!a.Lk)
            return [];
        if (void 0 === b)
            return w("Cannot find movie to get quality level.", {}, "VIDEO_"),
            [];
        if (void 0 === b.GetQualityLevels)
            return w("GetQualityLevel is not available on this player.", {}, "VIDEO_"),
            [];
        var c = [];
        b.GetQualityLevels().forEach(function(a) {
            c.push({
                label: a.label,
                eh: a.toggled,
                value: a.value
            })
        });
        return c
    }
    k.stop = function() {
        swfobject.removeSWF(this.Wd);
        this.Lk = !1;
        ln(this)
    }
    ;
    k.paused = function() {
        return !1
    }
    ;
    k.Gd = function() {
        void 0 !== this.R && this.R.A();
        this.pb.show();
        $m(this)
    }
    ;
    k.ae = function() {
        void 0 !== this.R && this.R.show();
        this.pb.A();
        $m(this)
    }
    ;
    function $m(a) {
        if (T() && T() === a.a)
            if (a.ol)
                a.Ka.style.height = "calc(100% - " + a.pb.Mb() + "px)",
                a.Ka.style.width = "100%",
                a.Ka.style.left = "0px",
                a.Ka.style.top = "0px";
            else {
                var b = window.innerWidth
                  , c = window.innerHeight - a.Mb()
                  , d = c / (a.rl ? .5625 : .75);
                b < d && (d = b,
                c = d * (a.rl ? .5625 : .75));
                b = [d, c];
                a.Ka.style.width = b[0] + "px";
                a.Ka.style.left = (window.innerWidth - b[0]) / 2 + "px";
                a.Ka.style.height = b[1] + "px";
                a.Ka.style.top = (window.innerHeight - (b[1] + a.pb.Mb())) / 2 + "px"
            }
        else
            a.Ka.style.height = "calc(100% - " + a.pb.Mb() + "px)",
            a.Ka.style.width = "100%",
            a.Ka.style.left = "0px",
            a.Ka.style.top = "0px"
    }
    k.C = function() {
        $m(this)
    }
    ;
    k.Ef = function() {
        var a = this;
        this.Np();
        setTimeout(function() {
            $m(a);
            a.Y()
        }, 500)
    }
    ;
    k.Mb = function() {
        return this.pb.Mb()
    }
    ;
    k.Zj = function(a) {
        this.R = a
    }
    ;
    k.fi = function() {
        return this.uj || !!T()
    }
    ;
    k.Np = function() {
        !0 !== !!T() ? (Hh(this.a),
        this.Gd(),
        null === this.Ka.style.width && (this.Ka.style.left = "0px")) : (Ih(),
        this.Ka.style.height = "calc(100% - " + this.pb.Mb() + "px)",
        this.Ka.style.width = "100%",
        this.Y())
    }
    ;
    function pn(a) {
        v.call(this);
        var b = this;
        this.Gb = a;
        this.ah = !0;
        this.fk = !1;
        this.Tc = new n("possibleQualityLevelsChanged");
        this.ze = new n("videoOfflineChange");
        this.sl = 0;
        this.xj = !1;
        this.ek = this.muted = !0;
        this.url = "";
        this.wj = 0;
        this.Fa = new Aa;
        this.pl = !0;
        gn(new fn("JpegPushPlayer"));
        this.a.style.background = "#333333 url(https://ssl-ccstatic.highwebmedia.com/theatermodeassets/cam_notice_background.jpg) center center / cover";
        this.a.className = "videoPlayerDiv";
        this.a.style.position = "static";
        this.Gc = document.createElement("img");
        this.Gc.style.width = "100%";
        this.Gc.style.height = "100%";
        this.Gc.style.cs = "contain";
        this.Gc.onload = function() {
            b.Gc.style.display = "inline";
            b.xj = !1
        }
        ;
        this.Gc.onerror = function() {
            b.Gc.style.display = "none";
            b.wj -= 100;
            b.xj = !1
        }
        ;
        this.a.appendChild(this.Gc);
        this.requestAnimationFrame = Ua();
        qn(this, 0)
    }
    m(pn, v);
    function rn(a) {
        a.wj = Date.now();
        void 0 !== a.fa && A("get_edge_hls_url_ajax/", {
            room_slug: a.fa,
            jpeg: "1"
        }).then(function(b) {
            var c = new D(b.responseText)
              , d = J(c, "success");
            "offline" === dm(H(c, "room_status")) ? (u(a.ze, !0),
            a.stop()) : (u(a.ze, !1),
            c.Qc("url"),
            c.Qc("hidden_message"),
            L(c),
            d ? a.url = H(c, "cbjpeg_url", !1) : w("Unable to refresh JPEG stream", {
                error: b.responseText
            }, "VIDEO_"))
        })["catch"](function(a) {
            w("Unable to refresh JPEG stream", {
                error: a
            }, "VIDEO_")
        })
    }
    function qn(a, b) {
        5E3 < Date.now() - a.wj && rn(a);
        a.ek || "" === a.url ? "data:image/gif;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs%3D" !== a.Gc.src && (a.Gc.src = "data:image/gif;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs%3D") : (0 === a.sl || 140 < b - a.sl) && !a.xj && (a.xj = !0,
        a.sl = b,
        a.Gc.src = a.url + "&f=" + Math.random());
        a.requestAnimationFrame.call(window, function(b) {
            qn(a, b)
        })
    }
    k = pn.prototype;
    k.ej = function(a) {
        var b = this;
        this.ek = !1;
        Da(this.Fa);
        za(q(a.j.event.kb, function(a) {
            void 0 !== b.Gb.be.get(a.Aa) || b.Mc ? b.stop() : b.ek = !1
        }), this.Fa);
        this.fa = a.h.i;
        this.url = "https://cbjpeg.stream.highwebmedia.com/stream?room=" + this.fa;
        void 0 !== this.R && this.R.show()
    }
    ;
    k.Xg = function(a) {
        this.volume = a
    }
    ;
    k.Cg = function() {
        return this.muted ? 0 : this.volume
    }
    ;
    k.Fd = function(a) {
        this.muted = a
    }
    ;
    k.stop = function() {
        this.Gc.src = "data:image/gif;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs%3D";
        this.ek = !0
    }
    ;
    k.ue = function() {}
    ;
    k.Gd = function() {
        this.pl = !1;
        void 0 !== this.R && this.R.show()
    }
    ;
    k.ae = function() {
        this.pl = !0;
        void 0 !== this.R && this.R.show()
    }
    ;
    k.fi = function() {
        return this.pl
    }
    ;
    k.Mb = function() {
        return 0
    }
    ;
    k.Ef = function() {
        Hh(this.Gc)
    }
    ;
    k.Yg = function() {}
    ;
    k.Zj = function(a) {
        this.R = a
    }
    ;
    k.Bi = function() {
        this.stop();
        Da(this.Fa)
    }
    ;
    function sn(a, b) {
        v.call(this);
        this.Gb = a;
        this.fk = this.ah = !0;
        this.L = document.createElement("video");
        this.Re = !1;
        this.Ge = document.createElement("div");
        this.uh = [];
        this.Tc = new n("possibleQualityLevelsChanged");
        this.ze = new n("videoOfflineChange");
        this.Mc = this.ml = !1;
        this.a.style.position = "static";
        this.a.style.background = "#333333 url(https://ssl-ccstatic.highwebmedia.com/theatermodeassets/cam_notice_background.jpg) center center / cover";
        this.a.className = "videoPlayerDiv";
        this.L.setAttribute("webkit-playsinline", "");
        this.L.setAttribute("playsinline", "");
        this.L.setAttribute("autoplay", "");
        this.L.style.margin = "0";
        this.L.style.padding = "0";
        this.L.style.width = "100%";
        this.L.style.height = "100%";
        this.L.style["object-fit"] = "contain";
        Ka() && (this.L.style.backgroundColor = "rgba(0, 0, 0, 0.0)");
        this.a.appendChild(this.L);
        window.videoElement = this.L;
        this.Ge.style.backgroundColor = "#000000";
        this.Ge.style.opacity = "0.6";
        this.Ge.style.position = "absolute";
        this.Ge.style.width = "inherit";
        this.Ge.style.height = "inherit";
        this.xk = new hn(b);
        gn(this.xk)
    }
    m(sn, v);
    function tn(a, b) {
        return new Promise(function(c, d) {
            if (b.h.Gh !== a.io && void 0 !== b.h.Gh && "" !== b.h.Gh) {
                a.io = b.h.Gh;
                var e = b.j.status;
                a.Mc && (e = "offline");
                c([un(b.h.Gh), e])
            } else
                A("get_edge_hls_url_ajax/", {
                    room_slug: b.h.i,
                    bandwidth: "high",
                    current_edge: ""
                }).then(function(b) {
                    var e = JSON.parse(b.responseText);
                    e.success ? (a.io = e.url,
                    c([un(e.url), dm(e.room_status)])) : d(b.responseText)
                })["catch"](d)
        }
        )
    }
    function un(a) {
        var b = eb();
        return void 0 !== b.edge ? a.replace(/(^https:\/\/edge)\d*(.+)/, "$1" + b.edge + "$2") : a
    }
    function vn(a, b) {
        a.stop();
        b.h.i === a.i && tn(a, b).then(function(c) {
            var d = l(c);
            c = d.next().value;
            d = d.next().value;
            "offline" === a.Xa && d !== a.Xa && (a.Re = !0);
            a.Mc = "offline" === d;
            u(a.ze, a.Mc);
            "notconnected" !== a.Xa && "offline" === d ? wn(a, b) : a.mo(b, c)
        })["catch"](function(a) {
            w("Unable to refresh HLS stream", {
                error: a
            })
        })
    }
    k = sn.prototype;
    k.play = function() {
        var a = this
          , b = this.Xh.play();
        void 0 !== b && b["catch"](function(c) {
            "NotAllowedError" === c.name ? (u(Ej, void 0),
            a.L.muted || a.Fd(!0),
            b = a.Xh.play(),
            void 0 !== b && b["catch"](function(b) {
                a.Re || r("Unable to play twice in a row", {
                    err: b
                }, "VIDEO_");
                a.gl()
            })) : a.L.paused && 0 === a.L.currentTime && (N("NoPromiseAutoplayPolicy"),
            u(Ej, void 0),
            a.L.muted || a.Fd(!0),
            a.Xh.play())
        })
    }
    ;
    k.stop = function() {
        void 0 !== this.Gb.be.get(this.Xa) && (xn(this),
        yn(this));
        this.Re && yn(this)
    }
    ;
    function yn(a) {
        for (var b = l(a.uh), c = b.next(); !c.done; c = b.next())
            clearTimeout(c.value);
        clearTimeout(a.Hj);
        a.Hj = void 0;
        clearTimeout(a.Wf);
        a.Wf = void 0
    }
    k.Xg = function(a) {
        this.L.volume = a / 100
    }
    ;
    k.Cg = function() {
        return this.L.muted ? 0 : 100 * this.L.volume
    }
    ;
    k.Gd = function() {
        this.ml = !1;
        void 0 !== this.R && this.R.A();
        this.L.setAttribute("controls", "")
    }
    ;
    k.ae = function() {
        this.ml = !0;
        this.L.removeAttribute("controls");
        void 0 !== this.R && this.R.show()
    }
    ;
    k.Yg = function() {}
    ;
    k.Ef = function() {}
    ;
    k.Zj = function(a) {
        this.R = a
    }
    ;
    k.Mb = function() {
        return 0
    }
    ;
    k.gl = function() {
        zn(this)
    }
    ;
    k.ej = function(a) {
        var b = this;
        this.Mc = !1;
        this.L.poster = "https://cbjpeg.stream.highwebmedia.com/stream?room=" + a.h.i + "&f=" + Math.random();
        this.i = a.h.i;
        q(a.j.event.kb, function(c) {
            b.Xa = c.Aa;
            if (void 0 !== b.Gb.be.get(c.Aa) || b.Mc)
                b.stop(),
                zn(b),
                b.Mc ? wn(b, a) : An(b, a);
            else
                switch (clearTimeout(b.Wf),
                b.Wf = void 0,
                c.Vc) {
                case "notconnected":
                case "grouprequesting":
                case "privaterequesting":
                    break;
                default:
                    switch (c.Aa) {
                    case "notconnected":
                        Bn(b, a.h.i);
                        setTimeout(function() {
                            vn(b, a)
                        }, 0);
                        break;
                    case "grouprequesting":
                    case "privaterequesting":
                        switch (c.Vc) {
                        case "groupnotwatching":
                            b.uh.push(setTimeout(function() {
                                Bn(b, a.h.i)
                            }, 0)),
                            b.uh.push(setTimeout(function() {
                                vn(b, a)
                            }, 3E3))
                        }
                        break;
                    default:
                        b.uh.push(setTimeout(function() {
                            Bn(b, a.h.i)
                        }, 0)),
                        b.uh.push(setTimeout(function() {
                            vn(b, a)
                        }, 3E3))
                    }
                }
        })
    }
    ;
    function Bn(a, b) {
        a.a.style.background = "#333333 url(https://cbjpeg.stream.highwebmedia.com/stream?room=" + b + "&f=" + Math.random() + ") center center / cover";
        var c = a.Gb;
        c.Z.innerText = "Connecting";
        c.text.innerText = "Please wait while we connect you to this webcam broadcast.";
        c.a.style.display = "block";
        c.ac.style.display = "none";
        c.Ng.style.display = "none";
        c.Kh = !0;
        a.a.insertBefore(a.Ge, a.a.firstChild);
        a.Re = !0
    }
    function xn(a) {
        a.Re = !1;
        var b = a.Gb;
        b.Kh && (b.Z.innerText = "",
        b.text.innerText = "",
        b.a.style.display = "none",
        b.Kh = !1);
        a.Ge.parentElement === a.a && a.a.removeChild(a.Ge);
        a.a.style.background = "#333333 url(https://ssl-ccstatic.highwebmedia.com/theatermodeassets/cam_notice_background.jpg) center center / cover"
    }
    function An(a, b) {
        void 0 === a.Wf && (a.Wf = setTimeout(function() {
            tn(a, b).then(function(c) {
                c = l(c);
                c.next();
                "offline" === c.next().value ? (a.Mc = !0,
                a.Wf = void 0,
                wn(a, b),
                u(a.ze, a.Mc)) : (a.Wf = void 0,
                An(a, b))
            })
        }, 1E4))
    }
    function wn(a, b) {
        void 0 === a.Hj && (a.Hj = setTimeout(function() {
            a.Hj = void 0;
            vn(a, b)
        }, 1E4))
    }
    function Cn(a, b, c) {
        c = void 0 === c ? function() {}
        : c;
        zn(a);
        a.gh = setTimeout(function() {
            Xh("Video seems to be stuck, refreshing");
            vn(a, b);
            c()
        }, 1E4)
    }
    function zn(a) {
        void 0 !== a.gh && (clearTimeout(a.gh),
        a.gh = void 0)
    }
    k.fi = function() {
        return this.ml
    }
    ;
    function Dn(a) {
        sn.call(this, a, "HlsNativePlayer");
        this.Te = 0;
        this.Xh = this.L;
        this.a.appendChild(this.L);
        jn(this.xk, this.L)
    }
    m(Dn, sn);
    k = Dn.prototype;
    k.mo = function(a, b) {
        var c = this;
        Cn(this, a);
        this.L.ontimeupdate = function() {
            var b = c.L.currentTime;
            5 < b - c.Te && (c.Te = b,
            Cn(c, a, function() {
                c.Te = 0
            }))
        }
        ;
        this.L.onclick = function() {
            c.play()
        }
        ;
        this.L.onactivate = function() {}
        ;
        this.L.onpause = function() {}
        ;
        this.L.onplay = function() {}
        ;
        this.L.oncanplay = function() {
            c.Re && xn(c)
        }
        ;
        this.L.oncanplaythrough = function() {
            c.play()
        }
        ;
        this.L.onseeking = function() {}
        ;
        this.L.onerror = function(a) {
            w(a, {}, "VIDEO_")
        }
        ;
        this.L.onvolumechange = function() {
            var a = c.L.volume
              , b = c.L.muted;
            0 === a && (b = !0);
            void 0 !== c.R && (c.R.pi(b),
            c.R.Gm(100 * a))
        }
        ;
        "WebKitPlaybackTargetAvailabilityEvent"in window && (this.L.onwebkitcurrentplaybacktargetiswirelesschanged = function() {
            c.L.webkitCurrentPlaybackTargetIsWireless ? N("Video_CastStart", {
                CastType: "airplay"
            }) : N("Video_CastStop", {
                CastType: "airplay"
            })
        }
        );
        "" !== b ? this.L.src = b : this.L.removeAttribute("src");
        this.L.load();
        this.L.style.display = "inline";
        "oncanplaythrough"in window || this.play();
        "" === b && (this.stop(),
        zn(this))
    }
    ;
    k.Xg = function(a) {
        this.L.volume = a / 100
    }
    ;
    k.Cg = function() {
        return this.L.muted ? 0 : 100 * this.L.volume
    }
    ;
    k.Fd = function(a) {
        this.L.muted = a;
        void 0 !== this.R && this.R.pi(a);
        a ? this.L.setAttribute("muted", "") : this.L.removeAttribute("muted")
    }
    ;
    k.stop = function() {
        sn.prototype.stop.call(this);
        this.Te = 0;
        this.L.style.display = "none";
        this.L.pause();
        this.L.removeAttribute("src");
        this.L.load()
    }
    ;
    k.$d = function() {
        this.L.setAttribute("controls", "")
    }
    ;
    k.Eh = function() {
        this.L.removeAttribute("controls")
    }
    ;
    k.Gd = function() {
        this.$d();
        void 0 !== this.R && this.R.A()
    }
    ;
    k.ae = function() {
        this.Eh();
        void 0 !== this.R && this.R.show()
    }
    ;
    k.ue = function() {}
    ;
    k.Yg = function(a, b) {
        this.L.volume = a / 100;
        0 === a || b ? (this.L.muted = !0,
        this.L.setAttribute("muted", "")) : (this.L.muted = !1,
        this.L.removeAttribute("muted"))
    }
    ;
    k.Mb = function() {
        return 0
    }
    ;
    k.Ef = function() {
        Hh(this.L);
        this.Gd()
    }
    ;
    k.Np = function() {
        this.Ef()
    }
    ;
    var En = window.videojs;
    function Fn(a) {
        sn.call(this, a, "VideoJsPlayer");
        var b = this;
        this.Og = !1;
        this.Te = 0;
        this.na = !0;
        this.Hl = !1;
        this.L.className = "video-js vjs-default-skin";
        En.log.level("error");
        var c = this;
        this.a.onclick = function() {
            b.Hl && void 0 !== b.R && (b.fi() ? b.ae() : b.Gd(),
            b.Hl = !1)
        }
        ;
        var d = En(this.L, {
            autoplay: !0,
            html5: {
                hlsjsConfig: {
                    debug: !1,
                    liveSyncDuration: 3,
                    liveSyncDurationCount: void 0,
                    liveDurationInfinity: Infinity,
                    liveMaxLatencyDuration: 7,
                    liveMaxLatencyDurationCount: void 0,
                    liveBackBufferLength: 30
                }
            },
            controlBar: {
                playToggle: !1,
                progressControl: !1,
                remainingTimeDisplay: !1,
                durationDisplay: !1,
                currentTimeDisplay: !1,
                timeDivider: !1
            }
        });
        this.ja = d;
        if (void 0 !== d) {
            jn(this.xk, d);
            void 0 !== En.Html5Hlsjs && En.Html5Hlsjs.addHook("beforeinitialize", function(a, c) {
                a === d && b.xk.jq(c)
            });
            this.Xh = d;
            d.on("playing", function() {
                void 0 !== c.gh && (clearTimeout(c.gh),
                c.gh = void 0)
            });
            d.on("volumechange", function() {
                c.Xg(c.Cg())
            });
            d.on("canplay", function() {
                c.play();
                c.Re && xn(c)
            });
            d.on("canplaythrough", function() {
                c.play()
            });
            var e = this.Tc;
            d.on("updateVideoControlQuality", function(a, b) {
                for (var c = [], d = l(b), f = d.next(); !f.done; f = d.next())
                    f = f.value,
                    c.push({
                        label: f.label,
                        eh: f.selected,
                        value: f.id
                    });
                u(e, c)
            })
        }
        d.controls(this.Og);
        window.videoJsPlayer = d
    }
    m(Fn, sn);
    k = Fn.prototype;
    k.mo = function(a, b) {
        var c = this;
        if ("" === b)
            this.stop();
        else {
            Cn(this, a);
            var d = this;
            void 0 !== this.ja ? (this.ja.off("waiting"),
            this.ja.on("waiting", function() {
                void 0 !== d && Cn(d, a)
            }),
            this.ja.on("timeupdate", function() {
                if (void 0 !== d.ja) {
                    var b = d.ja.currentTime();
                    5 < b - d.Te && (d.Te = b,
                    Cn(d, a, function() {
                        d.Te = 0
                    }))
                }
            }),
            this.ja.off("error"),
            this.ja.on("error", function(b) {
                if (void 0 !== d.ja) {
                    var c = d.ja.error();
                    null === c || 2 !== c.code && 4 !== c.code || (d.Re || vn(d, a),
                    w(c.message, b, "VIDEO_"))
                } else
                    w("Videojs error type undefined", b, "VIDEO_")
            }),
            this.ja.src({
                src: b,
                type: "application/x-mpegURL"
            }),
            this.ja.qualityPickerPlugin(),
            this.ja.show(),
            this.ja.muted(this.na),
            this.Xh = this.ja,
            this.L.style.display = "inline",
            this.Br = this.ja.tech(),
            this.Br.on("loadedqualitydataTS", function(a, b) {
                for (var d = [], e = l(b.qualityData.video), f = e.next(); !f.done; f = e.next())
                    f = f.value,
                    d.push({
                        label: f.label,
                        eh: f.selected,
                        value: f.id
                    });
                u(c.Tc, d)
            })) : U("something went wrong")
        }
    }
    ;
    k.Xg = function(a) {
        void 0 !== this.R && void 0 !== this.ja && (this.na = 0 === a || this.ja.muted(),
        this.R.Gm(a),
        this.R.pi(this.na))
    }
    ;
    k.Fd = function(a) {
        this.na = a;
        void 0 !== this.R && this.R.pi(a);
        void 0 !== this.ja && this.ja.muted(a)
    }
    ;
    k.Yg = function(a, b) {
        this.na = b;
        void 0 !== this.ja && (this.ja.volume(a / 100),
        this.ja.muted(b))
    }
    ;
    k.Cg = function() {
        return void 0 !== this.ja ? this.ja.muted() ? 0 : 100 * this.ja.volume() : 0
    }
    ;
    k.stop = function() {
        sn.prototype.stop.call(this);
        void 0 !== this.ja && (this.ja.reset(),
        this.ja.hide())
    }
    ;
    k.ue = function(a) {
        void 0 !== this.ja && this.ja.selectQualityButton(a)
    }
    ;
    k.gl = function() {
        sn.prototype.gl.call(this);
        void 0 !== this.R && (this.R.show(),
        this.Hl = !0)
    }
    ;
    k.Gd = function() {
        this.Og = !0;
        void 0 !== this.R && this.R.A();
        void 0 !== this.ja && this.ja.controls(this.Og)
    }
    ;
    k.ae = function() {
        this.Og = !1;
        void 0 !== this.R && this.R.show();
        void 0 !== this.ja && this.ja.controls(this.Og)
    }
    ;
    k.Ef = function() {
        void 0 !== this.ja && (this.ja.requestFullscreen(),
        this.Gd())
    }
    ;
    k.Mb = function() {
        return 0
    }
    ;
    k.fi = function() {
        return !this.Og
    }
    ;
    function Gn(a, b) {
        v.call(this);
        this.Gg = a;
        this.Gb = b;
        var c = Hn(this);
        switch (c) {
        case "flash":
            this.Gg = !0;
            this.M = this.N(new kn);
            va("player", "flash");
            break;
        case "videojs":
            this.M = this.N(new Fn(this.Gb));
            va("player", "videojs");
            break;
        case "hlsnative":
            this.M = this.N(new Dn(this.Gb));
            va("player", "hlsnative");
            break;
        case "jpeg":
            this.Gg = !1;
            this.M = this.N(new pn(this.Gb));
            va("player", "jpeg");
            break;
        default:
            w("received unexpected player name: " + c),
            U("unknown player: " + c)
        }
        this.Gb.Pg = this.M.ah;
        In(this.Gb, this.M.ze);
        this.ic = document.createElement("img");
        this.ic.src = "https://ssl-ccstatic.highwebmedia.com/theatermodeassets/logo.svg";
        this.ic.className = "cbLogo";
        this.ic.style.position = "fixed";
        this.ic.style.width = "90px";
        this.ic.style.top = "8px";
        this.ic.style.right = "8px";
        this.a.appendChild(this.ic)
    }
    m(Gn, v);
    function Jn() {
        return (void 0 === navigator.userAgent ? 0 : /android/i.test(navigator.userAgent)) || !Ta() ? "videojs" : "hlsnative"
    }
    function Hn(a) {
        var b = eb().player;
        if (void 0 !== b)
            return "hls" === b && (b = Jn()),
            b;
        if (b = Fm)
            a: {
                b = navigator.userAgent.match(/Firefox\/([0-9]+)/);
                if (null !== b && 2 === b.length && (b = parseInt(b[1]),
                !isNaN(b) && 55 <= b)) {
                    b = !0;
                    break a
                }
                b = !1
            }
        return b || 0 >= swfobject.getFlashPlayerVersion().major ? Ta() || Ia.isSupported() ? a.Gg ? Jn() : "jpeg" : (cn({
            eventName: "jpegFallback",
            hlsNativeSupported: Ta(),
            hlsJsSupported: bn.isSupported()
        }),
        "jpeg") : "flash"
    }
    function Kn(a, b, c) {
        a.Gg = !0;
        a.removeChild(a.M);
        var d = "hlsnative" === Jn() ? new Dn(a.Gb) : new Fn(a.Gb);
        y("loadeddata", d.L, function() {
            d.Fd(!1)
        });
        d.ej(b);
        vn(d, b);
        d.Xg(100);
        d.Fd(!1);
        d.Zj(c);
        d.N(c);
        a.M.fi() ? d.ae() : d.Gd();
        a.M = Ea(a, d)
    }
    function Ln(a, b, c) {
        var d = b / (c ? .5625 : .75);
        d > a && (d = a,
        b = d * (c ? .5625 : .75));
        return [d, b]
    }
    ;var Mn = new Set;
    function Nn() {
        for (var a = [], b = l(Mn), c = b.next(); !c.done; c = b.next())
            a.push(c.value);
        x() && window.localStorage.setItem("ignorelist", JSON.stringify({
            ye: a
        }))
    }
    function On(a) {
        try {
            Mn = new Set(JSON.parse(a).users)
        } catch (b) {
            w("Cannot parse ignore list: " + b)
        }
    }
    function Pn() {
        Qn();
        (0 === Mn.size || Rn()) && z("api/ignored_user_list/").then(function(a) {
            x() && (window.localStorage.setItem("ignorelist", a.responseText),
            window.localStorage.setItem("ignorelistlastfetch", Date.now().toString()));
            On(a.responseText)
        })["catch"](function(a) {
            w("Network error retrieving ignore list: " + a)
        })
    }
    function Rn() {
        if (!x())
            return !0;
        var a = window.localStorage.getItem("ignorelistlastfetch");
        return null === a ? !0 : parseInt(a, 10) + 3E5 < Date.now()
    }
    function Qn() {
        if (x()) {
            var a = window.localStorage.getItem("ignorelist");
            null !== a && On(a)
        }
    }
    ;function Sn(a) {
        return {
            cc: 1,
            message: a
        }
    }
    ;var Tn;
    q(S, function(a) {
        Tn = a.h.i
    });
    function Un(a) {
        var b = Vn(a);
        b.Bg = G(a, "gender", !1);
        return b
    }
    function Vn(a) {
        var b = nb(a);
        1 !== b.size && w('getStrings(["from_username", "user", "username"]): ' + b + " length is not 1 in " + a.Hd);
        b = b.values().next().value;
        return {
            username: b,
            Oe: b === Tn,
            Bc: J(a, "in_fanclub"),
            je: J(a, "has_tokens"),
            Dc: J(a, "is_mod"),
            lf: J(a, "trecently"),
            kf: J(a, "tlots"),
            mf: J(a, "ttons")
        }
    }
    var Wn = {
        o: "room-owner",
        m: "moderator",
        f: "fanclub-user",
        tr: "tipped-recently-user",
        g: "standard-user",
        t: "tipped-user",
        p: "tipped-lots-user",
        l: "tipped-tons-user"
    };
    function Xn(a) {
        this.fe = this.nf = 0;
        this.ye = [];
        var b = a.split(",");
        a = ~~b[0];
        b = b.slice(1);
        var c = [];
        b = l(b);
        for (var d = b.next(); !d.done; d = b.next()) {
            var e = l(d.value.split("|"));
            d = e.next().value;
            e = e.next().value;
            e = Wn[e];
            d = {
                username: d,
                Oe: !1,
                Bc: !1,
                je: !1,
                Dc: !1,
                lf: !1,
                kf: !1,
                mf: !1
            };
            "room-owner" === e ? d.Oe = !0 : "fanclub-user" === e ? d.Bc = !0 : "tipped-user" === e ? d.je = !0 : "moderator" === e ? d.Dc = !0 : "tipped-recently-user" === e ? d.lf = !0 : "tipped-lots-user" === e ? d.kf = !0 : "tipped-tons-user" === e && (d.mf = !0);
            c.push(d)
        }
        this.nf = a + c.length;
        this.fe = a;
        this.ye = c
    }
    ;function Yn(a) {
        this.Ib = [];
        this.Mf = [];
        var b = a.match(/#[a-z0-9\-_]{2,50}/g);
        if (null !== b) {
            b = l(b);
            for (var c = b.next(); !c.done; c = b.next())
                this.Mf.push(c.value.substr(1))
        }
        this.Ib = a.split(/#[a-z0-9\-_]{2,50}/g)
    }
    ;function Zn(a) {
        return 0 === a.length ? Promise.resolve([]) : z("tags/approved/?tags=" + a.join(",")).then(function(a) {
            return a.responseText.split(",")
        })
    }
    ;var yo = {
        connected: $n,
        error: ao,
        roomMessage: bo,
        privateMessage: co,
        roomNotice: eo,
        roomEntry: fo,
        roomLeave: go,
        stateChange: ho,
        privateRequest: io,
        groupRequestUpdate: jo,
        notifyTokenUpdate: ko,
        updateRoomCount: lo,
        titleChange: mo,
        settingsUpdate: no,
        removeUserMessages: oo,
        tipAlert: po,
        soundNotice: qo,
        shuttingDown: ro,
        notifyAppTabRefresh: so,
        notifyAppClear: to,
        notifyRefreshPanel: uo,
        notifyHiddenShowMessageChange: vo,
        log: wo,
        ping: xo
    };
    function zo(a) {
        this.B = a
    }
    zo.prototype.Sl = function(a) {
        var b = H(a, "t", !1);
        if (0 <= Object.keys(yo).indexOf(b))
            yo[b]({
                B: this.B,
                p: a
            });
        else
            w("Unknown message method: " + b + " data: " + a)
    }
    ;
    function $n(a) {
        a.B.lm()
    }
    function ao(a) {
        if (J(a.p, "allow_reconnect", !0)) {
            var b = H(a.p, "errortype");
            a = H(a.p, "message", !1);
            w("Error: " + b + " Mesage: " + a)
        } else
            a.B.disconnect()
    }
    function fo(a) {
        var b = new D(pb(a.p, "profile"));
        a.B.bm(Un(b))
    }
    function go(a) {
        var b = new D(pb(a.p, "profile"));
        a.B.cm(Un(b))
    }
    function ho(a) {
        var b = H(a.p, "state");
        a.B.ma({
            offline: "offline",
            away: "away",
            "public": "public",
            hidden: "hidden",
            hidden_watching: "public",
            group_requesting: "grouprequesting",
            group_not_watching: "groupnotwatching",
            group: "groupwatching",
            private_requesting: "privaterequesting",
            private_not_watching: "privatenotwatching",
            "private": "privatewatching",
            private_spying: "privatespying",
            password_protect: "passwordprotected"
        }[b])
    }
    function bo(a) {
        var b = new D(pb(a.p, "profile"));
        b = {
            fb: Un(b),
            message: H(a.p, "msg"),
            font: G(a.p, "f", !1),
            Kd: G(a.p, "c", !1),
            backgroundColor: G(a.p, "background", !1)
        };
        a.B.ne(b.fb.username) || u(a.B.event.hg, b)
    }
    function co(a) {
        var b = new D(pb(a.p, "profile"));
        b = {
            fb: Un(b),
            message: H(a.p, "msg"),
            font: G(a.p, "f", !1),
            Kd: G(a.p, "c", !1),
            backgroundColor: G(a.p, "background", !1),
            Ll: H(a.p, "otherUsername")
        };
        a.B.ne(b.fb.username) || u(a.B.event.ag, b)
    }
    function eo(a) {
        var b = []
          , c = []
          , d = pb(a.p, "profile", !1);
        "" !== d && (d = new D(d),
        c.push({
            cc: 0,
            hc: Vn(d)
        }));
        c.push(Sn(H(a.p, "message")));
        b.push(c);
        b = {
            U: b,
            V: J(a.p, "show_in_pm", !1)
        };
        c = H(a.p, "bg", !1);
        "" !== c && (b.background = c);
        c = H(a.p, "fg", !1);
        "" !== c && (b.oa = c);
        c = H(a.p, "weight", !1);
        "" !== c && (b.weight = c);
        u(a.B.event.P, b)
    }
    function mo(a) {
        function b(b) {
            for (var e = 0; e < f.Ib.length; e += 1) {
                var h = Sn(f.Ib[e]);
                h.Ml = !1;
                d.push(h);
                e < f.Mf.length && (h = f.Mf[e],
                d.push(-1 !== b.indexOf(h) ? {
                    cc: 2,
                    message: h
                } : Sn("#" + h)))
            }
            u(a.B.event.P, {
                U: [d],
                oa: "#DC5500",
                weight: "bold",
                V: !1
            });
            u(a.B.event.nk, c)
        }
        var c = H(a.p, "title")
          , d = []
          , e = Cb(c)
          , f = new Yn(e);
        Zn(f.Mf).then(b)["catch"](function() {
            b([])
        })
    }
    function io(a) {
        var b = new D(pb(a.p, "profile"));
        u(a.B.event.Ql, {
            Er: H(b, "username"),
            Dm: K(a.p, "tokens_per_minute")
        })
    }
    function jo(a) {
        u(a.B.event.Eg, {
            Ld: K(a.p, "users_waiting"),
            pg: K(a.p, "users_required"),
            Dm: K(a.p, "tokens_per_minute", !1)
        })
    }
    function ko(a) {
        u(a.B.event.ni, {
            Sb: K(a.p, "tokens")
        })
    }
    function lo(a) {
        u(a.B.event.Tg, K(a.p, "count"))
    }
    function no(a) {
        var b = a.p
          , c = {
            kd: J(b, "allow_privates"),
            Wa: K(b, "private_price"),
            wb: K(b, "spy_price"),
            Ze: K(b, "private_min_minutes"),
            vf: J(b, "allow_show_recordings"),
            jd: J(b, "allow_groups"),
            Ta: K(b, "group_price"),
            Bl: K(b, "minimum_users_for_group_show")
        };
        L(b);
        u(a.B.event.Zd, c);
        a.B.Wa !== c.Wa && (a.B.Wa = c.Wa,
        b = Sn(Hb(a.B.i(), c.Wa)),
        u(a.B.event.P, {
            U: [[b]],
            V: !0
        }));
        a.B.Ta !== c.Ta && (a.B.Ta = c.Ta,
        b = Sn(Gb(a.B.i(), c.Ta)),
        u(a.B.event.P, {
            U: [[b]],
            V: !0
        }));
        a.B.wb !== c.wb && 0 !== c.wb && (a.B.wb = c.wb,
        c = Sn(Ib(a.B.i(), c.wb)),
        u(a.B.event.P, {
            U: [[c]],
            V: !0
        }))
    }
    function oo(a) {
        u(a.B.event.Rg, {
            username: H(a.p, "username")
        })
    }
    function so(a) {
        u(a.B.event.Ck, {})
    }
    function to(a) {
        u(a.B.event.Qi, {})
    }
    function uo(a) {
        u(a.B.event.Rj, {})
    }
    function vo(a) {
        u(a.B.event.hj, H(a.p, "hidden_message"))
    }
    function po(a) {
        var b = new D(pb(a.p, "profile"));
        b = {
            fb: Un(b),
            message: H(a.p, "message", !1),
            La: K(a.p, "amount")
        };
        a.B.i() !== E(a.p, "to_username") && w("Username does not match in tip");
        a.B.qb && u(a.B.event.ik, b)
    }
    function wo(a) {
        void 0 === G(a.p, "msg") && w("handleLog with an empty msg")
    }
    function xo(a) {
        a.B.ei({
            t: "pong"
        })
    }
    function qo(a) {
        var b = H(a.p, "sound");
        "" !== b ? u(a.B.event.re, b) : w("Sound is missing for soundNotice")
    }
    function ro(a) {
        var b = H(a.p, "migrate_to");
        "" === b ? w("Missing migrateTo server for shutdownhandler") : (a = a.B,
        a.Wk = b,
        Ao(a),
        a.$e(b))
    }
    ;function Bo() {
        this.Ag = 0
    }
    function Co(a) {
        a.Ag += 1;
        return Math.min(1E4, 500 * a.Ag)
    }
    Bo.prototype.reset = function() {
        this.Ag = 0
    }
    ;
    function Do(a) {
        this.d = a;
        this.status = "offline";
        this.gd = void 0;
        this.lg = !0;
        this.qb = this.mc = !1;
        this.event = {
            Sf: new n("messageSent"),
            hg: new n("roomMessage"),
            P: new n("roomNotice"),
            ag: new n("privateMessage"),
            kb: new n("statusChange",{
                sd: 15
            }),
            hj: new n("hiddenMessageChange"),
            Rj: new n("refreshPanel"),
            nk: new n("titleChange"),
            Qi: new n("clearApp"),
            Rg: new n("removeMessages"),
            Zd: new n("settingsUpdate"),
            Tg: new n("roomCountUpdate"),
            Cl: new n("modStatusChange"),
            ni: qj,
            Ql: new n("privateShowRequest"),
            Eg: new n("groupShowUpdate"),
            re: new n("playSound"),
            Ck: new n("appTabRefresh"),
            ik: new n("tipAlert")
        };
        this.qb = this.username() === this.i();
        this.cg = new Bo;
        this.Al = new zo(this);
        this.ob = 0 === a.rh.indexOf("__anonymous__");
        !1 === this.ob && Pn();
        this.Wk = a.host;
        this.$e(a.host);
        this.mc = a.mc;
        this.zd = a.qa.zd;
        this.Bd = a.qa.Bd;
        this.Wa = a.Wa;
        this.wb = a.wb;
        this.Ta = a.Ta;
        this.zp = this.qb ? "public" : a.Xa;
        this.ma("notconnected")
    }
    k = Do.prototype;
    k.disconnect = function() {
        this.lg = !1;
        Ao(this)
    }
    ;
    function Ao(a) {
        void 0 !== a.gd && (a.gd.close(),
        a.gd = void 0)
    }
    k.ma = function(a) {
        if (a !== this.status) {
            var b = this.status;
            this.status = a;
            u(this.event.kb, {
                Vc: b,
                Aa: this.status
            })
        }
    }
    ;
    k.Op = function(a, b) {
        this.zd = a;
        this.Bd = b
    }
    ;
    k.al = function() {
        var a = this;
        if ("offline" === this.status || "notconnected" === this.status || "unknown" === this.status)
            return new Promise(function(a) {
                a({
                    nf: 0,
                    fe: 0,
                    ye: []
                })
            }
            );
        if (void 0 !== this.Cd)
            return this.Cd;
        var b = {
            roomname: this.i(),
            "private": Eo(this)
        };
        return this.Cd = new Promise(function(c, d) {
            z("api/getchatuserlist/?" + db(b)).then(function(b) {
                b = new Xn(b.responseText);
                c(b);
                a.Cd = void 0
            })["catch"](function(b) {
                d(b);
                a.Cd = void 0
            })
        }
        )
    }
    ;
    function Eo(a) {
        switch (a.status) {
        case "groupwatching":
        case "privatewatching":
            return !0;
        case "privaterequesting":
        case "privatenotwatching":
        case "passwordprotected":
        case "grouprequesting":
        case "groupnotwatching":
        case "public":
        case "away":
        case "hidden":
        case "offline":
        case "privatespying":
        case "notconnected":
            return !1;
        default:
            return r("unexpected status: " + a.status),
            !1
        }
    }
    k.$e = function(a) {
        var b = this, c;
        if (0 < a.length) {
            var d = new SockJS(this.Wk + "/connect/");
            this.gd = d;
            d.onclose = function(d) {
                Fo("Connection closed:" + JSON.stringify(d));
                void 0 !== c ? u(b.event.P, {
                    U: [[Sn("Trying to reconnect")]],
                    V: !0
                }) : (u(b.event.P, {
                    U: [[Sn("Chat disconnected")]],
                    V: !0
                }),
                b.lg && a === b.Wk && (c = setTimeout(function() {
                    u(b.event.P, {
                        U: [[Sn("Trying to reconnect")]],
                        V: !0
                    });
                    b.$e(a)
                }, Co(b.cg))))
            }
            ;
            d.onerror = function(d) {
                Fo("Connection error:" + JSON.stringify(d));
                void 0 === c && b.lg && (c = setTimeout(function() {
                    b.$e(a)
                }, Co(b.cg)))
            }
            ;
            d.onmessage = function(a) {
                b.Al.Sl(new D(a.data))
            }
            ;
            d.onopen = function() {
                d.send(JSON.stringify({
                    t: "connect",
                    username: b.username(),
                    password: b.d.Nm,
                    room: b.i(),
                    room_password: b.d.em
                }))
            }
        }
    }
    ;
    k.lm = function() {
        u(this.event.P, {
            U: [[Sn("Connection established")]],
            V: !0
        });
        this.cg.reset()
    }
    ;
    k.ei = function(a) {
        a = JSON.stringify(a);
        this.gd.send(a)
    }
    ;
    k.im = function(a) {
        u(this.event.Sf, void 0);
        N("SendRoomMessage");
        this.ei({
            t: "sendMessage",
            from_username: this.username(),
            message: a
        })
    }
    ;
    k.hm = function(a, b) {
        u(this.event.Sf, void 0);
        this.ei({
            t: "sendMessage",
            from_username: this.username(),
            to_username: b,
            message: a
        })
    }
    ;
    k.Hm = function() {}
    ;
    k.bp = function(a, b) {
        var c = this;
        return new Promise(function(d, e) {
            "public" !== c.status ? e("unexpected status: " + c.status) : A("tipping/private_show_request/" + c.i() + "/", {
                chat_username: c.username(),
                price: "" + a,
                private_show_minimum_minutes: "" + b
            }).then(function(a) {
                a = new D(a.responseText);
                var b = H(a, "message", !1);
                J(a, "success") ? d("") : ("" === b && (w("unknown cannot start private show reason"),
                b = "Cannot start private show."),
                d(b))
            })
        }
        )
    }
    ;
    k.cp = function() {
        var a = this;
        return new Promise(function(b) {
            A("tipping/spy_on_private_show_request/" + a.i() + "/", {
                chat_username: a.username()
            }).then(function(a) {
                a = new D(a.responseText);
                var c = J(a, "success")
                  , e = H(a, "message", !1);
                L(a);
                c ? b("") : ("" === e && (e = "Error joining spy show"),
                b(e))
            })
        }
        )
    }
    ;
    k.Oh = function() {
        var a = this;
        switch (this.status) {
        case "groupwatching":
            break;
        case "grouprequesting":
            break;
        default:
            r("unexpected status: " + this.status)
        }
        return new Promise(function(b) {
            return A("tipping/group_show_cancel/" + a.i() + "/", {}).then(function() {
                b("")
            })
        }
        )
    }
    ;
    k.Ph = function() {
        var a = this;
        return new Promise(function(b) {
            var c = "tipping/private_show_cancel/" + a.i() + "/"
              , d = a.status;
            A(c, {}).then(function(a) {
                a = new D(a.responseText);
                var c = J(a, "success")
                  , e = K(a, "remaining_seconds")
                  , g = I(a, "private_show_minimum_minutes");
                L(a);
                switch (d) {
                case "privatewatching":
                    if (!c) {
                        a = "You have started a private show with a " + g + " minute minimum. You cannot cancel your private show yet." + (" It still has " + e + " seconds remaining.");
                        b(a);
                        break
                    }
                    b("");
                    break;
                case "privatespying":
                    if (!c) {
                        b("Unable to cancel spy show");
                        break
                    }
                    b("");
                    break;
                case "privaterequesting":
                    break;
                default:
                    r("leave private show from status: " + d),
                    b("")
                }
            })
        }
        )
    }
    ;
    k.ap = function(a) {
        var b = this;
        return "public" !== this.status && "groupnotwatching" !== this.status ? new Promise(function(a, d) {
            d("unexpected status: " + b.status)
        }
        ) : new Promise(function(c, d) {
            A("tipping/group_show_request/" + b.i() + "/", {
                chat_username: b.username(),
                price: "" + a
            }).then(function(a) {
                a = new D(a.responseText);
                var b = H(a, "message", !1);
                J(a, "success") ? (L(a),
                c("")) : ("" === b && (w("unknown cannot start group show reason"),
                b = "Cannot start group show."),
                c(b))
            })["catch"](function(a) {
                d(a)
            })
        }
        )
    }
    ;
    k.i = function() {
        return this.d.i
    }
    ;
    k.username = function() {
        return this.d.rh
    }
    ;
    k.Qc = function(a) {
        var b = this;
        A("chat_ignore_list/", {
            username: a
        }).then(function(c) {
            "max" === c.responseText ? u(b.event.P, {
                U: [[Sn("Please signup/login to enable this feature.")]],
                V: !0
            }) : (Mn.add(a),
            Nn())
        })["catch"](function(a) {
            w("Error posting ignore user: " + a)
        });
        u(this.event.P, {
            U: [[Sn("Ignoring " + a)]],
            V: !0
        })
    }
    ;
    k.qk = function(a) {
        A("chat_ignore_list/", {
            username: a,
            remove: "1"
        }).then(function() {
            Mn["delete"](a);
            Nn()
        })["catch"](function(a) {
            w("Error posting unignore user: " + a)
        });
        u(this.event.P, {
            U: [[Sn("No longer ignoring " + a)]],
            V: !0
        })
    }
    ;
    k.ne = function(a) {
        var b = Mn;
        return void 0 === b ? (w("ignoreSet should not be undefined"),
        !1) : b.has(a)
    }
    ;
    k.bm = function(a) {
        this.gi(this.zd, a) && u(this.event.P, {
            U: [[Sn(Jl(this.i(), a)), {
                cc: 0,
                hc: a
            }, Sn(" has joined the room.")]],
            V: a.username === this.username()
        })
    }
    ;
    k.cm = function(a) {
        this.gi(this.Bd, a) && u(this.event.P, {
            U: [[Sn(Jl(this.i(), a)), {
                cc: 0,
                hc: a
            }, Sn(" has left the room.")]],
            V: a.username === this.username()
        })
    }
    ;
    k.gi = function(a, b) {
        return b.username === this.username() || 3 === a && (this.qb || this.mc) ? !1 : 1 === a || !this.qb && !this.mc || 2 === a && (b.username === this.i() || b.Dc || b.Bc) ? !0 : !1
    }
    ;
    var Go;
    q(S, function(a) {
        Go = a.h.i
    });
    function Ho(a) {
        var b = Io(a);
        b.Bg = G(a, "gender", !1);
        return b
    }
    function Io(a) {
        var b = nb(a);
        1 !== b.size && w('getStrings(["from_username", "user", "username"]): ' + b + " length is not 1 in " + a.Hd);
        b = b.values().next().value;
        return {
            username: b,
            Oe: b === Go,
            Bc: J(a, "in_fanclub"),
            je: J(a, "has_tokens"),
            Dc: J(a, "is_mod"),
            lf: J(a, "tipped_recently"),
            kf: J(a, "tipped_alot_recently"),
            mf: J(a, "tipped_tons_recently")
        }
    }
    function Jo(a) {
        var b = new D(a[1])
          , c = {
            fb: Ho(b),
            message: H(b, "m"),
            font: G(b, "f", !1),
            Kd: G(b, "c", !1),
            backgroundColor: G(b, "background", !1)
        };
        c.fb.username !== a[0] && w("Parsing username match failed");
        !0 !== E(b, "X-Successful") && w("X-Successful not true?");
        L(b);
        return c
    }
    function Ko(a, b) {
        var c = {
            fb: Ho(b),
            message: H(b, "message"),
            La: K(b, "amount")
        };
        a !== E(b, "to_username") && w("Username does not match in tip");
        !0 !== E(b, "history") && w("history != true? " + history);
        "" !== E(b, "dont_send_to") && E(b, "dont_send_to") !== a && w("dont_send_to not unexpected value: " + E(b, "dont_send_to"));
        return c
    }
    ;function Z(a) {
        return {
            cc: 1,
            message: a
        }
    }
    function Lo(a) {
        return {
            cc: 0,
            hc: a
        }
    }
    ;function Mo(a) {
        this.B = a
    }
    Mo.prototype.Sl = function(a) {
        var b = JSON.parse(a);
        b = {
            method: b.method,
            Rr: b.callback,
            G: b.args
        };
        if (0 <= Object.keys(No).indexOf(b.method))
            No[b.method](this.B, b);
        else
            w("Unknown message method: " + b.method + " args: " + a)
    }
    ;
    window.debugAllMessages = function() {}
    ;
    var No = {
        onAuthResponse: Oo,
        onRoomCountUpdate: Po,
        onRoomMsg: Qo,
        onNotify: Ro,
        onTitleChange: So,
        onPrivateMsg: To,
        onPromotion: Uo,
        onRevoke: Vo,
        onPersonallyKicked: Wo,
        onSilence: Xo,
        onKick: Yo,
        onNotifyPrivateShowRequest: Zo,
        onNotifyPrivateShowApprove: $o,
        onNotifyPrivateShowCancel: ap,
        onNotifyLeavePrivateRoom: bp,
        onNotifyGroupShowRequest: cp,
        onNotifyGroupShowApprove: dp,
        onNotifyGroupShowCancel: ep,
        onNotifyTokenBalanceUpdate: fp,
        onNotifyAwayModeCancel: gp
    };
    function Po(a, b) {
        1 > b.G.length ? w("Invalid onRoomCountUpdate args: " + b.G) : "" !== b.G[0] && u(a.event.Tg, parseInt(b.G[0]))
    }
    function Oo(a, b) {
        if (1 <= b.G.length && "1" === b.G[0]) {
            var c = a.zp;
            "notconnected" !== a.status && (c = a.status);
            switch (c) {
            case "offline":
            case "away":
            case "grouprequesting":
            case "groupnotwatching":
            case "privaterequesting":
            case "privatenotwatching":
            case "privatespying":
            case "hidden":
            case "public":
                hp(a);
                a.ma(c);
                break;
            case "privatewatching":
            case "groupwatching":
                "notconnected" === a.status ? (ip(a),
                a.ma(c)) : im(a.i()).then(function(b) {
                    "privatewatching" === b.Xa || "groupwatching" === b.Xa ? ip(a) : hp(a);
                    a.ma(b.Xa)
                })["catch"](function(b) {
                    w("Error getting room dossier on reconnect: " + b, {
                        room: a.i()
                    })
                });
                break;
            default:
                r("unexpected status: " + c)
            }
            a.lm()
        } else
            0 < b.G.length && "0" === b.G[0] || w("Error connecting!", {
                message: b
            })
    }
    function So(a, b, c) {
        1 > b.G.length ? w("Invalid onTitleChange args: " + b.G) : 1 < b.G.length && "0" === b.G[1] || jp(a, b.G[0], c)
    }
    function jp(a, b, c) {
        if ("" !== b) {
            var d = []
              , e = Cb(b)
              , f = new Yn(e)
              , h = function(c) {
                for (var e = 0; e < f.Ib.length; e += 1) {
                    var g = Z(f.Ib[e]);
                    g.Ml = !1;
                    d.push(g);
                    e < f.Mf.length && (g = f.Mf[e],
                    d.push(-1 !== c.indexOf(g) ? {
                        cc: 2,
                        message: g
                    } : Z("#" + g)))
                }
                u(a.event.P, {
                    U: [d],
                    oa: "#DC5500",
                    weight: "bold",
                    V: !1
                });
                u(a.event.nk, b)
            };
            void 0 !== c ? h(c) : Zn(f.Mf).then(h)["catch"](function() {
                h([])
            })
        }
    }
    function To(a, b) {
        if ("" === b.G[0])
            1 < b.G.length ? u(a.event.P, {
                U: [[Z(b.G[1])]],
                V: !0
            }) : w("handlePrivateMessage args: " + b.G);
        else {
            var c = b.G
              , d = Jo(c);
            d.Ll = "" !== c[2] ? c[2] : c[0];
            a.ne(d.fb.username) || u(a.event.ag, d)
        }
    }
    function Uo(a, b) {
        if (2 !== b.G.length)
            w("Invalid onPromotion args: " + b.G);
        else {
            b.G[0] === a.username() && u(a.event.Cl, {});
            var c = b.G[1];
            var d = b.G[0];
            c = Q(P("%(from)s has granted moderator privileges to %(to)s."), {
                from: c,
                to: d
            }, !0);
            u(a.event.P, {
                U: [[Z(c)]],
                V: 0 <= b.G.indexOf(a.username())
            })
        }
    }
    function Vo(a, b) {
        if (2 !== b.G.length)
            w("Invalid onRevoke args: " + b.G);
        else {
            b.G[0] === a.username() && u(a.event.Cl, {});
            var c = b.G[1];
            var d = b.G[0];
            c = Q(P("%(from)s has revoked moderator privileges from %(to)s."), {
                from: c,
                to: d
            }, !0);
            u(a.event.P, {
                U: [[Z(c)]],
                V: 0 <= b.G.indexOf(a.username())
            })
        }
    }
    function Wo(a, b) {
        if (1 !== b.G.length)
            w("Invalid onPersonallyKicked args: " + b.G);
        else {
            var c = b.G[0];
            "rejoined" === c ? c = dd : "kicked" === c ? c = ed : (r("createPersonallyKickedMessage called with unknown argument " + c),
            c = fd);
            u(a.event.P, {
                U: [[Z(c)]],
                V: !0
            })
        }
    }
    function Xo(a, b) {
        if (2 !== b.G.length)
            w("Invalid onSilence args: " + b.G);
        else {
            u(a.event.Rg, {
                username: b.G[0]
            });
            var c = b.G[0];
            var d = b.G[1];
            c = Q(P("User %(username)s was silenced by %(silencer)s and his/her messages have been removed"), {
                username: c,
                silencer: d
            }, !0);
            u(a.event.P, {
                U: [[Z(c)]],
                V: 0 <= b.G.indexOf(a.username())
            })
        }
    }
    function Yo(a, b) {
        if (1 !== b.G.length)
            w("Invalid onKick args: " + b.G);
        else {
            u(a.event.Rg, {
                username: b.G[0]
            });
            var c = a.event.P;
            var d = b.G[0];
            d = Q(P("User %(username)s was kicked out of the room and his/her messages have been removed"), {
                username: d
            }, !0);
            u(c, {
                U: [[Z(d)]],
                V: b.G[0] === a.username()
            })
        }
    }
    function Qo(a, b) {
        if ("" === b.G[0])
            1 < b.G.length ? "Chat disconnected. The broadcaster has set a new password on this room." === b.G[1] ? (a.ma("passwordprotected"),
            u(a.event.P, {
                U: [[Z(Gc)]],
                V: !0
            })) : u(a.event.P, {
                U: [[Z(b.G[1])]],
                V: !0
            }) : w("roomMessage invalid args: " + b.G);
        else {
            var c = Jo(b.G);
            a.ne(c.fb.username) || u(a.event.hg, c)
        }
    }
    function Zo(a, b) {
        if (2 > b.G.length)
            w("PrivateShowRequest invalid args: " + b.G);
        else if (u(a.event.Ql, {
            Er: b.G[0],
            Dm: parseInt(b.G[1])
        }),
        a.qb) {
            switch (a.status) {
            case "grouprequesting":
            case "public":
            case "away":
                break;
            default:
                r("unexpected status: " + a.status)
            }
            a.ma("privaterequesting");
            a.qb && u(a.event.re, "show")
        }
    }
    function $o(a, b) {
        if (1 !== b.G.length)
            w("Invalid privateShowApprove args: " + b.G);
        else {
            switch (a.status) {
            case "public":
                a.ma("privatenotwatching");
                break;
            case "grouprequesting":
                a.Oh();
                a.ma("privatenotwatching");
                break;
            case "privaterequesting":
                kp(a);
                ip(a);
                a.ma("privatewatching");
                break;
            default:
                r("private show approve unexpected status: " + a.status)
            }
            var c = Z(Kc);
            "privatewatching" === a.status ? u(a.event.P, {
                U: [[c]],
                oa: "#222",
                background: "#ff8b45",
                weight: "bold",
                V: !0
            }) : u(a.event.P, {
                U: [[c, Z(" ("), {
                    cc: 3
                }, Z(")")]],
                oa: "#222",
                background: "#ff8b45",
                weight: "bold",
                V: !0
            })
        }
    }
    function ap(a, b) {
        function c() {
            a.ma("away");
            u(a.event.P, {
                U: [[Z(Lc)]],
                oa: "#222",
                background: "#ff8b45",
                weight: "bold",
                V: !0
            })
        }
        0 !== b.G.length && w("Invalid privateShowCancel args: " + b.G);
        switch (a.status) {
        case "privaterequesting":
            a.ma("public");
            u(a.event.P, {
                U: [[Z(Mc)]],
                oa: "#222",
                background: "#ff8b45",
                weight: "bold",
                V: !0
            });
            break;
        case "away":
        case "grouprequesting":
        case "hidden":
        case "public":
            break;
        case "privatewatching":
            lp(a);
            hp(a);
            c();
            break;
        case "privatespying":
        case "privatenotwatching":
            c();
            break;
        default:
            r("privateShowCancel wrong room status: " + a.status)
        }
    }
    function bp(a, b) {
        1 !== b.G.length && r("Invalid leavePrivateRoom args: " + b.G);
        if (b.G[0] === a.username())
            switch (a.status) {
            case "groupwatching":
                a.ma("groupnotwatching");
                lp(a);
                hp(a);
                u(a.event.P, {
                    U: [[Z(Zc)]],
                    oa: "#222",
                    background: "#ff8b45",
                    weight: "bold",
                    V: !0
                });
                break;
            default:
                w("unexpected status: " + a.status)
            }
    }
    function cp(a, b) {
        3 !== b.G.length && r("Invalid groupShowRequest args: " + b.G);
        var c = Number(b.G[0])
          , d = Number(b.G[1]);
        u(a.event.Eg, {
            Ld: c,
            pg: d,
            Dm: Number(b.G[2])
        });
        switch (a.status) {
        case "groupwatching":
        case "groupnotwatching":
            break;
        default:
            0 !== c && (a.qb ? ("grouprequesting" !== a.status && a.ma("grouprequesting"),
            c === d && u(a.event.re, "show"),
            u(a.event.P, {
                U: [[Z(Fb(c, d))]],
                oa: "#222",
                background: "#ff8b45",
                weight: "bold",
                V: !1
            })) : u(a.event.P, {
                U: [[Z(Fb(c, d)), Z(" ("), {
                    cc: 4
                }, Z(")")]],
                oa: "#222",
                background: "#ff8b45",
                weight: "bold",
                V: !1
            }))
        }
    }
    function dp(a, b) {
        1 !== b.G.length && w("Invalid groupShowApprove args: " + b.G);
        switch (a.status) {
        case "groupnotwatching":
            break;
        case "grouprequesting":
            kp(a);
            ip(a);
            a.ma("groupwatching");
            break;
        case "public":
        case "privaterequesting":
            a.ma("groupnotwatching");
            break;
        default:
            r("unexpected status: " + a.status)
        }
        u(a.event.P, {
            U: [[Z($c)]],
            oa: "#222",
            background: "#ff8b45",
            weight: "bold",
            V: !0
        })
    }
    function ep(a, b) {
        u(a.event.Eg, {
            Ld: 0
        });
        0 !== b.G.length && w("Invalid groupShowCancel args: " + b.G);
        var c = {
            U: [[Z(ad)]],
            oa: "#222",
            background: "#ff8b45",
            weight: "bold",
            V: !0
        };
        switch (a.status) {
        case "grouprequesting":
            a.ma("public");
            u(a.event.P, {
                U: [[Z(bd)]],
                oa: "#222",
                background: "#ff8b45",
                weight: "bold",
                V: !0
            });
            break;
        case "away":
        case "hidden":
        case "public":
        case "privaterequesting":
            break;
        case "groupwatching":
            lp(a);
            hp(a);
            a.ma("away");
            u(a.event.P, c);
            break;
        case "groupnotwatching":
            a.ma("away");
            u(a.event.P, c);
            break;
        default:
            r("groupShowCancel wrong room status: " + a.status)
        }
    }
    function fp(a, b) {
        2 !== b.G.length ? w("Invalid tokenBalanceUpdate args: " + b.G) : u(a.event.ni, {
            Sb: Number(b.G[1])
        })
    }
    function gp(a, b) {
        0 !== b.G.length && r("Invalid awayModeCancel args: " + b.G);
        switch (a.status) {
        case "away":
        case "grouprequesting":
        case "privaterequesting":
            u(a.event.P, {
                U: [[Z(cd)]],
                oa: "#222",
                background: "#ff8b45",
                weight: "bold",
                V: !0
            });
            a.ma("public");
            break;
        default:
            w("unexpected state: " + a.status)
        }
    }
    function Ro(a, b) {
        var c = new D(b.G[0])
          , d = H(c, "type");
        c.Qc("send_to");
        c.Qc("to_user");
        if ("function" !== typeof mp[d])
            w("Unhandled onNotify type " + d, {
                message: b
            });
        else
            (0,
            mp[d])({
                B: a,
                p: c,
                En: 1 < b.G.length && "true" === b.G[1]
            })
    }
    var mp = {
        tip_alert: np,
        log: op,
        refresh_panel: pp,
        app_tab_refresh: qp,
        appnotice: rp,
        apperrorlog: sp,
        clear_app: tp,
        room_entry: up,
        room_leave: vp,
        purchase_notification: wp,
        settingsupdate: xp,
        hidden_show_status_change: yp,
        hidden_show_approve: zp,
        hidden_show_deny: Ap,
        is_restricted_hls_allowed: Bp
    };
    function np(a) {
        var b = Ko(a.B.i(), a.p);
        void 0 !== a.En && a.En || u(a.B.event.re, Cp(b.La));
        a.B.qb && u(a.B.event.ik, b);
        var c = [Lo(b.fb), Z(" tipped " + b.La + " " + (1 < b.La ? "tokens" : "token"))];
        a.B.qb && ("" !== b.message && c.push(Z(" -- " + b.message)),
        z("tipping/get_token_balance/").then(function(b) {
            b = new D(b.responseText);
            J(b, "success") && u(a.B.event.ni, {
                Sb: K(b, "token_balance")
            })
        }));
        u(a.B.event.P, {
            U: [c],
            background: "#ff3",
            oa: "#000",
            weight: "bold",
            V: b.fb.username === a.B.username() || a.B.qb
        })
    }
    function op(a) {
        void 0 === G(a.p, "msg") && w("handleLog with an empty msg")
    }
    function qp(a) {
        u(a.B.event.Ck, {})
    }
    function pp(a) {
        u(a.B.event.Rj, {})
    }
    function rp(a) {
        var b = E(a.p, "msg")
          , c = [];
        if ("string" === typeof b)
            c.push([Z("Notice: " + b)]);
        else if (b instanceof Array) {
            b = l(b);
            for (var d = b.next(); !d.done; d = b.next())
                c.push([Z("Notice: " + d.value)])
        } else {
            w("handleAppNotice error: Invalid message type " + (void 0 === b ? "undefined" : b.toString()));
            return
        }
        u(a.B.event.P, {
            U: c,
            background: G(a.p, "background", !1),
            oa: G(a.p, "foreground", !1),
            weight: G(a.p, "weight", !1),
            V: !1
        })
    }
    function sp(a) {
        if (a.B.qb) {
            var b = E(a.p, "msg")
              , c = [];
            c.push([Z("Error: ")]);
            if ("string" === typeof b)
                c.push([Z(b)]);
            else if (b instanceof Array) {
                b = l(b);
                for (var d = b.next(); !d.done; d = b.next())
                    c.push([Z(d.value)])
            } else {
                w("handleAppErrorLog error: Invalid message type " + (void 0 === b ? "undefined" : b.toString()));
                return
            }
            u(a.B.event.P, {
                U: c,
                V: !1
            })
        }
    }
    function tp(a) {
        u(a.B.event.Qi, {})
    }
    function up(a) {
        a.B.bm(Io(a.p))
    }
    function vp(a) {
        a.B.cm(Io(a.p))
    }
    function wp(a) {
        var b = Io(a.p);
        !0 !== J(a.p, "history") && r("purchase history is not true?");
        for (var c = [], d = l(H(a.p, "message").split(new RegExp("\\b" + b.username + "\\b","gi"))), e = d.next(); !e.done; e = d.next())
            e = e.value,
            "" !== e && c.push(Z(e)),
            c.push(Lo(b));
        c.pop();
        b.username === a.B.username() && z("tipping/get_token_balance/").then(function(b) {
            b = new D(b.responseText);
            J(b, "success") && u(a.B.event.ni, {
                Sb: K(b, "token_balance")
            })
        });
        u(a.B.event.P, {
            U: [c],
            background: "#33ff33",
            oa: "#000",
            weight: "bold",
            V: b.username === a.B.username()
        })
    }
    function xp(a) {
        var b = a.p
          , c = {
            kd: J(b, "allow_privates"),
            Wa: K(b, "private_price"),
            wb: K(b, "spy_price"),
            Ze: K(b, "private_min_minutes"),
            vf: J(b, "allow_show_recordings"),
            jd: J(b, "allow_groups"),
            Ta: K(b, "group_price"),
            Bl: K(b, "minimum_users_for_group_show")
        };
        L(b);
        u(a.B.event.Zd, c);
        a.B.Wa !== c.Wa && (a.B.Wa = c.Wa,
        b = Z(Hb(a.B.i(), c.Wa)),
        u(a.B.event.P, {
            U: [[b]],
            V: !0
        }));
        a.B.Ta !== c.Ta && (a.B.Ta = c.Ta,
        b = Z(Gb(a.B.i(), c.Ta)),
        u(a.B.event.P, {
            U: [[b]],
            V: !0
        }));
        a.B.wb !== c.wb && 0 !== c.wb && (a.B.wb = c.wb,
        c = Z(Ib(a.B.i(), c.wb)),
        u(a.B.event.P, {
            U: [[c]],
            V: !0
        }))
    }
    function yp(a) {
        if (!a.B.qb) {
            var b = J(a.p, "is_starting") ? "hidden" : "public"
              , c = H(a.p, "hidden_message");
            L(a.p);
            u(a.B.event.hj, c);
            switch (a.B.status) {
            case "grouprequesting":
                a.B.Oh();
                break;
            case "privaterequesting":
                a.B.Ph();
                break;
            case "public":
            case "hidden":
                break;
            default:
                w("handleHiddenShowStatusChange unexpected status: " + a.B.status)
            }
            "public" === b ? setTimeout(function() {
                a.B.ma("public")
            }, 3E3) : a.B.ma(b)
        }
    }
    function zp(a) {
        J(a.p, "initial_hide_cam") ? setTimeout(function() {
            a.B.ma("public")
        }, 2E3) : "public" !== a.B.status && a.B.ma("public")
    }
    function Ap(a) {
        "public" === a.B.status && a.B.ma("hidden")
    }
    function Bp() {}
    function Cp(a) {
        return 1E3 <= a ? "huge" : 500 <= a ? "large" : 100 <= a ? "medium" : 15 <= a ? "small" : "tiny"
    }
    ;function Dp() {
        this.Ag = 0
    }
    Dp.prototype.reset = function() {
        this.Ag = 0
    }
    ;
    var Ep = {
        o: "room-owner",
        m: "moderator",
        f: "fanclub-user",
        tr: "tipped-recently-user",
        g: "standard-user",
        t: "tipped-user",
        p: "tipped-lots-user",
        l: "tipped-tons-user"
    };
    function Fp(a) {
        this.fe = this.nf = 0;
        this.ye = [];
        var b = a.split(",");
        a = ~~b[0];
        b = b.slice(1);
        var c = [];
        b = l(b);
        for (var d = b.next(); !d.done; d = b.next()) {
            var e = l(d.value.split("|"));
            d = e.next().value;
            e = e.next().value;
            e = Ep[e];
            d = {
                username: d,
                Oe: !1,
                Bc: !1,
                je: !1,
                Dc: !1,
                lf: !1,
                kf: !1,
                mf: !1
            };
            "room-owner" === e ? d.Oe = !0 : "fanclub-user" === e ? d.Bc = !0 : "tipped-user" === e ? d.je = !0 : "moderator" === e ? d.Dc = !0 : "tipped-recently-user" === e ? d.lf = !0 : "tipped-lots-user" === e ? d.kf = !0 : "tipped-tons-user" === e && (d.mf = !0);
            c.push(d)
        }
        this.nf = a + c.length;
        this.fe = a;
        this.ye = c
    }
    ;function Gp(a) {
        this.d = a;
        this.status = "offline";
        this.gd = void 0;
        this.lg = !0;
        this.mc = !1;
        this.Pk = 2;
        this.qb = !1;
        this.event = {
            Sf: new n("messageSent"),
            hg: new n("roomMessage"),
            P: new n("roomNotice"),
            ag: new n("privateMessage"),
            kb: new n("statusChange",{
                sd: 15
            }),
            hj: new n("hiddenMessageChange"),
            Rj: new n("refreshPanel"),
            nk: new n("titleChange"),
            Qi: new n("clearApp"),
            Rg: new n("removeMessages"),
            Zd: new n("settingsUpdate"),
            Tg: new n("roomCountUpdate"),
            Cl: new n("modStatusChange"),
            ni: qj,
            Ql: new n("privateShowRequest"),
            Eg: new n("groupShowUpdate"),
            re: new n("playSound"),
            Ck: new n("appTabRefresh"),
            ik: new n("tipAlert")
        };
        this.Pk = /cb2=1/.test(document.cookie) ? 2 : 1;
        this.qb = this.username() === this.i();
        this.cg = new Dp;
        this.Al = new Mo(this);
        this.ob = 0 === a.rh.indexOf("__anonymous__");
        !1 === this.ob && Sl();
        this.$e();
        this.mc = a.mc;
        this.zd = a.qa.zd;
        this.Bd = a.qa.Bd;
        this.zp = this.qb ? "public" : a.Xa;
        this.Wa = a.Wa;
        this.wb = a.wb;
        this.Ta = a.Ta;
        this.ma("notconnected")
    }
    k = Gp.prototype;
    k.disconnect = function() {
        this.lg = !1;
        void 0 !== this.gd && (this.gd.close(),
        this.gd = void 0)
    }
    ;
    k.ma = function(a) {
        if (a !== this.status) {
            var b = this.status;
            this.status = a;
            u(this.event.kb, {
                Vc: b,
                Aa: this.status
            })
        }
    }
    ;
    k.Op = function(a, b) {
        this.zd = a;
        this.Bd = b
    }
    ;
    k.al = function() {
        var a = this;
        if ("offline" === this.status || "notconnected" === this.status || "unknown" === this.status)
            return new Promise(function(a) {
                a({
                    nf: 0,
                    fe: 0,
                    ye: []
                })
            }
            );
        if (void 0 !== this.Cd)
            return this.Cd;
        var b = {
            roomname: this.i(),
            "private": Eo(this)
        };
        return this.Cd = new Promise(function(c, d) {
            z("api/getchatuserlist/?" + db(b)).then(function(b) {
                b = new Fp(b.responseText);
                c(b);
                a.Cd = void 0
            })["catch"](function(b) {
                d(b);
                a.Cd = void 0
            })
        }
        )
    }
    ;
    k.$e = function() {
        var a = this, b, c;
        0 < this.d.host.length && (this.gd = b = "http" === this.d.host.substring(0, 4) ? new SockJS(this.d.host) : new WebSocket(this.d.host),
        b.onclose = function(b) {
            Fo("Connection closed:" + JSON.stringify(b));
            void 0 !== c ? u(a.event.P, {
                U: [[Z("Trying to reconnect")]],
                V: !0
            }) : (u(a.event.P, {
                U: [[Z("Chat disconnected")]],
                V: !0
            }),
            a.lg && (c = setTimeout(function() {
                u(a.event.P, {
                    U: [[Z("Trying to reconnect")]],
                    V: !0
                });
                a.$e()
            }, Co(a.cg))))
        }
        ,
        b.onerror = function(b) {
            Fo("Connection error:" + JSON.stringify(b));
            void 0 === c && a.lg && (c = setTimeout(function() {
                a.$e()
            }, Co(a.cg)))
        }
        ,
        b.onmessage = function(b) {
            a.Al.Sl(b.data)
        }
        ,
        b.onopen = function() {
            a.$c("connect", {
                user: a.d.rh,
                password: a.d.Nm,
                room: a.d.i,
                room_password: a.d.em
            })
        }
        )
    }
    ;
    k.lm = function() {
        u(this.event.P, {
            U: [[Z(Fc)]],
            V: !0
        });
        this.cg.reset();
        jp(this, this.d.bb)
    }
    ;
    k.ei = function(a) {
        this.gd.send(a)
    }
    ;
    k.$c = function(a, b) {
        b = void 0 === b ? {} : b;
        this.ei(JSON.stringify({
            method: a,
            data: b
        }))
    }
    ;
    k.im = function(a) {
        u(this.event.Sf, void 0);
        N("SendRoomMessage");
        this.$c(Eo(this) ? "messagePrivateRoom" : "messageRoom", {
            room: this.d.i,
            msg: JSON.stringify({
                v: this.Pk,
                m: a,
                f: "",
                c: ""
            })
        })
    }
    ;
    k.hm = function(a, b) {
        u(this.event.Sf, void 0);
        this.$c(Eo(this) ? "addPrivateUserMsgToUser" : "addUserMsgToUser", {
            msg: JSON.stringify({
                v: this.Pk,
                m: a,
                f: "",
                c: ""
            }),
            room: this.i(),
            to_user: b
        })
    }
    ;
    k.Hm = function() {
        this.$c("updateRoomCount", {
            model_name: this.i(),
            private_room: "privatewatching" === this.status || "groupwatching" === this.status
        })
    }
    ;
    k.bp = function(a, b) {
        var c = this;
        return new Promise(function(d, e) {
            "public" !== c.status ? e("unexpected status: " + c.status) : A("tipping/private_show_request/" + c.i() + "/", {
                chat_username: c.username(),
                price: "" + a,
                private_show_minimum_minutes: "" + b
            }).then(function(a) {
                a = new D(a.responseText);
                var b = H(a, "message", !1);
                J(a, "success") ? (L(a),
                c.ma("privaterequesting"),
                u(c.event.P, {
                    U: [[Z(Qc)]],
                    oa: "#222",
                    background: "#ff8b45",
                    weight: "bold",
                    V: !0
                }),
                d("")) : ("" === b && (w("unknown cannot start private show reason"),
                b = "Cannot start private show."),
                d(b))
            })["catch"](function(a) {
                e(a)
            })
        }
        )
    }
    ;
    k.cp = function() {
        var a = this;
        return new Promise(function(b, c) {
            "privatenotwatching" !== a.status ? c("unexpected status: " + a.status) : A("tipping/spy_on_private_show_request/" + a.i() + "/", {
                chat_username: a.username()
            }).then(function(c) {
                c = new D(c.responseText);
                var d = J(c, "success")
                  , f = H(c, "message", !1);
                L(c);
                d ? (a.ma("privatespying"),
                u(a.event.P, {
                    U: [[Z("You are spying on the private show")]],
                    oa: "#222",
                    background: "#ff8b45",
                    weight: "bold",
                    V: !0
                }),
                b("")) : ("" === f && (f = "Error joining spy show"),
                b(f))
            })["catch"](function(a) {
                c(a)
            })
        }
        )
    }
    ;
    k.Oh = function() {
        var a = this;
        switch (this.status) {
        case "groupwatching":
            break;
        case "grouprequesting":
            this.ma("public");
            break;
        default:
            r("unexpected status: " + this.status)
        }
        return new Promise(function(b) {
            return A("tipping/group_show_cancel/" + a.i() + "/", {}).then(function() {
                b("")
            })
        }
        )
    }
    ;
    k.Ph = function() {
        var a = this;
        return new Promise(function(b) {
            var c = "tipping/private_show_cancel/" + a.i() + "/"
              , d = a.status;
            A(c, {}).then(function(c) {
                c = new D(c.responseText);
                var e = J(c, "success")
                  , h = K(c, "remaining_seconds")
                  , g = I(c, "private_show_minimum_minutes");
                L(c);
                switch (d) {
                case "privatewatching":
                    if (!e) {
                        0 === h ? b(Oc) : (c = "You have started a private show with a " + g + " minute minimum. You cannot cancel your private show yet." + (" It still has " + h + " seconds remaining."),
                        b(c));
                        break
                    }
                    b("");
                    break;
                case "privatespying":
                    if (!e) {
                        b("Unable to cancel spy show");
                        break
                    }
                    a.ma("privatenotwatching");
                    b("");
                    break;
                case "privaterequesting":
                    if (!e) {
                        b("Unable to cancel private request");
                        break
                    }
                    b("");
                    break;
                default:
                    r("leave private show from status: " + d),
                    e ? b("") : b("Unable to leave private show from status: " + d)
                }
            })
        }
        )
    }
    ;
    k.ap = function(a) {
        var b = this;
        if ("public" !== this.status && "groupnotwatching" !== this.status)
            return new Promise(function(a, c) {
                c("unexpected status: " + b.status)
            }
            );
        var c = this.status;
        this.ma("grouprequesting");
        return new Promise(function(d, e) {
            A("tipping/group_show_request/" + b.i() + "/", {
                chat_username: b.username(),
                price: "" + a
            }).then(function(a) {
                a = new D(a.responseText);
                var e = H(a, "message", !1);
                J(a, "success") ? (u(b.event.P, {
                    U: [[Z("Sending group show request.")]],
                    oa: "#222",
                    background: "#ff8b45",
                    weight: "bold",
                    V: !0
                }),
                L(a),
                d("")) : (b.ma(c),
                "" === e && (w("unknown cannot start group show reason"),
                e = "Cannot start group show."),
                d(e))
            })["catch"](function(a) {
                b.ma(c);
                e(a)
            })
        }
        )
    }
    ;
    function hp(a) {
        a.$c("joinRoom", {
            room: a.i()
        })
    }
    function kp(a) {
        a.$c("leaveRoom", {
            room: a.i()
        })
    }
    function ip(a) {
        setTimeout(function() {
            a.$c("joinPrivateRoom", {
                room: a.i()
            })
        }, 500)
    }
    function lp(a) {
        a.$c("leavePrivateRoom", {
            room: a.i()
        })
    }
    k.i = function() {
        return this.d.i
    }
    ;
    k.username = function() {
        return this.d.rh
    }
    ;
    k.Qc = function(a) {
        var b = this;
        A("chat_ignore_list/", {
            username: a
        }).then(function(c) {
            "max" === c.responseText ? u(b.event.P, {
                U: [[Z("Please signup/login to enable this feature.")]],
                V: !0
            }) : (Pl.add(a),
            Ql())
        })["catch"](function(a) {
            w("Error posting ignore user: " + a)
        });
        u(this.event.P, {
            U: [[Z("Ignoring " + a)]],
            V: !0
        })
    }
    ;
    k.qk = function(a) {
        A("chat_ignore_list/", {
            username: a,
            remove: "1"
        }).then(function() {
            Pl["delete"](a);
            Ql()
        })["catch"](function(a) {
            w("Error posting unignore user: " + a)
        });
        u(this.event.P, {
            U: [[Z("No longer ignoring " + a)]],
            V: !0
        })
    }
    ;
    k.ne = function(a) {
        var b = Pl;
        return void 0 === b ? (w("ignoreSet should not be undefined"),
        !1) : b.has(a)
    }
    ;
    k.bm = function(a) {
        this.gi(this.zd, a) && u(this.event.P, {
            U: [[Z(Jl(this.i(), a)), Lo(a), Z(" has joined the room.")]],
            V: a.username === this.username()
        })
    }
    ;
    k.cm = function(a) {
        this.gi(this.Bd, a) && u(this.event.P, {
            U: [[Z(Jl(this.i(), a)), Lo(a), Z(" has left the room.")]],
            V: a.username === this.username()
        })
    }
    ;
    k.gi = function(a, b) {
        if (b.username === this.username())
            return !1;
        if (3 !== a)
            if (2 === a) {
                if (b.username === this.i() || b.Dc || b.Bc)
                    return !0
            } else if (1 === a) {
                if (b.username === this.i() || b.Dc || b.Bc || b.je && (this.qb || this.mc))
                    return !0
            } else if (0 === a && (b.username === this.i() || b.Dc || b.Bc || this.qb || this.mc))
                return !0;
        return !1
    }
    ;
    function Hp(a) {
        v.call(this);
        var b = this;
        this.Oi = a;
        this.Wh = [];
        this.a.style.position = "fixed";
        this.a.style.height = "";
        this.a.style.textShadow = "1px 1px 0 #000, -1px -1px 0 #000, 1px -1px 0 #000, -1px 1px 0 #000";
        this.a.style.fontSize = "12px";
        this.ra = document.createElement("div");
        this.ra.style.position = "absolute";
        this.ra.style.bottom = "0";
        this.a.appendChild(this.ra);
        q(a.nh, function(a) {
            Ip(b, a.zl(), a.nr)
        });
        q(vh, function() {
            b.clear()
        });
        q(S, function(a) {
            b.a.style.fontSize = a.h.qa.fontSize
        });
        q(th, function(a) {
            b.a.style.fontSize = a.fontSize
        })
    }
    m(Hp, v);
    Hp.prototype.pa = function() {
        this.a.scrollTop = this.a.scrollHeight - this.a.offsetHeight
    }
    ;
    Hp.prototype.clear = function() {
        for (var a = l(this.Wh), b = a.next(); !b.done; b = a.next())
            clearTimeout(b.value);
        for (this.Wh = []; null !== this.ra.firstChild; )
            this.ra.removeChild(this.ra.firstChild)
    }
    ;
    function Ip(a, b, c) {
        a.ra.appendChild(b);
        Jp(a, function() {
            b.style.opacity = "0"
        });
        a.Y();
        void 0 !== c && !0 !== c || a.pa()
    }
    function Jp(a, b) {
        var c = setTimeout(function() {
            var d = a.Wh.indexOf(c);
            0 <= d && a.Wh.splice(d, 1);
            b()
        }, 2E4);
        a.Wh.push(c)
    }
    Hp.prototype.Tm = function() {
        var a = this;
        q(this.Oi.Vi, function() {
            a.Y()
        })
    }
    ;
    Hp.prototype.C = function() {
        var a = this.Oi.xa
          , b = this.Oi.Oa
          , c = a.getBoundingClientRect();
        this.a.style.left = c.left + "px";
        this.a.style.width = a.clientWidth + "px";
        this.ra.style.width = a.clientWidth + "px";
        this.a.style.top = b.getBoundingClientRect().top + "px";
        this.a.style.height = b.clientHeight + "px";
        this.ra.style.height = b.clientHeight + "px"
    }
    ;
    var Kp = new n("switchedToHLS");
    new n("openTipCalloutRequest");
    new n("loadRoomRequest");
    new n("userPanelRequest");
    new n("sendMessageInputFocus");
    new n("sendMessageInputBlur");
    new n("userMenuPmClicked");
    function Lp(a, b) {
        var c = document.createElement("div");
        c.innerText = a;
        c.style.color = "#72C0FF";
        c.style.cursor = "pointer";
        c.style.paddingTop = "5px";
        c.onmouseenter = function() {
            c.style.textDecoration = "underline"
        }
        ;
        c.onmouseleave = function() {
            c.style.textDecoration = "none"
        }
        ;
        c.style.pointerEvents = "auto";
        c.onclick = function(a) {
            b(a)
        }
        ;
        return c
    }
    function Mp(a) {
        a = void 0 === a ? !0 : a;
        v.call(this);
        var b = this;
        this.Oo = a;
        this.Ng = Lp("More Rooms", function(a) {
            u(Dj, void 0);
            a.stopPropagation()
        });
        this.be = new Map;
        this.ac = Lp("", function() {});
        this.Ig = this.Kh = this.yr = !1;
        this.a.style.display = "none";
        this.a.style.position = this.Oo ? "fixed" : "absolute";
        this.a.style.height = "auto";
        this.a.style.width = "auto";
        this.a.style.boxSizing = "border-box";
        this.a.style.padding = "0 50px";
        this.a.style.left = "0";
        this.a.style.textAlign = "center";
        this.a.style.pointerEvents = "none";
        this.a.style.zIndex = "1";
        this.Z = document.createElement("div");
        this.Z.style.fontSize = "32px";
        this.Z.style.color = "#72c0ff";
        this.Z.style.marginBottom = "20px";
        this.a.appendChild(this.Z);
        this.body = document.createElement("div");
        this.body.style.fontSize = "16px";
        this.body.style.color = "#ffffff";
        this.text = document.createElement("span");
        this.body.appendChild(this.text);
        this.Ng.style.display = "none";
        this.body.appendChild(this.Ng);
        this.ac.style.display = "none";
        this.body.appendChild(this.ac);
        this.a.appendChild(this.body);
        var c = new Aa;
        q(S, function(a) {
            b.Vk = a.j;
            za(q(a.j.event.kb, function(c) {
                Np(b, a.j, c.Aa)
            }), c);
            za(q(a.j.event.hj, function(a) {
                b.Fg = a
            }), c);
            za(q(Kp, function() {
                b.Pg = !0;
                Np(b, a.j, a.h.Xa)
            }), c);
            void 0 !== a.h.Fg ? b.Fg = a.h.Fg : w("hiddenMessage is not defined.");
            Np(b, a.j, a.h.Xa);
            b.Ig = a.h.Ig
        });
        q(vh, function() {
            Da(c);
            b.Fg = "";
            b.Mc = !1
        });
        this.be.set("away", function() {
            b.Z.innerText = "Performer Is Away";
            b.text.innerText = "You may continue chatting while you wait for the broadcaster to return.";
            b.ac.style.display = "none";
            Op(b, !0);
            b.a.style.display = "block"
        });
        this.be.set("groupnotwatching", function(a) {
            b.Z.innerText = "Group Show In Progress";
            b.text.innerText = "You may continue chatting while you wait for the broadcaster to return from the group show. If a goal had been set when the broadcaster returns it will continue where it left off.";
            void 0 === b.Pg ? w("unexpected") : !0 === b.Pg && (b.ac.innerText = "JOIN THIS GROUP SHOW",
            b.ac.style.display = "block",
            b.ac.onclick = function() {
                lk(a)
            }
            ,
            b.ac.style.display = "block");
            Op(b, !0);
            b.a.style.display = "block"
        });
        this.be.set("privatenotwatching", function(a) {
            b.Z.innerText = "Private Show In Progress";
            b.text.innerText = "You may continue chatting while you wait for the broadcaster to return from the private show. If a goal had been set when the broadcaster returns it will continue where it left off.";
            void 0 === b.Pg ? w("unexpected") : !0 === b.Pg && (b.ac.innerText = "SPY ON THIS PRIVATE SHOW",
            b.ac.style.display = "block",
            b.ac.onclick = function() {
                kk(a)
            }
            ,
            b.ac.style.display = "block");
            Op(b, !0);
            b.a.style.display = "block"
        });
        this.be.set("hidden", function() {
            b.Z.innerText = "Cam is Hidden";
            b.text.innerText = b.Fg;
            b.ac.style.display = "none";
            Op(b, !1);
            b.a.style.display = "block"
        });
        this.be.set("offline", function() {
            b.Z.innerText = "Offline";
            b.text.innerText = "The member you are trying to view is currently offline. Please wait or choose another member to view.";
            b.ac.style.display = "none";
            b.a.style.display = "block";
            Op(b, !0)
        });
        this.be.set("passwordprotected", function() {
            b.Z.innerText = "Password Required";
            b.text.innerText = "The broadcaster has set a password which is required for viewing. If you know the password, refresh this page to enter it.";
            b.ac.style.display = "none";
            Op(b, !0);
            b.a.style.display = "block";
            T() && Ih()
        })
    }
    m(Mp, v);
    function In(a, b) {
        q(b, function(b) {
            a.Mc = b;
            void 0 !== a.Vk && Np(a, a.Vk, a.Vk.status)
        })
    }
    Mp.prototype.C = function() {
        if (void 0 !== this.parent) {
            var a = this.parent.a.getBoundingClientRect()
              , b = document.documentElement.clientWidth
              , c = document.documentElement.clientHeight;
            a.width <= b ? (this.a.style.width = a.width + "px",
            this.Oo && (this.a.style.left = a.left + "px")) : (this.a.style.width = b + "px",
            this.a.style.left = "0");
            T() && this.Ig ? (b = parseInt(null !== this.parent.a.style.height ? this.parent.a.style.height : ""),
            this.a.style.top = .5 * (b ? b : c) - .5 * this.a.offsetHeight + "px") : this.a.style.top = a.height <= c ? "fixed" === this.parent.a.style.position ? a.top + .5 * a.height - .5 * this.a.offsetHeight + "px" : .5 * a.height - .5 * this.a.offsetHeight + "px" : .5 * c - .5 * this.a.offsetHeight + "px";
            this.a.style.maxHeight = a.height + "px"
        }
    }
    ;
    function Np(a, b, c) {
        a.Mc && (c = "offline");
        var d = a.be.get(c);
        if (void 0 !== d)
            d(b),
            a.Y();
        else
            switch (a.Kh || (a.Ng.style.display = "none",
            a.a.style.display = "none"),
            c) {
            case "public":
            case "privaterequesting":
            case "privatewatching":
            case "privatespying":
            case "grouprequesting":
            case "groupwatching":
            case "notconnected":
                break;
            default:
                r("unexpected status: " + c)
            }
    }
    function Op(a, b) {
        var c = a.Kh && !a.Mc;
        a.Ng.style.display = void 0 !== b && !b || c || a.yr ? "none" : "block"
    }
    ;function Pp() {
        var a = this;
        this.hi = new Map;
        this.vm = !0;
        if (this.vm = Sa() && Oa() || Ma())
            if (Oa()) {
                Xh("Using audio context");
                this.context = "webkitAudioContext"in window ? new webkitAudioContext : new AudioContext;
                y("beforeunload", window, function() {
                    void 0 !== a.context && ("close"in a.context && a.context.close(),
                    a.context = void 0)
                });
                var b = !1
                  , c = function() {
                    if (!b) {
                        if (void 0 === a.context)
                            w("unexpected");
                        else {
                            var c = a.context.createBufferSource();
                            c.buffer = a.context.createBuffer(1, 1, 22050);
                            c.connect(a.context.destination);
                            c.start(0)
                        }
                        b = !0
                    }
                };
                y("click", document, c);
                y("touchstart", document, c);
                y("touchend", document, c)
            } else
                Xh("Using audio elements");
        else
            r("Sounds are not supported.")
    }
    function Qp(a, b, c) {
        a.hi.set(b, {
            ready: !1
        });
        if (void 0 !== a.context) {
            var d = new XMLHttpRequest;
            d.open("GET", c, !0);
            d.responseType = "arraybuffer";
            d.onload = function() {
                200 > d.status || 300 <= d.status ? w("Error requesting sound " + b + " " + d.status + " " + d.statusText) : void 0 === a.context ? w("unexpected") : a.context.decodeAudioData(d.response.slice(0), function(c) {
                    a.hi.set(b, {
                        ready: !0,
                        buffer: c
                    })
                }, function(a) {
                    w("Error decoding sound " + b + " " + a)
                })
            }
            ;
            d.onerror = function() {
                w("Error requesting sound " + b + " (" + new jb(d) + ")")
            }
            ;
            d.send()
        } else
            Ma() && a.hi.set(b, {
                ready: !0,
                Fk: new Audio(c)
            })
    }
    Pp.prototype.re = function(a, b) {
        if (Rp(this, a, b)) {
            var c = this.hi.get(a);
            if (void 0 !== c && c.ready)
                if (void 0 !== c.buffer)
                    if (void 0 === this.context)
                        w("unexpected");
                    else {
                        var d = this.context.createBufferSource();
                        d.buffer = c.buffer;
                        c = this.context.createGain();
                        d.connect(c);
                        c.connect(this.context.destination);
                        c.gain.value = b / 3.5 / 100;
                        d.start(0)
                    }
                else
                    void 0 !== c.Fk ? (c.Fk.volume = b / 100,
                    d = c.Fk.play(),
                    void 0 !== d && d["catch"](function(b) {
                        w("Error playing sound " + a + " (" + b + ")")
                    })) : w("Missing sound buffer or element for " + a)
        }
    }
    ;
    function Rp(a, b, c) {
        return a.vm && 0 !== c ? 100 < c || 0 > c || isNaN(c) ? (w("setSoundVolume: invalid volume " + c),
        !1) : a.hi.has(b) ? !0 : (w("Unknown sound: " + b),
        !1) : !1
    }
    ;var Sp = {
        Tr: 17,
        shift: 16,
        alt: 18,
        $r: 91,
        es: 93,
        Sr: 20,
        js: 9
    }
      , Tp = {
        left: 37,
        ms: 38,
        right: 39,
        Ur: 40
    };
    function Up(a) {
        if (void 0 === a || 13 === a || 27 === a)
            return !1;
        for (var b in Tp)
            if (a === Tp[b])
                return !1;
        for (var c in Sp)
            if (a === Sp[c])
                return !1;
        return !0
    }
    ;var Vp;
    function Wp() {
        "WebSocket"in window && "recommender_websocket_url"in window && "" !== window.recommender_websocket_url && (q(S, function(a) {
            void 0 === a.h.Wo ? w("recommenderHmac not available") : (void 0 !== Vp && w("socket not closed"),
            Vp = Xp(a.h.userName, a.h.i, a.h.Wo))
        }),
        q(vh, function() {
            void 0 === Vp ? w("socket not open") : (Vp.close(),
            Vp = void 0)
        }))
    }
    function Xp(a, b, c) {
        var d = new WebSocket(window.recommender_websocket_url,"cbrec");
        d.onopen = function() {
            d.send(JSON.stringify({
                cmd: "open",
                room: b,
                user: a,
                hmac: c
            }))
        }
        ;
        d.onerror = function(a) {
            w("communicateWithRecommender error", {
                event: a
            })
        }
        ;
        var e = setInterval(function() {
            d.readyState === WebSocket.OPEN && d.send(JSON.stringify({
                cmd: "ping"
            }))
        }, 2E4);
        d.onclose = function() {
            clearInterval(e)
        }
        ;
        return d
    }
    ;var Yp, Zp = ["total_viewers", "image_url", "password_warning"];
    function $p(a) {
        clearTimeout(Yp);
        if (!a.ob) {
            var b = "contest/log/" + a.i() + "/";
            Yp = setTimeout(function() {
                z(b).then(function(b) {
                    b = new D(b.responseText);
                    for (var c = J(b, "can_access"), e = J(b, "is_banned"), f = J(b, "has_warnings"), h = l(Zp), g = h.next(); !g.done; g = h.next())
                        b.Qc(g.value);
                    c || window.location.reload();
                    e && (window.location.href = "/");
                    f && (window.location.href = "/accounts/site_message/?next=" + window.location.href);
                    $p(a);
                    L(b)
                })["catch"](function(a) {
                    w("log_presence failed " + a)
                })
            }, 55E3)
        }
    }
    ;var aq = window.canAutoplay;
    function bq() {
        var a = new en;
        return new Promise(function(b) {
            function c(c) {
                var f = 0;
                !0 === c && cb("visibilitychange", document, d);
                Za() && 0 !== e && (f = window.performance.now() - e);
                if (void 0 === aq)
                    r("canAutoplay library is undefined. Resolving autoplay to true"),
                    a.send(!0, f, Error("canAutoplay library is undefined. Resolving autoplay to true")),
                    b(!0);
                else
                    aq.video({
                        inline: !0,
                        muted: !0,
                        timeout: 1E3
                    }).then(function(c) {
                        a.send(c.result, f, c.error);
                        b(c.result)
                    })["catch"](function(c) {
                        a.send(!1, f, c);
                        b(!1)
                    })
            }
            function d() {
                "visible" === document.visibilityState && c(!0)
            }
            var e = 0;
            document.hidden ? (Za() && (e = window.performance.now()),
            y("visibilitychange", document, d)) : c()
        }
        )
    }
    ;function cq() {
        v.call(this);
        var a = this;
        this.ra = document.createElement("div");
        this.label = document.createElement("span");
        this.lh = document.createElement("div");
        this.ng = document.createElement("div");
        this.Cf = document.createElement("div");
        this.qf = document.createElement("span");
        this.Je = document.createElement("span");
        this.Qb = document.createElement("img");
        this.Pb = document.createElement("img");
        this.xd = document.createElement("span");
        this.fa = "";
        this.Lc = 0;
        this.Zp = !1;
        this.lp = ig;
        this.label.textContent = this.lp;
        this.label.style.margin = "0 3px";
        this.label.style.verticalAlign = "top";
        this.ra.appendChild(this.label);
        this.lh.style.display = "inline-block";
        this.lh.style.verticalAlign = "top";
        this.ng.style.display = "inline-block";
        this.ng.style.cursor = "pointer";
        this.ng.style.verticalAlign = "top";
        this.qf.style.margin = "0 3px";
        this.qf.style.verticalAlign = "top";
        this.ng.appendChild(this.qf);
        dq(this);
        this.ng.appendChild(this.Qb);
        this.lh.appendChild(this.ng);
        this.xd.style.margin = "0 3px";
        this.xd.style.verticalAlign = "top";
        this.lh.appendChild(this.xd);
        this.Cf.style.display = "inline-block";
        this.Cf.style.cursor = "pointer";
        this.Cf.style.verticalAlign = "top";
        eq(this);
        this.Cf.appendChild(this.Pb);
        this.Je.style.margin = "0 3px";
        this.Je.style.verticalAlign = "top";
        this.Cf.appendChild(this.Je);
        this.lh.appendChild(this.Cf);
        this.ra.appendChild(this.lh);
        this.a.appendChild(this.ra);
        q(S, function(b) {
            a.Ee = b.j;
            a.fa = b.h.i;
            a.Lc = b.h.Lc;
            b.h.Tn && void 0 === b.h.dk ? (a.qf.style.display = "none",
            a.xd.style.display = "none",
            a.Je.style.display = "none") : (a.qf.innerText = "" + b.h.cf.qf,
            a.xd.innerText = b.h.cf.xd + "%",
            a.Je.innerText = "" + b.h.cf.Je,
            a.qf.style.display = "inline",
            a.xd.style.display = "inline",
            a.Je.style.display = "inline",
            a.xd.style.color = 85 < b.h.cf.xd ? "#298A08" : 65 < b.h.cf.xd ? "#af5c01" : "#af0101");
            a.label.textContent = b.h.Tn && void 0 !== b.h.dk ? ih : a.lp;
            0 === b.h.jo ? fq(a) : 10 === b.h.jo ? gq(a) : (a.gj = !1,
            a.kh = void 0,
            a.Qb.src = "https://ssl-ccstatic.highwebmedia.com/theatermodeassets/thumbs-up-inactive.svg",
            a.Pb.src = "https://ssl-ccstatic.highwebmedia.com/theatermodeassets/thumbs-down-inactive.svg");
            a.Zp = b.h.Oq;
            25 <= a.Lc ? a.Zi() : a.Wi()
        });
        q(pj, function(b) {
            if (25 <= b && (a.Zi(),
            25 > a.Lc)) {
                var c = a.Ee.event.P;
                var d = a.fa;
                d = Q(P("Note: For tipping at least %(tokens)s tokens today, you can now vote what you think about %(room)s. Your vote is confidential. See below for the satisfaction feedback controls."), {
                    tokens: 25,
                    room: tb(d)
                }, !0);
                u(c, {
                    U: [[Z(d)]],
                    V: !0
                })
            }
            a.Lc = b
        })
    }
    m(cq, v);
    function dq(a) {
        a.Qb.src = "https://ssl-ccstatic.highwebmedia.com/theatermodeassets/thumbs-up-inactive.svg";
        a.Qb.height = 15;
        a.Qb.width = 15;
        a.Qb.style.height = "15px";
        a.Qb.style.width = "15px";
        a.Qb.style.verticalAlign = "top";
        a.Qb.style.margin = "0 3px";
        a.Qb.style.opacity = "0.8";
        a.ng.onclick = function(b) {
            b.stopPropagation();
            25 <= a.Lc && (!1 === a.gj || "down" === a.kh) ? A("tipping/rate_model/" + a.fa + "/", {
                rating: "10"
            }).then(function() {
                gq(a);
                var b = a.Ee.event.P;
                var d = a.fa;
                d = Q(P("Note: Your confidential vote of thumbs up for %(room)s has been recorded. You may change your vote at any time today. Thank you for your feedback."), {
                    room: tb(d)
                }, !0);
                u(b, {
                    U: [[Z(d)]],
                    V: !0
                });
                hq(a, !0)
            })["catch"](function(a) {
                w("Error rating model: " + a)
            }) : "up" === a.kh && hq(a, !1)
        }
    }
    function eq(a) {
        a.Pb.src = "https://ssl-ccstatic.highwebmedia.com/theatermodeassets/thumbs-down-inactive.svg";
        a.Pb.height = 15;
        a.Pb.width = 15;
        a.Pb.style.height = "15px";
        a.Pb.style.width = "15px";
        a.Pb.style.verticalAlign = "top";
        a.Pb.style.margin = "0 3px";
        a.Pb.style.opacity = "0.8";
        a.Cf.onclick = function(b) {
            b.stopPropagation();
            25 <= a.Lc && (!1 === a.gj || "up" === a.kh) ? Ki(Qb(a.fa), function() {
                A("tipping/rate_model/" + a.fa + "/", {
                    rating: "0"
                }).then(function() {
                    var b = a.fa;
                    b = Q(P("Note: Your confidential vote of thumbs down for %(room)s has been recorded. You may vote once every 90 days, and you may change your vote at any time today.  Thank you for your feedback."), {
                        room: tb(b)
                    }, !0);
                    a.Zp && (b = a.fa,
                    b = Q(P("Note: Your confidential vote of thumbs down for %(room)s has been refreshed and will expire in 90 days. You may change your vote at any time today. Thank you for your feedback."), {
                        room: tb(b)
                    }, !0));
                    fq(a);
                    u(a.Ee.event.P, {
                        U: [[Z(b)]],
                        V: !0
                    });
                    hq(a, !0)
                })["catch"](function(a) {
                    w("Error rating model: " + a)
                })
            }) : "down" === a.kh && hq(a, !1)
        }
    }
    cq.prototype.Zi = function() {
        this.Qb.style.opacity = "1";
        this.Pb.style.opacity = "1";
        this.Qb.style.cursor = "pointer";
        this.Pb.style.cursor = "pointer"
    }
    ;
    cq.prototype.Wi = function() {
        this.Qb.style.opacity = "0.65";
        this.Pb.style.opacity = "0.65";
        this.Qb.style.cursor = "";
        this.Pb.style.cursor = ""
    }
    ;
    function gq(a) {
        a.gj = !0;
        a.kh = "up";
        a.Qb.src = "https://ssl-ccstatic.highwebmedia.com/theatermodeassets/thumbs-up-active.svg";
        a.Pb.src = "https://ssl-ccstatic.highwebmedia.com/theatermodeassets/thumbs-down-inactive.svg"
    }
    function fq(a) {
        a.gj = !0;
        a.kh = "down";
        a.Pb.src = "https://ssl-ccstatic.highwebmedia.com/theatermodeassets/thumbs-down-active.svg";
        a.Qb.src = "https://ssl-ccstatic.highwebmedia.com/theatermodeassets/thumbs-up-inactive.svg"
    }
    ;function iq() {
        var a = decodeURIComponent(document.cookie).split(";");
        a = l(a);
        for (var b = a.next(); !b.done; b = a.next()) {
            for (b = b.value; " " === b.charAt(0); )
                b = b.substring(1);
            if (0 === b.indexOf("agreeterms="))
                return b.substring(11, b.length)
        }
        return ""
    }
    function jq(a, b) {
        var c = document.createElement("li");
        c.textContent = b;
        a.appendChild(c)
    }
    function kq(a, b, c) {
        var d = document.createElement("img");
        d.src = b;
        if (void 0 === c ? 0 : c)
            d.style.margin = "0 10px";
        a.appendChild(d)
    }
    function lq() {
        function a(a) {
            a.style.marginTop = "1em";
            a.style.marginBottom = "1em"
        }
        function b(a, b) {
            a.style.width = "200px";
            a.style.height = "30px";
            a.style.lineHeight = "30px";
            a.style.fontWeight = "700";
            a.style.margin = "10px 5px 0";
            a.style.color = "#0a5a83";
            a.style.display = "inline-block";
            a.style.cursor = "pointer";
            a.style.background = b
        }
        v.call(this);
        var c = this;
        this.Ak = new n("agreementMade");
        this.a.style.display = "none";
        this.a.style.lineHeight = "16.8px";
        (this.aq = "1" === iq()) ? u(this.Ak, void 0) : this.a.style.display = "block";
        var d = document.createElement("div");
        d.style.position = "fixed";
        d.style.top = "0";
        d.style.left = "0";
        d.style.width = "100%";
        d.style.height = "100%";
        d.style.zIndex = "2000";
        d.style.background = "black";
        d.style.fontSize = "12px";
        this.a.appendChild(d);
        var e = document.createElement("div")
          , f = document.createElement("div")
          , h = document.createElement("div")
          , g = document.createElement("p")
          , p = document.createElement("h1")
          , t = document.createElement("p")
          , C = document.createElement("p")
          , B = document.createElement("ul")
          , M = document.createElement("p")
          , F = document.createElement("a")
          , O = document.createElement("a");
        d.appendChild(e);
        e.appendChild(p);
        e.appendChild(f);
        e.appendChild(M);
        e.appendChild(h);
        e.appendChild(g);
        f.appendChild(t);
        f.appendChild(C);
        f.appendChild(B);
        h.appendChild(F);
        h.appendChild(O);
        kq(g, "https://ssl-ccstatic.highwebmedia.com/images/badges/safelabeling.gif");
        kq(g, "https://ssl-ccstatic.highwebmedia.com/images/badges/88x31_RTA-5042-1996-1400-1577-RTA_a.gif", !0);
        kq(g, "https://ssl-ccstatic.highwebmedia.com/images/badges/ApprovedASACPmember.gif");
        jq(B, "I have attained the Age of Majority in my jurisdiction;");
        jq(B, "The sexually explicit material I am viewing is for my own personal use and I will not expose any minors to the material;");
        jq(B, "I desire to receive/view sexually explicit material;");
        jq(B, "I believe that as an adult it is my inalienable constitutional right to receive/view sexually explicit material;");
        jq(B, "I believe that sexual acts between consenting adults are neither offensive nor obscene;");
        jq(B, "The viewing, reading and downloading of sexually explicit materials does not violate the standards of any community, town, city, state or country where I will be viewing, reading and/or downloading the Sexually Explicit Materials;");
        jq(B, "I am solely responsible for any false disclosures or legal ramifications of viewing, reading or downloading any material appearing on this site. I further agree that neither this website nor its affiliates will be held responsible for any legal ramifications arising from any fraudulent entry into or use of this website; ");
        jq(B, "I understand that my use of this website is governed by the website\u2019s Terms which I have reviewed and accepted, and I agree to be bound by such Terms. ");
        jq(B, "I agree that by entering this website, I am subjecting myself, and any business entity in which I have any legal or equitable interest, to the personal jurisdiction of the State of Florida, Miami-Dade County, should any dispute arise at any time between this website, myself and/or such business entity; ");
        jq(B, "This warning page constitutes a legally binding agreement between me, this website and/or any business in which I have any legal or equitable interest. If any provision of this Agreement is found to be unenforceable, the remainder shall be enforced as fully as possible and the unenforceable provision shall be deemed modified to the limited extent required to permit its enforcement in a manner most closely representing the intentions as expressed herein;");
        jq(B, "All performers on this site are over the age of 18, have consented being photographed and/or filmed, believe it is their right to engage in consensual sexual acts for the entertainment and education of other adults and I believe it is my right as an adult to watch them doing what adults do; ");
        jq(B, "The videos and images in this site are intended to be used by responsible adults as sexual aids, to provide sexual education and to provide sexual entertainment; ");
        jq(B, "I understand that providing a false declaration under the penalties of perjury is a criminal offense; and ");
        jq(B, "I agree that this agreement is governed by the Electronic Signatures in Global and National Commerce Act (commonly known as the \u201cE-Sign Act\u201d), 15 U.S.C. \u00a7 7000, et seq., and by choosing to click on \u201cI Agree. Enter Here\u201d and indicating my agreement to be bound by the terms of this agreement, I affirmatively adopt the signature line below as my signature and the manifestation of my consent to be bound by the terms of this agreement. ");
        p.textContent = "YOU MUST BE OVER 18 AND AGREE TO THE TERMS BELOW BEFORE CONTINUING:";
        t.textContent = "This website contains information, links, images and videos of sexually explicit material (collectively, the \u201cSexually Explicit Material\u201d). Do NOT continue if: (i) you are not at least 18 years of age or the age of majority in each and every jurisdiction in which you will or may view the Sexually Explicit Material, whichever is higher (the \u201cAge of Majority\u201d), (ii) such material offends you, or (iii) viewing the Sexually Explicit Material is not legal in each and every community where you choose to view it.";
        C.textContent = "By choosing to enter this website you are affirming under oath and penalties of perjury pursuant to Title 28 U.S.C. \u00a7 1746 and other applicable statutes and laws that all of the following statements are true and correct:";
        M.textContent = "THIS SITE ACTIVELY COOPERATES WITH LAW ENFORCEMENT IN ALL INSTANCES OF SUSPECTED ILLEGAL USE OF THE SERVICE, ESPECIALLY IN THE CASE OF UNDERAGE USAGE OF THE SERVICE.";
        F.textContent = "Exit";
        O.textContent = "I AGREE";
        O.onclick = function() {
            hb("agreeterms", "1", 365);
            u(c.Ak, void 0)
        }
        ;
        F.href = "https://google.com/";
        e.style.position = "absolute";
        e.style.top = "50px";
        e.style.left = "20%";
        e.style.right = "20%";
        e.style.minWidth = "700px";
        e.style.border = "2px solid #ccc";
        e.style.background = "white";
        e.style.padding = "20px";
        e.style.color = "#7f7f7f";
        e.style.borderRadius = "20px";
        p.style.color = "#222";
        p.style.textAlign = "center";
        p.style.fontSize = "inherit";
        f.style.height = "200px";
        f.style.overflow = "auto";
        f.style.padding = "0 15px 0 10px";
        f.style.border = "1px solid #ccc";
        f.style.lineHeight = "16px";
        a(t);
        a(C);
        a(g);
        g.style.marginTop = "25px";
        M.style.color = "red";
        M.style.fontWeight = "bold";
        M.style.fontSize = "11px";
        M.style.marginTop = "1em";
        M.style.marginBottom = "1em";
        h.style.textAlign = "center";
        b(F, "#ccc");
        b(O, "orange");
        g.style.textAlign = "center"
    }
    m(lq, v);
    var mq = new n("showJoinOverlay")
      , nq = !1;
    q(S, function(a) {
        !nq && a.j.ob && void 0 !== eb().join_overlay && u(oq, void 0)
    });
    function pq() {
        rk.call(this);
        var a = this;
        this.a.style.display = "none";
        this.a.style.position = "fixed";
        this.a.style.width = "560px";
        this.a.style.height = "auto";
        this.a.style.backgroundColor = "#ffffff";
        this.a.style.border = "2px solid #cccccc";
        this.a.style.borderRadius = "20px";
        this.a.style.padding = "10px";
        this.a.style.top = "190px";
        this.a.style.fontSize = "12px";
        this.a.style.fontFamily = "UbuntuRegular', Arial, Helvetica, sans-serif";
        this.O.style.backgroundColor = "#000000";
        this.a.appendChild(qq());
        this.Ud = document.createElement("iframe");
        this.Ud.frameBorder = "0";
        this.Ud.marginWidth = "0";
        this.Ud.scrolling = "no";
        this.Ud.marginHeight = "0";
        this.Ud.style.width = "560px";
        this.Ud.style.height = "650px";
        this.Ud.src = "/accounts/register_iframe/";
        this.a.appendChild(this.Ud);
        var b = document.createElement("hr");
        b.style.margin = "15px 0 10px 0";
        b.style.border = "0";
        b.style.color = "#e4e4e4";
        b.style.backgroundColor = "#e4e4e4";
        b.style.height = "1px";
        this.a.appendChild(b);
        b = document.createElement("span");
        var c = document.createElement("img");
        c.src = "https://ssl-ccstatic.highwebmedia.com/theatermodeassets/arrow_back.gif";
        c.style.marginRight = "4px";
        c.style.verticalAlign = "bottom";
        b.appendChild(c);
        this.Wc = document.createElement("a");
        this.Wc.innerText = rf;
        this.Wc.style.color = "#0a5a83";
        this.Wc.style.position = "relative";
        this.Wc.style.top = "-1px";
        this.Wc.onmouseenter = function() {
            a.Wc.style.textDecoration = "underline"
        }
        ;
        this.Wc.onmouseleave = function() {
            a.Wc.style.textDecoration = "none"
        }
        ;
        b.appendChild(this.Wc);
        this.a.appendChild(b);
        b = document.createElement("span");
        b.style.cssFloat = "right";
        c = document.createElement("img");
        c.src = "https://ssl-ccstatic.highwebmedia.com/theatermodeassets/key.gif";
        c.style.marginRight = "4px";
        c.style.verticalAlign = "bottom";
        b.appendChild(c);
        c = document.createElement("span");
        c.innerText = sf;
        c.style.color = "#7f7f7f";
        c.style.position = "relative";
        c.style.top = "-1px";
        c.style.marginRight = "2px";
        b.appendChild(c);
        var d = document.createElement("a");
        d.href = "/auth/login/";
        d.innerText = tf;
        d.style.color = "#0a5a83";
        d.style.position = "relative";
        d.style.top = "-1px";
        d.onmouseenter = function() {
            d.style.textDecoration = "underline"
        }
        ;
        d.onmouseleave = function() {
            d.style.textDecoration = "none"
        }
        ;
        b.appendChild(d);
        this.a.appendChild(b);
        q(this.bc, function() {
            a.A()
        })
    }
    m(pq, rk);
    pq.prototype.show = function(a) {
        var b = this;
        this.Wc.href = a.anchor.href;
        this.Wc.onclick = a.qj;
        y("click", this.Wc, function() {
            b.A()
        });
        this.tc();
        this.a.style.display = "block";
        this.C();
        nq = !0;
        this.Ud.onload = function() {
            var c = b.Ud.contentDocument;
            if (null !== c) {
                var d = c.getElementById("husername");
                null !== d && d.focus();
                c = c.querySelector("a.next");
                c.href = a.anchor.href;
                c.onclick = a.qj;
                y("click", c, function() {
                    b.A()
                })
            }
        }
        ;
        N("RegisterIframe_Shown")
    }
    ;
    function qq() {
        var a = document.createElement("div");
        a.style.animationName = "spin";
        a.style.position = "absolute";
        a.style.top = "50%";
        a.style.left = "50%";
        a.style.width = "30px";
        a.style.height = "31px";
        a.style.margin = "-15px 0 0 -15px";
        a.style.background = 'url("https://ssl-ccstatic.highwebmedia.com/theatermodeassets//loading_spinner.svg")';
        a.style.backgroundSize = "cover";
        a.style.animationName = "spin";
        a.style.webkitAnimationName = "spin";
        a.style.Mr = "2s";
        a.style.webkitAnimationDuration = "2s";
        a.style.Or = "linear";
        a.style.webkitAnimationTimingFunction = "linear";
        a.style.Nr = "infinite";
        a.style.webkitAnimationIterationCount = "infinite";
        a.style.zIndex = "-1";
        return a
    }
    pq.prototype.C = function() {
        this.a.style.left = (document.documentElement.clientWidth - this.a.offsetWidth) / 2 + "px"
    }
    ;
    pq.prototype.A = function() {
        this.$b();
        this.a.style.display = "none"
    }
    ;
    var oq = new n("commandeerJoinOverlayAnchors",{
        sd: 500,
        yj: 1
    })
      , rq = new n("restoreJoinOverlayAnchors",{
        sd: 500,
        yj: 1
    })
      , sq = !1;
    function tq(a) {
        var b = this;
        this.anchor = a = void 0 === a ? document.createElement("a") : a;
        if (void 0 !== eb().join_overlay) {
            var c = function() {
                sq = !0;
                uq(b)
            };
            sq ? setTimeout(function() {
                uq(b)
            }, 0) : q(oq, c);
            var d = function() {
                sq = !1;
                b.restore();
                oq.removeListener(c);
                setTimeout(function() {
                    rq.removeListener(d)
                }, 0)
            };
            q(rq, d)
        }
    }
    function uq(a) {
        a.qj = a.anchor.onclick;
        a.anchor.onclick = function(b) {
            b.preventDefault();
            u(mq, a);
            u(rq, void 0)
        }
    }
    tq.prototype.restore = function() {
        this.anchor.onclick = this.qj;
        this.qj = null
    }
    ;
    function vq(a) {
        a = l(a.getElementsByTagName("a"));
        for (var b = a.next(); !b.done; b = a.next())
            new tq(b.value)
    }
    ;function wq(a, b) {
        v.call(this);
        var c = this;
        this.a.style.display = "none";
        this.a.style.height = "auto";
        this.a.style.left = "-1px";
        this.a.style.backgroundColor = R.Tp;
        this.a.style.border = "1px solid #2C6990";
        this.a.style.borderTop = "none";
        this.a.style.borderRadius = "0 0 5px 5px";
        this.a.style.fontFamily = "UbuntuBold, Helvetica, Arial, sans-serif";
        this.a.style.padding = "5px 0";
        this.pc = document.createElement("a");
        this.pc.style.textDecoration = "none";
        this.pc.innerText = yc;
        this.pc.style.display = "block";
        this.pc.style.fontSize = "14px";
        this.pc.style.padding = "5px 0 5px 10px";
        this.pc.style.color = R.Lm;
        this.pc.onmouseenter = function() {
            c.pc.style.backgroundColor = R.Km
        }
        ;
        this.pc.onmouseleave = function() {
            c.pc.style.backgroundColor = "transparent"
        }
        ;
        this.pc.onclick = function() {
            c.A()
        }
        ;
        this.a.appendChild(this.pc);
        var d = document.createElement("form");
        d.method = "post";
        d.action = "/auth/logout/";
        d.style.display = "none";
        var e = document.createElement("input");
        e.type = "hidden";
        e.name = "csrfmiddlewaretoken";
        e.value = gb("csrftoken");
        d.appendChild(e);
        this.a.appendChild(d);
        var f = document.createElement("a");
        f.style.textDecoration = "none";
        f.innerText = zc;
        f.style.display = "block";
        f.style.fontSize = "14px";
        f.style.padding = "5px 0 5px 10px";
        f.href = "#";
        f.style.color = R.Lm;
        f.onmouseenter = function() {
            f.style.backgroundColor = R.Km
        }
        ;
        f.onmouseleave = function() {
            f.style.backgroundColor = "transparent"
        }
        ;
        f.href = "/auth/logout/";
        f.onclick = function(a) {
            a.preventDefault();
            c.A();
            Ki("Are you sure you want to log out?", function() {
                e.value = gb("csrftoken");
                d.submit()
            })
        }
        ;
        this.a.appendChild(f);
        void 0 !== b && (this.pc.href = "/p/" + b + "/?tab=bio");
        void 0 !== a && (a.onclick = function(a) {
            a.preventDefault();
            c.toggle()
        }
        )
    }
    m(wq, v);
    wq.prototype.toggle = function() {
        "none" === this.a.style.display ? this.show() : this.A()
    }
    ;
    wq.prototype.show = function() {
        this.a.style.display = "block"
    }
    ;
    wq.prototype.A = function() {
        this.a.style.display = "none"
    }
    ;
    function xq(a) {
        function b(a, b, c) {
            g.src = void 0 === b ? "" : zj(b);
            p.innerText = a;
            d.ro.pc.href = "/p/" + a + "/?tab=bio";
            d.sa.href = "/p/" + a + "/?tab=tokens";
            c ? (C.innerText = wc,
            B.style.visibility = "hidden") : (C.innerText = xc,
            B.style.visibility = "visible")
        }
        function c() {
            h.onmouseenter = function() {
                return p.style.textDecoration = ""
            }
            ;
            cb("touchstart", window, c)
        }
        v.call(this);
        var d = this;
        this.a.style.display = "none";
        this.a.style.position = "static";
        this.a.style.margin = "13px 12px 0 0";
        this.a.style.maxHeight = "73px";
        this.a.style.width = "auto";
        this.a.style.cssFloat = "right";
        this.a.style.overflow = "visible";
        var e = document.createElement("div");
        e.style.width = "auto";
        e.style.border = "1px solid " + R.Dj;
        e.style.borderBottom = "none";
        e.style.borderRadius = "5px 5px 0 0";
        e.style.color = R.Ba;
        e.style.fontSize = "11px";
        e.style.background = R.Jm;
        e.style.position = "relative";
        e.style.boxSizing = "border-box";
        this.a.appendChild(e);
        var f = document.createElement("div");
        f.style.background = R.Dj;
        f.style.padding = "2px 0 3px 0";
        f.style.width = "100%";
        e.appendChild(f);
        var h = document.createElement("div");
        h.style.cursor = "pointer";
        f.appendChild(h);
        var g = document.createElement("img");
        g.style.verticalAlign = "top";
        g.style.paddingLeft = "2px";
        g.style.position = "relative";
        g.style.marginLeft = "3px";
        g.style.top = "-1px";
        g.style.width = "16px";
        g.style.height = "16px";
        h.appendChild(g);
        var p = document.createElement("span");
        p.style.textDecoration = "none";
        p.style.display = "inline-block";
        p.style.color = R.Vp;
        p.style.fontFamily = "UbuntuBold, Helvetica, Arial, sans-serif";
        p.style.fontSize = "12px";
        p.style.position = "relative";
        p.style.margin = "0 5px";
        p.style.maxWidth = "150px";
        p.style.whiteSpace = "nowrap";
        p.style.overflow = "hidden";
        p.style.textOverflow = "ellipsis";
        p.style.verticalAlign = "bottom";
        p.style.lineHeight = "17px";
        p.style.cursor = "pointer";
        h.appendChild(p);
        f = document.createElement("img");
        f.style.border = "none";
        f.style.verticalAlign = "top";
        f.style.position = "relative";
        f.style.top = "4px";
        f.style.width = "8px";
        f.style.height = "8px";
        f.src = "https://ssl-ccstatic.highwebmedia.com/theatermodeassets/userinfoarrowdown.svg";
        h.appendChild(f);
        h.onmouseenter = function() {
            p.style.textDecoration = "underline"
        }
        ;
        h.onmouseleave = function() {
            p.style.textDecoration = ""
        }
        ;
        this.ro = new wq(h,void 0);
        e.appendChild(this.ro.a);
        y("touchstart", window, c);
        f = document.createElement("div");
        f.style.display = "table";
        f.style.background = R.Jm;
        f.style.height = "100%";
        f.style.width = "100%";
        f.style.padding = "8px 6px 8px 7.025px";
        f.style.boxSizing = "border-box";
        e.appendChild(f);
        e = document.createElement("div");
        e.style.display = "table-row";
        e.style.height = "19px";
        f.appendChild(e);
        var t = document.createElement("div");
        t.innerText = Ac + ":";
        t.style.color = R.sk;
        t.style.display = "table-cell";
        t.style.whiteSpace = "nowrap";
        t.style.width = "49px";
        t.style.paddingRight = "5px";
        t.style.fontFamily = "UbuntuMedium, Helvetica, Arial, sans-serif";
        e.appendChild(t);
        var C = document.createElement("div");
        C.style.color = R.sk;
        C.style.display = "table-cell";
        C.style.whiteSpace = "nowrap";
        C.style.width = "107px";
        C.style.padding = "0 5px 0 2px";
        C.style.fontFamily = "UbuntuMedium, Helvetica, Arial, sans-serif";
        e.appendChild(C);
        t = document.createElement("div");
        t.style.display = "table-cell";
        t.style.whiteSpace = "nowrap";
        t.style.width = "55px";
        t.style.fontFamily = "UbuntuMedium, Helvetica, Arial, sans-serif";
        e.appendChild(t);
        e = document.createElement("div");
        t.appendChild(e);
        var B = document.createElement("a");
        B.style.textDecoration = "none";
        B.href = "/supporter/upgrade/";
        B.innerText = "(" + vc + ")";
        B.style.color = R.Cm;
        B.style.display = "block";
        B.style.visibility = "hidden";
        yq(B);
        e.appendChild(B);
        e = document.createElement("div");
        e.style.display = "table-row";
        e.style.height = "15px";
        f.appendChild(e);
        f = document.createElement("div");
        f.innerText = "" + Bc;
        f.style.color = R.sk;
        f.style.display = "table-cell";
        f.style.whiteSpace = "nowrap";
        f.style.width = "49px";
        f.style.paddingRight = "5px";
        f.style.lineHeight = "15px";
        f.style.fontFamily = "UbuntuMedium, Helvetica, Arial, sans-serif";
        e.appendChild(f);
        f = document.createElement("div");
        f.style.display = "table-cell";
        f.style.whiteSpace = "nowrap";
        f.style.width = "107px";
        f.style.padding = "0 5px 0 2px";
        f.style.fontFamily = "UbuntuBold, Helvetica, Arial, sans-serif";
        e.appendChild(f);
        this.sa = document.createElement("a");
        this.sa.style.textDecoration = "none";
        this.sa.style.fontFamily = "UbuntuBold, Helvetica, Arial, sans-serif";
        this.sa.style.fontSize = "13px";
        this.sa.style.color = R.Ba;
        this.sa.style.display = "block";
        this.sa.style.lineHeight = "13px";
        yq(this.sa);
        f.appendChild(this.sa);
        f = document.createElement("div");
        f.style.display = "table-cell";
        f.style.whiteSpace = "nowrap";
        f.style.width = "55px";
        f.style.lineHeight = "15px";
        f.style.fontFamily = "UbuntuMedium, Helvetica, Arial, sans-serif";
        e.appendChild(f);
        e = document.createElement("a");
        e.style.textDecoration = "none";
        e.href = "/tipping/purchase_tokens/";
        e.innerText = "(" + Cc + ")";
        e.style.color = R.Cm;
        e.style.display = "block";
        yq(e);
        zq(e);
        f.appendChild(e);
        void 0 !== a && (b(a.username, a.Bg, a.Se),
        this.rf(a.sa),
        this.show());
        q(S, function(a) {
            b(a.h.userName, a.h.Yp, a.h.Se);
            d.rf(a.h.sa)
        });
        q(qj, function(a) {
            d.rf(isNaN(a.Sb) ? 0 : a.Sb)
        })
    }
    m(xq, v);
    xq.prototype.A = function() {
        this.a.style.display = "none"
    }
    ;
    xq.prototype.show = function() {
        this.a.style.display = ""
    }
    ;
    xq.prototype.rf = function(a) {
        this.sa.innerText = a + " " + Ab(a, !0)
    }
    ;
    function yq(a) {
        a.onmouseenter = function() {
            return a.style.textDecoration = "underline"
        }
        ;
        a.onmouseleave = function() {
            return a.style.textDecoration = ""
        }
    }
    function zq(a) {
        a.onclick = function(a) {
            a.preventDefault();
            Mh("/tipping/purchase_tokens/", "_blank", "height=615, width=850")
        }
    }
    ;function Aq(a, b) {
        function c(a, b) {
            var c = void 0 === c ? !1 : c;
            var d = (new tq).anchor;
            d.href = b;
            d.innerText = a;
            d.style.color = R.yo;
            d.style.textDecoration = "none";
            d.style.fontSize = "13.997px";
            d.style.fontWeight = "400";
            d.style.marginRight = "20px";
            d.style.textShadow = "1px 1px 0 #000";
            d.style.fontFamily = "UbuntuMedium, Helvetica, Arial, sans-serif";
            d.onmouseenter = function() {
                d.style.textDecoration = "underline"
            }
            ;
            d.onmouseleave = function() {
                d.style.textDecoration = "none"
            }
            ;
            c && (d.target = "_blank");
            return d
        }
        v.call(this);
        var d = this;
        this.a.style.position = "static";
        this.a.style.minWidth = "500px";
        this.a.style.height = "auto";
        this.a.style.maxHeight = "125px";
        this.a.style.background = R.Qn;
        this.a.style.backgroundColor = R.bgColor;
        this.tk = new xq;
        this.N(this.tk);
        this.Rh = document.createElement("a");
        this.Rh.href = "/";
        var e = document.createElement("img");
        e.src = "https://ssl-ccstatic.highwebmedia.com/theatermodeassets/logo.svg";
        e.style.padding = "6px 0 0 15px";
        e.style.border = "none";
        e.style.width = "198px";
        e.style.height = "61px";
        this.Rh.appendChild(e);
        this.a.appendChild(this.Rh);
        R.up && (e = this.N(a.tf.header),
        e.a.style.width = "468px",
        e.a.style.height = "60px",
        e.a.style.position = "relative",
        e.a.style.top = "-2px",
        e.a.style.left = "55px");
        e = document.createElement("div");
        e.innerText = R.Fp;
        e.style.fontSize = "9.999px";
        e.style.fontFamily = "UbuntuMedium, Helvetica, Arial, sans-serif";
        e.style.lineHeight = "7px";
        e.style.color = R.ym;
        e.style.margin = "4px 5px 2px 32px";
        e.className = "cbLogo";
        this.a.appendChild(e);
        this.ib = document.createElement("div");
        this.ib.style.backgroundColor = R.Dj;
        this.ib.style.borderBottom = "3px solid " + R.Fl;
        this.ib.style.width = "100%";
        this.ib.style.minWidth = "600px";
        this.ib.style.height = "39px";
        this.ib.style.padding = "11px 0 0 32px";
        this.ib.style.overflow = "hidden";
        this.ib.style.boxSizing = "border-box";
        this.ib.style.zIndex = "1000";
        e = document.createElement("a");
        e.href = "/";
        e.style.display = "none";
        e.style.cssFloat = "left";
        e.style.position = "relative";
        e.style.top = "-5px";
        e.style.left = "-15px";
        var f = document.createElement("img");
        f.src = "https://ssl-ccstatic.highwebmedia.com/theatermodeassets/header_logo.png";
        f.style.height = "28px";
        e.appendChild(f);
        this.ib.appendChild(e);
        this.ib.appendChild(c(dc, "/"));
        this.Jk = c("BROADCAST YOURSELF", "/b/asdfboobs/");
        this.ib.appendChild(this.Jk);
        R.vp && this.ib.appendChild(c(ec, "/tube/"));
        this.ib.appendChild(c(fc, "/tags/"));
        R.wp && this.ib.appendChild(c(gc, "/tipping/free_tokens/"));
        this.Cj = c(hc, "/my_collection/");
        this.Cj.style.display = "none";
        this.ib.appendChild(this.Cj);
        this.Kg = c(lc, "/auth/login/");
        this.Kg.style.display = "none";
        this.Kg.href = "/auth/login/";
        this.Kg.onclick = function(a) {
            a.preventDefault();
            u(b, void 0)
        }
        ;
        this.ib.appendChild(this.Kg);
        this.uc = document.createElement("div");
        this.uc.style.backgroundColor = R.qm;
        this.uc.style.borderRadius = "4px 4px 0 0";
        this.uc.style.height = "36px";
        this.uc.style.padding = "8px 15px 0 15px";
        this.uc.style.cssFloat = "right";
        this.uc.style.position = "relative";
        this.uc.style.top = "-11px";
        this.uc.style.right = "20px";
        this.uc.style.display = "none";
        var h = document.createElement("a");
        h.href = "/accounts/register/?src=header";
        h.innerHTML = mc;
        h.style.fontSize = "20px";
        h.style.fontWeight = "bold";
        h.style.color = "#ffffff";
        h.style.textDecoration = "none";
        h.style.backgroundColor = R.qm;
        h.style.padding = "0px 6px 0px 0px";
        h.style.cssFloat = "left";
        h.style.position = "relative";
        h.style.top = "2px";
        h.onmouseenter = function() {
            h.style.textDecoration = "underline"
        }
        ;
        h.onmouseleave = function() {
            h.style.textDecoration = "none"
        }
        ;
        this.uc.appendChild(h);
        e = document.createElement("div");
        e.style.borderTop = "6px solid transparent";
        e.style.borderBottom = "6px solid transparent";
        e.style.borderLeft = "6.5px solid white";
        e.style.top = "8px";
        e.style.position = "relative";
        e.style.cssFloat = "right";
        this.uc.appendChild(e);
        this.ib.appendChild(this.uc);
        this.a.appendChild(this.ib);
        q(S, function(a) {
            a.j.ob ? (d.tk.A(),
            d.uc.style.display = "",
            d.Kg.style.display = "",
            d.Cj.style.display = "none",
            d.Jk.href = "/accounts/register/?src=broadcast") : (d.tk.show(),
            d.uc.style.display = "none",
            d.Kg.style.display = "none",
            d.Cj.style.display = "",
            d.Jk.href = "/b/" + a.j.username() + "/")
        })
    }
    m(Aq, v);
    function Bq() {
        v.call(this);
        var a = this;
        this.tf = {
            header: new Cq,
            rightTop: new Cq,
            rightMiddle: new Cq,
            rightBottom: new Cq,
            footerLeft: new Cq,
            footerMiddle: new Cq,
            footerRight: new Cq
        };
        q(S, function(b) {
            if (!b.h.Ig && !b.h.Se && "" !== b.h.Ai.header) {
                for (var c in a.tf)
                    if (a.tf.hasOwnProperty(c)) {
                        for (var d = a.tf[c]; null !== d.a.firstChild; )
                            d.a.removeChild(d.a.firstChild);
                        d.tg = document.createElement("ins");
                        d.tg.style.display = "inline-block";
                        d.tg.style.width = "100%";
                        d.tg.style.height = "100%";
                        d.tg.className = "adsbyxa";
                        d.a.appendChild(d.tg);
                        "" !== b.h.Ai[c] && a.tf[c].tg.setAttribute("data-ad-zone", b.h.Ai[c])
                    }
                window.xadqueue = [];
                window.xadgo()
            }
        })
    }
    m(Bq, v);
    function Cq() {
        v.call(this);
        this.a.style.display = "inline-block";
        this.a.style.position = "relative";
        this.a.className = "ad"
    }
    m(Cq, v);
    function Dq(a) {
        switch (a) {
        case "rightTop":
            return "top_skyscraper";
        case "rightMiddle":
            return "mid_skyscraper";
        case "rightBottom":
            return "tbot_skyscraper";
        case "footerLeft":
            return "left_footer";
        case "footerMiddle":
            return "mid_footer";
        case "footerRight":
            return "right_footer";
        default:
            return w("Unexpected ad zone for track: " + a),
            ""
        }
    }
    ;function Eq(a) {
        Aq.call(this, a, sh);
        a = (new tq).anchor;
        a.href = "/";
        var b = document.createElement("img");
        b.src = R.qo;
        b.style.padding = "6px 0 0 15px";
        b.style.border = "none";
        b.style.width = (void 0 !== R.yl ? R.yl : 198) + "px";
        b.style.maxWidth = Math.max(734, window.innerWidth - 281) + "px";
        b.style.height = "61px";
        b.className = "cbLogo";
        a.appendChild(b);
        this.a.replaceChild(a, this.Rh);
        this.Rh = a;
        this.Qq = b
    }
    m(Eq, Aq);
    Eq.prototype.Y = function() {
        this.Qq.style.maxWidth = Math.max(734, window.innerWidth - 281) + "px"
    }
    ;
    var Fq = new n("newFollowedOnline")
      , Gq = new n("onlineFollowedStorage")
      , Hq = new n("onlineFollowedStorage");
    function Iq() {
        if (x()) {
            var a = window.localStorage.getItem("onlineFollowedTab");
            null !== a && (a = JSON.parse(a),
            a.timestamp = Date.now(),
            window.localStorage.setItem("onlineFollowedTab", JSON.stringify(a)))
        }
    }
    function Jq() {
        if (x()) {
            var a = window.localStorage.getItem("onlineFollowedTab");
            if (null !== a) {
                a = JSON.parse(a);
                var b = a.username
                  , c = a.onlineFollowedList
                  , d = {
                    nc: {
                        Ye: c.onlineFollowed.online,
                        total: c.onlineFollowed.total
                    },
                    Vj: []
                };
                c = l(c.roomList);
                for (var e = c.next(); !e.done; e = c.next())
                    e = e.value,
                    d.Vj.push({
                        i: e.room,
                        Ih: e.image
                    });
                c = a.seenRooms;
                e = [];
                for (var f = l(a.unseenRooms), h = f.next(); !h.done; h = f.next())
                    h = h.value,
                    e.push({
                        i: h.room,
                        Ih: h.image
                    });
                return {
                    username: b,
                    Lj: d,
                    Yc: c,
                    Fm: e,
                    Gf: a.flash,
                    timestamp: a.timestamp
                }
            }
        }
    }
    function Kq() {
        x() && y("storage", window, function(a) {
            if ("onlineFollowedTab" === a.key) {
                var b = Jq();
                void 0 !== b && u(Gq, b)
            }
            "followedDropdownClicked" === a.key && null !== a.newValue && (b = Jq(),
            void 0 !== b && (b.timestamp = JSON.parse(a.newValue).timestamp,
            b.Gf = !1,
            b.Fm = [],
            u(Hq, b)))
        })
    }
    function Lq(a) {
        a = a.split(/[()/]+/);
        return 4 !== a.length ? {
            Ye: 0,
            total: 0
        } : {
            Ye: parseInt(a[1]),
            total: parseInt(a[2])
        }
    }
    function Mq() {
        function a(a) {
            return a.map(function(a) {
                return {
                    i: a.room,
                    Ih: a.image
                }
            })
        }
        return z("follow/api/online_followed_rooms/").then(function(b) {
            b = new D(b.responseText);
            return {
                nc: {
                    Ye: K(b, "online"),
                    total: K(b, "total")
                },
                Vj: a(E(b, "online_rooms"))
            }
        })
    }
    ;function Nq(a, b, c) {
        v.call(this);
        var d = this;
        this.la = a;
        this.rd = b;
        this.jc = c;
        this.Ea = document.createElement("div");
        this.mb = document.createElement("div");
        this.$g = !1;
        this.first = !0;
        this.yg = [];
        this.Ue = [];
        this.Yc = {};
        this.Cn = {};
        this.a.onclick = function() {
            d.A()
        }
        ;
        y("click", document, function() {
            d.A()
        });
        a = getComputedStyle(this.la);
        this.a.style.fontSize = a.fontSize;
        this.a.style.fontFamily = a.fontFamily;
        this.a.style.width = "auto";
        this.a.style.height = "auto";
        this.a.style.position = "absolute";
        this.a.style.display = "none";
        this.a.style.overflow = "";
        this.a.style.cursor = "default";
        this.a.style.color = R.lb;
        this.a.style.zIndex = "1000";
        this.a.style.top = "0px";
        this.a.style.left = "0px";
        y("mousedown", this.a, function(a) {
            a.preventDefault()
        });
        this.mb.style.position = "relative";
        this.mb.style.height = "31px";
        this.mb.style.borderRadius = "4px 4px 0px 0px";
        this.mb.style.webkitBorderRadius = "4px 4px 0px 0px";
        this.mb.style.borderTop = "1px solid " + R.cd;
        this.mb.style.borderLeft = "1px solid " + R.cd;
        this.mb.style.borderRight = "1px solid " + R.cd;
        this.mb.style.backgroundColor = R.bd;
        this.mb.style.zIndex = "2";
        this.mb.style.padding = "5px 11px 4px";
        this.mb.style.display = "inline-block";
        this.mb.style.boxSizing = "border-box";
        this.mb.style.cursor = "pointer";
        this.mb.innerHTML = this.la.innerHTML;
        this.jc && (this.mb.firstChild.textContent = Yd + " ");
        this.Vo = this.mb.querySelector(".followed_counts");
        this.nq = this.mb.querySelector(".followed_arrow_container");
        this.Oc = this.mb.querySelector(".followed_arrow");
        this.Oc.style.fill = R.lb;
        this.la.onclick = function(a) {
            a.preventDefault();
            a.stopPropagation();
            d.A()
        }
        ;
        this.a.appendChild(this.mb);
        this.Ea.style.color = "#0a5a83";
        this.Ea.style.width = "314px";
        this.Ea.style.maxHeight = "450px";
        this.Ea.style.overflowY = "auto";
        this.Ea.style.overflowX = "hidden";
        this.Ea.style.position = "relative";
        this.Ea.style.boxSizing = "border-box";
        this.Ea.style.padding = "8px";
        this.Ea.style.border = "1px solid " + R.cd;
        this.Ea.style.borderRadius = "0px 4px 4px 4px";
        this.Ea.style.webkitBorderRadius = "0px 4px 4px 4px";
        this.Ea.style.backgroundColor = R.bd;
        this.Ea.style.marginTop = "-1px";
        this.Ea.style.zIndex = "1";
        this.Ea.onclick = function(a) {
            a.stopPropagation()
        }
        ;
        this.a.appendChild(this.Ea);
        q(mh, function() {
            d.A()
        });
        q(S, function() {
            Oq(d)
        });
        Oq(this)
    }
    m(Nq, v);
    function Pq(a, b) {
        for (var c = l(b), d = c.next(); !d.done; d = c.next())
            d = d.value,
            a.Yc[d] = !0,
            a.Cn[d] = !0
    }
    Nq.prototype.update = function(a, b) {
        var c = this;
        b = void 0 === b ? !1 : b;
        this.nc = a.nc;
        Qq(this);
        for (var d = {}, e = [], f = [], h = l(a.Vj), g = h.next(); !g.done; g = h.next())
            g = g.value,
            void 0 !== this.Yc[g.i] || this.first ? f.push(g) : e.push(g),
            this.Yc[g.i] = !0,
            d[g.i] = g;
        h = 0 < e.length;
        this.Ue = this.Ue.reduce(function(a, b) {
            void 0 === d[b.i] || c.Cn[b.i] || a.push(d[b.i]);
            return a
        }, []);
        e = l(e);
        for (g = e.next(); !g.done; g = e.next())
            this.Ue.push(g.value);
        e = {};
        this.yg = [];
        var p = l(this.Ue);
        for (g = p.next(); !g.done; g = p.next())
            g = g.value,
            e[g.i] = !0,
            this.yg.push(new Rq(g,!0,this.jc));
        f = l(f);
        for (g = f.next(); !g.done; g = f.next())
            g = g.value,
            void 0 === e[g.i] && this.yg.push(new Rq(g,!1,this.jc));
        Oq(this);
        (f = !this.first && h) && !b && u(Fq, void 0);
        this.first = !1;
        return {
            Gf: f,
            jp: this.Ue
        }
    }
    ;
    function Oq(a) {
        if (!a.$g) {
            for (; null !== a.Ea.firstChild; )
                a.Ea.removeChild(a.Ea.firstChild);
            if (!a.rd) {
                var b = document.createElement("div");
                b.style.margin = "5px 3px";
                var c = document.createElement("a");
                c.style.height = "auto";
                c.style.width = "auto";
                c.style.border = "none";
                c.style.borderRadius = "0px";
                c.style.backgroundColor = R.bd;
                c.style.textDecoration = "none";
                c.style.cursor = "pointer";
                c.style.cssFloat = "none";
                c.style.padding = "0px";
                c.style.color = R.lc;
                c.textContent = Ag;
                c.href = a.la.href;
                c.onmouseenter = function() {
                    c.style.textDecoration = "underline";
                    c.style.color = R.lb
                }
                ;
                c.onmouseleave = function() {
                    c.style.textDecoration = "none";
                    c.style.color = R.lc
                }
                ;
                c.onclick = function() {
                    N("FollowedDropdownSeeAll")
                }
                ;
                b.appendChild(c);
                a.Ea.appendChild(b)
            }
            a.Ea.style.width = "auto";
            b = document.createElement("div");
            b.style.display = "table";
            for (var d = document.createElement("div"), e = 0, f = l(a.yg), h = f.next(); !h.done; h = f.next()) {
                h = h.value;
                0 === e % 2 && (d = document.createElement("div"),
                d.style.display = "table-row",
                b.appendChild(d));
                var g = document.createElement("div");
                g.style.display = "table-cell";
                g.appendChild(h.ge);
                d.appendChild(g);
                e += 1
            }
            a.Ea.appendChild(b);
            if (0 === a.yg.length) {
                b = document.createElement("div");
                b.style.margin = "5px 3px 5px 3px";
                b.style.fontSize = "12px";
                b.style.fontFamily = "UbuntuRegular";
                b.style.color = R.Kd;
                if (a.rd) {
                    var p = document.createElement("a");
                    p.style.height = "auto";
                    p.style.width = "auto";
                    p.style.border = "none";
                    p.style.borderRadius = "0px";
                    p.style.backgroundColor = R.bd;
                    p.style.color = R.lc;
                    p.style.textDecoration = "none";
                    p.style.cursor = "pointer";
                    p.style.cssFloat = "none";
                    p.style.padding = "0px";
                    p.href = "/accounts/register/?src=followed_tab&next=" + window.location.pathname;
                    p.textContent = nc;
                    p.onmouseenter = function() {
                        p.style.textDecoration = "underline"
                    }
                    ;
                    p.onmouseleave = function() {
                        p.style.textDecoration = "none"
                    }
                    ;
                    d = document.createElement("span");
                    d.textContent = " " + Gf + " ";
                    var t = document.createElement("a");
                    t.style.height = "auto";
                    t.style.width = "auto";
                    t.style.border = "none";
                    t.style.borderRadius = "0px";
                    t.style.backgroundColor = R.bd;
                    t.style.color = R.lc;
                    t.style.textDecoration = "none";
                    t.style.cursor = "pointer";
                    t.style.cssFloat = "none";
                    t.style.padding = "0px";
                    t.href = "/auth/login/?next=" + window.location.pathname;
                    t.textContent = jc;
                    t.onclick = function(b) {
                        return !a.jc || b.metaKey || b.ctrlKey ? !0 : (a.A(),
                        u(sh, void 0),
                        !1)
                    }
                    ;
                    t.onmouseenter = function() {
                        t.style.textDecoration = "underline"
                    }
                    ;
                    t.onmouseleave = function() {
                        t.style.textDecoration = "none"
                    }
                    ;
                    b.appendChild(p);
                    b.appendChild(d);
                    b.appendChild(t)
                }
                d = document.createElement("span");
                d.textContent = a.rd ? Bg : Cg;
                b.appendChild(d);
                a.Ea.appendChild(b);
                a.Ea.style.width = "331px"
            }
        }
    }
    function Qq(a) {
        a.Vo.textContent = "(" + a.nc.Ye + "/" + a.nc.total + ")"
    }
    Nq.prototype.show = function() {
        this.$g || (this.nq.style.display = "inline-block",
        this.a.style.display = "block",
        this.a.style.zIndex = "2000",
        this.Ea.scrollTop = 0,
        this.$g = !0,
        this.Ue = [])
    }
    ;
    Nq.prototype.A = function() {
        if (this.$g) {
            this.a.style.display = "none";
            this.$g = !1;
            for (var a = l(this.yg), b = a.next(); !b.done; b = a.next())
                b.value.hh();
            Oq(this)
        }
    }
    ;
    function Rq(a, b, c) {
        var d = this;
        this.i = a;
        this.Wn = b;
        this.jc = c;
        this.ge = document.createElement("div");
        this.Da = document.createElement("a");
        this.ge.style.display = "inline-block";
        this.ge.style.width = "140px";
        this.ge.style.height = "130px";
        this.ge.style.borderRadius = "4px";
        this.ge.style.border = "1px solid rgba(100, 100, 100, 0.5)";
        this.ge.style.margin = "3px";
        this.ge.style.cursor = "pointer";
        this.Da.style.width = "140px";
        this.Da.style.height = "130px";
        this.Da.style.borderRadius = "4px";
        this.Da.style.border = "none";
        this.Da.style.padding = "0px";
        this.Da.style.margin = "0px";
        this.Da.style.display = "block";
        this.Da.style.cursor = "pointer";
        this.Da.style.overflow = "hidden";
        this.Da.style.textOverflow = "ellipsis";
        this.Da.style.color = R.ec;
        this.Da.href = "/" + this.i.i + "/";
        this.Da.style.backgroundColor = this.Wn ? "#f9eed0" : "#f0f1f1";
        this.Da.onmouseenter = function() {
            d.Da.style.color = R.lb
        }
        ;
        this.Da.onmouseleave = function() {
            d.Da.style.color = R.ec
        }
        ;
        a = document.createElement("img");
        a.src = this.i.Ih;
        a.width = 140;
        a.height = 105;
        a.style.borderRadius = "3px 3px 0px 0px";
        this.Da.appendChild(a);
        a = document.createElement("span");
        a.textContent = this.i.i;
        a.style.padding = "5px";
        this.Da.appendChild(a);
        this.Da.onclick = function(a) {
            N("FollowedDropdownVisit");
            return !d.jc || a.metaKey || a.ctrlKey ? !0 : (u(mh, d.i.i),
            !1)
        }
        ;
        this.ge.appendChild(this.Da)
    }
    Rq.prototype.hh = function() {
        this.Wn = !1;
        this.Da.style.backgroundColor = "#f0f1f1";
        this.Da.style.color = R.ec
    }
    ;
    function Sq(a, b, c, d, e) {
        function f(a, b) {
            b = void 0 === b ? !1 : b;
            a.timestamp <= g.Nh || a.username !== g.username || (g.nc = a.Lj.nc,
            Qq(g),
            g.kc.Yc = a.Yc,
            g.kc.Ue = a.Fm,
            g.kc.update(a.Lj, !0),
            0 < a.Fm.length ? (g.Xf = !0,
            g.la.style.backgroundColor = "#FAE6C4",
            g.la.style.color = R.lb,
            g.Oc.style.fill = R.lb) : (g.Xf = !1,
            g.hh()),
            a.Gf && !b && g.Gf(),
            g.Nh = a.timestamp)
        }
        function h() {
            clearInterval(g.Hf);
            g.Hf = void 0;
            g.Xf = !1;
            g.hh();
            N("FollowedDropdownOpened");
            g.kc.show();
            if (x()) {
                var a = window.localStorage.getItem("onlineFollowedTab");
                if (null !== a) {
                    a = JSON.parse(a);
                    for (var b = l(a.unseenRooms), c = b.next(); !c.done; c = b.next())
                        a.seenRooms[c.value.room] = !0;
                    a.timestamp = Date.now();
                    a.flash = !1;
                    a.unseenRooms = [];
                    window.localStorage.setItem("onlineFollowedTab", JSON.stringify(a))
                }
            }
            x() && window.localStorage.setItem("followedDropdownClicked", JSON.stringify({
                timestamp: Date.now()
            }))
        }
        var g = this;
        this.la = a;
        this.kc = b;
        this.rd = c;
        this.username = d;
        this.jc = e;
        this.Xf = !1;
        this.Vo = this.la.querySelector(".followed_counts");
        this.Oc = this.la.querySelector(".followed_arrow");
        this.la.onmouseenter = function() {
            g.jc ? (g.la.style.borderBottom = "1px solid " + R.Si,
            g.la.style.color = R.lb,
            g.la.style.backgroundColor = R.bd,
            g.Oc.style.fill = R.lb) : (g.la.style.color = "",
            g.la.style.backgroundColor = "",
            g.Oc.style.fill = "")
        }
        ;
        this.la.onmouseleave = function() {
            g.Xf ? (g.la.style.backgroundColor = "#FAE6C4",
            g.la.style.color = R.lb,
            g.Oc.style.fill = R.lb,
            g.la.style.borderBottom = "1px solid " + R.cd) : g.jc && (g.la.style.borderBottom = "1px solid " + R.cd,
            g.la.style.color = R.ec,
            g.la.style.backgroundColor = R.mg,
            g.Oc.style.fill = R.ec)
        }
        ;
        this.nc = Lq(this.la.innerText);
        Qq(this);
        this.la.onclick = function(a) {
            a.metaKey || a.ctrlKey || (a.preventDefault(),
            a.stopPropagation(),
            h())
        }
        ;
        y("keydown", this.la, function(a) {
            if (32 === a.keyCode || "Space" === a.code)
                a.preventDefault(),
                h()
        });
        !0 === window.showcam && "string" === typeof window.broadcaster && Pq(this.kc, [window.broadcaster]);
        q(S, function(a) {
            Pq(g.kc, [a.h.i])
        });
        if (!this.rd) {
            Kq();
            q(Gq, function(a) {
                f(a)
            });
            q(Hq, function(a) {
                a.timestamp <= g.Nh || (g.nc = a.Lj.nc,
                Qq(g),
                g.kc.Yc = a.Yc,
                g.kc.Ue = [],
                g.kc.update(a.Lj, !0),
                g.Nh = a.timestamp,
                clearInterval(g.Hf),
                g.Hf = void 0,
                g.Xf = !1,
                g.hh())
            });
            window._followedTabUpdate = function(a) {
                Pq(g.kc, [a]);
                Tq(g, !0)
            }
            ;
            a = 0 === window.location.pathname.lastIndexOf("/followed-cams/", 0) ? void 0 : Jq();
            void 0 !== a && a.username === this.username && 3E4 > Date.now() - a.timestamp ? (this.kc.first = !1,
            f(a, !0),
            Iq(),
            Tq(this, !0)) : Tq(this);
            var p = function(a) {
                clearInterval(g.Pp);
                g.Pp = setInterval(function() {
                    g.kc.$g || Tq(g)
                }, a)
            };
            p(1E4);
            setTimeout(function() {
                p(3E4)
            }, 6E5);
            setTimeout(function() {
                p(6E5)
            }, 18E5);
            setTimeout(function() {
                clearInterval(g.Pp)
            }, 216E5);
            q(Fq, function() {
                g.Gf()
            })
        }
    }
    Sq.prototype.Gf = function() {
        var a = this;
        if (void 0 === this.Hf) {
            this.Xf = !0;
            var b = 3
              , c = !0;
            this.Hf = setInterval(function() {
                if (c) {
                    if (a.la.style.backgroundColor = "#FAE6C4",
                    a.la.style.color = R.lb,
                    a.Oc.style.fill = R.lb,
                    --b,
                    0 === b) {
                        clearInterval(a.Hf);
                        a.Hf = void 0;
                        return
                    }
                } else
                    a.jc ? (a.la.style.color = R.ec,
                    a.la.style.backgroundColor = R.mg,
                    a.Oc.style.fill = R.ec) : (a.la.style.backgroundColor = "",
                    a.la.style.color = "",
                    a.Oc.style.fill = "");
                c = !c
            }, 500)
        }
    }
    ;
    function Tq(a, b) {
        (void 0 === b || !b) && 1E4 > Date.now() - a.Nh || Mq().then(function(b) {
            a.Nh = Date.now();
            a.nc = b.nc;
            Qq(a);
            var c = a.kc.update(b);
            0 === c.jp.length && (a.Xf = !1,
            a.hh());
            var e = a.username
              , f = a.kc.Yc
              , h = c.Gf
              , g = c.jp;
            if (x()) {
                c = {
                    onlineFollowed: {
                        online: b.nc.Ye,
                        total: b.nc.total
                    },
                    roomList: []
                };
                b = l(b.Vj);
                for (var p = b.next(); !p.done; p = b.next())
                    p = p.value,
                    c.roomList.push({
                        room: p.i,
                        image: p.Ih
                    });
                b = [];
                g = l(g);
                for (p = g.next(); !p.done; p = g.next())
                    p = p.value,
                    b.push({
                        room: p.i,
                        image: p.Ih
                    });
                window.localStorage.setItem("onlineFollowedTab", JSON.stringify({
                    username: e,
                    onlineFollowedList: c,
                    seenRooms: f,
                    unseenRooms: b,
                    flash: h,
                    timestamp: Date.now()
                }))
            }
        })
    }
    Sq.prototype.hh = function() {
        this.jc ? (this.la.style.color = R.ec,
        this.la.style.backgroundColor = R.mg,
        this.Oc.style.fill = R.ec) : (this.la.style.color = "",
        this.la.style.backgroundColor = "",
        this.Oc.style.fill = "")
    }
    ;
    function Uq(a) {
        v.call(this);
        var b = this;
        this.vn = !1;
        this.a.style.position = "static";
        this.a.style.display = "inline-block";
        this.a.style.overflow = "visible";
        this.a.style.height = "38px";
        this.a.style.padding = "16px 19px 0 32px";
        this.a.style.backgroundColor = R.wo;
        this.a.style.backgroundImage = R.xo;
        this.a.style.borderBottom = "1px solid " + R.cd;
        this.a.style.boxSizing = "border-box";
        this.yi = new Vq(Db(a),"",!0);
        this.yi.link.title = ub(a) + "'s Cam";
        this.a.appendChild(this.yi.a);
        this.a.appendChild((new Vq(Qd,"/female-cams/")).a);
        this.a.appendChild((new Vq(Sd,"/male-cams/")).a);
        this.a.appendChild((new Vq(Xd,"/couple-cams/")).a);
        this.a.appendChild((new Vq(Ud,"/trans-cams/")).a);
        var c = new Vq(Zd,"/spy-on-cams/");
        c.a.style.display = "none";
        this.a.appendChild(c.a);
        var d = new Vq("","/followed-cams/");
        a = document.createElement("span");
        a.textContent = Yd + " ";
        a.style.verticalAlign = "top";
        d.link.appendChild(a);
        var e = document.createElement("span");
        e.style.verticalAlign = "top";
        e.innerText = "";
        e.className = "followed_counts";
        d.link.appendChild(e);
        var f = new Wq;
        f.color(R.ec);
        d.link.appendChild(f.span);
        d.link.onmouseenter = function() {
            d.link.style.color = R.lb;
            d.link.style.backgroundColor = R.bd;
            d.link.style.borderBottom = "1px solid  " + R.bd;
            f.color(R.lb)
        }
        ;
        d.link.onmouseleave = function() {
            d.link.style.color = R.ec;
            d.link.style.backgroundColor = R.mg;
            d.link.style.border = "1px solid " + R.cd;
            f.color(R.ec)
        }
        ;
        this.a.appendChild(d.a);
        q(S, function(a) {
            Xq(b, a.j.i());
            c.a.style.display = a.j.ob ? "none" : "";
            if (!b.vn) {
                e.innerText = "(" + a.h.Vq + "/" + a.h.Uq + ")";
                d.a.style.display = "";
                d.a.style.overflow = "visible";
                d.a.style.position = "relative";
                d.a.style.cursor = "pointer";
                var f = d.a
                  , h = d.link
                  , t = a.j.username();
                a = a.j.ob;
                f.style.position = "relative";
                f.style.overflow = "visible";
                f.style.cursor = "pointer";
                h.style.cursor = "pointer";
                var C = new Nq(h,a,!0);
                f.appendChild(C.a);
                new Sq(h,C,a,t,!0);
                b.vn = !0
            }
        })
    }
    m(Uq, v);
    function Xq(a, b) {
        a.yi.link.innerText = ub(b) + "'s Cam";
        a.yi.link.title = ub(b) + "'s Cam"
    }
    function Vq(a, b, c) {
        var d = this;
        c = void 0 === c ? !1 : c;
        this.a = document.createElement("div");
        this.link = (new tq).anchor;
        this.a.style.cssFloat = "left";
        this.a.style.marginRight = "2px";
        this.a.style.top = "-5px";
        this.a.style.position = "relative";
        this.link.href = b;
        this.link.style.boxSizing = "border-box";
        this.link.style.height = "27px";
        this.link.style.display = "inline-block";
        this.link.style.padding = "5px 11px 0 11px";
        this.link.style.fontFamily = "UbuntuMedium, Helvetica, Arial, sans-serif";
        this.link.style.border = "1px solid " + R.cd;
        this.link.style.borderRadius = "4px 4px 0 0";
        this.link.style.fontSize = "13px";
        this.link.innerText = a;
        c ? (this.link.removeAttribute("href"),
        this.link.style.fontSize = "15px",
        this.link.style.color = R.lb,
        this.link.style.backgroundColor = R.bd,
        this.link.style.borderBottom = "1px solid " + R.Si,
        this.link.style.padding = "4px 11px 0 11px",
        this.link.style.maxWidth = "180px",
        this.link.style.whiteSpace = "nowrap",
        this.link.style.overflow = "hidden",
        this.link.style.textOverflow = "ellipsis") : (this.link.style.color = R.ec,
        this.link.style.backgroundColor = R.mg,
        this.link.onmouseenter = function() {
            d.link.style.color = R.lb;
            d.link.style.backgroundColor = R.bd;
            d.link.style.borderBottom = "1px solid " + R.bd
        }
        ,
        this.link.onmouseleave = function() {
            d.link.style.color = R.ec;
            d.link.style.backgroundColor = R.mg;
            d.link.style.border = "1px solid " + R.cd
        }
        );
        this.a.appendChild(this.link)
    }
    function Wq() {
        this.span = document.createElement("span");
        this.path = document.createElementNS("http://www.w3.org/2000/svg", "path");
        this.span.style.display = "inline-block";
        this.span.style.verticalAlign = "top";
        this.span.style.cursor = "pointer";
        this.span.style.paddingLeft = "5px";
        this.span.style.marginRight = "-4px";
        this.span.className = "followed_arrow_container";
        var a = document.createElementNS("http://www.w3.org/2000/svg", "svg");
        a.setAttribute("height", "16");
        a.setAttribute("width", "12");
        this.path.setAttribute("d", "M0 6l6 6L12 6H0z");
        this.path.setAttribute("class", "followed_arrow");
        a.appendChild(this.path);
        this.span.appendChild(a)
    }
    Wq.prototype.color = function(a) {
        this.path.style.fill = a
    }
    ;
    function Yq(a, b) {
        function c(a) {
            var c = (new tq).anchor
              , d = a.rk ? "/photo_videos/photoset/detail/" + b + "/" + a.id : "/photo_videos/purchase/" + a.id + "/";
            -1 !== window.location.search.indexOf("from_tube") && (d = d + "?from_tube=" + b);
            c.href = d;
            c.style.position = "relative";
            c.style.display = "inline-block";
            c.style.marginBottom = "17px";
            c.style.marginRight = "5px";
            c.className = "userUpload";
            c.title = a.name;
            c.onmouseenter = function() {
                c.style.cursor = "pointer";
                f.style.textDecoration = "underline"
            }
            ;
            c.onmouseleave = function() {
                c.style.cursor = "default";
                f.style.textDecoration = "none"
            }
            ;
            c.onclick = function() {
                Mh(d, void 0, "resizable,dependent,scrollbars,height=600,width=800,top=" + (screen.height / 2 - 600) + ",left=" + (screen.width / 2 - 800));
                return !1
            }
            ;
            var e = document.createElement("img");
            e.src = a.hn;
            e.width = 150;
            e.height = 100;
            e.style.padding = "4px";
            e.style.border = "1px solid #ccc";
            e.style.display = "block";
            c.appendChild(e);
            a.rk || (e = document.createElement("img"),
            e.src = "https://ssl-ccstatic.highwebmedia.com/theatermodeassets/locked_rectangle4.png",
            e.width = 150,
            e.height = 100,
            e.style.position = "absolute",
            e.style.left = "1px",
            e.style.top = "1px",
            e.style.padding = "4px",
            e.style.border = "none",
            c.appendChild(e));
            !0 === a.fo && (e = document.createElement("img"),
            e.src = "https://ssl-ccstatic.highwebmedia.com/theatermodeassets/play3.png",
            e.height = 20,
            e.style.position = "absolute",
            e.style.top = "7px",
            e.style.left = "7px",
            c.appendChild(e));
            "" !== a.vj && (e = document.createElement("span"),
            e.innerText = a.vj,
            e.style.backgroundColor = a.ho,
            e.style.color = "#fff",
            e.style.position = "absolute",
            e.style.top = "89px",
            e.style.right = "10px",
            e.style.fontSize = "10px",
            e.style.padding = "1px 3px",
            e.style.lineHeight = "11px",
            c.appendChild(e));
            var f = document.createElement("div");
            f.innerText = a.name;
            f.style.fontSize = "14px";
            f.style.left = "0px";
            f.style.maxWidth = "145px";
            f.style.whiteSpace = "nowrap";
            f.style.textOverflow = "ellipsis";
            f.style.overflow = "hidden";
            f.style.lineHeight = "16px";
            f.style.color = "#0a5a83";
            f.style.textDecoration = "none";
            c.appendChild(f);
            return c
        }
        var d = document.createElement("div");
        d.style.position = "relative";
        d.style.margin = "9px 0";
        d.style.width = "100%";
        d.style.lineHeight = "15px";
        d.style.fontFamily = "UbuntuMedium, Arial, Helvetica, sans-serif";
        var e = document.createElement("span");
        e.innerText = se + ":";
        e.style.display = "inline-block";
        e.style.color = "#0a5a83";
        e.style.fontSize = "14px";
        e.style.width = "150px";
        e.style.verticalAlign = "top";
        e.style.height = "16px";
        d.appendChild(e);
        e = document.createElement("div");
        e.style.display = "inline-block";
        e.style.width = "87.5%";
        e.style.fontFamily = "UbuntuRegular, Arial, Helvetica, sans-serif";
        e.style.color = "#0a5a83";
        for (var f = 0, h = {}, g = l(a), p = g.next(); !p.done; h = {
            sc: h.sc
        },
        p = g.next()) {
            p = p.value;
            if (8 === f) {
                f = document.createElement("div");
                h.sc = (new tq).anchor;
                h.sc.href = "/photo_videos/photoset/list_popup/" + b + "/";
                h.sc.innerText = re;
                h.sc.style.fontSize = "14px";
                h.sc.style.color = "#0a5a83";
                h.sc.onclick = function(a) {
                    var c = "/photo_videos/photoset/list_popup/" + b + "/";
                    -1 !== window.location.search.indexOf("from_tube") && (c = c + "?from_tube=" + b);
                    Mh(c, void 0, "resizable,dependent,scrollbars,height=600,width=800,top=" + screen.height / 2 + ",left=" + screen.width / 2);
                    a.preventDefault()
                }
                ;
                h.sc.onmouseenter = function(a) {
                    return function() {
                        a.sc.style.cursor = "pointer";
                        a.sc.style.textDecoration = "underline"
                    }
                }(h);
                h.sc.onmouseleave = function(a) {
                    return function() {
                        a.sc.style.cursor = "";
                        a.sc.style.textDecoration = ""
                    }
                }(h);
                f.appendChild(h.sc);
                e.appendChild(f);
                break
            }
            e.appendChild(c(p));
            f += 1
        }
        d.appendChild(e);
        return d
    }
    function Zq(a, b, c) {
        var d = document.createElement("div");
        d.style.fontWeight = "normal";
        d.style.margin = "9px 0";
        d.style.lineHeight = "15px";
        d.style.fontFamily = "UbuntuRegular, Arial, Helvetica, sans-serif";
        var e = document.createElement("span");
        e.innerText = a + ":";
        e.style.display = "inline-block";
        e.style.color = "#0a5a83";
        e.style.fontFamily = "UbuntuMedium, Arial, Helvetica, sans-serif";
        e.style.fontSize = "14px";
        e.style.width = "150px";
        e.style.height = "16px";
        d.appendChild(e);
        a = document.createElement("span");
        c ? a.innerHTML = b : a.innerText = b;
        a.style.color = "#000000";
        a.style.fontSize = "14px";
        a.style.width = "87.5%";
        d.appendChild(a);
        return d
    }
    function $q(a, b) {
        var c = Zq(tb(b), a, !0)
          , d = c.children[0];
        d.style.display = "inline-block";
        d.style.verticalAlign = "top";
        d = c.children[1];
        d.style.display = "inline-block";
        d.style.width = "100%";
        d.style.lineHeight = "16px";
        var e = document.createElement("div");
        c.replaceChild(e, d);
        e.appendChild(d);
        e.style.display = "inline-block";
        0 !== d.children.length && (d.children[0].style.display = "unset");
        return {
            root: c,
            Hp: e
        }
    }
    function ar() {
        v.call(this);
        var a = this;
        this.Xj = [];
        this.a.style.position = "static";
        this.a.style.backgroundColor = "#ffffff";
        this.a.style.padding = "21px 15px 15px";
        this.a.style.boxSizing = "border-box";
        this.a.className = "bio-container";
        yh(this.a);
        this.za = document.createElement("div");
        this.za.style.overflow = "hidden";
        this.za.style.minHeight = "500px";
        q(S, function(b) {
            b = b.h;
            var c = Bh(b.i);
            a.za.parentElement === a.a && a.a.removeChild(a.za);
            a.za = document.createElement("div");
            var d = document.createElement("div");
            d.innerText = tb(b.i) + "'s Bio and Free Webcam";
            d.style.fontSize = "14.4px";
            d.style.fontFamily = "UbuntuMedium, Arial, Helvetica, sans-serif";
            d.style.color = R.Ba;
            d.style.margin = "10px 0 20px";
            d.style.lineHeight = "20px";
            a.za.appendChild(d);
            c.then(function() {
                return a.za.removeChild(d)
            });
            a.za.appendChild(d);
            a.a.appendChild(a.za);
            a.td(b, c)
        })
    }
    m(ar, v);
    ar.prototype.show = function() {
        this.a.style.height = "";
        this.a.style.width = "";
        this.a.style.padding = "21px 15px 15px"
    }
    ;
    ar.prototype.A = function() {
        this.a.style.height = "0px";
        this.a.style.width = "0px";
        this.a.style.padding = "0px"
    }
    ;
    function br(a, b) {
        "" !== b.Oj && a.za.appendChild(Zq(kd, b.Oj, !1));
        a.za.appendChild(Zq(ld, isNaN(b.$i) ? "0" : b.$i.toString(), !1));
        "" !== b.Xi && a.za.appendChild(Zq(od, b.Xi, !1));
        isNaN(b.he) || a.za.appendChild(Zq(pd, b.he.toString(), !1));
        a.za.appendChild(Zq(md, b.$j, !1));
        if ("" !== b.sj) {
            var c = b.sj;
            for (c = c.slice(1, c.length - 1); c.includes('"'); )
                c = c.replace('"', "");
            c = c.split(",").join(", ");
            a.za.appendChild(Zq(sd, c, !1))
        }
        "" !== b.location && a.za.appendChild(Zq(td, b.location, !1));
        void 0 !== b.Mh && "" !== b.Mh && a.za.appendChild(Zq(vd, b.Mh, !1));
        "" !== b.languages && a.za.appendChild(Zq(ud, b.languages, !1));
        "" !== b.Ji && a.za.appendChild(Zq(qd, b.Ji, !1));
        "" !== b.ck && a.za.appendChild(Zq(yd, b.ck, !1));
        "" !== b.Ii && a.za.appendChild(Zq(rd, b.Ii, !1))
    }
    ar.prototype.td = function(a, b) {
        var c = this;
        b.then(function(b) {
            var d = b.Nf;
            if (d.Pn && !d.eo) {
                var f = document.createElement("div");
                f.style.textAlign = "right";
                f.style.lineHeight = "16px";
                var h = (new tq).anchor;
                h.href = "/fanclub/join/" + a.i + "/";
                h.innerText = Eb(a.i);
                h.style.color = "#0a5a83";
                h.style.fontSize = "12px";
                h.style.textDecoration = "none";
                h.style.width = "100%";
                h.style.lineHeight = "16px";
                h.style.verticalAlign = "top";
                h.onmouseenter = function() {
                    h.style.textDecoration = "underline"
                }
                ;
                h.onmouseleave = function() {
                    h.style.textDecoration = "none"
                }
                ;
                f.appendChild(h);
                c.za.appendChild(f)
            }
            f = document.createElement("h1");
            f.innerText = tb(a.i) + "'s " + Ad;
            f.style.fontSize = "14.4px";
            f.style.fontFamily = "UbuntuMedium, Arial, Helvetica, sans-serif";
            f.style.color = R.Ba;
            f.style.margin = "10px 0 20px";
            f.style.lineHeight = "20px";
            c.za.appendChild(f);
            br(c, d);
            1 <= b.Nj.length && (d = Yq(b.Nj, b.Nf.username),
            c.Xj.push({
                root: d,
                Hp: d.children[1]
            }),
            c.za.appendChild(d));
            1 <= b.xi.length && (d = $q(b.xi, hd),
            c.Xj.push(d),
            vq(d.root),
            c.za.appendChild(d.root));
            1 <= b.yk.length && (b = $q(b.yk, id),
            c.Xj.push(b),
            vq(b.root),
            c.za.appendChild(b.root));
            c.C()
        })["catch"](function() {
            var a = document.createElement("div");
            a.style.position = "absolute";
            a.style.height = "100%";
            a.style.width = "100%";
            a.style.top = "0";
            a.style.left = "0";
            a.style.display = "block";
            var b = document.createElement("span");
            b.innerText = zd;
            b.style.textAlign = "center";
            b.style.display = "block";
            b.style.padding = "10px";
            a.appendChild(b);
            c.za.style.position = "relative";
            c.za.appendChild(a)
        })
    }
    ;
    ar.prototype.C = function() {
        var a = this;
        this.Xj.forEach(function(b) {
            b.Hp.style.width = a.a.clientWidth - 30 - 150 + "px"
        })
    }
    ;
    function cr() {
        v.call(this);
        this.rows = [];
        this.Ja = document.createElement("p");
        this.Z = document.createElement("h2");
        this.Ff = document.createElement("p");
        this.a.style.minHeight = "800px";
        this.a.style.background = "rgb(255,255,255)";
        this.a.style.padding = "15px";
        this.a.style.width = "100%";
        this.a.style.boxSizing = "border-box";
        this.a.style.position = "static";
        this.Ja.style.display = "block";
        this.Ja.style.fontSize = "12px";
        this.Ja.textContent = "loading...";
        this.Z.innerText = ve + ":";
        this.Z.style.fontSize = "12px";
        this.Z.style.color = R.Xb;
        this.Z.style.fontWeight = "bold";
        this.Ff.innerText = we;
        this.Ff.style.color = R.Xb;
        this.Ff.style.fontSize = "12px";
        this.Ff.style.padding = "0.5em 0 1em 0";
        this.a.appendChild(this.Ja);
        this.a.appendChild(this.Z);
        this.a.appendChild(this.Ff)
    }
    m(cr, v);
    cr.prototype.td = function() {
        return new Promise(function(a) {
            z("contest/leaderboard_json/?_=" + (new Date).getTime()).then(function(b) {
                b = new D(b.responseText);
                for (var c = [], d = l(qb(b, "top10")), e = d.next(); !e.done; e = d.next())
                    e = e.value,
                    c.push({
                        gm: ub(e.room_user),
                        ip: e.room_slug,
                        ar: e.points,
                        Kq: e.image_url,
                        Hr: e.viewers
                    });
                L(b);
                a(c)
            })
        }
        )
    }
    ;
    function dr(a) {
        a.rows.forEach(function(b) {
            a.a.removeChild(b)
        });
        a.rows = []
    }
    function er(a, b) {
        function c(a) {
            function b(a) {
                a.style.display = "table-cell";
                a.style.verticalAlign = "middle";
                a.style.padding = "5px";
                a.style.borderBottom = "1px solid #ccc"
            }
            function c(b, c) {
                b.onclick = function(b) {
                    b.preventDefault();
                    u(mh, a.ip)
                }
                ;
                b.style.cursor = "pointer";
                if (void 0 === c ? 0 : c)
                    b.onmouseenter = function() {
                        return b.style.textDecoration = "underline"
                    }
                    ,
                    b.onmouseleave = function() {
                        return b.style.textDecoration = ""
                    }
            }
            function e(a, b) {
                var c = document.createElement("span")
                  , d = document.createElement("span")
                  , e = document.createElement("span");
                c.style.fontSize = "12px";
                c.style.fontWeight = "400";
                d.style.color = "#0b5d81";
                d.style.fontWeight = "bold";
                e.style.color = "#000";
                d.style.verticalAlign = "middle";
                e.style.verticalAlign = "middle";
                e.style.margin = "0 3px";
                d.textContent = a;
                e.textContent = "" + b;
                c.appendChild(d);
                c.appendChild(e);
                return c
            }
            var p = 1 === d ? "#dc5500" : "#0c6a93"
              , t = document.createElement("div")
              , C = document.createElement("div")
              , B = document.createElement("img")
              , M = document.createElement("a")
              , F = document.createElement("div")
              , O = document.createElement("div")
              , ca = document.createElement("a")
              , xa = document.createElement("div")
              , Ba = e(te + ":", a.Hr)
              , Ca = e(ue + ":", a.ar);
            M.appendChild(B);
            O.appendChild(ca);
            t.style.display = "block";
            F.style.padding = "5px";
            F.style.color = p;
            F.style.width = "500px";
            C.style.fontSize = 1 === d ? "20px" : "16px";
            C.style.color = p;
            C.style.fontWeight = "700";
            C.style.padding = "5px";
            C.style.width = "25px";
            O.style.color = p;
            ca.style.color = p;
            ca.style.fontFamily = "UbuntuMedium, Arial, Helvetica, sans-serif";
            O.style.fontSize = 1 === d ? "20px" : "16px";
            xa.style.color = "#0c6a93";
            B.style.height = "52px";
            B.style.width = "52px";
            p = "/" + a.ip + "/";
            ca.href = p;
            M.href = p;
            B.src = a.Kq;
            ca.textContent = "" + a.gm[0].toUpperCase() + a.gm.slice(1, a.gm.length);
            C.textContent = d + ".";
            d += 1;
            b(C);
            b(M);
            b(F);
            c(ca, !0);
            c(B, !1);
            Ba.style.margin = "0 5px";
            Ca.style.margin = "0 5px";
            O.style.margin = "0 5px";
            F.appendChild(O);
            F.appendChild(xa);
            xa.appendChild(Ba);
            xa.appendChild(Ca);
            t.appendChild(C);
            t.appendChild(M);
            t.appendChild(F);
            return t
        }
        var d = 1;
        b.forEach(function(b) {
            b = c(b);
            a.rows.push(b);
            a.a.appendChild(b)
        })
    }
    cr.prototype.show = function() {
        var a = this;
        this.a.style.display = "";
        this.Ja.style.display = "block";
        this.Z.style.display = "none";
        this.Ff.style.display = "none";
        dr(this);
        this.td().then(function(b) {
            a.Ja.style.display = "none";
            a.Z.style.display = "block";
            a.Ff.style.display = "block";
            er(a, b)
        })
    }
    ;
    cr.prototype.A = function() {
        dr(this);
        this.a.style.display = "none"
    }
    ;
    function zr() {
        v.call(this);
        var a = this;
        this.Ja = document.createElement("span");
        this.Z = document.createElement("div");
        this.ak = [];
        this.rd = !0;
        this.a.style.minHeight = "395px";
        this.a.style.background = "rgb(255,255,255)";
        this.a.style.padding = "15px";
        this.a.style.width = "100%";
        this.a.style.boxSizing = "border-box";
        this.a.style.position = "static";
        this.a.style.color = R.Ba;
        this.Ja.style.display = "block";
        this.Ja.style.fontSize = "12px";
        this.Ja.textContent = "loading...";
        this.a.appendChild(this.Ja);
        var b = document.createElement("div")
          , c = document.createElement("div")
          , d = document.createElement("div");
        b.textContent = ng;
        c.textContent = og;
        d.textContent = pg;
        b.style.fontSize = "14.4115px";
        b.style.lineHeight = "20.1761px";
        b.style.fontFamily = "UbuntuMedium, Helvetica, Arial, sans-serif";
        b.style.marginBottom = "20px";
        c.style.fontSize = "14.4115px";
        c.style.lineHeight = "20.1761px";
        c.style.margin = "14.4115px 0";
        c.style.whiteSpace = "nowrap";
        d.style.color = "#0b5d81";
        d.style.marginBottom = "12.0108px";
        d.style.fontSize = "12.0108px";
        d.style.lineHeight = "16.8px";
        d.style.fontWeight = "bold";
        this.Z.appendChild(b);
        this.Z.appendChild(c);
        this.Z.appendChild(d);
        this.a.appendChild(this.Z);
        q(S, function(b) {
            a.rd = b.j.ob;
            a.He = b.h;
            a.Ja.style.display = "block";
            a.Z.style.display = "none";
            a.sh();
            "block" === a.a.style.display && a.show()
        })
    }
    m(zr, v);
    zr.prototype.td = function() {
        var a = eb()
          , b = db({
            room: a.Pr,
            _: (new Date).getTime().toString()
        });
        return new Promise(function(a) {
            z("affiliates/intropanel_json/?" + b).then(function(b) {
                b = new D(b.responseText);
                var c = {
                    Mi: H(b, "campaignslug"),
                    ls: H(b, "toururl")
                };
                L(b);
                a(c)
            })
        }
        )
    }
    ;
    zr.prototype.sh = function() {
        var a = this;
        this.ak.forEach(function(b) {
            a.a.removeChild(b)
        });
        this.ak = []
    }
    ;
    function Ar(a, b) {
        function c(b, c) {
            var d = document.createElement("div")
              , e = document.createElement("label")
              , f = document.createElement("input");
            d.style.margin = "12.0108px 0";
            d.style.lineHeight = "16.8px";
            f.type = "text";
            f.value = c;
            e.textContent = b;
            e.style.display = "block";
            e.style.color = "#0b5d81";
            e.style.fontSize = "12.012px";
            e.style.lineHeight = "16.8px";
            e.style.fontWeight = "bold";
            e.style.cursor = "pointer";
            e.style.width = "375px";
            e.onclick = function() {
                f.select()
            }
            ;
            f.style.display = "block";
            f.style.borderRadius = "4px";
            f.style.fontSize = "12px";
            f.style.fontFamily = "Arial, Helvetica, sans-serif";
            f.style.verticalAlign = "middle";
            f.style.width = "375px";
            f.style.marginTop = "4px";
            f.style.height = "20px";
            f.style.lineHeight = "18px";
            f.style.border = "1px solid #b1b1b1";
            f.style.padding = "2px 4px";
            f.readOnly = !0;
            f.onclick = function() {
                f.select()
            }
            ;
            a.ak.push(d);
            d.appendChild(e);
            d.appendChild(f);
            a.a.appendChild(d)
        }
        var d = ub(a.He.i);
        c(Q(P("Share %(username)s's Cam"), {
            username: d
        }, !0) + ":", "https://chaturbate.com/in/?tour=7Bge&campaign=" + b.Mi + "&room=" + a.He.i);
        c(cg + " Chaturbate.com:", "https://chaturbate.com/in/?tour=OT2s&campaign=" + b.Mi);
        c(Q(P("Embed %(username)s's Cam on Your Webpage"), {
            username: d
        }, !0) + ":", "<iframe src='https://chaturbate.com/in/?tour=SHBY&campaign=" + b.Mi + "&track=embed&room=" + a.He.i + "&bgcolor=white' height=528 width=850 style='border: none;'></iframe>");
        c(qg + ":", "<iframe src='https://chaturbate.com/in/?tour=nvfS&campaign=" + b.Mi + "&track=embed&bgcolor=white' height=528 width=850 style='border: none;'></iframe>");
        d = document.createElement("div");
        var e = document.createElement("a");
        e.href = "/affiliates/stats/";
        e.textContent = rg;
        e.style.color = "#0a5a83";
        e.onmouseenter = function() {
            e.style.textDecoration = "underline"
        }
        ;
        e.onmouseleave = function() {
            e.style.textDecoration = "none"
        }
        ;
        d.textContent = sg;
        d.style.color = R.Ba;
        d.style.fontSize = "12.012px";
        d.style.lineHeight = "16.8px";
        d.style.marginTop = "30px";
        d.appendChild(e);
        a.ak.push(d);
        a.a.appendChild(d)
    }
    zr.prototype.show = function() {
        var a = this;
        this.Ja.style.display = "block";
        this.Z.style.display = "none";
        this.a.style.display = "block";
        this.sh();
        this.rd || this.td().then(function(b) {
            a.Ja.style.display = "none";
            a.Z.style.display = "block";
            Ar(a, b)
        })
    }
    ;
    zr.prototype.A = function() {
        this.a.style.display = "none";
        this.Ja.style.display = "none"
    }
    ;
    function Br(a) {
        cq.call(this);
        var b = this;
        this.nb = document.createElement("div");
        this.Em = !1;
        this.a.style.width = "auto";
        this.a.style.fontFamily = "UbuntuMedium, Helvetica, Arial, sans-serif";
        this.a.style.fontSize = "12px";
        this.a.style.padding = "4px 10px";
        this.a.style.position = "relative";
        this.a.style.top = "-4px";
        this.a.style.right = "1px";
        this.a.style.cssFloat = "right";
        this.a.style.borderRadius = "4px";
        this.a.style.cursor = "pointer";
        this.a.style.marginRight = "10px";
        this.a.onmouseenter = function() {
            if (!b.Em) {
                b.nb.style.display = "block";
                var c = b.a.getBoundingClientRect();
                b.nb.style.top = c.top + Ya().scrollTop + a.offsetHeight + 3 + "px";
                b.nb.style.left = c.left + Ya().scrollLeft + (c.width - b.nb.offsetWidth) / 2 + "px"
            }
        }
        ;
        this.a.onmouseleave = function() {
            b.nb.style.display = "none"
        }
        ;
        this.nb.style.position = "absolute";
        this.nb.style.backgroundColor = "#fdf294";
        this.nb.style.width = "220px";
        this.nb.style.border = "2px solid #ff9130";
        this.nb.style.borderRadius = "4px";
        this.nb.style.zIndex = "1";
        this.nb.style.fontSize = "12px";
        this.nb.style.padding = "12px";
        this.nb.style.display = "none";
        this.nb.appendChild(vm("#ff9130", "#fdf294", "123px"));
        this.nb.appendChild(document.createElement("span"));
        a.appendChild(this.nb);
        q(S, function(c) {
            void 0 !== b.Fe && (Cr(b.Fe),
            a.removeChild(b.Fe.a));
            b.Fe = new Dr(c,b.a,function() {
                b.Em = !1
            }
            );
            a.appendChild(b.Fe.a)
        });
        this.Wi()
    }
    m(Br, cq);
    Br.prototype.C = function() {
        void 0 !== this.Fe && this.Fe.C()
    }
    ;
    function hq(a, b) {
        a.Em = !0;
        void 0 !== a.Fe && (a.Fe.show(b),
        a.nb.style.display = "none")
    }
    Br.prototype.Zi = function() {
        this.a.style.backgroundColor = "#ffffff";
        La() && (this.a.style.background = "linear-gradient(180deg, #ffffff 0%, #ffffff 50%, #dddddd 60%)");
        this.a.style.color = "#0a5a83";
        this.a.style.textShadow = "#94d2e6 1px 1px 0px";
        cq.prototype.Zi.call(this)
    }
    ;
    Br.prototype.Wi = function() {
        this.a.style.backgroundColor = "#bbbbbb";
        La() && (this.a.style.background = "linear-gradient(180deg, #bbbbbb 0%, #bbbbbb 50%, #aaaaaa 60%)");
        this.a.style.color = "#777777";
        this.a.style.textShadow = "#bababa 1px 1px 0px";
        cq.prototype.Wi.call(this);
        var a = this.fa;
        a = Q(P("After tipping %(min_tokens_to_vote)s tokens in a day, you'll be able to vote regarding your satisfaction with %(room)s."), {
            min_tokens_to_vote: 25,
            room: tb(a)
        }, !0);
        var b = this.nb.lastChild;
        b.style.color = R.Xb;
        b.innerText = a
    }
    ;
    function Dr(a, b, c) {
        v.call(this);
        var d = this;
        this.kp = b;
        this.Wq = c;
        this.od = document.createElement("div");
        this.label = document.createElement("label");
        this.yb = document.createElement("textarea");
        this.xb = document.createElement("input");
        this.fa = "";
        this.Zh = !1;
        this.hidden = !0;
        this.fa = a.h.i;
        this.a.style.display = "none";
        this.a.style.width = "350px";
        this.a.style.height = "auto";
        this.a.style.backgroundColor = "#ffffff";
        this.a.style.border = "2px solid #0b5d81";
        this.a.style.borderRadius = "4px";
        this.a.style.top = "0px";
        this.a.style.right = "0px";
        this.a.style.overflow = "";
        this.a.style.zIndex = "11";
        this.a.onclick = function(a) {
            a.stopPropagation()
        }
        ;
        this.a.appendChild(vm("#0b5d81", "#e0e0e0", "176px"));
        b = document.createElement("div");
        b.innerText = fg;
        b.style.boxSizing = "border-box";
        b.style.color = "#0b5d81";
        b.style.backgroundColor = "#e0e0e0";
        b.style.width = "100%";
        b.style.fontSize = "15px";
        b.style.fontWeight = "bold";
        b.style.padding = "6px";
        b.style.borderRadius = "4px 4px 0px 0px";
        this.a.appendChild(b);
        b = document.createElement("div");
        b.style.padding = "6px";
        b.style.fontSize = "13px";
        b.style.color = "#777";
        b.innerText = Ob(a.h.i);
        this.a.appendChild(b);
        this.a.appendChild(this.od);
        Er(this);
        this.A = function() {
            Cr(d)
        }
        ;
        this.a.onmousemove = function() {
            Fr(d, 1E4)
        }
    }
    m(Dr, v);
    Dr.prototype.show = function(a) {
        this.hidden && (y("click", document, this.A),
        this.hidden = !1);
        this.a.style.display = "block";
        if (a) {
            for (this.yb.value = ""; null !== this.od.firstChild; )
                this.od.removeChild(this.od.firstChild);
            this.od.appendChild(this.label);
            this.od.appendChild(this.yb);
            this.od.appendChild(this.xb)
        }
        if (null !== this.a.parentNode) {
            a = this.kp.getBoundingClientRect();
            var b = this.a.parentElement;
            this.a.style.top = a.top + Ya().scrollTop + b.offsetHeight + "px";
            this.a.style.left = a.left + Ya().scrollLeft + (a.width - this.a.offsetWidth) / 2 + "px"
        }
        null !== this.yb.parentNode && this.yb.focus();
        Fr(this, 1E4)
    }
    ;
    Dr.prototype.C = function() {
        if (null !== this.a.parentNode) {
            var a = this.kp.getBoundingClientRect()
              , b = this.a.parentElement;
            this.a.style.top = a.top + Ya().scrollTop + b.offsetHeight + 3 + "px";
            this.a.style.left = a.left + Ya().scrollLeft + (a.width - this.a.offsetWidth) / 2 + "px"
        }
    }
    ;
    function Er(a) {
        a.label.style.fontSize = "13px";
        a.label.style.fontWeight = "bold";
        a.label.style.fontFamily = "UbuntuMedium, Helvetica, Arial, sans-serif";
        a.label.style.color = "#777";
        a.label.style.margin = "6px";
        a.label.innerText = gg;
        a.yb.maxLength = 155;
        a.yb.style.width = "338px";
        a.yb.style.resize = "none";
        a.yb.style.fontSize = "13px";
        a.yb.style.margin = "6px";
        a.yb.style.padding = "4px";
        a.yb.style.border = "1px solid #777";
        a.yb.style.borderRadius = "4px";
        a.yb.style.boxSizing = "border-box";
        a.yb.style.display = "block";
        a.yb.style.height = "81px";
        a.yb.value = "";
        a.yb.oninput = function() {
            Gr(a)
        }
        ;
        a.xb.setAttribute("type", "submit");
        a.xb.style.fontSize = "12px";
        a.xb.style.fontFamily = "UbuntuMedium, Helvetica, Arial, sans-serif";
        a.xb.style.color = "rgb(255,255,255)";
        a.xb.style.padding = "5px 8px 4px";
        a.xb.style.marginRight = "6px";
        a.xb.style.marginBottom = "10px";
        La() ? a.xb.style.background = "linear-gradient(rgb(255, 151, 53) 0%, rgb(255, 158, 54) 50%, rgb(255, 112, 2) 60%)" : a.xb.style.backgroundColor = "rgb(255,151,53)";
        a.xb.style.borderRadius = "4px";
        a.xb.style.borderStyle = "none";
        a.xb.style.boxSizing = "border-box";
        a.xb.style.cursor = "pointer";
        a.xb.style.display = "inline-block";
        a.xb.style.cssFloat = "right";
        a.xb.value = "SUBMIT";
        a.xb.onclick = function(b) {
            b.stopImmediatePropagation();
            Hr(a, a.yb.value)
        }
    }
    function Hr(a, b) {
        A("tipping/add_comment/" + a.fa + "/", {
            comment: b
        }).then(function() {
            for (; null !== a.od.firstChild; )
                a.od.removeChild(a.od.firstChild);
            var b = document.createElement("div");
            b.style.width = "300px";
            b.style.margin = "25px";
            b.style.textAlign = "center";
            b.style.fontSize = "13px";
            b.style.color = "#777";
            b.innerText = hg;
            a.od.appendChild(b);
            a.Zh = !1;
            Fr(a, 5E3)
        })["catch"](function(a) {
            w("Error rating model: " + a)
        })
    }
    function Gr(a) {
        clearTimeout(a.ij);
        a.Zh = !0;
        a.ij = setTimeout(function() {
            a.Zh = !1;
            Fr(a, 1E4)
        }, 3E4)
    }
    function Fr(a, b) {
        a.Zh || (clearTimeout(a.ij),
        a.ij = setTimeout(function() {
            Cr(a)
        }, b))
    }
    function Cr(a) {
        a.hidden || (clearTimeout(a.ij),
        cb("click", document, a.A),
        a.Zh = !1,
        a.Wq(),
        a.a.style.display = "none",
        a.hidden = !0)
    }
    ;function Ir(a) {
        X.call(this);
        var b = this, c = window.localStorage.getItem("numPushNotificationTimeouts"), d = window.localStorage.getItem("pushNotificationTimeout"), e = new Date, f;
        if (null !== c && null !== d) {
            var h = new Date(d);
            if (e < h)
                return
        }
        this.parentElement = a;
        this.parentElement.appendChild(this.a);
        this.parentElement.insertBefore(this.O, this.a);
        this.tc();
        this.a.style.position = "absolute";
        this.a.style.height = "";
        this.a.style.width = "264px";
        this.a.style.backgroundColor = "#e0e0e0";
        this.a.style.padding = "6px 12px";
        this.a.style.borderRadius = "3px";
        this.a.style.marginLeft = "-221px";
        this.a.style.marginTop = "32px";
        this.a.style.boxShadow = "0 0 5px 0 rgba(0,0,0,0.75)";
        this.a.style.fontSize = "12.012px";
        this.a.style.color = R.Ba;
        this.a.style.overflow = "";
        this.a.style.lineHeight = "16.8px";
        a = document.createElement("div");
        a.innerText = "\u25b2";
        a.style.position = "absolute";
        a.style.top = "-11px";
        a.style.left = "250px";
        a.style.color = "#e0e0e0";
        a.style.textShadow = "0 -2px 4px rgba(0,0,0,0.65)";
        a.style.fontSize = "15px";
        this.a.appendChild(a);
        a = document.createElement("div");
        a.innerText = "\u25b2";
        a.style.position = "absolute";
        a.style.top = "-10px";
        a.style.left = "247px";
        a.style.color = "#e0e0e0";
        a.style.fontSize = "21px";
        this.a.appendChild(a);
        a = document.createElement("div");
        a.innerText = Hf;
        a.style.color = "#0c6a93";
        this.a.appendChild(a);
        a = document.createElement("div");
        a.innerText = If;
        a.style.margin = "5px 0 10px";
        a.style.fontSize = "11px";
        this.a.appendChild(a);
        var g = document.createElement("div");
        g.innerText = Kf;
        g.style.display = "inline-block";
        g.style.backgroundColor = "#0c6a93";
        g.style.padding = "5px 30px";
        g.style.borderRadius = "3px";
        g.style.color = "#eeeeee";
        g.style.cursor = "pointer";
        g.onmouseenter = function() {
            g.style.backgroundColor = "#0c6a93cc"
        }
        ;
        g.onmouseleave = function() {
            g.style.backgroundColor = "#0c6a93"
        }
        ;
        g.onclick = function() {
            window.subscribeUserToPush()["catch"](function(a) {
                400 === a.status && U(Jf)
            });
            Jr(b)
        }
        ;
        this.a.appendChild(g);
        var p = document.createElement("div");
        p.innerText = Lf;
        p.style.display = "inline-block";
        p.style.marginLeft = "10px";
        p.style.cursor = "pointer";
        p.onmouseenter = function() {
            p.style.textDecoration = "underline"
        }
        ;
        p.onmouseleave = function() {
            p.style.textDecoration = ""
        }
        ;
        p.onclick = function() {
            Jr(b);
            e = new Date;
            null !== c && null !== d ? (f = parseInt(c) + 1,
            h = new Date(d)) : (f = 1,
            h = new Date);
            h.setTime(e.getTime() + 36E5 * (168 < Math.pow(2, f) ? 168 : Math.pow(2, f)));
            window.localStorage.setItem("numPushNotificationTimeouts", f.toString());
            window.localStorage.setItem("pushNotificationTimeout", h.toString())
        }
        ;
        this.a.appendChild(p);
        q(this.bc, function() {
            Jr(b)
        })
    }
    m(Ir, X);
    function Jr(a) {
        a.parentElement.removeChild(a.O);
        a.parentElement.removeChild(a.a)
    }
    ;function Kr(a, b) {
        var c = document.createElement("div");
        c.style.display = "inline-block";
        c.style.width = "180px";
        c.style.backgroundColor = R.Ym;
        c.style.cssFloat = "left";
        c.style.margin = "0 7px 7px 0";
        c.style.border = "1px solid " + R.Zm;
        c.style.borderRadius = "4px";
        c.style.webkitBorderRadius = "4px";
        c.style.maxHeight = "222px";
        c.style.overflow = "hidden";
        c.style.position = "relative";
        var d = document.createElement("a");
        d.style.textDecoration = "none";
        d.style.color = R.Nk;
        d.href = "/" + a.room + "/";
        d.onclick = b;
        var e = document.createElement("img");
        e.style.display = "block";
        e.style.borderStyle = "none";
        e.width = 180;
        e.height = 135;
        e.src = a.img;
        d.appendChild(e);
        c.appendChild(d);
        c.appendChild(Lr(a.label));
        d = document.createElement("div");
        d.style.padding = "4px 7px 0";
        d.style.textAlign = "left";
        e = document.createElement("div");
        e.style.width = "100%";
        e.style.overflow = "hidden";
        e.style.borderBottom = "1px solid #ACACAC";
        var f = document.createElement("a");
        f.style.cssFloat = "left";
        f.style.color = R.Nk;
        f.style.font = "14px 'UbuntuMedium',Arial,Helvetica,sans-serif";
        f.style.width = "125px";
        f.style.overflow = "hidden";
        f.style.textDecoration = "none";
        f.textContent = a.room;
        f.href = "/" + a.room + "/";
        f.onclick = b;
        e.appendChild(f);
        f = document.createElement("span");
        f.style.cssFloat = "right";
        f.style.color = R.Mk;
        f.style.padding = "2px 18px 0 0";
        f.style.font = "12px 'UbuntuMedium',Arial,Helvetica,sans-serif";
        f.style.minHeight = "15px";
        f.style.background = "url(https://ssl-ccstatic.highwebmedia.com/theatermodeassets//icon" + {
            f: "Female",
            m: "Male",
            s: "Trans",
            c: "Couple"
        }[a.gender] + ".png) no-repeat 100% 0";
        f.textContent = a.display_age;
        e.appendChild(f);
        d.appendChild(e);
        e = document.createElement("ul");
        e.style.height = null === a.location ? "32px" : "16px";
        e.style.listStyle = "none";
        e.style.margin = "0";
        e.style.padding = "0";
        e.style.fontSize = "11px";
        e.style.lineHeight = "16px";
        e.style.width = "168px";
        e.style.overflow = "hidden";
        e.style.color = R.$m;
        f = document.createElement("li");
        f.style.width = "auto";
        f.style.background = "0 0";
        f.style.border = "0";
        f.style.margin = "0";
        f.style.padding = "0 2px";
        f.innerHTML = a.subject;
        for (var h = l(f.querySelectorAll("a")), g = h.next(); !g.done; g = h.next())
            g.value.style.color = R.an;
        e.appendChild(f);
        d.appendChild(e);
        e = document.createElement("ul");
        e.style.listStyle = "none";
        e.style.margin = "0";
        e.style.padding = "0 0 4px";
        e.style.fontSize = "11px";
        e.style.color = R.Mk;
        e.style.overflow = "hidden";
        null !== a.location && (f = document.createElement("li"),
        f.style.width = "145px",
        f.style.height = "15px",
        f.style.overflow = "hidden",
        f.style.margin = "0",
        f.style.padding = "0 0 0 18px",
        f.style.background = "url(https://ssl-ccstatic.highwebmedia.com/theatermodeassets//ico-01.png) no-repeat 0 50%",
        f.style.whiteSpace = "nowrap",
        f.textContent = "" + a.location,
        e.appendChild(f));
        f = document.createElement("li");
        f.style.width = "145px";
        f.style.height = "15px";
        f.style.overflow = "hidden";
        f.style.margin = "0";
        f.style.padding = "0 0 0 18px";
        f.style.background = "url(https://ssl-ccstatic.highwebmedia.com/theatermodeassets//ico-cams.png) no-repeat 0 50%";
        f.style.whiteSpace = "nowrap";
        f.textContent = a.time_online + ", " + a.viewers + " viewers";
        e.appendChild(f);
        d.appendChild(e);
        c.appendChild(d);
        return c
    }
    function Lr(a) {
        var b = document.createElement("div");
        b.style.position = "absolute";
        b.style.top = "119px";
        b.style.right = "3px";
        b.style.padding = "1px 3px";
        b.style.textAlign = "center";
        b.style.font = "10px 'UbuntuRegular',Arial,Helvetica,sans-serif";
        b.style.color = R.Ep;
        switch (a) {
        case "recommended":
            b.textContent = $g;
            b.style.backgroundColor = R.ym;
            break;
        case "offline":
            b.textContent = ah;
            b.style.backgroundColor = R.Gp;
            break;
        case "exhib":
            b.textContent = bh;
            b.style.backgroundColor = R.Dp;
            break;
        case "new":
            b.textContent = ch;
            b.style.backgroundColor = "#71b404";
            b.style.color = "#FFFFFF";
            break;
        case "hd":
            b.textContent = dh;
            b.style.backgroundColor = R.gk;
            break;
        case "hd_plus":
            b.textContent = eh;
            b.style.backgroundColor = R.gk;
            break;
        case "private":
            b.textContent = fh;
            b.style.backgroundColor = R.zm;
            break;
        case "group":
            b.textContent = gh;
            b.style.backgroundColor = R.zm;
            break;
        default:
            b.textContent = hh,
            b.style.backgroundColor = R.gk
        }
        return b
    }
    ;function Mr(a) {
        v.call(this);
        var b = this;
        this.Ja = document.createElement("span");
        this.body = document.createElement("div");
        this.Uf = document.createElement("div");
        this.Fj = document.createElement("div");
        this.jc = !0;
        void 0 !== a && (this.i = a,
        this.jc = !1);
        this.a.style.minHeight = "395px";
        this.a.style.background = "rgb(255,255,255)";
        this.a.style.padding = "15px";
        this.a.style.width = "100%";
        this.a.style.boxSizing = "border-box";
        this.a.style.position = "static";
        this.a.style.color = R.Ba;
        this.a.style.margin = "10px 0";
        this.Ja.style.display = "block";
        this.Ja.style.fontSize = "12px";
        this.Ja.textContent = "loading...";
        this.a.appendChild(this.Ja);
        this.Uf.style.display = "none";
        this.Uf.style.fontSize = "14px";
        a = document.createElement("div");
        a.textContent = Zg;
        a.style.marginBottom = "10px";
        this.Fj.style.marginBottom = "10px";
        this.Uf.appendChild(this.Fj);
        this.Uf.appendChild(a);
        this.a.appendChild(this.Uf);
        this.a.appendChild(this.body);
        q(S, function(a) {
            b.Ja.style.display = "block";
            b.i = a.h.i;
            "block" === b.a.style.display && b.show()
        })
    }
    m(Mr, v);
    Mr.prototype.sh = function() {
        for (; null !== this.body.firstChild; )
            this.body.removeChild(this.body.firstChild);
        this.Fj.textContent = "";
        this.Uf.style.display = "none"
    }
    ;
    Mr.prototype.td = function(a) {
        var b = this;
        a = void 0 === a ? !1 : a;
        if (void 0 !== this.i) {
            var c = this.i;
            z("api/more_like/" + this.i + "/").then(function(d) {
                if (b.i === c) {
                    a && b.sh();
                    var e = new D(d.responseText);
                    b.Ja.style.display = "none";
                    J(e, "has_recommendations") || (b.Fj.textContent = Q(P("Sorry, we don't have any rooms similar to %(room)s yet."), {
                        room: c
                    }, !0),
                    b.Uf.style.display = "block");
                    d = function(a, c) {
                        return function(d) {
                            N("LoadRoomFromRoomList", {
                                location: "more_like_this",
                                from_username: c,
                                username: a.room
                            });
                            !b.jc || d.metaKey || d.ctrlKey || (d.preventDefault(),
                            u(mh, a.room))
                        }
                    }
                    ;
                    e = qb(e, "rooms");
                    e = l(e);
                    for (var f = e.next(); !f.done; f = e.next())
                        f = f.value,
                        f.room !== b.i && b.body.appendChild(Kr(f, d(f, b.i)))
                }
            })
        }
    }
    ;
    Mr.prototype.show = function() {
        var a = this;
        this.Ja.style.display = "block";
        this.a.style.display = "block";
        this.sh();
        this.td();
        clearInterval(this.Ul);
        this.Ul = setInterval(function() {
            a.td(!0)
        }, 45E3)
    }
    ;
    Mr.prototype.A = function() {
        this.a.style.display = "none";
        this.Ja.style.display = "none";
        clearInterval(this.Ul);
        this.Ul = void 0
    }
    ;
    var Pi;
    function Nr() {
        function a() {
            c.style.display = "block";
            var a = b.a.getBoundingClientRect();
            c.style.top = a.top + Ya().scrollTop + b.dc.offsetHeight + 8 + "px";
            c.style.left = a.left + Ya().scrollLeft - c.offsetWidth + b.a.clientWidth + 16 + "px"
        }
        v.call(this);
        var b = this;
        this.Be = "Bio";
        this.Cp = [];
        this.a.style.position = "static";
        this.a.style.border = "1px solid #acacac";
        this.a.style.borderRadius = "4px 4px 0 0";
        this.a.style.boxSizing = "border-box";
        this.a.style.backgroundColor = "#FFFFFF";
        this.dc = document.createElement("div");
        this.dc.style.height = "30px";
        this.dc.style.padding = "8px 4px";
        this.dc.style.backgroundColor = "#7f7f7f";
        this.dc.style.boxSizing = "border-box";
        this.a.appendChild(this.dc);
        var c = document.createElement("div");
        c.innerHTML = $f;
        c.style.position = "absolute";
        c.style.backgroundColor = "#fdf294";
        c.style.color = R.Xb;
        c.style.width = "220px";
        c.style.border = "2px solid #ff9130";
        c.style.borderRadius = "6px";
        c.style.zIndex = "1";
        c.style.fontSize = "12px";
        c.style.padding = "12px";
        c.style.display = "none";
        c.appendChild(vm("#ff9130", "#fdf294", "170px"));
        this.dc.appendChild(c);
        var d = new n("followingEvent");
        q(d, function(a) {
            a.If ? (f.style.display = "none",
            h.style.display = "inline") : (f.style.display = "inline",
            h.style.display = "none")
        });
        var e = document.createElement("div");
        e.style.display = "inline-block";
        e.style.cssFloat = "right";
        this.dc.appendChild(e);
        var f = document.createElement("span");
        f.innerText = bg;
        f.style.color = "#ffffff";
        f.style.backgroundColor = "#ff7002";
        La() && (f.style.background = "linear-gradient(180deg, #ff9735 0%, #ff9e36 50%, #ff7002 60%)");
        f.style.fontFamily = "UbuntuMedium, Helvetica, Arial, sans-serif";
        f.style.fontSize = "12px";
        f.style.textShadow = "#f18112 1px 1px 0px";
        f.style.padding = "4px 10px 3px 10px";
        f.style.position = "relative";
        f.style.top = "-4px";
        f.style.right = "1px";
        f.style.cssFloat = "right";
        f.style.borderRadius = "4px";
        f.style.cursor = "pointer";
        f.style.display = "none";
        f.onclick = function() {
            Oi(d);
            if (!b.rd) {
                var a = window.isBrowserNotificationSupported
                  , c = window.isSubscribedToBrowserNotifications;
                void 0 !== a && !0 === a() && void 0 !== c && x() && c(!1, function() {
                    new Ir(e)
                })
            }
        }
        ;
        f.onmouseenter = function() {
            a()
        }
        ;
        f.onmouseleave = function() {
            c.style.display = "none"
        }
        ;
        e.appendChild(f);
        var h = document.createElement("span");
        h.innerText = ag;
        h.style.color = "#ffffff";
        h.style.backgroundColor = "#ff7002";
        La() && (h.style.background = "linear-gradient(180deg, #a9a9a9 0%, #adadad 50%, #8b8b8b 60%)");
        h.style.fontFamily = "UbuntuMedium, Helvetica, Arial, sans-serif";
        h.style.fontSize = "12px";
        h.style.textShadow = "#383838 1px 1px 0px";
        h.style.padding = "4px 10px 3px 10px";
        h.style.position = "relative";
        h.style.top = "-4px";
        h.style.right = "1px";
        h.style.cssFloat = "right";
        h.style.borderRadius = "4px";
        h.style.cursor = "pointer";
        h.style.display = "none";
        h.onclick = function() {
            Qi(d)
        }
        ;
        h.onmouseenter = function() {
            a()
        }
        ;
        h.onmouseleave = function() {
            c.style.display = "none"
        }
        ;
        e.appendChild(h);
        this.cf = new Br(this.dc);
        this.dc.appendChild(this.cf.a);
        var g = {
            Bio: new ar,
            "Contest Stats": new cr,
            Share: new zr,
            "More Rooms Like This": new Mr
        }, p;
        for (p in g)
            this.N(g[p]),
            g[p].A();
        Or(this, g);
        var t = Pr(this, "Share", cg, g.Share);
        t.anchor.style.display = "none";
        var C = Pr(this, "Bio", dg, g.Bio);
        this.dc.appendChild(C.anchor);
        R.om && this.dc.appendChild(Pr(this, "Contest Stats", eg, g["Contest Stats"]).anchor);
        var B = Pr(this, "More Rooms Like This", Yg, g["More Rooms Like This"]);
        this.dc.appendChild(B.anchor);
        R.om && this.dc.appendChild(t.anchor);
        g[this.Be].show();
        q(S, function(a) {
            b.rd = a.j.ob;
            t.anchor.style.display = b.rd ? "none" : "";
            a.j.i() === a.j.username() ? (B.anchor.style.display = "none",
            "More Rooms Like This" === b.Be && Qr(b, "Bio", C)) : B.anchor.style.display = "";
            Pi = a.j.i();
            a.h.If ? (f.style.display = "none",
            h.style.display = "inline") : (f.style.display = "inline",
            h.style.display = "none")
        })
    }
    m(Nr, v);
    function Pr(a, b, c, d) {
        var e = document.createElement("a");
        e.href = "#";
        e.innerText = c;
        e.style.borderRadius = "4px 4px 0 0";
        e.style.textDecoration = "none";
        e.style.fontSize = "13px";
        e.style.marginRight = "2px";
        e.style.fontFamily = "UbuntuMedium, Helvetica, Arial, sans-serif";
        e.style.padding = "4px 8px 4px 8px";
        e.style.color = "#4c4c4c";
        e.style.background = "#c9c9c9";
        b === a.Be && Rr(e);
        var f = {
            anchor: e,
            bo: d
        };
        a.Cp.push(f);
        e.onclick = function(c) {
            c.preventDefault();
            c.stopPropagation();
            Qr(a, b, f);
            c = a.Be;
            x() && window.localStorage.setItem("selectedRoomTab", c)
        }
        ;
        e.onmouseenter = function() {
            Rr(e)
        }
        ;
        e.onmouseleave = function() {
            a.Be !== b && Sr(e)
        }
        ;
        return f
    }
    function Or(a, b) {
        if (x()) {
            var c = window.localStorage.getItem("selectedRoomTab");
            null !== c && void 0 !== b[c] && (a.Be = c)
        }
    }
    function Rr(a) {
        a.style.color = "#dc5500";
        a.style.background = "#fff"
    }
    function Sr(a) {
        a.style.color = "#4c4c4c";
        a.style.background = "#c9c9c9"
    }
    function Qr(a, b, c) {
        a.Be !== b && (a.Cp.forEach(function(a) {
            Sr(a.anchor);
            a.bo.A()
        }),
        Rr(c.anchor),
        c.bo.show(),
        a.Be = b)
    }
    Nr.prototype.C = function() {
        this.cf.C()
    }
    ;
    function Tr() {
        X.call(this);
        var a = this;
        this.a.style.visibility = "hidden";
        this.a.style.width = "350px";
        this.a.style.height = "203px";
        this.a.style.backgroundColor = "#ffffff";
        this.a.style.border = "2px solid #0b5d81";
        this.a.style.borderRadius = "4px";
        this.a.style.overflow = "";
        y("keydown", this.a, function(b) {
            27 === b.keyCode && a.A()
        });
        this.a.appendChild(vm("#0b5d81", "#e0e0e0", "304px"));
        this.Z = document.createElement("div");
        this.Z.innerText = Jd;
        this.Z.style.color = "#0b5d81";
        this.Z.style.backgroundColor = "#e0e0e0";
        this.Z.style.fontSize = "15px";
        this.Z.style.fontFamily = "UbuntuBold, Arial, Helvetica, sans-serif";
        this.Z.style.padding = "6px";
        this.a.appendChild(this.Z);
        q(this.bc, function() {
            a.A()
        })
    }
    m(Tr, X);
    Tr.prototype.show = function(a) {
        var b = this;
        this.de = this.N(new Dm);
        q(this.de.Ri, function() {
            b.A()
        });
        this.yd = a;
        this.tc();
        this.C();
        this.de.a.style.height = Math.max(0, this.a.clientHeight - this.Z.clientHeight) + "px";
        this.a.style.visibility = "visible";
        this.de.md.focus();
        this.Y()
    }
    ;
    Tr.prototype.C = function() {
        void 0 === this.yd ? this.A() : (this.a.style.top = this.yd.offsetHeight - 4 + "px",
        this.a.style.left = this.yd.offsetLeft + this.yd.offsetWidth - this.a.offsetWidth - 5 + "px")
    }
    ;
    Tr.prototype.A = function() {
        this.removeChild(this.de);
        this.$b();
        this.a.style.visibility = "hidden"
    }
    ;
    function Ur(a) {
        v.call(this);
        var b = this;
        this.H = a;
        this.state = {
            videoWidth: 500
        };
        this.Lh = !1;
        Vr(this);
        this.a.style.display = "inline-block";
        this.a.style.width = "8px";
        this.a.style.boxSizing = "border-box";
        y("mousedown", this.a, function(c) {
            c.preventDefault();
            b.H.a.onmousemove = function(c) {
                c = c.pageX - a.Pa.a.getBoundingClientRect().left - b.a.offsetWidth / 2;
                Wr(b, c);
                b.H.Y()
            }
            ;
            b.H.a.onmouseup = function() {
                Xr(b)
            }
            ;
            b.H.a.onmouseleave = function() {
                Xr(b)
            }
            ;
            b.Lh = !0
        });
        y("touchmove", this.a, function(c) {
            c.preventDefault();
            1 === c.targetTouches.length && (b.H.a.ontouchmove = function() {
                var d = c.targetTouches.item(0);
                null !== d && (d = d.pageX - a.Pa.a.getBoundingClientRect().left - b.a.offsetWidth / 2,
                Wr(b, d),
                b.H.Y())
            }
            ,
            b.H.a.ontouchend = function() {
                Xr(b)
            }
            ,
            b.Lh = !0)
        });
        this.a.onmouseenter = function() {
            b.a.style.backgroundImage = 'url("https://ssl-ccstatic.highwebmedia.com/theatermodeassets/resize.gif")'
        }
        ;
        this.a.onmouseleave = function() {
            b.Lh || (b.a.style.backgroundImage = 'url("https://ssl-ccstatic.highwebmedia.com/theatermodeassets/resize_arrows.gif")')
        }
        ;
        setTimeout(function() {
            b.a.style.cursor = Yr(b);
            b.a.style.background = '#e0e0e0 url("https://ssl-ccstatic.highwebmedia.com/theatermodeassets/resize_arrows.gif") no-repeat left center';
            Wr(b);
            Xr(b)
        }, 0);
        q(Nk, function(a) {
            b.S = a.Ya;
            "default" === b.S && (Vr(b),
            Wr(b))
        })
    }
    m(Ur, v);
    function Wr(a, b) {
        b = void 0 === b ? a.state.videoWidth : b;
        var c = Math.max(b, 502);
        c = Math.min(Zr(a), c);
        switch (a.S) {
        case "default":
            a.H.Pa.a.style.width = c + "px";
            a.H.Pa.u.a.style.height = c * (a.H.Pa.u.Ec ? .5625 : .75) + a.H.Pa.u.M.Mb() + "px";
            a.H.ia.a.style.width = a.H.a.clientWidth - c - a.a.offsetWidth - 2 * a.H.gg + "px";
            break;
        case "theater":
            a.H.Pa.a.style.width = a.H.a.clientWidth - 2 * a.H.gg + "px";
            break;
        case "fullscreen":
            a.H.Pa.a.style.width = window.innerWidth + "px";
            break;
        default:
            w("Unexpected VideoMode: " + a.S)
        }
        a.state.videoWidth = c;
        "default" === a.S && (c = Yr(a),
        a.a.style.cursor = c,
        a.H.a.style.cursor = c)
    }
    function Xr(a) {
        "default" === a.S && (a.Dd(),
        a.H.a.onmousemove = function() {}
        ,
        a.H.a.onmouseup = function() {}
        ,
        a.H.a.onmouseleave = function() {}
        ,
        a.H.a.ontouchend = function() {}
        ,
        a.H.a.ontouchmove = function() {}
        ,
        a.H.a.style.cursor = "",
        a.a.style.backgroundImage = 'url("https://ssl-ccstatic.highwebmedia.com/theatermodeassets/resize_arrows.gif")',
        a.Lh = !1)
    }
    function Zr(a) {
        return "default" === a.S ? a.H.a.clientWidth - 250 : a.H.a.clientWidth - 2 * a.H.gg
    }
    function Yr(a) {
        var b = a.H.Pa.a.offsetWidth;
        return 500 >= b ? "e-resize" : b >= Zr(a) ? "w-resize" : "ew-resize"
    }
    function Vr(a) {
        if (x()) {
            var b = window.localStorage.getItem("defaultVideoWidth");
            if (null !== b) {
                a.state = {
                    videoWidth: JSON.parse(b).videoWidth
                };
                return
            }
        }
        if ("default" === a.S) {
            b = window.innerWidth;
            if (Zr(a) > b / 2) {
                var c = b / 2;
                b = c * (a.H.Pa.u.Ec ? .5625 : .75)
            } else
                b = window.innerHeight - 166,
                c = b / (a.H.Pa.u.Ec ? .5625 : .75);
            b > window.innerHeight - 166 && (b = window.innerHeight - 166,
            c = b / (a.H.Pa.u.Ec ? .5625 : .75));
            500 > c && (c = 500);
            a.state.videoWidth = c
        }
    }
    Ur.prototype.Dd = function() {
        x() && window.localStorage.setItem("defaultVideoWidth", JSON.stringify({
            videoWidth: this.state.videoWidth
        }))
    }
    ;
    Ur.prototype.C = function() {
        Wr(this);
        this.Lh || Xr(this)
    }
    ;
    function $r(a, b) {
        function c() {
            if ("default" === d.S)
                d.Bb.a.style.padding = "0",
                u(d.Bb.Mj, void 0);
            else {
                for (var a = 0; a < d.Tb.children.length; a += 1)
                    d.Tb.removeChild(d.Tb.children.item(a));
                d.Bb.a.style.padding = "3px 5px";
                f ? zl(b.X.ia.ca.Bk) : setTimeout(function() {
                    zl(b.X.ia.ca.Bk)
                }, 2E3)
            }
        }
        v.call(this);
        var d = this;
        this.u = a;
        this.ul = !1;
        this.a.style.position = "relative";
        this.a.style.display = "inline-block";
        this.a.style.backgroundColor = "#ffffff";
        this.a.style.borderRadius = "4px 4px 0 0";
        this.a.style.boxSizing = "border-box";
        this.a.style.minWidth = "500px";
        this.a.style.height = "auto";
        this.a.style.fontFamily = "UbuntuRegular, Helvetica, Arial, sans-serif";
        this.a.style.fontSize = "11px";
        this.a.style.overflow = "visible";
        var e;
        this.Fb = document.createElement("a");
        this.Fb.href = "#";
        this.Fb.innerText = Kd;
        this.Fb.style.color = "#0c6a93";
        this.Fb.style.fontSize = "10px";
        this.Fb.style.fontFamily = "UbuntuMedium, Helvetica, Arial, sans-serif";
        this.Fb.style.textDecoration = "none";
        this.Fb.style.padding = "8px 6px 12px 0";
        this.Fb.style.cssFloat = "right";
        this.Fb.onmouseenter = function() {
            d.Fb.style.textDecoration = "underline"
        }
        ;
        this.Fb.onmouseleave = function() {
            d.Fb.style.textDecoration = "none"
        }
        ;
        this.Fb.onclick = function(a) {
            a.preventDefault();
            void 0 === e && (e = d.N(new Tr));
            e.show(d.Fb)
        }
        ;
        this.a.appendChild(this.Fb);
        this.vb = document.createElement("div");
        this.vb.style.color = R.Ba;
        this.vb.style.fontFamily = "UbuntuMedium, Helvetica, Arial, sans-serif";
        this.vb.style.height = "22px";
        this.vb.style.lineHeight = "24px";
        this.vb.style.overflow = "hidden";
        this.vb.style.fontSize = "12px";
        this.vb.style.padding = "4px 0 4px 9px";
        this.vb.onmouseenter = function() {
            d.vb.clientHeight < d.vb.scrollHeight - 2 && (d.ab.style.visibility = "visible",
            d.ab.style.top = d.vb.offsetTop + d.vb.offsetHeight + 2 + "px")
        }
        ;
        this.vb.onmouseleave = function() {
            d.ab.style.visibility = "hidden"
        }
        ;
        this.ab = document.createElement("div");
        this.ab.style.visibility = "hidden";
        this.ab.style.fontFamily = "UbuntuRegular, Arial, Helvetica, sans-serif";
        this.ab.style.color = R.hp;
        this.ab.style.fontSize = "12px";
        this.ab.style.boxSizing = "border-box";
        this.ab.style.zIndex = "999";
        this.ab.style.position = "absolute";
        this.ab.style.borderRadius = "4px";
        this.ab.style.border = "1px solid black";
        this.ab.style.background = "white";
        this.ab.style.padding = "10px";
        this.ab.style.width = "500px";
        this.ab.style.height = "auto";
        this.ab.style.lineHeight = "16px";
        this.a.appendChild(this.vb);
        this.a.appendChild(this.ab);
        this.N(this.u);
        this.N(b);
        this.Tb = document.createElement("span");
        this.Tb.style.width = "270px";
        this.Tb.style.height = "69px";
        this.Tb.style.display = "inline-block";
        this.Tb.style.verticalAlign = "top";
        this.Tb.style.margin = "0";
        this.a.appendChild(this.Tb);
        this.Bb = b.X.ia.ca.Bk.Bb;
        q(this.Bb.Mj, function() {
            if ("default" === d.S) {
                for (var a = 0; a < d.Tb.children.length; a += 1)
                    d.Tb.removeChild(d.Tb.children.item(a));
                d.Tb.appendChild(d.Bb.a.cloneNode(!0))
            }
        });
        var f = !1;
        setTimeout(function() {
            f = !0
        }, 2E3);
        q(Nk, function(a) {
            d.S = a.Ya;
            "default" === d.S ? (d.a.style.border = "1px solid #acacac",
            d.vb.style.display = "block",
            d.Fb.style.display = "inline",
            d.Tb.style.display = "inline-block",
            d.Kc.style.display = "inline-block") : (d.a.style.border = "none",
            d.vb.style.display = "none",
            d.Fb.style.display = "none",
            d.Tb.style.display = "none",
            d.Kc.style.display = "none");
            c()
        });
        q(uh, function() {
            c()
        });
        this.Kc = document.createElement("div");
        this.Kc.style.height = "69px";
        this.Kc.style.width = "230px";
        this.Kc.style.boxSizing = "border-box";
        this.Kc.style.fontSize = "11px";
        this.Kc.style.color = R.Ba;
        this.Kc.style.overflow = "hidden";
        this.Kc.style.display = "inline-block";
        this.Kc.style.verticalAlign = "top";
        this.Kc.style.margin = "0";
        this.a.appendChild(this.Kc);
        var h = document.createElement("div");
        h.style.display = "block";
        h.style.boxSizing = "border-box";
        h.style.width = "100%";
        h.style.height = "100%";
        h.style.padding = "2px 10px";
        this.Kc.appendChild(h);
        var g = document.createElement("div");
        g.style.display = "block";
        g.style.height = "19px";
        g.style.borderBottom = "1px solid #000000";
        h.appendChild(g);
        var p = document.createElement("span");
        p.innerText = de;
        g.appendChild(p);
        this.sa = document.createElement("span");
        this.sa.innerText = "0";
        this.sa.style.fontSize = "15px";
        this.sa.style.fontFamily = "UbuntuBold, Helvetica, Arial, sans-serif";
        g.appendChild(this.sa);
        this.mi = document.createElement("span");
        this.mi.innerText = " tokens";
        g.appendChild(this.mi);
        g = document.createElement("div");
        g.style.display = "inline-block";
        g.style.verticalAlign = "top";
        g.style.width = "130px";
        g.style.textOverflow = "ellipsis";
        g.style.overflow = "hidden";
        g.style.whiteSpace = "nowrap";
        h.appendChild(g);
        var t = as(Ec);
        t.style.margin = "2px 0 0 0";
        t.href = "/tipping/purchase_tokens/";
        t.onclick = function() {
            Mh("/tipping/purchase_tokens/", "_blank", "height=615, width=850");
            return !1
        }
        ;
        t.onmouseenter = function() {
            t.style.textDecoration = "underline"
        }
        ;
        t.onmouseleave = function() {
            t.style.textDecoration = "none"
        }
        ;
        g.appendChild(t);
        this.Hc = as(Ic);
        this.Hc.style.display = "none";
        g.appendChild(this.Hc);
        this.Ac = document.createElement("div");
        this.Ac.style.display = "none";
        this.Ac.style.overflow = "hidden";
        this.Ac.style.textOverflow = "ellipsis";
        this.Ac.style.lineHeight = "1.3";
        this.Ac.style.maxWidth = "130px";
        g.appendChild(this.Ac);
        this.qd = document.createElement("a");
        this.qd.style.maxWidth = "90px";
        this.qd.style.marginRight = "2px";
        this.qd.style.display = "inline-block";
        this.qd.style.overflow = "hidden";
        this.qd.style.textOverflow = "ellipsis";
        this.qd.style.verticalAlign = "bottom";
        this.Ac.appendChild(this.qd);
        this.cj = document.createElement("span");
        this.Ac.appendChild(this.cj);
        this.ie = document.createElement("div");
        this.ie.style.display = "none";
        this.ie.style.width = "135px";
        g.appendChild(this.ie);
        this.Lf = document.createElement("div");
        this.Lf.style.display = "none";
        this.ie.appendChild(this.Lf);
        this.Dg = as(Uc);
        this.Dg.style.display = "inline-block";
        this.Dg.style.maxWidth = "87px";
        this.Dg.style.marginRight = "2px";
        this.Dg.style.verticalAlign = "top";
        this.Lf.appendChild(this.Dg);
        var C = document.createElement("span");
        C.style.display = "inline-block";
        C.style.verticalAlign = "top";
        this.Lf.appendChild(C);
        this.Ch = as(Sc);
        this.ie.appendChild(this.Ch);
        this.Dh = as(Wc);
        this.ie.appendChild(this.Dh);
        var B = 0
          , M = 0
          , F = document.createElement("div");
        F.innerText = tc;
        F.style.display = "none";
        F.style.marginTop = "10px";
        h.appendChild(F);
        this.jm = document.createElement("span");
        this.jm.style.display = "none";
        h.appendChild(this.jm);
        var O = new Aa;
        q(S, function(a) {
            if (void 0 === a.h.hb || void 0 === a.h.kd || void 0 === a.h.jd)
                w("Dossier is missing context.");
            else {
                M = a.h.Bo;
                B = a.h.Co;
                d.vb.innerText = a.h.bb;
                d.ab.innerText = a.h.bb;
                var b = isNaN(a.h.sa) ? 0 : a.h.sa;
                d.sa.innerText = "" + b;
                d.mi.innerText = " " + Ab(b, !1);
                d.hb = a.h.hb;
                var c = !0 === a.h.kd
                  , e = !0 === a.h.jd;
                d.hb ? (d.D.style.display = "inline-block",
                F.style.display = "none",
                q(a.j.event.Zd, function(a) {
                    M = a.Bl;
                    C.innerText = "( " + B + " / " + M + " )"
                }),
                q(a.j.event.Zd, function(b) {
                    c = b.kd;
                    e = b.jd;
                    bs(d, c, a.j, a.j.status);
                    cs(d, e, a.j, a.j.status)
                }),
                q(a.j.event.kb, function(b) {
                    d.u.M.ah && (bs(d, c, a.j, b.Aa),
                    cs(d, e, a.j, b.Aa),
                    "groupnotwatching" === b.Aa && "groupwatching" === b.Vc && --B,
                    e && (b = "( " + B + " / " + M + " )",
                    C.innerText = b,
                    d.cj.innerText = b))
                }),
                q(a.j.event.Eg, function(a) {
                    var b = a.pg;
                    void 0 === b && (b = M);
                    B = a.Ld;
                    a = "( " + a.Ld + " / " + b + " )";
                    C.innerText = a;
                    d.cj.innerText = a
                }),
                q(a.j.event.Tg, function(b) {
                    "groupwatching" === a.j.status && (B = b - 1,
                    d.cj.innerText = "( " + B + " / " + M + " )")
                })) : (d.D.style.display = "none",
                bs(d, !1, a.j, a.h.Xa),
                cs(d, !1, a.j, a.h.Xa),
                F.style.display = "block");
                za(q(Kp, function() {
                    bs(d, c, a.j, a.h.Xa);
                    cs(d, e, a.j, a.h.Xa)
                }), O);
                za(q(a.j.event.nk, function(a) {
                    d.vb.innerText = a;
                    d.ab.innerText = a
                }), O);
                f = !1;
                setTimeout(function() {
                    f = !0
                }, 2E3)
            }
        });
        q(qj, function(a) {
            d.sa.innerText = "" + a.Sb;
            d.mi.innerText = " " + Ab(a.Sb, !1)
        });
        this.D = document.createElement("span");
        this.D.innerText = qc;
        this.D.title = qc;
        this.D.style.overflow = "hidden";
        this.D.style.lineHeight = "1.4";
        this.D.style.height = "21px";
        this.D.style.maxWidth = "80px";
        this.D.style.fontSize = "12px";
        this.D.style.fontFamily = "UbuntuMedium, Helvetica, Arial, sans-serif";
        this.D.style.textShadow = "#588d3d 1px 1px 0px";
        this.D.style.color = "#ffffff";
        this.D.style.margin = "11px 0px 11px 0px";
        this.D.style.textOverflow = "ellipsis";
        this.D.style.padding = "3px 10px 3px";
        this.D.style.backgroundColor = "#53843a";
        La() && (this.D.style.background = "linear-gradient(180deg, #73aa58 0%, #73aa58 50%, #568a3d 60%)");
        this.D.style.borderRadius = "4px";
        this.D.style.boxSizing = "border-box";
        this.D.style.cursor = "pointer";
        this.D.style.display = "none";
        h.appendChild(this.D);
        this.D.onclick = function() {
            u(rh, {})
        }
        ;
        q(vh, function() {
            Da(O)
        })
    }
    m($r, v);
    function as(a) {
        var b = document.createElement("a");
        b.innerText = a;
        b.title = a;
        b.style.lineHeight = "1.3";
        b.style.display = "block";
        b.style.maxWidth = "130px";
        b.style.overflow = "hidden";
        b.style.textOverflow = "ellipsis";
        b.style.color = "#0a5a83";
        b.style.textDecoration = "none";
        b.style.cursor = "pointer";
        b.onmouseenter = function() {
            b.style.textDecoration = "underline"
        }
        ;
        b.onmouseleave = function() {
            b.style.textDecoration = "none"
        }
        ;
        return b
    }
    function bs(a, b, c, d) {
        a.Hc.innerText = Ic;
        var e = ["privaterequesting", "privatewatching", "privatespying"];
        if (b || -1 !== e.indexOf(d))
            switch (a.Hc.style.display = "block",
            a.Hc.onclick = function() {
                jk(c)
            }
            ,
            b = function(b) {
                N("LeavePrivateOrSpyShow");
                b.preventDefault();
                a.ul || (a.ul = !0,
                c.Ph().then(function(b) {
                    a.ul = !1;
                    "" !== b && U(b)
                }))
            }
            ,
            d) {
            case "privaterequesting":
                a.Hc.innerText = Nc;
                a.Hc.onclick = b;
                break;
            case "privatenotwatching":
                a.Hc.innerText = Jc;
                a.Hc.onclick = function() {
                    kk(c)
                }
                ;
                break;
            case "privatewatching":
            case "privatespying":
                a.Hc.innerText = Pc;
                a.Hc.onclick = b;
                break;
            case "grouprequesting":
            case "groupnotwatching":
            case "groupwatching":
            case "offline":
            case "away":
            case "hidden":
            case "notconnected":
                a.Hc.style.display = "none";
                break;
            case "public":
                break;
            default:
                r("unexpected status for updatePrivateState: " + d)
            }
        else
            a.Hc.style.display = "none"
    }
    function cs(a, b, c, d) {
        var e = ["grouprequesting", "groupwatching"];
        if (b || -1 !== e.indexOf(d))
            switch (a.ie.style.display = "inline-block",
            a.Dg.onclick = function() {
                lk(c)
            }
            ,
            b = function() {
                mk(c)
            }
            ,
            d) {
            case "grouprequesting":
                a.Ch.onclick = b;
                a.qd.innerText = Tc;
                a.Lf.style.display = "none";
                a.Ac.style.display = "block";
                a.Ch.style.display = "block";
                a.Dh.style.display = "none";
                a.qd.style.color = R.Ba;
                break;
            case "groupwatching":
                a.Dh.onclick = b;
                a.qd.innerText = Rc;
                a.Lf.style.display = "none";
                a.Ac.style.display = "block";
                a.Ch.style.display = "none";
                a.Dh.style.display = "block";
                a.qd.style.color = R.Ba;
                break;
            case "privaterequesting":
            case "privatenotwatching":
            case "privatewatching":
            case "privatespying":
            case "offline":
            case "away":
            case "hidden":
            case "notconnected":
                a.Ac.style.display = "none";
                a.ie.style.display = "none";
                break;
            case "groupnotwatching":
            case "public":
                a.Lf.style.display = "block";
                a.Ac.style.display = "none";
                a.Ch.style.display = "none";
                a.Dh.style.display = "none";
                break;
            default:
                r("unexpected status for updateGroupState: " + d)
            }
        else
            a.ie.style.display = "none",
            a.Ac.style.display = "none"
    }
    ;var ds, es, fs;
    q(S, function(a) {
        ds = a.j.i();
        es = a.j.username();
        fs = a.j
    });
    function gs(a, b) {
        var c = document.createElement("span");
        c.style.color = yj(a);
        void 0 !== b && b || (c.setAttribute("data-purecolor", a.Oe ? "#ff6200" : a.Dc ? "#DC0000" : a.Bc ? "#00ff00" : a.mf ? "#ad62e1" : a.kf ? "#d4a0ff" : a.lf ? "#8a98ff" : a.je ? "#84c6dc" : "#b3b3b3"),
        c.className = "purecolor");
        c.style.fontWeight = "bold";
        c.innerText = a.username;
        c.onmouseenter = function() {
            c.style.textDecoration = "underline";
            c.style.cursor = "pointer"
        }
        ;
        c.onmouseleave = function() {
            c.style.textDecoration = "none";
            c.style.cursor = "default"
        }
        ;
        y("click", c, function() {
            null === c.parentElement ? w("Username span has no parent") : (N("OpenUserContextMenu", {
                username: a.username
            }),
            hs(a.username, c.parentElement))
        });
        return c
    }
    function is() {
        var a = document.createElement("hr");
        a.style.margin = "4px";
        a.style.backgroundColor = "#8b8b8b";
        a.style.height = "1px";
        a.style.border = "none";
        return a
    }
    function hs(a, b) {
        2 === js && (js = 0,
        Cj(a).then(function(c) {
            var d = new ks(a,c,b);
            b.style.cursor = "default";
            d.position();
            d.xg(c.xg, a)
        })["catch"](function(a) {
            js = 2;
            w("Could not process user menu request: " + a)
        }))
    }
    function ks(a, b, c) {
        var d = this;
        js = 0;
        this.d = b;
        this.ka = c;
        this.T = document.createElement("div");
        this.T.style.border = "1px solid #6B6B6B";
        this.T.style.position = "absolute";
        this.T.style.background = "#e1e1e1";
        this.T.style.borderRadius = "4px";
        this.T.style.display = "none";
        this.T.style.zIndex = "999";
        this.T.style.top = "0";
        this.T.style.fontWeight = "normal";
        this.T.style.fontFamily = "UbuntuRegular, Arial, Helvetica, sans-serif";
        this.T.style.fontSize = "12px";
        this.T.style.width = "260px";
        this.T.style.color = "#004563";
        this.T.onmouseenter = function() {
            d.T.style.cursor = "default"
        }
        ;
        this.O = document.createElement("div");
        this.O.style.position = "fixed";
        this.O.style.top = "0";
        this.O.style.left = "0";
        this.O.style.height = "100%";
        this.O.style.width = "100%";
        this.O.style.display = "block";
        this.ka.appendChild(this.O);
        this.O.onclick = function() {
            d.Jd()
        }
        ;
        b = document.createElement("div");
        c = document.createElement("img");
        var e = document.createElement("span");
        c.src = "https://ssl-ccstatic.highwebmedia.com/theatermodeassets/red-arrow-down.gif";
        e.innerText = a;
        b.style.margin = "6px 4px";
        b.style.color = "#383838";
        b.style.fontWeight = "bold";
        c.style.paddingRight = "6px";
        b.appendChild(c);
        b.appendChild(e);
        this.T.appendChild(b);
        this.T.appendChild(is());
        b = document.createElement("div");
        b.style.margin = "0 6px 0 6px";
        b.style.display = "block";
        c = document.createElement("div");
        c.style.display = "inline-block";
        c.style.margin = "4px";
        c.style.verticalAlign = "top";
        b.appendChild(c);
        if (this.d.bn) {
            e = document.createElement("span");
            e.style.display = "inline-block";
            e.style.color = "#575757";
            e.style.fontWeight = "bold";
            e.style.fontSize = "13px";
            e.style.marginRight = "5px";
            c.appendChild(e);
            var f = document.createElement("img");
            f.src = zj(this.d.Bg);
            f.style.display = "inline-block";
            f.height = 16;
            f.width = 16;
            e.innerText = isNaN(this.d.he) ? "" : "" + this.d.he;
            c.appendChild(f)
        }
        e = document.createElement("div");
        var h = document.createElement("a");
        e.appendChild(h);
        c.appendChild(e);
        h.href = "/" + a + "/";
        h.target = "_blank";
        h.innerText = ne;
        h.style.textDecoration = "none";
        h.style.color = "#004563";
        h.onmouseenter = function() {
            h.style.textDecoration = "underline";
            h.style.cursor = "pointer"
        }
        ;
        h.onmouseleave = function() {
            h.style.textDecoration = "none";
            h.style.cursor = "default"
        }
        ;
        y("click", e, function(b) {
            N("ViewProfile", {
                username: a
            });
            b.stopPropagation();
            d.Jd()
        });
        this.d.Ye && (e = document.createElement("img"),
        e.src = this.d.oj,
        e.height = 52,
        e.width = 62,
        e.style.display = "inline-block",
        e.style.margin = "4px 4px 0 4px",
        e.style.border = "1px solid #f37220",
        b.insertBefore(e, c));
        this.T.appendChild(b);
        this.T.appendChild(is());
        if (es !== a) {
            if (void 0 === fs) {
                w("ignoreText: Chatconnection should be defined");
                return
            }
            b = document.createElement("div");
            var g = document.createElement("div")
              , p = document.createElement("div");
            c = document.createElement("img");
            c.src = "https://ssl-ccstatic.highwebmedia.com/theatermodeassets/icon-email.gif";
            var t = document.createElement("span");
            t.innerText = oe;
            var C = document.createElement("span");
            fs.ne(a) ? (C.style.paddingLeft = "13px",
            C.innerText = Qf) : (C.style.paddingLeft = "7px",
            C.innerText = Sf,
            e = document.createElement("img"),
            e.src = "https://ssl-ccstatic.highwebmedia.com/theatermodeassets/icon-ignore.gif",
            p.appendChild(e));
            p.appendChild(C);
            c.style.paddingRight = "4px";
            g.style.padding = "3px 9px 3px 6px";
            g.style.whiteSpace = "nowrap";
            p.style.padding = "3px 9px 3px 6px";
            b.style.marginBottom = "7px";
            b.style.color = "#004563";
            g.appendChild(c);
            g.appendChild(t);
            b.appendChild(g);
            b.appendChild(p);
            p.onmouseenter = function() {
                p.style.background = "#6B6B6B";
                p.style.cursor = "pointer";
                C.style.color = "#e1e1e1"
            }
            ;
            p.onmouseleave = function() {
                p.style.background = "#e1e1e1";
                p.style.cursor = "default";
                C.style.color = "#004563"
            }
            ;
            p.onclick = function() {
                N("IgnoreUser", {
                    username: a
                });
                void 0 === fs ? w("no chatConn defined in ignoreLink.onclick") : (fs.ne(a) ? fs.qk(a) : fs.Qc(a),
                d.Jd())
            }
            ;
            g.onmouseenter = function() {
                g.style.background = "#6B6B6B";
                g.style.cursor = "pointer";
                t.style.color = "#e1e1e1"
            }
            ;
            g.onmouseleave = function() {
                g.style.background = "#e1e1e1";
                g.style.cursor = "default";
                t.style.color = "#004563"
            }
            ;
            y("click", g, function(b) {
                d.Jd();
                Ni('You must be logged in to send a private message. Click "OK" to login.') || (N("StartPrivateMessage", {
                    username: a
                }),
                u(Gj, a),
                b.stopPropagation())
            });
            this.T.appendChild(b)
        }
        this.T.style.display = "block";
        js = 1
    }
    ks.prototype.xg = function(a, b) {
        var c = this;
        if (a) {
            this.we = document.createElement("div");
            var d = document.createElement("div")
              , e = document.createElement("span");
            e.innerText = ug;
            e.style.paddingLeft = "3px";
            d.style.padding = "3px 9px 3px 9px";
            this.we.style.marginBottom = "7px";
            this.we.style.color = "#004563";
            d.appendChild(e);
            this.we.appendChild(d);
            d.onmouseenter = function() {
                d.style.background = "#6B6B6B";
                d.style.cursor = "pointer";
                d.style.color = "#e1e1e1"
            }
            ;
            d.onmouseleave = function() {
                d.style.background = "#e1e1e1";
                d.style.cursor = "default";
                d.style.color = "#004563"
            }
            ;
            d.onclick = function() {
                Ki("Silence " + b + "?", function() {
                    N("SilenceUser", {
                        username: b
                    });
                    A("roomsilence/" + b + "/" + ds + "/", {}).then(function() {})["catch"](function(a) {
                        w(a);
                        U("Error silencing user " + b)
                    })
                });
                c.Jd()
            }
            ;
            this.T.appendChild(is());
            this.T.appendChild(this.we)
        }
    }
    ;
    ks.prototype.position = function() {
        this.ka.appendChild(this.T);
        var a = this.ka.getBoundingClientRect()
          , b = this.ka.offsetParent.getBoundingClientRect();
        this.T.style.top = a.top - b.top + "px";
        this.T.offsetTop + this.T.offsetHeight > window.document.documentElement.clientHeight - 8 && (this.T.style.top = window.document.documentElement.clientHeight - this.T.offsetHeight - 8 + "px")
    }
    ;
    ks.prototype.Jd = function() {
        this.ka.removeChild(this.T);
        this.ka.removeChild(this.O);
        js = 2
    }
    ;
    var js = 2;
    var ls = !1, ms, ns, os;
    q(S, function(a) {
        ls = a.h.qa.Zg;
        ms = a.h.Ad;
        ns = a.j;
        os = a.h.nj
    });
    q(th, function(a) {
        ls = a.Zg
    });
    function ps(a) {
        var b = ls
          , c = new fk(a);
        a = document.createElement("span");
        a.className = "emoticonImage";
        a.innerText = c.Ib()[0];
        for (var d = {
            ha: 1
        }; d.ha < c.Ib().length; d = {
            ha: d.ha
        },
        d.ha += 1) {
            if (!b || 0 <= os.indexOf(c.da()[d.ha - 1].name)) {
                var e = document.createElement("span");
                e.style.color = "lightskyblue";
                e.innerText = ":" + c.da()[d.ha - 1].name;
                e.style.cursor = "pointer";
                e.onclick = function(a) {
                    return function() {
                        var b = {
                            vc: c.da()[a.ha - 1].name,
                            url: c.da()[a.ha - 1].me
                        };
                        u(tk, b)
                    }
                }(d)
            } else {
                e = document.createElement("img");
                var f = c.da()[d.ha - 1].hk;
                e.src = void 0 !== f ? f : c.da()[d.ha - 1].me;
                e.title = ":" + c.da()[d.ha - 1].name;
                e.style.cursor = "pointer";
                e.onclick = function(a) {
                    return function() {
                        var b = {
                            vc: c.da()[a.ha - 1].name,
                            url: c.da()[a.ha - 1].me
                        };
                        u(tk, b)
                    }
                }(d);
                e.name = c.da()[d.ha - 1].name;
                e.height = c.da()[d.ha - 1].height;
                e.width = c.da()[d.ha - 1].width
            }
            a.appendChild(e);
            "" !== c.Ib()[d.ha] && (e = document.createElement("span"),
            e.innerText = c.Ib()[d.ha],
            a.appendChild(e))
        }
        return a
    }
    function qs(a) {
        var b = rs(a.fb.username)
          , c = document.createElement("div");
        c.style.width = "100%";
        c.style.display = "inline-block";
        c.style.padding = "2px";
        c.style.padding = "0";
        var d = gs(a.fb, void 0 !== a.backgroundColor);
        d.style.paddingRight = "4px";
        c.appendChild(d);
        c.style.marginRight = "4px";
        var e = ps(a.message)
          , f = void 0 !== a.Kd ? a.Kd : "#494949";
        e.style.color = f;
        void 0 !== a.font && (d.style.fontFamily = a.font,
        e.style.fontFamily = a.font);
        e.setAttribute("data-purecolor", Kl(f));
        e.className = "purecolor";
        void 0 !== a.backgroundColor && (c.style.background = a.backgroundColor);
        c.appendChild(e);
        b.appendChild(c);
        return b
    }
    function ss(a) {
        var b = rs();
        b.style.padding = "1px 5px";
        var c = document.createElement("div");
        c.style.display = "inline-block";
        if (void 0 !== a.background)
            c.style.background = a.background,
            c.style.overflow = "hidden",
            c.style.marginTop = "1px",
            c.style.textShadow = "none",
            "#ff8b45" === a.background && (c.style.padding = "0px 4px");
        else {
            var d = Kl(void 0 !== a.oa ? a.oa : "#aaaaaa");
            c.setAttribute("data-purecolor", d);
            c.className = "purecolor"
        }
        c.style.color = void 0 !== a.oa ? a.oa : "#494949";
        c.style.fontWeight = void 0 !== a.weight ? a.weight : "normal";
        b.appendChild(ts(c, a.U, void 0 !== a.background));
        return b
    }
    function ts(a, b, c) {
        b = l(b);
        for (var d = b.next(); !d.done; d = b.next()) {
            var e = document.createElement("div");
            d = l(d.value);
            for (var f = d.next(); !f.done; f = d.next())
                switch (f = f.value,
                f.cc) {
                case 1:
                    e.appendChild(us(f));
                    break;
                case 0:
                    e.appendChild(gs(f.hc, c));
                    break;
                case 2:
                    e.appendChild(vs(f.message));
                    break;
                case 4:
                    e.appendChild(ws("join group show", function() {
                        "grouprequesting" === ns.status ? U("You are already requesting to join this group show.") : lk(ns)
                    }));
                    break;
                case 3:
                    e.appendChild(ws("spy private show", function() {
                        "privatespying" !== ns.status && kk(ns)
                    }));
                    break;
                default:
                    w("Unknown roomNotice type for: " + JSON.stringify(f))
                }
            a.appendChild(e)
        }
        return a
    }
    function ws(a, b) {
        var c = document.createElement("a");
        c.style.color = "#85FFFF";
        c.style.textDecoration = "underline";
        c.innerText = a;
        c.onclick = b;
        return c
    }
    function vs(a) {
        var b = document.createElement("a");
        b.href = "/tag/" + a + "/" + ms;
        b.style.color = "#0a5a83";
        b.style.textDecoration = "none";
        b.innerText = "#" + a;
        return b
    }
    function xs(a) {
        var b = rs();
        b.innerText = a;
        b.setAttribute("data-purecolor", Kl("#aaaaaa"));
        b.className = "purecolor";
        return b
    }
    function ys(a, b) {
        var c = rs()
          , d = document.createElement("span");
        d.innerText = Mb(a);
        c.appendChild(d);
        for (d = 0; d < b.length; d += 1) {
            var e = document.createElement("a");
            e.href = b[d].Um;
            e.target = "_blank";
            e.innerText = b[d].Dk;
            e.style.color = "#0a5a83";
            e.style.textDecoration = "none";
            c.appendChild(e);
            d < b.length - 1 && (e = document.createElement("span"),
            e.innerText = ", ",
            c.appendChild(e))
        }
        c.setAttribute("data-purecolor", Kl("#aaaaaa"));
        c.className = "purecolor";
        return c
    }
    function rs(a) {
        a = void 0 === a ? "" : a;
        var b = document.createElement("div");
        b.style.color = "#494949";
        b.style.fontFamily = "Tahoma,Arial,Helvetica,sans-serif";
        b.style.boxSizing = "border-box";
        b.style.paddingTop = "2px";
        b.style.paddingBottom = "3px";
        b.style.paddingLeft = "5px";
        b.style.paddingRight = "5px";
        0 < a.length && b.setAttribute("data-nick", a);
        return b
    }
    function us(a) {
        var b = document.createElement("span");
        !1 !== a.Ml ? b = ps(a.message) : b.innerText = a.message;
        b.style.color = void 0 !== a.oa ? a.oa : "";
        b.style.backgroundColor = void 0 !== a.background ? a.background : "";
        b.style.fontWeight = void 0 !== a.weight ? a.weight : "";
        return b
    }
    ;function zs() {
        var a = document.createElement("div");
        a.style.boxSizing = "border-box";
        a.style.overflow = "auto";
        a.style.paddingRight = "5px";
        return a
    }
    function As() {
        var a = document.createElement("div");
        a.style.width = "100%";
        yh(a);
        a.style.cursor = "text";
        a.style.backgroundColor = "#ffffff";
        a.style.paddingBottom = "4px";
        return a
    }
    function Bs() {
        var a = document.createElement("form");
        a.style.display = "inline-block";
        a.style.boxSizing = "border-box";
        return a
    }
    function Cs() {
        var a = document.createElement("div");
        a.style.height = "28px";
        a.style.boxSizing = "border-box";
        a.style.border = "1px solid #acacac";
        a.style.borderRadius = "4px 4px 0 0";
        a.style.backgroundColor = "#ffffff";
        a.style.fontSize = "12px";
        a.style.overflow = "hidden";
        return a
    }
    function Ds() {
        var a = document.createElement("input");
        a.style.outline = "none";
        a.style.border = "none";
        a.style.margin = "6px";
        a.style.boxSizing = "border-box";
        a.style.fontSize = "12px";
        a.style.fontFamily = "Helvetica, Arial, sans-serif";
        a.autocomplete = "off";
        return a
    }
    function Es() {
        var a = document.createElement("span");
        a.innerText = Fd;
        a.style.lineHeight = "1.2";
        a.style.fontFamily = "UbuntuMedium, Helvetica, Arial, sans-serif";
        a.style.fontSize = "12px";
        a.style.textShadow = "#bc2b1d 1px 1px 0";
        a.style.color = "#ffffff";
        a.style.padding = "3px 5px";
        a.style.backgroundColor = "#eb3404";
        La() && (a.style.background = "linear-gradient(180deg, #ff4d30 0%, #ef6352 50%, #f2240f 60%, #e63125 100%)");
        a.style.borderRadius = "4px";
        a.style.boxSizing = "border-box";
        a.style.cursor = "pointer";
        a.style.display = "inline-block";
        return a
    }
    function Fs() {
        var a = document.createElement("img");
        a.src = "https://ssl-ccstatic.highwebmedia.com/theatermodeassets/ico-smile.gif";
        a.style.cursor = "pointer";
        a.style.display = "inline-block";
        a.style.marginRight = "6px";
        a.style.verticalAlign = "text-bottom";
        a.style.paddingRight = "5px";
        a.style.width = "15px";
        return a
    }
    function Gs(a) {
        v.call(this);
        var b = this;
        this.$c = a;
        this.nh = new n("addMessageHtml",{
            Xd: !1
        });
        this.Zl = new n("removeMessageHtml",{
            Xd: !1
        });
        this.xa = As();
        this.Oa = zs();
        this.ud = 0;
        this.Fa = new Aa;
        this.a.style.position = "static";
        this.a.style.boxSizing = "border-box";
        this.a.style.padding = "5px 0 0 5px";
        this.a.style.fontSize = "12px";
        this.Oa.appendChild(this.xa);
        this.a.appendChild(this.Oa);
        var c = Bs();
        this.aa = Cs();
        this.K = Ds();
        this.Ia = Fs();
        this.jb = Es();
        c.appendChild(this.K);
        this.aa.appendChild(c);
        this.aa.appendChild(this.Ia);
        this.aa.appendChild(this.jb);
        y("mousedown", this.aa, function(a) {
            if (a.target === b.aa || a.target === c)
                b.K.focus(),
                a.preventDefault()
        });
        y("click", this.jb, function() {
            b.di()
        });
        y("click", this.Ia, function() {
            u(Fj, b.Ia)
        });
        y("submit", c, function(a) {
            a.preventDefault();
            b.di()
        });
        this.a.appendChild(this.aa);
        this.Fa.add(q(S, function(a) {
            b.a.style.fontSize = a.h.qa.fontSize
        }));
        this.Fa.add(q(th, function(a) {
            b.a.style.fontSize = a.fontSize
        }));
        this.Ah = this.N(new Rk({
            Nb: this.K,
            lo: 10,
            fp: 207
        }))
    }
    m(Gs, v);
    k = Gs.prototype;
    k.zg = function() {
        Da(this.Fa);
        this.Ah.zg()
    }
    ;
    k.di = function() {
        if (!Ni('You must be logged in to send a message. Click "OK" to login.')) {
            this.pa();
            var a = this.K.value;
            this.K.value = "";
            if ("" !== a.trim()) {
                var b = Mk(a);
                void 0 !== b ? u(rh, b) : this.$c(a)
            }
            this.K.blur()
        }
    }
    ;
    k.C = function() {
        this.aa.style.width = this.a.clientWidth - 10 + "px";
        this.K.style.width = Math.max(0, this.aa.clientWidth - 18 - Math.ceil(this.jb.offsetWidth) - Math.ceil(this.Ia.offsetWidth) - 4) + "px";
        this.pa();
        this.Ah.a.style.bottom = this.aa.offsetHeight + 2 + "px"
    }
    ;
    k.pa = function() {
        this.Oa.scrollTop = this.Oa.scrollHeight - this.Oa.offsetHeight
    }
    ;
    k.ta = function(a) {
        var b = kl(this);
        this.xa.appendChild(a);
        for (var c = this.xa.childElementCount - 1E3; 0 < c; --c) {
            var d = this.xa.firstElementChild;
            null !== d && this.xa.removeChild(d)
        }
        b || this.pa();
        u(this.nh, {
            zl: function() {
                return a.cloneNode(!0)
            }
        });
        this.ud += 1;
        return a
    }
    ;
    k.Uj = function(a) {
        this.xa.removeChild(a)
    }
    ;
    k.Bh = function() {
        return this.ud
    }
    ;
    k.Aj = function(a) {
        return this.ud - a
    }
    ;
    k.dj = function(a) {
        for (var b = [], c = l(this.xa.childNodes), d = c.next(); !d.done; d = c.next())
            d = d.value,
            d.getAttribute("data-nick") === a && b.push(d);
        a = l(b);
        for (b = a.next(); !b.done; b = a.next())
            this.xa.removeChild(b.value)
    }
    ;
    k.clear = function() {
        for (this.ud = 0; null !== this.xa.firstChild; )
            this.xa.removeChild(this.xa.firstChild)
    }
    ;
    k.Zb = function() {
        this.K.focus()
    }
    ;
    k.Od = function() {
        this.K.blur()
    }
    ;
    k.Yl = function(a) {
        u(this.Zl, a)
    }
    ;
    function Hs() {
        W.call(this);
        var a = this;
        this.W = 0;
        this.a.style.overflow = "";
        this.w = new Gs(function(b) {
            a.j.im(b);
            a.w.K.blur()
        }
        );
        this.N(this.w);
        q(S, function(b) {
            a.j = b.j;
            a.w.ta(xs("Rules: No spamming. Do not insist the cam hosts to do as you please. Do not announce other rooms or websites that would conflict with this room. Avoid any argumentative and/or rude posts related to the cam viewing. Do not attempt to post your e-mail address in the public chat."));
            a.w.ta(xs("To go to next room, press CTRL+/. To send a tip, press CTRL+S or type \"/tip 25\". To disable emoticons or adjust autocomplete settings, click the 'Gear' tab above."));
            0 < b.h.ug.length && a.w.ta(ys(b.h.i, b.h.ug))
        });
        q(vh, function() {
            a.w.clear()
        })
    }
    m(Hs, W);
    k = Hs.prototype;
    k.C = function() {
        W.prototype.C.call(this);
        this.w.pa()
    }
    ;
    k.pa = function() {
        this.w.pa()
    }
    ;
    k.Kf = function() {
        return 99 < this.W ? Ed + " (99+)" : 0 < this.W ? Ed + " (" + this.W + ")" : Ed
    }
    ;
    k.Me = function() {
        return "chat-tab-default"
    }
    ;
    k.Zb = function() {
        this.w.Zb()
    }
    ;
    k.Od = function() {
        this.w.Od()
    }
    ;
    k.Qe = function() {
        return document.activeElement === this.w.K
    }
    ;
    k.show = function() {
        W.prototype.show.call(this);
        this.W = 0;
        this.qc();
        this.w.pa();
        this.w.K.focus()
    }
    ;
    k.dj = function(a) {
        for (var b = [], c = l(this.w.xa.childNodes), d = c.next(); !d.done; d = c.next())
            d = d.value,
            d.getAttribute("data-nick") === a && b.push(d);
        a = l(b);
        for (b = a.next(); !b.done; b = a.next())
            this.w.xa.removeChild(b.value)
    }
    ;
    k.Jf = function() {
        return this.w.K.value
    }
    ;
    k.kg = function(a) {
        this.w.K.value = a
    }
    ;
    k.Le = function() {
        return this.w.K
    }
    ;
    function Is(a) {
        var b = document.createElement("div")
          , c = document.createElement("div")
          , d = document.createElement("div")
          , e = document.createElement("img");
        e.src = "https://ssl-ccstatic.highwebmedia.com/theatermodeassets/icon-close-off.png";
        e.height = 16;
        e.width = 16;
        d.style.padding = "5px 5px 0 10px";
        d.style.verticalAlign = "top";
        d.style.display = "inline-block";
        d.style.overflow = "hidden";
        c.innerText = a.jg.hc + " " + (0 === a.jg.W ? "" : "(" + a.jg.W + ")");
        c.style.whiteSpace = "nowrap";
        c.style.textOverflow = "ellipsis";
        c.style.overflow = "hidden";
        c.style.padding = "5px 5px 5px 15px";
        c.style.display = "inline-block";
        c.style.width = a.size - 16 - 5 - 10 - 20 + "px";
        b.style.color = "#0a5a83";
        b.style.width = a.size + "px";
        b.onmouseenter = function() {
            e.src = "https://ssl-ccstatic.highwebmedia.com/theatermodeassets/icon-close-on.png";
            b.style.background = "#7E7E7E";
            b.style.cursor = "pointer";
            b.style.color = "#e1e1e1"
        }
        ;
        b.onmouseleave = function() {
            e.src = "https://ssl-ccstatic.highwebmedia.com/theatermodeassets/icon-close-off.png";
            b.style.background = "#e1e1e1";
            b.style.cursor = "default";
            b.style.color = "#0a5a83"
        }
        ;
        y("click", c, function() {
            a.Il()
        });
        y("click", e, function() {
            a.Jl()
        });
        b.appendChild(c);
        d.appendChild(e);
        b.appendChild(d);
        return b
    }
    function Js(a) {
        W.call(this);
        var b = this;
        this.tl = a;
        this.W = 0;
        this.Yh = new n("incomingPm");
        this.Ca = new Map;
        this.va = new Dl;
        this.a.style.overflow = "";
        q(Gj, function(a) {
            b.Pc();
            void 0 === b.Ca.get(a) && b.Jh(a, !1);
            Gl(b.va, a);
            b.qc();
            b.ve();
            Yj(b)
        });
        q(S, function(a) {
            b.j = a.j
        });
        q(vh, function() {
            for (var a = l(b.Ca.keys()), d = a.next(); !d.done; d = a.next())
                b.nd(d.value)
        });
        q(Hj, function(a) {
            b.nd(a)
        })
    }
    m(Js, W);
    k = Js.prototype;
    k.pa = function() {
        var a = Y(this);
        void 0 !== a && a.w.pa()
    }
    ;
    k.Kf = function() {
        return 99 < this.W ? "PM (99+)" : 0 < this.W ? "PM (" + this.W + ")" : "PM"
    }
    ;
    k.Jf = function() {
        var a = Y(this);
        return void 0 === a ? "" : a.w.K.value
    }
    ;
    k.kg = function(a) {
        var b = Y(this);
        void 0 !== b && (b.w.K.value = a)
    }
    ;
    k.bh = function(a) {
        var b = this;
        N("FocusPMTab", {
            unread: this.W
        });
        if (1 === this.Ca.size)
            Yj(this),
            this.Pc(),
            Gl(this.va, this.Ca.keys().next().value),
            this.ve();
        else {
            var c = document.createElement("div")
              , d = document.createElement("div")
              , e = document.createElement("div")
              , f = document.createElement("span")
              , h = document.createElement("img");
            h.src = "https://ssl-ccstatic.highwebmedia.com/theatermodeassets/red-arrow-down.gif";
            h.height = 6;
            h.style.padding = "0 0 2px 4px";
            f.innerText = Nd;
            f.style.padding = "0 5px 0 5px";
            f.style.whiteSpace = "nowrap";
            f.style.fontFamily = "UbuntuRegular, Helvetica, Arial, sans-serif";
            f.style.fontSize = "12px";
            f.style.fontWeight = "bold";
            f.style.color = R.Ba;
            e.style.minWidth = "150px";
            e.appendChild(h);
            e.appendChild(f);
            f = document.createElement("hr");
            f.style.margin = "4px";
            c.appendChild(e);
            c.appendChild(f);
            c.style.border = "1px solid #6B6B6B";
            c.style.fontSize = "12px";
            c.style.position = "absolute";
            c.style.background = "#e1e1e1";
            c.style.borderRadius = "4px";
            c.style.display = "none";
            c.style.padding = "6px 0 6px 0";
            c.style.maxHeight = "300px";
            c.style.overflow = "auto";
            c.style.fontFamily = "UbuntuRegular, Helvetica, Arial, sans-serif";
            var g = function() {
                document.body.removeChild(c);
                document.body.removeChild(d);
                S.removeListener(g)
            };
            q(S, g, !1);
            e = [];
            f = l(this.Ca.keys());
            for (h = f.next(); !h.done; h = f.next())
                e.push(h.value);
            f = e.sort();
            e = {};
            f = l(f);
            for (h = f.next(); !h.done; e = {
                Ed: e.Ed
            },
            h = f.next())
                e.Ed = h.value,
                h = this.Ca.get(e.Ed),
                void 0 === h ? w("can't find pmsession") : c.appendChild(Is({
                    jg: h,
                    Jl: function(a) {
                        return function() {
                            b.nd(a.Ed);
                            g()
                        }
                    }(e),
                    Il: function(a) {
                        return function() {
                            Yj(b);
                            b.Pc();
                            Gl(b.va, a.Ed);
                            b.ve();
                            g()
                        }
                    }(e),
                    size: 250
                }));
            a = a.getBoundingClientRect();
            c.style.top = a.top + window.scrollY + "px";
            c.style.left = a.left + window.scrollX + "px";
            c.style.zIndex = "999";
            document.body.appendChild(c);
            c.style.display = "block";
            d.style.position = "fixed";
            d.style.height = "100%";
            d.style.width = "100%";
            d.style.top = "0";
            d.style.left = "0";
            d.style.zIndex = "998";
            d.onclick = g;
            document.body.insertBefore(d, c);
            window.document.documentElement.clientHeight < c.offsetTop + c.offsetHeight && (c.style.height = window.document.documentElement.clientHeight - a.top - 18 + "px");
            window.document.documentElement.clientWidth < c.offsetLeft + c.offsetWidth + 8 && (c.style.left = window.document.documentElement.clientWidth - c.offsetWidth + window.scrollX - 8 + "px")
        }
    }
    ;
    k.Jg = function() {
        return 0 === this.Ca.size
    }
    ;
    k.Me = function() {
        return "pm-tab-default"
    }
    ;
    k.Zb = function() {
        var a = Y(this);
        void 0 === a ? w("session undefined") : a.w.Zb()
    }
    ;
    k.Od = function() {
        var a = Y(this);
        void 0 === a ? w("session undefined") : a.w.Od()
    }
    ;
    k.Qe = function() {
        var a = Y(this);
        return void 0 === a ? (w("session undefined"),
        !1) : document.activeElement === a.w.K
    }
    ;
    k.C = function() {
        W.prototype.C.call(this);
        var a = Y(this);
        void 0 !== a && a.w.pa()
    }
    ;
    k.Jh = function(a, b) {
        var c = this
          , d = this.Ca.get(a);
        if (void 0 === d) {
            var e = Il();
            e.innerHTML = Nb(a);
            e.style.textAlign = "center";
            e.style.width = "auto";
            e.style.borderBottom = "1px solid #acacac";
            e.style.margin = "0 10px 5px 5px";
            e.style.display = "none";
            e.style.color = "#cfcfcf";
            d = {
                w: new Gs(function(b) {
                    N("SendPrivateMessage", {
                        username: a
                    });
                    c.va.hd(a);
                    c.j.hm(b, a)
                }
                ),
                W: 0,
                hc: a,
                th: e
            };
            this.Ca.set(a, d);
            d.w.ta(xs(Of + " " + a));
            d.w.ta(xs(Pb()));
            d.w.a.style.display = "none";
            this.N(d.w);
            e = !0
        } else
            e = !1,
            d.th.style.display = "";
        !0 === b && u(this.Yh, {
            username: a,
            tj: e
        });
        return d
    }
    ;
    k.ta = function(a, b, c) {
        this.va.hd(c);
        var d = this.Jh(c, b !== this.j.username());
        this.j.username() === b || Zj(this) && this.va.Ma === c ? Gl(this.va, c) : (this.W += 1,
        d.W += 1,
        this.qc());
        return a = d.w.ta(a)
    }
    ;
    k.Pl = function(a) {
        if (Zj(this)) {
            var b = Y(this);
            void 0 !== b && b.w.ta(a)
        }
    }
    ;
    k.nd = function(a) {
        var b = this.Ca.get(a);
        if (void 0 !== b) {
            var c = Zj(this) && this.va.Ma === a;
            this.va.remove(a);
            b.w.zg();
            this.removeChild(b.w);
            this.Ca["delete"](a);
            c && (Nl(this) || this.tl());
            this.W -= b.W;
            this.qc();
            u(Hj, b.hc)
        }
    }
    ;
    k.zf = function() {
        var a = Y(this);
        void 0 === a ? w("cannot close current tab") : this.nd(a.hc)
    }
    ;
    k.ve = function() {
        if (void 0 === this.va.Ma)
            w("no currentpmsession");
        else {
            var a = this.Ca.get(this.va.Ma);
            void 0 === a ? w("no pm session") : (a.w.a.style.display = "block",
            a.w.Y(),
            a.w.K.focus(),
            0 < a.W && (this.W -= a.W,
            a.W = 0,
            this.qc()),
            u(wh, a.hc),
            a.w.ta(a.th))
        }
    }
    ;
    k.Pc = function() {
        var a = Y(this);
        void 0 !== a && (a.w.a.style.display = "none")
    }
    ;
    k.Le = function() {
        var a = Y(this);
        if (void 0 !== a)
            return a.w.K
    }
    ;
    var Ks = new n("userCountUpdate");
    function Ls() {
        function a() {
            void 0 !== b.parent && b.parent.qh(0)
        }
        W.call(this);
        var b = this;
        this.jh = 0;
        this.ti = [];
        this.a.style.overflow = "hidden";
        this.a.style.fontFamily = "Tahoma, Helvetica, Arial, sans-serif";
        this.a.style.boxSizing = "border-box";
        this.a.style.paddingLeft = "5px";
        this.ya = document.createElement("div");
        this.ya.style.width = "100%";
        this.ya.style.overflowX = "hidden";
        this.ya.style.overflowY = "scroll";
        this.ya.style.padding = "6px";
        this.ya.style.boxSizing = "border-box";
        this.a.appendChild(this.ya);
        this.aa = Cs();
        var c = Bs();
        this.K = Ds();
        this.Ia = Fs();
        this.Ia.style.top = "1px";
        this.K.onclick = a;
        this.K.onfocus = a;
        this.jb = Es();
        this.jb.onclick = a;
        c.appendChild(this.K);
        this.aa.appendChild(c);
        this.aa.appendChild(this.Ia);
        this.aa.appendChild(this.jb);
        this.a.appendChild(this.aa);
        this.Af();
        y("click", this.Ia, function() {
            u(Fj, b.Ia)
        });
        var d = ["privatewatching", "groupwatching"];
        q(S, function(a) {
            b.Ee = a.j;
            q(b.Ee.event.kb, function(a) {
                (0 <= d.indexOf(a.Vc) || 0 <= d.indexOf(a.Aa)) && setTimeout(function() {
                    b.refresh(!1)
                }, 2E3)
            });
            q(b.Ee.event.Tg, function(a) {
                b.jh = a;
                u(Ks, b.jh)
            });
            void 0 !== a.h.Gj && b.clear(a.h.Gj)
        });
        y("click", this.Ia, function() {
            u(Fj, b.Ia)
        })
    }
    m(Ls, W);
    k = Ls.prototype;
    k.dg = function() {
        for (; null !== this.ya.firstChild; )
            this.ya.removeChild(this.ya.firstChild)
    }
    ;
    k.Af = function() {
        var a = document.createElement("div");
        a.innerText = le + "...";
        a.style.margin = "8px 0";
        a.style.color = R.Xb;
        this.ya.style.fontSize = "12px";
        this.ya.appendChild(a)
    }
    ;
    k.Tk = function() {
        var a = this
          , b = document.createElement("div");
        b.style.margin = "6px";
        b.style.cssFloat = "right";
        var c = document.createElement("span");
        c.innerText = me;
        c.style.cursor = "pointer";
        c.style.color = "#0a5a83";
        c.onmouseenter = function() {
            c.style.textDecoration = "underline"
        }
        ;
        c.onmouseleave = function() {
            c.style.textDecoration = "none"
        }
        ;
        y("click", c, function(b) {
            a.dg();
            a.Af();
            a.refresh(!1);
            b.preventDefault()
        });
        b.appendChild(c);
        this.ya.appendChild(b)
    }
    ;
    k.clear = function(a) {
        this.jh = a;
        u(Ks, this.jh);
        this.dg();
        this.Af();
        this.C()
    }
    ;
    k.refresh = function(a) {
        var b = this;
        a = void 0 === a ? !0 : a;
        void 0 !== this.Ee && this.Ee.al().then(function(c) {
            b.jh = c.nf;
            u(Ks, c.nf);
            if (!a) {
                b.dg();
                b.Tk();
                b.ti = c.ye;
                for (var d = l(b.ti), e = d.next(); !e.done; e = d.next()) {
                    e = e.value;
                    var f = document.createElement("div");
                    f.style.width = "100%";
                    f.style.margin = "2px 0";
                    f.appendChild(gs(e));
                    b.ya.appendChild(f)
                }
                d = document.createElement("div");
                d.style.color = R.Xb;
                d.innerText = "+" + c.fe + " anonymous user" + (1 === c.fe ? "" : "s");
                d.style.width = "100%";
                d.style.margin = "2px 0";
                d.style.whiteSpace = "nowrap";
                b.ya.appendChild(d);
                b.C()
            }
        })["catch"](function(a) {
            w("Error retrieving user list: " + a)
        })
    }
    ;
    k.C = function() {
        this.aa.style.width = this.a.clientWidth - 10 + "px";
        this.K.style.width = Math.max(0, this.aa.clientWidth - 18 - Math.ceil(this.jb.offsetWidth) - Math.ceil(this.Ia.offsetWidth) - 4) + "px";
        this.ya.style.height = this.a.clientHeight - 28 - 4 + 1 + "px"
    }
    ;
    k.Kf = function() {
        return Id + " (" + this.jh + ")"
    }
    ;
    k.Me = function() {
        return "users-tab-default"
    }
    ;
    k.Zb = function() {
        this.K.focus()
    }
    ;
    k.Od = function() {
        this.K.blur()
    }
    ;
    k.Qe = function() {
        return document.activeElement === this.K
    }
    ;
    k.pa = function() {
        this.ya.scrollTop = this.ya.scrollHeight - this.ya.offsetHeight
    }
    ;
    k.Jf = function() {
        return this.K.value
    }
    ;
    k.kg = function(a) {
        this.K.value = a
    }
    ;
    k.Le = function() {
        return this.K
    }
    ;
    k.bh = function(a, b) {
        W.prototype.bh.call(this, a, b);
        this.refresh(!1)
    }
    ;
    function Ms() {
        function a() {
            pm.Fh();
            void 0 !== b.parent && b.parent.qh(0)
        }
        W.call(this);
        var b = this;
        this.a.style.color = R.Ba;
        this.a.style.fontSize = "12px";
        this.a.style.fontFamily = "Tahoma, Arial, Helvetica, sans-serif";
        this.a.style.boxSizing = "border-box";
        this.aa = Cs();
        this.aa.style.position = "absolute";
        this.aa.style.left = "5px";
        this.aa.style.bottom = "3px";
        var c = Bs();
        this.K = Ds();
        this.Ia = Fs();
        this.Ia.style.top = "1px";
        this.K.onclick = a;
        this.K.onfocus = a;
        this.jb = Es();
        this.jb.onclick = a;
        c.appendChild(this.K);
        this.aa.appendChild(c);
        this.aa.appendChild(this.Ia);
        this.aa.appendChild(this.jb);
        this.a.appendChild(this.aa);
        var d = document.createElement("div");
        d.innerText = Dd;
        d.style.position = "absolute";
        d.style.top = "30px";
        d.style.right = "10px";
        d.style.fontFamily = "UbuntuRegular, Helvetica, Arial, sans-serif";
        d.style.color = "#000000";
        d.style.backgroundColor = "#d8deea";
        d.style.border = "1px solid #acacac";
        d.style.borderRadius = "4px";
        d.style.padding = "8px 12px";
        d.style.opacity = "0";
        this.a.appendChild(d);
        y("click", this.Ia, function() {
            u(Fj, b.Ia)
        });
        q(Nk, function(a) {
            b.S = a.Ya;
            "default" === b.S && (Ea(b, pm),
            pm.a.style.margin = "0",
            pm.Fc.style.color = "#0a5a83")
        });
        var e;
        q(pm.Rk, function() {
            clearTimeout(e);
            xh(d, "250ms");
            d.style.opacity = "1";
            e = setTimeout(function() {
                xh(d, "1000ms");
                d.style.opacity = "0"
            }, 3E3)
        })
    }
    m(Ms, W);
    k = Ms.prototype;
    k.C = function() {
        "default" === this.S && (this.aa.style.width = this.a.clientWidth - 10 + "px",
        this.K.style.width = Math.max(0, this.aa.clientWidth - 18 - Math.ceil(this.jb.offsetWidth) - Math.ceil(this.Ia.offsetWidth) - 4) + "px",
        pm.a.style.height = this.a.clientHeight - 28 - 4 + 1 + "px")
    }
    ;
    k.Kf = function() {
        return "&nbsp;"
    }
    ;
    k.fl = function() {
        return "url(https://ssl-ccstatic.highwebmedia.com/theatermodeassets/ico-preferences.png)"
    }
    ;
    k.Me = function() {
        return "settings-tab-default"
    }
    ;
    k.Zb = function() {
        this.K.focus()
    }
    ;
    k.Od = function() {
        this.K.blur()
    }
    ;
    k.Qe = function() {
        return document.activeElement === this.K
    }
    ;
    k.pa = function() {
        pm.a.scrollTop = pm.a.scrollHeight - pm.a.offsetHeight
    }
    ;
    k.Jf = function() {
        return this.K.value
    }
    ;
    k.kg = function(a) {
        this.K.value = a
    }
    ;
    k.Le = function() {
        return this.K
    }
    ;
    function Ns() {
        Sj.call(this);
        var a = this;
        this.Vn = "#dc5500";
        this.xc.style.padding = "1px 3px 0 3px";
        this.xc.style.backgroundColor = "#7f7f7f";
        this.xc.style.boxSizing = "border-box";
        this.xc.style.borderBottom = "";
        this.window.style.width = "";
        this.window.style.position = "";
        this.a.style.display = "inline-block";
        this.a.style.fontFamily = "UbuntuMedium, Helvetica, Arial, sans-serif";
        this.a.style.backgroundColor = "#ffffff";
        this.a.style.borderRadius = "4px 4px 0 0";
        this.a.style.border = "1px solid #acacac";
        this.a.style.margin = "0 0 6px 8px";
        this.a.style.boxSizing = "border-box";
        this.a.style.overflow = "";
        this.ca = Tj(this, new Hs);
        this.Up = Tj(this, new Ls);
        this.wa = Tj(this, new Js(function() {
            Xj(a, a.ca)
        }
        ));
        Tj(this, new Ms);
        Xj(this, this.ca);
        q(S, function(b) {
            q(b.j.event.hg, function(b) {
                Cl(a.ca);
                a.ca.w.ta(qs(b))
            });
            q(b.j.event.P, function(b) {
                a.ca.w.ta(ss(b));
                b.V && a.wa.Pl(ss(b))
            });
            q(b.j.event.ag, function(b) {
                a.wa.ta(qs(b), b.fb.username, b.Ll)
            });
            q(b.j.event.Rg, function(b) {
                a.ca.dj(b.username);
                a.ca.w.Yl(b.username);
                a.wa.nd(b.username)
            });
            q(b.j.event.kb, function(b) {
                "notconnected" === b.Vc && a.Ie === a.Up && a.Up.refresh(!1)
            })
        });
        q(Ks, function() {
            a.qc()
        });
        var b = new rm;
        q(this.wa.Yh, function(c) {
            c.tj && sm(b, c.username) && a.ca.w.ta(ss({
                U: [[{
                    cc: 1,
                    message: "New private conversation from " + c.username + " "
                }, {
                    cc: 1,
                    message: "(Press TAB to cycle through your conversations)",
                    oa: "#cd0000",
                    weight: "700"
                }]],
                V: !1
            }))
        });
        q(Gj, function() {
            setTimeout(function() {
                a.Y()
            }, 0)
        })
    }
    m(Ns, Sj);
    k = Ns.prototype;
    k.C = function() {
        var a = this;
        Sj.prototype.C.call(this);
        var b = this.a.clientHeight - 28 - this.xc.offsetHeight - 8;
        this.ca.w.Oa.style.height = b + "px";
        setTimeout(function() {
            var c = Y(a.wa);
            void 0 !== c && (c.w.Oa.style.height = b + "px")
        }, 0)
    }
    ;
    k.Ek = function(a) {
        a = Sj.prototype.Ek.call(this, a);
        a.style.position = "";
        a.style.lineHeight = "1.2";
        a.style.height = "13px";
        a.style.borderRadius = "4px 4px 0 0";
        a.style.textDecoration = "none";
        a.style.fontSize = "11px";
        a.style.marginRight = "2px";
        a.style.padding = "3px 8px";
        a.style.backgroundRepeat = "no-repeat";
        a.style.backgroundPosition = "center";
        a.style.minWidth = "16px";
        return a
    }
    ;
    k.qh = function(a) {
        var b = "";
        void 0 !== this.Ie && (b = this.Ie.Jf());
        a >= this.children().length ? w("invalid tab index") : (this.Ie = this.children()[a],
        this.Ie.kg(b),
        Wj(this))
    }
    ;
    k.$a = function() {
        return Sj.prototype.$a.call(this)
    }
    ;
    k.children = function() {
        return Sj.prototype.children.call(this)
    }
    ;
    k.Xk = function() {
        pm.Fh();
        if (Sj.prototype.$a.call(this) === this.ca) {
            if (!this.wa.Jg()) {
                Xj(this, this.wa);
                var a = this.wa;
                a.Pc();
                Fl(a.va);
                Nl(this.wa) || w("no pmSessions")
            }
        } else
            0 < this.ca.W ? Xj(this, this.ca) : Nl(this.wa) || Xj(this, this.ca);
        this.Y()
    }
    ;
    k.zf = function() {
        Sj.prototype.$a.call(this) === this.wa && this.wa.zf()
    }
    ;
    function Os() {
        v.call(this);
        var a = this;
        this.zj = [];
        this.a.style.position = "static";
        this.a.style.overflow = "";
        this.a.style.color = R.Zk;
        q(S, function(b) {
            b = l(b.h.yh);
            for (var c = b.next(); !c.done; c = b.next())
                c = c.value,
                0 > a.zj.indexOf(c.id) && "1" !== gb("dism_msg" + c.id) && (a.zj.push(c.id),
                a.$l(c))
        })
    }
    m(Os, v);
    Os.prototype.$l = function(a) {
        var b = this
          , c = document.createElement("div");
        c.innerHTML = a.Rq;
        c.style.backgroundColor = "#cccccc";
        c.style.fontSize = "12px";
        c.style.padding = "3px";
        c.style.margin = "5px 0";
        c.style.borderRadius = "2px";
        c.style.lineHeight = "16px";
        var d = document.createElement("a");
        d.style.color = R.lc;
        d.textContent = pf;
        d.style.cursor = "pointer";
        var e = document.createElement("span");
        e.textContent = " (";
        var f = document.createElement("span");
        f.textContent = ")";
        c.appendChild(e);
        c.appendChild(d);
        c.appendChild(f);
        this.a.appendChild(c);
        e = {};
        f = l(c.getElementsByTagName("a"));
        for (var h = f.next(); !h.done; e = {
            link: e.link
        },
        h = f.next())
            e.link = h.value,
            e.link.style.color = R.lc,
            e.link.onmouseenter = function(a) {
                return function() {
                    a.link.style.textDecoration = "underline"
                }
            }(e),
            e.link.onmouseleave = function(a) {
                return function() {
                    a.link.style.textDecoration = ""
                }
            }(e);
        y("click", d, function() {
            b.a.removeChild(c);
            b.zj.splice(b.zj.indexOf(a.id), 1);
            hb("dism_msg" + a.id, "1", 60)
        })
    }
    ;
    var Ps = window.$;
    function Qs(a, b) {
        v.call(this);
        var c = this;
        this.i = a;
        this.rn = this.pm = !1;
        this.a.style.position = "static";
        this.a.style.height = "auto";
        var d = document.createElement("div");
        if (void 0 !== b.kn) {
            var e = document.createElement("div");
            e.id = "deletion_admin_notice";
            e.style.margin = "10px";
            e.innerHTML = b.kn;
            d.appendChild(e)
        }
        void 0 !== b.Qo && (e = document.createElement("div"),
        e.style.color = "#503d08",
        Ka() ? e.style.background = "rgba(247, 255, 0, 0.23)" : e.style.background = "rgb(247, 255, 0)",
        e.style.padding = "5px",
        e.style.width = "500px",
        e.textContent = "Previously known as " + b.Qo,
        d.appendChild(e));
        this.a.appendChild(d);
        d = document.createElement("div");
        this.a.appendChild(d);
        e = document.createElement("a");
        e.href = "#";
        e.style.color = "#0a5a83";
        e.style.marginRight = "15px";
        e.textContent = "Show/Hide Admin Info";
        e.onclick = function(a) {
            a.preventDefault();
            Rs(c)
        }
        ;
        d.appendChild(e);
        this.uf = document.createElement("div");
        this.uf.id = "admin-info-section";
        this.uf.style.display = "none";
        this.uf.textContent = "Loading admin information...";
        this.a.appendChild(this.uf);
        for (e = 0; e < b.sm.length; e += 1) {
            var f = Ss(this, b.sm[e]);
            d.appendChild(f);
            e !== b.sm.length - 1 && (f = document.createElement("span"),
            f.style.margin = "0 2px",
            f.textContent = "|",
            d.appendChild(f))
        }
        b.hq && Rs(this)
    }
    m(Qs, v);
    function Ss(a, b) {
        var c = document.createElement("a");
        c.href = b.href;
        c.textContent = b.text;
        c.style.color = "#0a5a83";
        "View Stream Status" === b.text && (c.onclick = function(b) {
            b.preventDefault();
            b = window.open("/view_user_obs_overlay/" + a.i + "/?graph=1", "obs_info", "status=0,toolbar=0,menubar=0,directories=0,resizable=1,scrollbars=1,height=600,width=750");
            null !== b && b.focus()
        }
        );
        return c
    }
    function Rs(a) {
        a.pm ? (a.uf.style.display = "none",
        a.pm = !1) : (a.rn || (Ts(a),
        a.rn = !0),
        a.uf.style.display = "block",
        a.pm = !0)
    }
    function Ts(a) {
        z("ax/room_info/" + a.i + "/").then(function(b) {
            Ps(a.uf).html(b.responseText)
        })
    }
    ;function Us(a, b, c) {
        function d() {
            e.Eb.a.style.visibility = "visible";
            var a = l(Ln(window.innerWidth, window.innerHeight, e.u.Ec))
              , b = a.next().value;
            a = a.next().value;
            e.u.a.style.height = "100%";
            e.u.a.style.top = "0";
            e.u.a.style.left = "0";
            e.u.M.a.style.top = "0";
            e.u.M.a.style.left = "0";
            e.u.M.a.style.width = b + "px";
            e.u.M.a.style.height = a + "px";
            e.J.a.style.visibility = "visible";
            e.J.a.style.position = "absolute";
            e.J.a.style.top = "0px";
            e.J.a.style.left = "0";
            e.J.a.style.height = a - 32 + "px";
            e.ia.a.style.display = "none";
            e.fg.a.style.display = "none";
            e.u.R.a.style.display = "block";
            e.Y()
        }
        v.call(this);
        var e = this;
        this.u = a;
        this.J = b;
        this.Eb = c;
        this.gg = 4;
        x() && (a = window.localStorage.getItem("isTheaterMode"),
        null !== a && JSON.parse(a).isTheaterMode && setTimeout(function() {
            u(Nk, {
                Yf: "default",
                Ya: "theater"
            });
            g.innerText = "default" === e.S ? "Theater Mode" : "Default Mode"
        }, 0));
        this.a.style.position = "static";
        this.a.style.display = "block";
        this.a.style.height = "auto";
        this.a.style.backgroundColor = "#e0e0e0";
        this.a.style.border = "1px solid #acacac";
        this.a.style.borderRadius = "4px";
        this.a.style.padding = "4px";
        this.a.style.margin = "8px 0 17px 32px";
        this.a.style.overflow = "visible";
        this.a.style.boxSizing = "border-box";
        this.vd = document.createElement("div");
        this.vd.innerText = Vf;
        this.vd.style.display = "none";
        this.vd.style.color = "#0b5d81";
        this.vd.style.fontFamily = "UbuntuRegular, Arial, Helvetica, sans-serif";
        this.vd.style.fontSize = "12px";
        this.vd.style.fontWeight = "bold";
        this.vd.style.margin = "13px 0";
        this.a.appendChild(this.vd);
        this.Ab = document.createElement("div");
        this.Ab.style.width = "100%";
        this.Ab.style.height = "auto";
        this.Ab.style.backgroundColor = "#e0e0e0";
        this.a.appendChild(this.Ab);
        this.Pa = this.N(new $r(this.u,this.J), this.Ab);
        this.fg = this.N(new Ur(this), this.Ab);
        this.ia = this.N(new Ns, this.Ab);
        var f;
        q(Fj, function(a) {
            M ? u(sh, void 0) : "default" === e.S && (void 0 === f && (f = e.N(new zm(e.ia), e.ia.a)),
            f.show(a))
        });
        a = document.createElement("div");
        a.style.backgroundColor = "#cccccc";
        a.style.fontSize = "12px";
        a.style.padding = "3px";
        a.style.margin = "5px 0";
        a.style.borderRadius = "2px";
        a.style.color = R.Ba;
        a.style.lineHeight = "16px";
        b = document.createElement("span");
        var h = document.createElement("span");
        h.innerText = "Check out the new ";
        b.appendChild(h);
        var g = document.createElement("a");
        g.href = "#";
        g.innerText = "Theater Mode";
        g.id = "video-mode";
        g.style.color = "#0a5a83";
        g.onclick = function(a) {
            a.preventDefault();
            a = "default";
            "default" === e.S && (a = "theater");
            u(Nk, {
                Yf: e.S,
                Ya: a
            });
            N("ChangeVideoMode", {
                videoMode: a
            })
        }
        ;
        b.appendChild(g);
        c = document.createElement("span");
        c.innerText = " player";
        b.appendChild(c);
        a.appendChild(b);
        b = document.createElement("span");
        b.style.display = "none";
        c = document.createElement("span");
        c.innerText = "|";
        c.style.color = R.Ba;
        c.style.margin = "0 4px";
        b.appendChild(c);
        var p = document.createElement("a");
        p.href = "#";
        p.innerText = "Interactive Full Screen";
        p.style.color = "#0a5a83";
        p.id = "fvm-link";
        p.onclick = function(a) {
            a.preventDefault();
            a = e.u.R;
            "fullscreen" === a.S ? (Ih(),
            a.S = a.vi ? "theater" : "default") : Hh(a.Gn);
            N("ChangeVideoMode", {
                videoMode: "fullscreen"
            })
        }
        ;
        b.appendChild(p);
        a.appendChild(b);
        !Jh() || (/iPad/i.test(navigator.userAgent) || "MacIntel" === navigator.platform && 1 < navigator.maxTouchPoints) && void 0 === window.MSStream || (b.style.display = "inline");
        this.Dl = a;
        this.a.appendChild(a);
        var t = document.createElement("div");
        t.style.backgroundColor = "#cccccc";
        t.style.fontSize = "12px";
        t.style.padding = "3px";
        t.style.margin = "5px 0";
        t.style.borderRadius = "2px";
        t.style.color = R.Ba;
        t.style.lineHeight = "16px";
        t.style.display = "none";
        this.a.appendChild(t);
        this.wc = document.createElement("div");
        this.wc.style.backgroundColor = "#cccccc";
        this.wc.style.fontSize = "12px";
        this.wc.style.padding = "3px";
        this.wc.style.margin = "5px 0";
        this.wc.style.borderRadius = "2px";
        this.wc.style.color = R.Ba;
        this.wc.style.lineHeight = "16px";
        a = document.createElement("span");
        a.innerText = R.oe ? Xf : Wf;
        this.wc.appendChild(a);
        this.a.appendChild(this.wc);
        var C = document.createElement("a");
        C.innerText = Yf;
        C.href = "https://www.surveymonkey.com/r/6NS9RJ5";
        C.target = "_blank";
        C.style.color = "#0a5a83";
        C.onmouseenter = function() {
            C.style.textDecoration = "underline"
        }
        ;
        C.onmouseleave = function() {
            C.style.textDecoration = "none"
        }
        ;
        this.wc.appendChild(C);
        var B = document.createElement("a");
        B.href = "#";
        B.innerText = Zf;
        B.style.color = "#0a5a83";
        B.style.cssFloat = "right";
        B.onclick = function(a) {
            a.preventDefault();
            A("toggle_theatermode/", {}).then(function(a) {
                a = new D(a.responseText);
                var b = J(a, "success");
                L(a);
                b ? (N("ChangeCBVersion", {
                    new_cb_version: 1
                }),
                window.location.reload()) : (U("An error occurred. Please try again."),
                w("Error switching to CB1"))
            })
        }
        ;
        B.onmouseenter = function() {
            B.style.textDecoration = "underline"
        }
        ;
        B.onmouseleave = function() {
            B.style.textDecoration = "none"
        }
        ;
        this.wc.appendChild(B);
        this.yh = this.N(new Os);
        var M = !0, F;
        q(S, function(a) {
            M = a.j.ob;
            B.href = "/" + a.h.i + "/";
            p.href = "/fullvideo/?b=" + a.h.i;
            void 0 !== F && (e.removeChild(F),
            F = void 0);
            void 0 !== a.h.dk ? (t.style.display = "block",
            F = e.N(new Qs(a.h.i,a.h.dk), t)) : t.style.display = "none"
        });
        this.hr = this.N(new Nr);
        var O = !0;
        q(Nk, function(a) {
            e.S = a.Ya;
            void 0 !== f && "default" !== e.S && f.A();
            if (O || e.S !== a.Yf)
                switch (a.Ya) {
                case "default":
                    e.a.style.padding = e.gg + "px";
                    e.a.style.marginTop = "8px";
                    e.a.style.marginLeft = "32px";
                    e.u.M.a.style.top = "0";
                    e.J.a.style.top = "0";
                    e.J.a.style.visibility = "hidden";
                    e.J.a.style.position = "fixed";
                    e.Eb.a.style.visibility = "hidden";
                    e.ia.a.style.display = "inline-block";
                    e.fg.a.style.display = "inline-block";
                    e.u.M instanceof pn || (e.u.R.a.style.display = "none");
                    e.Y();
                    Vs(!1);
                    h.innerText = "Check out the new ";
                    g.innerText = "Theater Mode";
                    break;
                case "theater":
                    e.a.style.marginTop = "5px";
                    e.a.style.marginLeft = "8px";
                    e.u.M.a.style.top = "0";
                    e.J.a.style.top = "0";
                    e.J.a.style.visibility = "visible";
                    e.J.a.style.position = "absolute";
                    e.Eb.a.style.visibility = "visible";
                    e.Eb.pa();
                    e.ia.a.style.display = "none";
                    e.fg.a.style.display = "none";
                    e.u.R.a.style.display = "block";
                    Wr(e.fg, e.a.clientWidth);
                    e.Y();
                    Vs(!0);
                    h.innerText = "Return to the ";
                    g.innerText = "Default Mode";
                    break;
                case "fullscreen":
                    d();
                    setTimeout(function() {
                        "fullscreen" === e.S && d()
                    }, 500);
                    break;
                default:
                    w("Unexpected VideoMode: " + a.Ya)
                }
            O = !1;
            setTimeout(function() {
                e.Y()
            }, 300)
        })
    }
    m(Us, v);
    Us.prototype.C = function() {
        var a = this.Pa.a.offsetHeight;
        "theater" === this.S && (a = Ws(this.u)[1],
        this.J.a.style.height = a - 32 + "px");
        this.ia.a.style.width = this.a.clientWidth - this.Pa.a.offsetWidth - 16 + "px";
        this.ia.a.style.height = a + "px";
        this.fg.a.style.height = a + "px";
        Wr(this.fg)
    }
    ;
    function Vs(a) {
        x() && window.localStorage.setItem("isTheaterMode", JSON.stringify({
            isTheaterMode: a
        }))
    }
    ;function Xs(a) {
        function b(b) {
            var c = document.createElement("div")
              , d = a.tf[b];
            d.a.style.width = "160px";
            d.a.style.height = "600px";
            d.a.style.margin = "10px 13px 6px 13px";
            c.appendChild(d.a);
            var e = document.createElement("a");
            e.href = "https://exoticads.com/aff/in/mrps/agBmX/?track=" + Dq(b);
            e.innerHTML = qe;
            e.style.display = "block";
            e.style.color = "#d90c5f";
            e.style.fontSize = "12px";
            e.style.textAlign = "center";
            e.style.textDecoration = "none";
            e.onmouseenter = function() {
                e.style.textDecoration = "underline"
            }
            ;
            e.onmouseleave = function() {
                e.style.textDecoration = "none"
            }
            ;
            c.appendChild(e);
            return c
        }
        v.call(this);
        var c = this;
        this.a.style.position = "static";
        this.a.style.width = "186px";
        this.a.style.height = "auto";
        this.a.style.backgroundColor = "#e0e0e0";
        this.a.style.border = "1px solid #acacac";
        this.a.style.borderRadius = "4px";
        this.a.style.paddingBottom = "16px";
        this.a.style.margin = "8px 20px 16px 0";
        this.a.style.display = "none";
        this.a.className = "ad";
        var d = document.createElement("a");
        d.href = "/supporter/upgrade/";
        d.style.display = "block";
        d.style.backgroundColor = "#fc760c";
        d.style.padding = "2px 4px";
        var e = document.createElement("img");
        e.src = "https://ssl-ccstatic.highwebmedia.com/theatermodeassets/close_icon.png";
        d.appendChild(e);
        var f = document.createElement("span");
        f.innerText = pe;
        f.style.color = "#ffffff";
        f.style.fontSize = "14px";
        f.style.fontWeight = "bold";
        f.style.textDecoration = "none";
        f.style.marginLeft = "8px";
        f.onmouseenter = function() {
            f.style.textDecoration = "underline"
        }
        ;
        f.onmouseleave = function() {
            f.style.textDecoration = "none"
        }
        ;
        d.appendChild(f);
        this.a.appendChild(d);
        this.a.appendChild(b("rightTop"));
        this.a.appendChild(b("rightMiddle"));
        this.a.appendChild(b("rightBottom"));
        q(Nk, function(a) {
            c.a.style.marginTop = "default" === a.Ya ? "8px" : "5px"
        })
    }
    m(Xs, v);
    function Ys(a) {
        function b(b) {
            var c = document.createElement("div");
            c.style.display = "inline-block";
            c.style.maxWidth = "33%";
            c.style.width = "33%";
            var d = a.tf[b];
            d.a.style.width = "100%";
            d.a.style.maxWidth = "300px";
            d.a.style.height = "250px";
            d.a.style.margin = "2px 3px 4px 3px";
            c.appendChild(d.a);
            var e = document.createElement("a");
            e.href = "https://exoticads.com/aff/in/mrps/agBmX/?track=" + Dq(b);
            e.innerHTML = qe;
            e.style.display = "block";
            e.style.color = "#d90c5f";
            e.style.fontSize = "12px";
            e.style.textAlign = "center";
            e.style.textDecoration = "none";
            e.onmouseenter = function() {
                e.style.textDecoration = "underline"
            }
            ;
            e.onmouseleave = function() {
                e.style.textDecoration = "none"
            }
            ;
            c.appendChild(e);
            return c
        }
        v.call(this);
        this.a.style.position = "relative";
        this.a.style.height = "auto";
        this.a.style.padding = "32px";
        this.a.style.textAlign = "center";
        this.a.style.display = "none";
        this.a.style.width = "auto";
        this.a.className = "ad";
        this.ya = document.createElement("div");
        this.ya.style.display = "inline-block";
        this.ya.style.width = "100%";
        this.ya.style.maxWidth = "918px";
        this.ya.style.borderRadius = "4px";
        this.ya.style.backgroundColor = "#ffffff";
        var c = document.createElement("a");
        c.href = "/supporter/upgrade/";
        c.style.display = "block";
        c.style.backgroundColor = "#fc760c";
        c.style.padding = "2px 4px";
        c.style.textAlign = "left";
        var d = document.createElement("img");
        d.src = "https://ssl-ccstatic.highwebmedia.com/theatermodeassets/close_icon.png";
        c.appendChild(d);
        var e = document.createElement("span");
        e.innerText = pe;
        e.style.color = "#ffffff";
        e.style.fontSize = "14px";
        e.style.fontWeight = "bold";
        e.style.textDecoration = "none";
        e.style.marginLeft = "8px";
        e.onmouseenter = function() {
            e.style.textDecoration = "underline"
        }
        ;
        e.onmouseleave = function() {
            e.style.textDecoration = "none"
        }
        ;
        c.appendChild(e);
        this.ya.appendChild(c);
        this.ya.appendChild(b("footerLeft"));
        this.ya.appendChild(b("footerMiddle"));
        this.ya.appendChild(b("footerRight"));
        this.a.appendChild(this.ya)
    }
    m(Ys, v);
    function Zs() {
        v.call(this);
        this.a.style.position = "static";
        this.a.style.height = "100%";
        this.a.style.background = R.yn;
        this.a.style.backgroundColor = R.bgColor;
        this.a.style.textAlign = "center";
        this.a.style.paddingTop = "45px";
        this.a.style.boxSizing = "border-box";
        this.a.style.minWidth = "1040px";
        R.rp && (this.a.style.borderTop = "3px solid " + R.Fl);
        var a = document.createElement("div");
        a.style.width = "100%";
        a.style.height = "180px";
        a.style.margin = "0 auto";
        a.style.lineHeight = "14px";
        this.a.appendChild(a);
        var b = $s(xe, [{
            innerText: ye,
            href: "/"
        }, {
            innerText: ze,
            href: "/female-cams/"
        }, {
            innerText: Ae,
            href: "/male-cams/"
        }, {
            innerText: Be,
            href: "/couple-cams/"
        }, {
            innerText: Ce,
            href: "/trans-cams/"
        }]);
        b.style.paddingLeft = "55px";
        a.appendChild(b);
        a.appendChild($s(De, [{
            innerText: Ee,
            href: "/teen-cams/"
        }, {
            innerText: Fe,
            href: "/18to21-cams/"
        }, {
            innerText: Ge,
            href: "/20to30-cams/"
        }, {
            innerText: He,
            href: "/30to50-cams/"
        }, {
            innerText: Ie,
            href: "/mature-cams/"
        }]));
        a.appendChild($s(Je, [{
            innerText: Ke,
            href: "/north-american-cams/"
        }, {
            innerText: Le,
            href: "/other-region-cams/"
        }, {
            innerText: Me,
            href: "/euro-russian-cams/"
        }, {
            innerText: Ne,
            href: "/asian-cams/"
        }, {
            innerText: Oe,
            href: "/south-american-cams/"
        }]));
        a.appendChild($s(Pe, [{
            innerText: Qe,
            href: "/exhibitionist-cams/"
        }, {
            innerText: Re,
            href: "/hd-cams/"
        }, {
            innerText: Se,
            href: "/spy-on-cams/"
        }, {
            innerText: Yc,
            href: "/spy-on-cams/"
        }, {
            innerText: Te,
            href: "/new-cams/"
        }]));
        var c = document.createElement("div");
        c.style.display = "inline-block";
        a.appendChild(c);
        a = document.createElement("div");
        a.style.height = "42px";
        this.a.appendChild(a);
        "" !== R.dl && (a = document.createElement("div"),
        a.style.fontSize = "10px",
        a.innerHTML = R.dl,
        this.a.appendChild(a));
        var d = document.createElement("div");
        this.a.appendChild(d);
        q(S, function(a) {
            for (; null !== c.firstChild; )
                c.removeChild(c.firstChild);
            for (c.appendChild($s(Se, [{
                innerText: Ue,
                href: "/6-tokens-per-minute-private-cams/" + a.h.Ad + "/"
            }, {
                innerText: Ve,
                href: "/12-tokens-per-minute-private-cams/" + a.h.Ad + "/"
            }, {
                innerText: We,
                href: "/18-tokens-per-minute-private-cams/" + a.h.Ad + "/"
            }, {
                innerText: Xe,
                href: "/30-tokens-per-minute-private-cams/" + a.h.Ad + "/"
            }, {
                innerText: Ye,
                href: "/60-tokens-per-minute-private-cams/" + a.h.Ad + "/"
            }, {
                innerText: Ze,
                href: "/90-tokens-per-minute-private-cams/" + a.h.Ad + "/"
            }])); null !== d.firstChild; )
                d.removeChild(d.firstChild);
            d.appendChild(at([{
                innerText: $e,
                href: "/terms/"
            }, {
                innerText: af,
                href: "/privacy/"
            }, {
                innerText: bf,
                href: "http://support.chaturbate.com/",
                ff: R.oe
            }, {
                innerText: bf,
                href: "mailto:support@cb-wl.com?subject=" + R.wi + "\u00a0Support\u00a0Request\u00a0-\u00a0Username:\u00a0" + a.h.userName,
                ff: !R.oe
            }, {
                innerText: cf,
                href: "https://www.surveymonkey.com/r/5XLZ85R"
            }, {
                innerText: df,
                href: "/security/"
            }, {
                innerText: ef,
                href: "/law_enforcement/"
            }, {
                innerText: ff,
                href: "/billingsupport/"
            }, {
                innerText: gf,
                href: "/accounts/disable/"
            }, {
                innerText: hf,
                href: "/apps/"
            }, {
                innerText: jf,
                href: "/contest/details/"
            }, {
                innerText: lf,
                href: "http://chaturbate.com/in/?tour=07kX&campaign=" + R.Yk + "&track=default",
                ff: !R.oe
            }, {
                innerText: kf,
                href: "/affiliates/",
                ff: R.oe || a.h.Iq
            }, {
                innerText: mf,
                href: "https://exoticads.com/",
                ff: R.oe
            }, {
                innerText: nf,
                href: "/jobs/",
                ff: R.oe
            }, {
                innerText: of,
                href: "/sitemap/",
                ff: R.oe
            }]));
            f.innerText = bt(a.h.sr)
        });
        R.sp && this.a.appendChild(ct());
        var e = (new tq).anchor;
        e.innerText = "18 U.S.C. 2257 Record Keeping Requirements Compliance Statement";
        e.href = "/2257/";
        e.style.display = "block";
        e.style.marginTop = "30px";
        e.style.fontSize = "10px";
        e.style.color = R.cl;
        e.onmouseenter = function() {
            e.style.textDecoration = "underline"
        }
        ;
        e.onmouseleave = function() {
            e.style.textDecoration = "none"
        }
        ;
        this.a.appendChild(e);
        R.tp ? (a = document.createElement("img"),
        a.src = "https://ssl-ccstatic.highwebmedia.com/theatermodeassets/footer_address.png",
        a.style.width = "589px",
        a.style.padding = "8px 0 4px 0") : (a = document.createElement("div"),
        a.style.height = "9px");
        this.a.appendChild(a);
        var f = document.createElement("div");
        f.innerText = bt("");
        f.style.color = R.fn;
        f.style.fontSize = "10px";
        f.style.marginBottom = "15px";
        this.a.appendChild(f);
        a = document.createElement("img");
        a.src = "https://ssl-ccstatic.highwebmedia.com/theatermodeassets/footer_safelabeling.gif";
        this.a.appendChild(a);
        a = (new tq).anchor;
        a.href = "http://www.rtalabel.org/";
        b = document.createElement("img");
        b.src = "https://ssl-ccstatic.highwebmedia.com/theatermodeassets/footer_rta.gif";
        b.style.margin = "0 12px";
        a.appendChild(b);
        this.a.appendChild(a);
        a = (new tq).anchor;
        a.href = "https://www.asacp.org/ASACP.php/";
        b = document.createElement("img");
        b.src = "https://ssl-ccstatic.highwebmedia.com/theatermodeassets/footer_asacp.gif";
        a.appendChild(b);
        this.a.appendChild(a);
        R.qp && (a = document.createElement("div"),
        a.style.fontSize = "10px",
        a.style.margin = "7px 0px 23px 0px",
        b = document.createElement("span"),
        b.innerText = "Whitelabel powered by ",
        b.style.color = R.Bn,
        a.appendChild(b),
        b = document.createElement("a"),
        b.innerText = "Chaturbate.com",
        b.href = "http://chaturbate.com/affiliates/in/?tour=grq0&campaign=" + R.Yk + "&track=default",
        b.style.color = R.cl,
        a.appendChild(b),
        this.a.appendChild(a))
    }
    m(Zs, v);
    function $s(a, b) {
        var c = document.createElement("div");
        c.style.display = "inline-block";
        c.style.marginRight = "15px";
        c.style.textAlign = "left";
        c.style.fontSize = "12.001199722290039px";
        c.style.verticalAlign = "top";
        c.style.width = "182px";
        var d = document.createElement("div");
        d.innerText = a;
        d.style.color = R.xn;
        d.style.fontWeight = "bold";
        d.style.marginBottom = "12px";
        c.appendChild(d);
        d = {};
        for (var e = l(b), f = e.next(); !f.done; d = {
            anchor: d.anchor
        },
        f = e.next())
            f = f.value,
            d.anchor = (new tq).anchor,
            d.anchor.innerText = f.innerText,
            d.anchor.href = f.href,
            d.anchor.style.display = "block",
            d.anchor.style.color = R.zn,
            d.anchor.onmouseenter = function(a) {
                return function() {
                    a.anchor.style.textDecoration = "underline"
                }
            }(d),
            d.anchor.onmouseleave = function(a) {
                return function() {
                    a.anchor.style.textDecoration = "none"
                }
            }(d),
            c.appendChild(d.anchor);
        return c
    }
    function at(a) {
        var b = document.createElement("div");
        b.style.margin = "0 0 0 1.5px";
        var c = document.createElement("a")
          , d = {};
        a = l(a);
        for (var e = a.next(); !e.done; d = {
            anchor: d.anchor
        },
        e = a.next())
            e = e.value,
            d.anchor = (new tq).anchor,
            d.anchor.innerText = e.innerText,
            d.anchor.href = e.href,
            d.anchor.style.color = R.An,
            d.anchor.style.display = "inline-block",
            d.anchor.style.fontSize = "13px",
            d.anchor.style.fontFamily = "UbuntuMedium, Helvetica, Arial, sans-serif",
            d.anchor.style.lineHeight = "11px",
            d.anchor.style.padding = "0 12px 0 11px",
            d.anchor.style.borderRight = "1.5px solid #6b6b6b",
            d.anchor.onmouseenter = function(a) {
                return function() {
                    a.anchor.style.textDecoration = "underline"
                }
            }(d),
            d.anchor.onmouseleave = function(a) {
                return function() {
                    a.anchor.style.textDecoration = "none"
                }
            }(d),
            void 0 !== e.ff && e.ff ? d.anchor.style.display = "none" : c = d.anchor,
            b.appendChild(d.anchor);
        c.style.borderRight = "";
        return b
    }
    function ct() {
        var a = document.createElement("div");
        a.style.marginTop = "12px";
        for (var b = document.createElement("a"), c = {}, d = l([{
            innerText: "\u0627\u0644\u0639\u0631\u0628\u064a\u0629",
            href: "https://ar.chaturbate.com/"
        }, {
            innerText: "Deutsch",
            href: "https://de.chaturbate.com/"
        }, {
            innerText: "English",
            href: "https://en.chaturbate.com/"
        }, {
            innerText: "\u0395\u03bb\u03bb\u03b7\u03bd\u03b9\u03ba\u03ac",
            href: "https://el.chaturbate.com/"
        }, {
            innerText: "Espa\u00f1ol",
            href: "https://es.chaturbate.com/"
        }, {
            innerText: "Fran\u00e7ais",
            href: "https://fr.chaturbate.com/"
        }, {
            innerText: "\u0939\u093f\u0928\u094d\u0926\u0940",
            href: "https://hi.chaturbate.com/"
        }, {
            innerText: "Italiano",
            href: "https://it.chaturbate.com/"
        }, {
            innerText: "\u65e5\u672c\u8a9e",
            href: "https://ja.chaturbate.com/"
        }, {
            innerText: "\ud55c\uad6d\uc5b4",
            href: "https://ko.chaturbate.com/"
        }, {
            innerText: "Nederlands",
            href: "https://nl.chaturbate.com/"
        }, {
            innerText: "Portugu\u00eas",
            href: "https://pt.chaturbate.com/"
        }, {
            innerText: "\u0440\u0443\u0441\u0441\u043a\u0438\u0439 \u044f\u0437\u044b\u043a",
            href: "https://ru.chaturbate.com/"
        }, {
            innerText: "T\u00fcrk\u00e7e",
            href: "https://tr.chaturbate.com/"
        }, {
            innerText: "\u4e2d\u6587",
            href: "https://zh.chaturbate.com/"
        }]), e = d.next(); !e.done; c = {
            anchor: c.anchor
        },
        e = d.next())
            b = e.value,
            c.anchor = (new tq).anchor,
            c.anchor.innerText = b.innerText,
            c.anchor.href = b.href,
            c.anchor.style.color = "#6b6b6b",
            c.anchor.style.display = "inline-block",
            c.anchor.style.fontFamily = "UbuntuMedium, Helvetica, Arial, sans-serif",
            c.anchor.style.fontSize = "12px",
            c.anchor.style.lineHeight = "11px",
            c.anchor.style.padding = "0 4px",
            c.anchor.style.borderRight = "1px solid #6b6b6b",
            c.anchor.onmouseenter = function(a) {
                return function() {
                    a.anchor.style.textDecoration = "underline"
                }
            }(c),
            c.anchor.onmouseleave = function(a) {
                return function() {
                    a.anchor.style.textDecoration = "none"
                }
            }(c),
            a.appendChild(c.anchor),
            b = c.anchor;
        b.style.borderRight = "";
        return a
    }
    function bt(a) {
        var b = "chaturbate.com";
        "" !== R.zk ? b = R.zk : R.wi !== b && (b = R.wi + "." + b);
        return "\u00a9 Copyright " + tb(b) + " 2011 - 2019. All Rights Reserved. " + a
    }
    ;function dt(a, b) {
        function c(a) {
            a !== d.state.volume && (d.state.volume = a,
            d.state.na = 0 === d.state.volume,
            et(d),
            d.sf(),
            d.Dd())
        }
        v.call(this);
        var d = this;
        this.u = a;
        this.S = "default";
        this.state = {
            volume: 60,
            na: !0
        };
        this.visible = this.Rn = this.vi = this.Hg = this.hb = !1;
        var e = eb().disable_sound;
        void 0 !== e && "true" === e.toLowerCase() || "1" === e ? (this.state.volume = 0,
        this.state.na = !0) : x() && (e = window.localStorage.getItem("videoControls"),
        null !== e ? (e = JSON.parse(e),
        this.state = {
            volume: e.volume,
            na: this.u.M.fk ? e.isMuted : !0
        }) : this.state.na = !this.u.M.fk);
        this.a.style.bottom = "0";
        this.a.style.left = "0";
        this.a.style.height = "auto";
        this.a.style.paddingTop = "4px";
        Ka() ? La() ? this.a.style.background = "linear-gradient(180deg, rgba(50, 50, 50, 0) 0%, rgba(50, 50, 50, 0.4) 66%, rgba(50, 50, 50, 0.7) 100%)" : this.a.style.backgroundColor = "rgba(50, 50, 50, 0.7)" : this.a.style.backgroundColor = "#000000";
        var f;
        this.Ha = document.createElement("img");
        this.Ha.style.height = "24px";
        this.Ha.style.width = "24px";
        this.Ha.style.margin = "0 6px 2px 6px";
        this.Ha.style.cursor = "pointer";
        this.Ha.style.cssFloat = "left";
        this.Ha.onclick = function() {
            d.u.Gg ? (N("ToggleMute", {
                newState: !d.state.na
            }),
            N("ChangeVolume", {
                volume: d.state.volume
            }),
            d.state.na ? d.oi() : d.Th(),
            d.$d()) : void 0 === f ? w("unexpected switch to hls", {}, "VIDEO_") : (N("ForceHLS"),
            Kn(d.u, f, d),
            d.oi(),
            u(qh, void 0))
        }
        ;
        this.a.appendChild(this.Ha);
        this.Qa = this.N(new Zl({
            gb: 12,
            Nd: 50
        }));
        this.Qa.a.style.margin = "6px 5px 0 0";
        this.Qa.a.style.cssFloat = "left";
        q(this.Qa.vk, function(a) {
            d.u.M.Fd(!1);
            c(a)
        });
        q(this.Qa.wk, c);
        q(this.Qa.uk, function(a) {
            c(a);
            0 === d.state.volume && d.u.M.Fd(!0);
            N("ChangeVolume", {
                volume: a
            });
            d.Dd()
        });
        this.cb = ft("");
        this.cb.style.cssFloat = "left";
        this.cb.onclick = function() {
            b.show()
        }
        ;
        this.a.appendChild(this.cb);
        this.Hg = Jh();
        document[Lh()] = function() {
            T() || (d.Hg = !1,
            d.Sa.style.display = "none",
            U("Full screen is unavailable."))
        }
        ;
        this.Sa = ft("Full Screen");
        this.Sa.id = "full-screen";
        Jh() || (this.Sa.style.display = "none");
        this.Sa.onclick = function() {
            T() ? Ih() : d.Hg && d.u.M.Ef();
            gt(d)
        }
        ;
        this.a.appendChild(this.Sa);
        this.vi = !1;
        this.gf = ft("Theater Mode");
        this.gf.onclick = function() {
            var a = "default" === d.S ? "theater" : "default";
            N("ChangeVideoMode", {
                videoMode: a
            });
            u(Nk, {
                Yf: d.S,
                Ya: a
            });
            gt(d)
        }
        ;
        this.a.appendChild(this.gf);
        this.yd = ft("Report Abuse");
        this.yd.style.display = "none";
        this.yd.onclick = function() {
            u(nh, void 0)
        }
        ;
        this.a.appendChild(this.yd);
        this.Mg = ft("More Rooms");
        this.Mg.id = "more-rooms";
        var h = !1;
        this.Mg.onclick = function() {
            u(Dj, void 0)
        }
        ;
        this.a.appendChild(this.Mg);
        this.og = ft(Hd + " (0)");
        this.og.id = "user-list";
        this.og.onclick = function() {
            u(kh, void 0)
        }
        ;
        q(Ks, function(a) {
            d.og.innerText = Hd + " (" + a + ")"
        });
        this.a.appendChild(this.og);
        this.yf = ft("Chat");
        this.yf.id = "chat-btn";
        this.yf.style.display = "none";
        this.yf.onclick = function() {
            u(ph, void 0)
        }
        ;
        this.a.appendChild(this.yf);
        this.D = ft(pc);
        this.D.id = "send-tip";
        this.D.onclick = function() {
            u(lh, {})
        }
        ;
        this.a.appendChild(this.D);
        this.Zf = ft(Hc);
        this.Zf.onclick = function() {
            u(oh, void 0)
        }
        ;
        this.a.appendChild(this.Zf);
        q(S, function(a) {
            if (void 0 === a.h.hb)
                w("Dossier is missing context.");
            else {
                if (a.h.hb) {
                    var b = !0 === a.h.kd
                      , c = !0 === a.h.jd;
                    ht(d, b || c, a.j.status);
                    q(a.j.event.kb, function(a) {
                        var e = a.Aa;
                        switch (e) {
                        case "unknown":
                        case "offline":
                        case "notconnected":
                        case "away":
                        case "groupnotwatching":
                        case "privatenotwatching":
                        case "hidden":
                        case "passwordprotected":
                            if ("fullscreen" === d.S) {
                                d.Sa.style.display = d.Hg ? "inline" : "none";
                                break
                            }
                            T() && Ih();
                            d.Sa.style.display = "none";
                            break;
                        case "public":
                        case "privatespying":
                        case "privatewatching":
                        case "privaterequesting":
                        case "grouprequesting":
                        case "groupwatching":
                            d.Sa.style.display = d.Hg ? "inline" : "none";
                            break;
                        default:
                            w("unexpected room status", e, "")
                        }
                        ht(d, b || c, a.Aa);
                        "privatespying" === a.Aa && u(oh, void 0)
                    });
                    q(a.j.event.Zd, function(e) {
                        b = e.kd;
                        c = e.jd;
                        ht(d, b || c, a.j.status)
                    })
                } else
                    ht(d, !1, a.j.status);
                f = a;
                d.hb = a.h.hb;
                d.Nq = "offline" === a.h.Xa;
                d.sf();
                d.u.Gg && it(d);
                gt(d);
                d.$d()
            }
        });
        q(Nk, function(a) {
            "theater" === d.S ? d.vi = !0 : "default" === d.S && (d.vi = !1);
            "fullscreen" === a.Yf && "fullscreen" !== a.Ya && h && (h = !1,
            setTimeout(function() {
                u(Dj, void 0)
            }, 200));
            d.S = a.Ya;
            gt(d)
        });
        q(uh, function() {
            d.$d()
        });
        q(Ej, function() {
            d.Th()
        });
        y(Kh(), document, function() {
            T() && T() === d.Gn ? u(Nk, {
                Yf: d.S,
                Ya: "fullscreen"
            }) : T() || u(Nk, {
                Yf: d.S,
                Ya: d.vi ? "theater" : "default"
            })
        });
        this.A()
    }
    m(dt, v);
    function ht(a, b, c) {
        var d = ["privaterequesting", "privatespying", "privatewatching", "grouprequesting", "groupwatching"];
        if (!b && -1 === d.indexOf(c) || !a.u.M.ah)
            a.Zf.style.display = "none";
        else
            switch (c) {
            case "unknown":
            case "offline":
            case "notconnected":
            case "away":
            case "hidden":
            case "passwordprotected":
            case "privatenotwatching":
                a.Zf.style.display = "none";
                break;
            default:
                a.Zf.style.display = a.u.M instanceof pn && "default" === a.S ? "none" : "block"
            }
    }
    function gt(a) {
        a.D.style.display = a.hb ? "inline" : "none";
        a.Mg.style.display = "none";
        a.og.style.display = "inline";
        a.gf.innerText = vg;
        a.gf.style.display = "inline";
        a.Sa.innerText = wg;
        a.yd.style.display = "none";
        a.yf.style.display = "inline";
        a.Zf.style.display = "block";
        a.Hg && (a.Sa.style.display = "inline");
        switch (a.S) {
        case "default":
            a.D.style.display = "none";
            a.og.style.display = "none";
            a.yf.style.display = "none";
            a.Zf.style.display = "none";
            a.gf.style.display = "none";
            a.Sa.style.display = "none";
            break;
        case "theater":
            a.gf.innerText = yg;
            a.gf.id = "default-view";
            a.Mg.style.display = "inline";
            break;
        case "fullscreen":
            a.yd.style.display = "inline";
            a.Mg.style.display = "inline";
            a.gf.style.display = "none";
            a.Sa.innerText = xg;
            break;
        default:
            w("Unexpected VideoMode: " + a.S, {}, "VIDEO_")
        }
        a.Nq && (a.D.style.display = "none",
        a.og.style.display = "none",
        a.yf.style.display = "none",
        a.yd.style.display = "none");
        a.$d()
    }
    k = dt.prototype;
    k.sf = function() {
        var a = this.Qa.value;
        0 === a || this.state.na ? (this.Ha.src = "https://ssl-ccstatic.highwebmedia.com/theatermodeassets/volume-mute.svg",
        this.Ha.id = "volume-mute") : 33 > a ? (this.Ha.src = "https://ssl-ccstatic.highwebmedia.com/theatermodeassets/volume-low.svg",
        this.Ha.id = "volume-low") : 66 > a ? (this.Ha.src = "https://ssl-ccstatic.highwebmedia.com/theatermodeassets/volume-medium.svg",
        this.Ha.id = "volume-medium") : (this.Ha.src = "https://ssl-ccstatic.highwebmedia.com/theatermodeassets/volume-high.svg",
        this.Ha.id = "volume-high")
    }
    ;
    function et(a) {
        a.u.M.Yg(a.state.volume, a.state.na)
    }
    k.Dd = function() {
        x() && window.localStorage.setItem("videoControls", JSON.stringify({
            volume: this.state.volume,
            isMuted: this.state.na
        }))
    }
    ;
    k.update = function() {
        it(this);
        this.Dd()
    }
    ;
    function it(a) {
        bm(a.Qa, a.state.na ? 0 : a.state.volume);
        et(a);
        a.sf()
    }
    k.Th = function() {
        this.state.na = !0;
        this.update()
    }
    ;
    k.oi = function() {
        this.state.na = !1;
        0 === this.state.volume && (this.state.volume = 60);
        this.update()
    }
    ;
    k.toggle = function() {
        this.visible ? this.Eh() : this.$d()
    }
    ;
    k.$d = function() {
        var a = this;
        this.visible = !0;
        if (this.u.M instanceof pn || "default" !== this.S)
            xh(this.a, "100ms"),
            this.a.style.opacity = "1",
            clearTimeout(this.Yq),
            this.Yq = setTimeout(function() {
                a.Eh()
            }, this.Rn ? 2E3 : 5E3)
    }
    ;
    k.Eh = function() {
        this.visible = !1;
        this.Rn = !0;
        xh(this.a, "500ms");
        this.a.style.opacity = "0"
    }
    ;
    k.show = function() {
        this.a.style.visibility = "visible";
        this.$d()
    }
    ;
    k.A = function() {
        this.a.style.visibility = "hidden"
    }
    ;
    k.ih = function(a) {
        this.state.volume = a;
        bm(this.Qa, this.state.na ? 0 : this.state.volume);
        this.sf()
    }
    ;
    k.ri = function(a) {
        this.state.na = a;
        bm(this.Qa, this.state.na ? 0 : this.state.volume);
        this.sf()
    }
    ;
    k.Gm = function(a) {
        this.ih(a);
        this.Dd()
    }
    ;
    k.pi = function(a) {
        this.ri(a);
        this.Dd()
    }
    ;
    k.Mb = function() {
        return 0
    }
    ;
    k.Sn = function() {}
    ;
    k.xp = function() {}
    ;
    function ft(a) {
        var b = document.createElement("span");
        b.innerText = a;
        b.style.color = "#ffffff";
        b.style.fontSize = "13px";
        b.style.padding = "5px";
        b.style.cssFloat = "right";
        b.style.marginRight = "4px";
        b.style.cursor = "pointer";
        return b
    }
    ;function jt(a) {
        X.call(this);
        var b = this;
        this.u = a;
        this.bg = new Map;
        this.a.style.visibility = "hidden";
        this.a.style.width = "auto";
        this.a.style.minWidth = "100px";
        this.a.style.height = "auto";
        this.a.style.backgroundColor = "black";
        this.a.style.position = "absolute";
        this.a.style.borderRadius = "2px";
        Ka() ? this.a.style.backgroundColor = "rgba(50, 50, 50, 0.7)" : this.a.style.backgroundColor = "#000000";
        this.a.onclick = function() {
            b.A()
        }
        ;
        q(this.u.M.Tc, function(a) {
            b.bg.clear();
            b.clear();
            for (var c = l(a), e = c.next(); !e.done; e = c.next())
                e = e.value,
                b.bg.set(e.label, e.value),
                b.a.appendChild(b.Sk(e)),
                e.eh && (b.u.R.cb.innerText = e.label);
            b.u.R.cb.style.display = 0 === a.length ? "none" : ""
        });
        q(this.bc, function() {
            b.A()
        })
    }
    m(jt, X);
    k = jt.prototype;
    k.C = function() {
        this.a.style.left = this.u.M.a.offsetLeft + this.u.R.cb.offsetLeft + "px";
        this.a.style.bottom = "default" === this.u.S ? this.u.R.a.offsetHeight + "px" : "0"
    }
    ;
    k.Sk = function(a) {
        var b = this
          , c = document.createElement("div");
        c.innerText = a.label;
        c.style.color = "#ffffff";
        c.style.fontSize = "13px";
        c.style.padding = "5px";
        c.style.marginRight = "6px";
        c.style.cursor = "pointer";
        c.style.width = "100%";
        a.eh ? (c.style.backgroundColor = "#ffffff",
        c.style.color = "#333333") : (c.onclick = function() {
            N("clickMenuLink:QualitySelect-" + a.label);
            b.ue(a.label);
            b.u.R.cb.innerText = a.label
        }
        ,
        c.onmouseenter = function() {
            Ka() ? c.style.backgroundColor = "rgba(150, 150, 150, 0.3)" : c.style.backgroundColor = "#444444"
        }
        ,
        c.onmouseleave = function() {
            c.style.backgroundColor = ""
        }
        );
        return c
    }
    ;
    k.ue = function(a) {
        a = this.bg.get(a);
        void 0 === a ? w("Undefined quality level key", {}, "VIDEO_") : this.u.M.ue(a)
    }
    ;
    k.clear = function() {
        for (; null !== this.a.firstChild; )
            this.a.removeChild(this.a.firstChild)
    }
    ;
    k.show = function() {
        this.tc();
        this.a.style.visibility = "visible";
        this.C()
    }
    ;
    k.A = function() {
        this.$b();
        this.a.style.visibility = "hidden"
    }
    ;
    function kt(a, b) {
        Gn.call(this, a, new Mp(!1));
        var c = this;
        this.Ke = b;
        this.Ec = !0;
        this.Xp = 80;
        this.a.style.position = "relative";
        this.a.style.backgroundColor = "#000000";
        this.a.style.minWidth = "500px";
        this.M.a.style.position = "relative";
        this.ic.style.position = "absolute";
        this.ic.style.width = "88px";
        this.ic.style.top = "6px";
        this.ic.style.right = "6px";
        this.ic.hidden = !0;
        this.M.a.appendChild(this.ic);
        this.Gb.Ng.style.visibility = "hidden";
        this.Gb.Pg = this.M.ah;
        Ea(this, this.Gb);
        this.qg = this.N(new jt(this));
        this.R = this.N(new dt(this,this.qg));
        this.M.Zj(this.R);
        this.R.show();
        q(Nk, function(a) {
            c.S = a.Ya
        })
    }
    m(kt, Gn);
    kt.prototype.C = function() {
        function a() {
            b.a.style.height = b.a.offsetWidth * (b.Ec ? .5625 : .75) + b.M.Mb() + "px";
            b.M.a.style.height = "100%";
            b.M.a.style.width = "100%";
            b.M.a.style.left = "0";
            b.M.Gd();
            b.ic.hidden = R.mm ? !0 : b.M instanceof kn ? !1 : !0
        }
        var b = this;
        switch (this.S) {
        case "default":
            a();
            break;
        case "theater":
            a();
            if (!T()) {
                this.ic.hidden = R.mm ? !0 : !1;
                var c = l(Ws(this))
                  , d = c.next().value;
                c = c.next().value;
                this.a.style.height = c + "px";
                this.M.a.style.width = d + "px";
                this.M.a.style.height = c + "px";
                this.M.ae()
            }
            break;
        case "fullscreen":
            this.M.ae();
            break;
        default:
            w("Unexpected VideoMode: " + this.S)
        }
    }
    ;
    function lt(a, b) {
        a.Ec = b;
        a.M instanceof kn && (a.M.rl = b);
        a.Y()
    }
    function Ws(a) {
        var b = a.H.a.clientWidth - 2 * a.H.gg
          , c = document.documentElement.clientHeight - a.Xp;
        430 >= c ? (c = 430,
        b = c / (a.Ec ? .5625 : .75)) : (c = l(Ln(b, c - a.Ke.offsetHeight, a.Ec)),
        b = c.next().value,
        c = c.next().value,
        430 >= c && (c = 430,
        b = c / (a.Ec ? .5625 : .75)));
        return [b, c]
    }
    ;function mt(a, b, c, d) {
        X.call(this);
        var e = this;
        this.Am = a;
        this.u = b;
        this.Ab = c;
        this.rr = d;
        this.ig = "public";
        this.Pe = !1;
        this.jf = new n("tipSent");
        this.sb = !1;
        this.a.style.display = "none";
        this.a.style.position = "absolute";
        this.a.style.width = "400px";
        this.a.style.height = "218px";
        this.a.style.top = "0";
        this.a.style.backgroundColor = "#ffffff";
        this.a.style.border = "2px solid #0b5d81";
        this.a.style.borderRadius = "4px";
        this.a.style.fontFamily = "UbuntuRegular, Helvetica, Arial, sans-serif";
        this.a.style.fontSize = "12px";
        this.a.style.color = R.Ba;
        this.a.style.overflow = "";
        y("keydown", this.a, function(a) {
            27 === a.keyCode && e.A();
            9 === a.keyCode && (a.shiftKey ? document.activeElement === e.D ? e.ea.focus() : document.activeElement === e.ea ? e.ba.focus() : e.D.focus() : document.activeElement === e.D ? e.ba.focus() : document.activeElement === e.ba ? e.ea.focus() : e.D.focus(),
            a.preventDefault())
        });
        this.Hk = wm("74px");
        this.a.appendChild(this.Hk);
        this.vl = xm();
        this.a.appendChild(this.vl);
        this.O.style.bottom = "";
        a = document.createElement("div");
        a.innerText = rc;
        a.style.color = "#0b5d81";
        a.style.backgroundColor = "#e0e0e0";
        a.style.fontSize = "15px";
        a.style.fontFamily = "UbuntuBold, Helvetica, Arial, sans-serif";
        a.style.padding = "6px";
        this.a.appendChild(a);
        a = document.createElement("div");
        a.style.display = "inline-block";
        a.style.width = "100%";
        b = document.createElement("span");
        b.innerText = ce;
        b.style.fontWeight = "bold";
        b.style.display = "inline-block";
        b.style.padding = "6px";
        a.appendChild(b);
        this.Jb = document.createElement("span");
        this.Jb.style.display = "inline-block";
        this.Jb.style.color = "green";
        this.Jb.style.fontWeight = "bold";
        this.Jb.style.padding = "6px 6px 6px 0";
        a.appendChild(this.Jb);
        b = document.createElement("a");
        b.innerText = uc;
        b.href = "/tipping/purchase_tokens/";
        b.style.display = "inline-block";
        b.style.color = "#d55215";
        b.style.fontSize = "11px";
        b.style.marginLeft = "12px";
        b.style.cssFloat = "right";
        b.style.padding = "6px";
        b.style.textDecoration = "underline";
        b.onclick = function() {
            Mh("/tipping/purchase_tokens/", "_blank", "height=615, width=850");
            return !1
        }
        ;
        a.appendChild(b);
        this.a.appendChild(a);
        this.Bm = a;
        this.Na = document.createElement("div");
        this.Na.style.display = "none";
        this.Na.style.color = "#ff0000";
        this.Na.style.fontWeight = "bold";
        this.Na.style.padding = "6px";
        this.Na.style.textAlign = "center";
        this.Na.style.position = "absolute";
        this.Na.style.left = "50%";
        a = document.createElement("div");
        a.innerText = ee;
        this.Na.appendChild(a);
        a = document.createElement("div");
        a.innerText = fe;
        this.Na.appendChild(a);
        this.a.appendChild(this.Na);
        this.ad = document.createElement("form");
        a = document.createElement("label");
        b = document.createElement("span");
        b.innerText = ge;
        b.style.display = "inline-block";
        b.style.padding = "6px";
        a.appendChild(b);
        this.ba = uj();
        this.ba.value = "25";
        this.ba.min = "1";
        this.ba.style.width = "5em";
        this.ba.style.display = "inline-block";
        this.ba.style.padding = "4px";
        this.ba.style.border = "1px solid #b4b4b4";
        this.ba.style.borderRadius = "4px";
        this.ba.style.marginTop = "12px";
        this.ba.onclick = function() {
            wj(e)
        }
        ;
        a.appendChild(this.ba);
        this.ad.appendChild(a);
        this.yc = document.createElement("div");
        this.yc.innerText = oc;
        this.yc.style.padding = "6px 6px 0 6px";
        this.ad.appendChild(this.yc);
        this.Jc = document.createElement("div");
        this.ea = document.createElement("textarea");
        this.ea.maxLength = 255;
        this.ea.style.width = "100%";
        this.ea.style.height = "40px";
        this.ea.style.resize = "none";
        this.ea.style.margin = "6px";
        this.ea.style.padding = "4px";
        this.ea.style.border = "1px solid #4b4c4b";
        this.ea.style.borderRadius = "4px";
        this.ea.style.boxSizing = "border-box";
        this.Jc.appendChild(this.ea);
        this.ad.appendChild(this.Jc);
        this.yc.onclick = function() {
            e.ea.select()
        }
        ;
        this.Cc = document.createElement("div");
        this.Cc.innerText = he;
        this.Cc.style.color = "red";
        this.Cc.style.display = "none";
        this.Cc.style.paddingLeft = "5px";
        a.appendChild(this.Cc);
        a = document.createElement("div");
        this.ba.oninput = function() {
            xj(e)
        }
        ;
        a.style.textAlign = "right";
        a.style.position = "relative";
        this.eb = document.createElement("div");
        this.eb.innerText = ie;
        this.eb.style.position = "absolute";
        this.eb.style.display = "none";
        this.eb.style.fontSize = "10px";
        this.eb.style.lineHeight = "1.3em";
        this.eb.style.padding = "6px";
        this.eb.style.textAlign = "left";
        this.eb.style.top = "6px";
        this.eb.style.left = "0px";
        a.appendChild(this.eb);
        this.D = document.createElement("button");
        this.D.innerText = sc;
        this.D.setAttribute("type", "submit");
        this.D.style.display = "inline-block";
        this.D.style.fontSize = "14px";
        this.D.style.fontFamily = "UbuntuMedium, Helvetica, Arial, sans-serif";
        this.D.style.margin = "6px";
        this.D.style.padding = "6px 18px";
        this.D.style.color = "#ffffff";
        this.D.style.backgroundColor = "#f47321";
        this.D.style.border = "1px solid #b1b1b1";
        this.D.style.borderRadius = "4px";
        this.D.style.cursor = "pointer";
        this.D.onmouseenter = function() {
            e.D.style.textDecoration = "underline"
        }
        ;
        this.D.onmouseleave = function() {
            e.D.style.textDecoration = "none"
        }
        ;
        b = document.createElement("span");
        b.style.display = "inline-block";
        b.style.fontSize = "0";
        b.style.marginLeft = "6px";
        b.style.position = "relative";
        b.style.top = "1px";
        b.style.borderBottom = "6px solid transparent";
        b.style.borderTop = "6px solid transparent";
        b.style.borderLeft = "6px solid #ffffff";
        this.D.appendChild(b);
        a.appendChild(this.D);
        this.ad.appendChild(a);
        this.a.appendChild(this.ad);
        q(S, function(a) {
            void 0 !== a.h.fj ? e.fj = a.h.fj : w("hasLowSatisfactionScore is not defined.");
            void 0 !== a.h.hb ? e.hb = a.h.hb : w("isAgeVerified is not defined.");
            e.fa = a.j.i();
            q(a.j.event.kb, function(a) {
                switch (a.Aa) {
                case "privatewatching":
                case "groupwatching":
                    e.ig = "private";
                    break;
                default:
                    e.ig = "public"
                }
            })
        });
        q(qj, function(a) {
            e.rf(a.Sb)
        });
        this.C();
        y("submit", this.ad, function(a) {
            a.preventDefault();
            var b = parseInt(e.ba.value);
            !e.sb || isNaN(b) || e.D.disabled || (N("SendTipClicked", {
                amount: b
            }),
            100 < b && !e.Pe ? (e.D.innerText = Bb(b),
            e.Pe = !0,
            e.C()) : (e.ba.blur(),
            tj({
                fa: e.fa,
                Lp: e.ba.value,
                message: "" + (void 0 !== e.ua ? e.ua.value : e.ea.value),
                source: e.Am,
                Mp: e.ig
            }).then(function(a) {
                a.Id ? N("SendTipSuccess", {
                    amount: b
                }) : void 0 !== a.error ? U(a.error) : w("unknown send tip error");
                e.Sj();
                e.ea.value = "";
                void 0 !== a.Lc && u(pj, a.Lc);
                u(e.jf, {
                    La: b,
                    Id: a.Id
                })
            })["catch"](function(a) {
                w("Error sending tip. status: " + a);
                u(e.jf, {
                    La: b,
                    Id: !1
                })
            }),
            e.A()))
        });
        q(this.bc, function() {
            e.A()
        })
    }
    m(mt, X);
    k = mt.prototype;
    k.C = function() {
        this.Jc.style.width = Math.max(0, this.a.clientWidth - 12) + "px";
        this.eb.style.maxWidth = Math.max(0, this.a.offsetWidth - this.D.offsetWidth - 15) + "px";
        if ("fixed" === this.Ab.style.position) {
            var a = this.rr.getBoundingClientRect();
            this.a.style.top = a.top + Ya().scrollTop - 30 + "px";
            this.a.style.left = a.left + a.width + 12 + "px";
            this.Hk.style.display = "none";
            this.vl.style.display = "block"
        } else
            this.a.style.top = this.u.a.getBoundingClientRect().bottom - this.a.offsetHeight + 21 + Ya().scrollTop + "px",
            this.a.style.left = "400px",
            this.Hk.style.display = "block",
            this.vl.style.display = "none";
        this.O.style.height = document.documentElement.offsetHeight + "px"
    }
    ;
    k.Sj = function() {
        this.D.innerText = sc;
        this.Pe = !1
    }
    ;
    k.show = function(a) {
        var b = this;
        this.hb ? Ni('You must be logged in to tip. Click "OK" to login.') || (void 0 !== a.La && (this.ba.value = a.La.toString(),
        xj(this)),
        this.ea.value = void 0 !== a.message ? a.message : "",
        this.Na.style.display = this.fj ? "block" : "none",
        this.eb.style.display = void 0 !== a.si && a.si ? "none" : "block",
        this.Jb.innerText = Cd + "...",
        rj(this.fa).then(function(a) {
            b.ba.max = a.sa.toString();
            if (void 0 !== a.dh) {
                b.yc.innerText = a.dh.label;
                void 0 !== b.ua && b.Jc.removeChild(b.ua);
                b.ua = document.createElement("select");
                b.ua.style.width = "100%";
                b.ua.style.fontSize = ".8125em";
                b.ua.style.margin = "6px";
                b.ua.style.border = "1px solid #4b4c4b";
                b.ua.style.boxSizing = "border-box";
                b.Jc.appendChild(b.ua);
                var c = document.createElement("option");
                c.innerText = "-- " + ke + " --";
                b.ua.appendChild(c);
                a = l(a.dh.options);
                for (c = a.next(); !c.done; c = a.next()) {
                    var e = c.value;
                    c = document.createElement("option");
                    c.innerText = e.label;
                    c.value = e.label;
                    b.ua.appendChild(c)
                }
                b.ea.style.display = "none"
            } else
                void 0 !== b.ua && (b.yc.innerText = oc,
                b.ea.value = "",
                b.ea.style.display = "block",
                b.Jc.removeChild(b.ua),
                b.ua = void 0);
            b.a.style.display = "block";
            b.tc();
            b.C();
            wj(b);
            b.sb = !0
        })["catch"](function(a) {
            w("Error getting token balance. status: " + a);
            b.Jb.innerText = zg
        })) : U("This broadcaster doesn't accept tips.")
    }
    ;
    k.A = function() {
        this.a.style.display = "none";
        this.ba.blur();
        this.$b();
        this.sb = !1
    }
    ;
    k.rf = function(a) {
        this.Jb.innerText = a + " " + Ab(a, !1)
    }
    ;
    k.Kn = function() {
        if (null === this.a.parentElement) {
            this.a.style.visibility = "hidden";
            document.body.appendChild(this.a);
            this.a.style.width = this.Bm.offsetWidth + "px";
            this.a.style.minWidth = "345px";
            this.a.style.height = "auto";
            var a = [this.a.offsetWidth, this.a.offsetHeight];
            document.body.removeChild(this.a);
            this.a.style.visibility = "visible";
            return a
        }
        w("called getContentSize when window is already attached");
        return [this.a.offsetWidth, this.a.offsetHeight]
    }
    ;
    function nt(a) {
        Hp.call(this, a);
        var b = this;
        this.Oi = a;
        q(Nk, function() {
            b.pa()
        });
        q(uh, function() {
            b.pa()
        })
    }
    m(nt, Hp);
    var ot, pt, qt;
    function rt(a, b, c) {
        function d() {
            return void 0 !== document.activeElement && null !== document.activeElement && 0 <= ["input", "textarea", "select", "button"].indexOf(document.activeElement.tagName.toLowerCase()) && !c.sb
        }
        function e() {
            return a.X.ia.$a().Qe() || b.$a().Qe()
        }
        function f() {
            "default" === ot ? b.$a().Zb() : a.X.ia.$a().Zb()
        }
        q(Nk, function(a) {
            ot = a.Ya;
            pt = void 0
        });
        var h = new Fi;
        Gi(h, {
            keyCode: 9,
            eg: !1,
            handle: function(g) {
                !e() && d() && a.Lb !== a.fc || c.sb || c.sb || (qi(a, a.X),
                g.shiftKey ? (um(b),
                um(a.X.ia)) : (b.Xk(),
                a.X.ia.Xk()),
                f(),
                g.preventDefault(),
                g.stopPropagation())
            }
        });
        Gi(h, {
            keyCode: 13,
            eg: !1,
            handle: function(b) {
                e() ? Ni() ? b.preventDefault() : Ya().scrollTop === qt && void 0 !== pt && (Ya().scrollTop = pt,
                pt = void 0) : (qi(a, a.X),
                f())
            }
        });
        Gi(h, {
            keyCode: 27,
            eg: !1,
            handle: function(d) {
                c.sb ? (c.A(),
                qi(a, void 0),
                pt = void 0) : (d.stopPropagation(),
                d.preventDefault(),
                void 0 !== a.Lb && a.Lb.pq && a.removeChild(a.Lb),
                qi(a, void 0),
                Ya().scrollTop === qt && void 0 !== pt && (b.$a().Od(),
                Ya().scrollTop = pt,
                pt = void 0))
            }
        });
        Gi(h, {
            keyCode: 76,
            eg: !0,
            handle: function(c) {
                if ("default" === ot || a.Lb === a.X)
                    if ("default" !== ot || "block" === b.wa.a.style.display)
                        c.stopPropagation(),
                        c.preventDefault(),
                        a.X.ia.zf(),
                        b.zf()
            }
        });
        Gi(h, {
            keyCode: 83,
            eg: !0,
            handle: function(b) {
                b.stopPropagation();
                b.preventDefault();
                "default" === ot || "hidden" === a.a.style.visibility ? u(rh, {
                    si: !0
                }) : u(lh, {
                    si: !0
                })
            }
        });
        var g = [32, 34, 33];
        y("keydown", document, function(b) {
            (27 === b.keyCode || b.ctrlKey || b.metaKey ? 0 : c.sb || -1 !== g.indexOf(b.keyCode) || !e() && d()) || void 0 !== a.Lb && vi(a.Lb, b) || Hi(h, b) || b.ctrlKey || b.metaKey || !Up(b.which) || (pt = Ya().scrollTop,
            qi(a, a.X),
            f(),
            setTimeout(function() {
                qt = Ya().scrollTop
            }, 0))
        })
    }
    ;function st() {
        v.call(this);
        var a = this;
        this.nl = !1;
        this.so = new n("messageRemoved");
        this.a.style.display = "none";
        this.a.style.position = "static";
        q(S, function(b) {
            a.Ip = "tfa-msg-" + b.h.userName;
            !b.j.ob && !a.nl && 0 < b.h.sa && !b.h.Dr && "1" !== gb(a.Ip) && (a.sq = "tfa",
            a.$l(tg))
        })
    }
    m(st, v);
    st.prototype.$l = function(a) {
        var b = this;
        this.a.style.display = "block";
        this.a.style.height = "auto";
        this.a.style.position = "static";
        this.a.style.padding = "5px 0px";
        this.a.style.textAlign = "center";
        this.a.style.boxSizing = "border-box";
        this.a.style.fontSize = "14px";
        this.a.style.fontWeight = "400";
        this.a.style.fontFamily = "UbuntuMedium, Helvetica, Arial, sans-serif";
        this.a.style.color = R.Zk;
        var c = document.createElement("div");
        c.style.padding = "15px";
        c.style.backgroundColor = "#ffffd3";
        c.style.border = "1px solid #ccc";
        var d = document.createElement("div");
        d.innerHTML = a;
        this.xh = document.createElement("a");
        this.xh.style.color = R.lc;
        this.xh.textContent = qf;
        this.xh.style.cursor = "pointer";
        a = document.createElement("span");
        a.textContent = " (";
        var e = document.createElement("span");
        e.textContent = ")";
        d.appendChild(a);
        d.appendChild(this.xh);
        d.appendChild(e);
        c.appendChild(d);
        this.a.appendChild(c);
        a = {};
        d = l(d.getElementsByTagName("a"));
        for (e = d.next(); !e.done; a = {
            link: a.link
        },
        e = d.next())
            a.link = e.value,
            a.link.style.color = R.lc,
            a.link.onmouseenter = function(a) {
                return function() {
                    a.link.style.textDecoration = "underline"
                }
            }(a),
            a.link.onmouseleave = function(a) {
                return function() {
                    a.link.style.textDecoration = ""
                }
            }(a);
        y("click", this.xh, function() {
            b.a.style.display = "none";
            b.a.removeChild(c);
            b.nl = !1;
            "tfa" === b.sq && hb(b.Ip, "1", 10);
            u(b.so, void 0)
        });
        this.nl = !0
    }
    ;
    function tt() {
        v.call(this);
        var a = this;
        this.a.style.display = "none";
        this.a.style.position = "static";
        this.a.style.margin = "21px 36px";
        this.a.style.color = R.Ba;
        this.a.style.fontSize = "12.006px";
        var b = document.createElement("div");
        b.innerText = Mf;
        b.style.fontFamily = "'UbuntuRegular', Arial, Helvetica, sans-serif";
        b.style.fontWeight = "700";
        this.a.appendChild(b);
        b = document.createElement("form");
        b.onsubmit = function(b) {
            b.preventDefault();
            a.submit()
        }
        ;
        var c = document.createElement("label");
        c.innerText = wf + ": *";
        c.setAttribute("for", "room_password");
        c.style.display = "inline-block";
        c.style.fontFamily = "'UbuntuMedium', Arial, Helvetica, sans-serif";
        c.style.fontWeight = "400";
        c.style.margin = "21px 8px 22px 62px";
        b.appendChild(c);
        this.input = document.createElement("input");
        this.input.type = "text";
        this.input.id = "room_password";
        this.input.style.border = "1px solid #b1b1b1";
        this.input.style.borderRadius = "4px";
        this.input.style.padding = "2px 4px";
        this.input.style.lineHeight = "18px";
        this.input.style.width = "225px";
        this.input.style.height = "20px";
        this.input.style.fontFamily = "Arial, Helvetica, sans-serif";
        b.appendChild(this.input);
        var d = document.createElement("button");
        d.innerText = Nf;
        d.setAttribute("type", "submit");
        d.style.display = "block";
        d.style.fontSize = "14px";
        d.style.fontFamily = "UbuntuMedium, Helvetica, Arial, sans-serif";
        d.style.margin = "0px 135px";
        d.style.padding = "5px 17px";
        d.style.color = "#ffffff";
        d.style.backgroundColor = "#f47321";
        d.style.border = "1px solid #b1b1b1";
        d.style.borderRadius = "4px";
        d.style.cursor = "pointer";
        d.onclick = function(b) {
            b.preventDefault();
            a.submit()
        }
        ;
        d.onmouseenter = function() {
            d.style.textDecoration = "underline"
        }
        ;
        d.onmouseleave = function() {
            d.style.textDecoration = "none"
        }
        ;
        c = document.createElement("span");
        c.style.display = "inline-block";
        c.style.fontSize = "0";
        c.style.marginLeft = "6px";
        c.style.position = "relative";
        c.style.top = "1px";
        c.style.borderBottom = "6px solid transparent";
        c.style.borderTop = "6px solid transparent";
        c.style.borderLeft = "6px solid #ffffff";
        d.appendChild(c);
        b.appendChild(d);
        this.a.appendChild(b)
    }
    m(tt, v);
    tt.prototype.submit = function() {
        var a = this;
        Ni() || A("roomlogin/" + this.i + "/", {
            password: this.input.value
        }).then(function(b) {
            b = new D(b.responseText);
            var c = G(b, "result");
            b.Qc("errors");
            b.Qc("html");
            L(b);
            "error" === c ? U("Incorrect password") : (u(mh, a.i),
            a.A());
            a.input.value = ""
        })["catch"](function(a) {
            w("Unable to login to room " + a.responseText);
            window.location.reload()
        })
    }
    ;
    tt.prototype.show = function(a) {
        this.i = a;
        this.a.style.display = "block"
    }
    ;
    tt.prototype.A = function() {
        this.a.style.display = "none"
    }
    ;
    function ut() {
        v.call(this);
        var a = this;
        this.a.style.fontSize = "12px";
        this.a.style.position = "relative";
        this.a.style.height = "auto";
        var b = document.createElement("div");
        b.style.display = "none";
        b.style.width = "auto";
        b.style.height = "auto";
        b.style.backgroundColor = "#FFFFD3";
        b.style.border = "1px solid #CCCCCC";
        b.style.borderRadius = "4px";
        b.style.padding = "2em";
        b.style.margin = "20px 32px";
        b.style.fontSize = "2em";
        b.style.lineHeight = "3em";
        b.style.textDecoration = "none";
        b.style.position = "relative";
        b.style.color = R.Ba;
        this.a.appendChild(b);
        var c = document.createElement("span");
        c.innerText = "\u00d7";
        c.style.position = "absolute";
        c.style.right = "0.5em";
        c.style.top = "0";
        c.style.fontSize = "2em";
        c.style.Fr = "none";
        c.style.webkitUserSelect = "none";
        b.appendChild(c);
        c.onclick = function() {
            hb("mobile_redirect_hide", "true", .75);
            a.a.style.display = "none"
        }
        ;
        c = document.createElement("div");
        var d = document.createElement("span");
        d.textContent = Af;
        c.appendChild(d);
        d = document.createElement("div");
        var e = document.createElement("ul");
        e.style.listStyleType = "disc";
        var f = document.createElement("li");
        f.textContent = ic;
        f.style.lineHeight = "1.1em";
        e.appendChild(f);
        d.appendChild(f);
        f = document.createElement("li");
        f.textContent = Bf;
        f.style.lineHeight = "1.1em";
        e.appendChild(f);
        d.appendChild(f);
        f = document.createElement("li");
        f.textContent = Dg;
        f.style.lineHeight = "1.1em";
        e.appendChild(f);
        d.appendChild(f);
        e = document.createElement("div");
        f = document.createElement("span");
        f.textContent = Cf;
        var h = document.createElement("a");
        h.style.color = "#0a5a83";
        h.textContent = Df;
        h.onclick = function() {
            N("MobileAlertLinkClicked")
        }
        ;
        var g = document.createElement("span");
        g.textContent = Ff;
        var p = document.createElement("a");
        p.textContent = Ef;
        p.style.color = "#0a5a83";
        p.href = "https://www.surveymonkey.com/r/CM5YXYL";
        p.onclick = function() {
            N("MobileAlertSurveyClicked")
        }
        ;
        var t = document.createElement("span");
        t.textContent = ".";
        e.appendChild(f);
        e.appendChild(h);
        e.appendChild(g);
        e.appendChild(p);
        e.appendChild(t);
        b.appendChild(c);
        b.appendChild(d);
        b.appendChild(e);
        q(S, function(a) {
            a.h.zr && (h.href = "https://m.chaturbate.com/mobile/?b=" + a.h.i + "&source=desktop",
            b.style.display = "block")
        })
    }
    m(ut, v);
    function vt() {
        var a = this;
        this.kk = this.jk = this.mk = this.lk = this.Lg = this.Sh = 0;
        this.km = !1;
        this.no = 0;
        this.Ic = {
            cbVersion: 2,
            href: "",
            unloadTimestamp: 0,
            count: 0
        };
        this.Wl = this.Vl = !1;
        this.Rb = this.j = void 0;
        this.kq = function(b) {
            b !== a.j && (void 0 !== a.j && (a.j.event.Sf.removeListener(a.Fo),
            a.j.event.hg.removeListener(a.Jj),
            a.j.event.ag.removeListener(a.Jj)),
            a.j = b,
            q(a.j.event.Sf, a.Fo),
            q(a.j.event.hg, a.Jj),
            q(a.j.event.ag, a.Jj))
        }
        ;
        this.Xm = function(b) {
            b !== a.Rb && (void 0 !== a.Rb && a.Rb.jf.removeListener(a.Ko),
            a.Rb = b,
            q(a.Rb.jf, a.Ko, !1))
        }
        ;
        this.Ni = function() {
            return void 0 !== a.j && !a.j.ob
        }
        ;
        this.ep = function() {
            a.Sh = 0;
            a.Lg = 0;
            a.lk = 0;
            a.mk = 0;
            a.jk = 0;
            a.kk = 0;
            a.km = !1
        }
        ;
        this.op = function() {
            if (!a.km && a.Ni()) {
                N("SessionMetrics", {
                    messageSuccessCount: a.Lg,
                    messageFailCount: a.Sh - a.Lg,
                    tipSuccessCount: a.lk,
                    tipSuccessTotal: a.mk,
                    tipFailCount: a.jk,
                    tipFailTotal: a.kk,
                    refreshCount: void 0 !== window.sessionStorage ? a.Ic.count : void 0
                });
                a.km = !0;
                a.Ic = {
                    cbVersion: 2,
                    href: window.location.href,
                    unloadTimestamp: (new Date).getTime(),
                    count: 0
                };
                try {
                    window.sessionStorage.setItem("refreshMeta", JSON.stringify(a.Ic))
                } catch (b) {}
            }
        }
        ;
        this.Do = function() {
            S.removeListener(a.Do);
            if (void 0 !== window.sessionStorage && a.Ni()) {
                a.no = (new Date).getTime();
                try {
                    var b = window.sessionStorage.getItem("refreshMeta");
                    null !== b && (a.Ic = Object.assign({}, a.Ic, JSON.parse(b)))
                } catch (c) {}
                a.ep();
                a.Gl = a.Gq();
                a.Vl = !1;
                a.Wl = !1
            }
        }
        ;
        this.Ko = function(b) {
            void 0 !== b.La && (!0 === b.Id ? (a.lk += 1,
            a.mk += b.La) : (a.jk += 1,
            0 < b.La && (a.kk += b.La)))
        }
        ;
        this.Fo = function() {
            a.Sh += 1
        }
        ;
        this.Jj = function(b) {
            void 0 !== a.j && b.fb.username === a.j.username() && a.Lg < a.Sh && (a.Lg += 1)
        }
        ;
        this.Gq = function() {
            if (void 0 !== window.performance) {
                var a = void 0 !== window.performance.getEntriesByType ? window.performance.getEntriesByType("navigation") : [];
                if (0 < a.length)
                    switch (a[0].type) {
                    case "navigate":
                        return 0;
                    case "reload":
                        return 1;
                    case "back_forward":
                        return 2;
                    case "prerender":
                        return 3;
                    default:
                        return 255
                    }
                else
                    return window.performance.navigation.type
            }
        }
        ;
        this.Ir = function() {
            return 1 === a.Gl ? 2 === a.Ic.cbVersion : 0 === a.Gl || void 0 === a.Gl ? a.Ic.href === window.location.href && a.Ic.unloadTimestamp > a.no - 1E4 && 2 === a.Ic.cbVersion : !1
        }
        ;
        this.Qp = function() {
            void 0 !== window.sessionStorage && !a.Vl && a.Ni() && (a.Ic.count = a.Ir() ? 1 : 0,
            a.Vl = !0)
        }
        ;
        this.mr = function() {
            if (void 0 !== window.sessionStorage && !a.Wl && a.Ni()) {
                a.Ic = {
                    cbVersion: 2,
                    href: window.location.href,
                    unloadTimestamp: (new Date).getTime(),
                    count: a.Ic.count
                };
                try {
                    window.sessionStorage.setItem("refreshMeta", JSON.stringify(a.Ic)),
                    a.Wl = !0
                } catch (b) {}
            }
        }
        ;
        this.Xq = function() {
            a.Qp();
            a.op()
        }
        ;
        this.Lo = function() {
            a.Qp();
            a.mr();
            a.op()
        }
        ;
        q(S, this.Do);
        q(S, this.ep);
        q(vh, this.Xq);
        y("beforeunload", window, this.Lo);
        y("unload", window, this.Lo)
    }
    ;function wt(a, b) {
        v.call(this);
        var c = this;
        this.Tq = a;
        this.Yc = [];
        this.ci = 15E3;
        this.Vg = this.Of = !1;
        this.u = b;
        this.a.style.height = "";
        this.a.style.width = "";
        this.a.style.position = "relative";
        this.a.style.cssFloat = "right";
        this.a.style.overflow = "visible";
        this.a.style.fontFamily = "UbuntuMedium, Helvetica, Arial, sans-serif";
        this.Hb = (new tq).anchor;
        this.Hb.href = "#";
        this.Hb.style.textDecoration = "none";
        this.Hb.style.fontSize = "10.008px";
        this.Hb.style.position = "relative";
        this.Hb.style.right = "5px";
        this.Hb.style.top = "1px";
        this.Hb.style.color = R.lb;
        this.Hb.style.backgroundColor = "transparent";
        this.Hb.onmouseenter = function() {
            c.Hb.style.textDecoration = "underline"
        }
        ;
        this.Hb.onmouseleave = function() {
            c.Hb.style.textDecoration = "none"
        }
        ;
        this.Hb.onclick = function(a) {
            a.preventDefault();
            a.stopPropagation();
            c.Of || (a = eb(),
            a.next_in = (c.ci / 1E3).toString(),
            a = db(a),
            window.history.replaceState(c.He.i, c.He.bb, "./?" + a),
            xt(c));
            yt(c)
        }
        ;
        this.Ug = document.createElement("span");
        this.Ug.innerText = jg;
        this.Hb.appendChild(this.Ug);
        this.Va = (new tq).anchor;
        this.Va.href = "#";
        this.Va.style.padding = "6px 3px 6px 5px";
        this.Va.innerText = kg + " (CTRL-/)";
        this.Va.style.textDecoration = "none";
        this.Va.style.fontSize = "10.008px";
        this.Va.style.position = "relative";
        this.Va.style.cssFloat = "right";
        this.Va.style.backgroundColor = R.zo;
        this.Va.style.border = "1px solid " + R.cd;
        this.Va.style.borderRadius = "4px 4px 0 0";
        this.Va.style.color = R.lb;
        this.Va.onmouseenter = function() {
            c.Va.style.textDecoration = "underline"
        }
        ;
        this.Va.onmouseleave = function() {
            c.Va.style.textDecoration = "none"
        }
        ;
        this.Va.onclick = function(a) {
            a.preventDefault();
            c.Of ? zt(c) : yt(c)
        }
        ;
        this.a.appendChild(this.Hb);
        this.a.appendChild(this.Va);
        y("keydown", document, function(a) {
            "Control" !== a.key && "Meta" !== a.key && "OS" !== a.key && ("/" !== a.key && 191 !== a.which || !a.ctrlKey && !a.metaKey ? c.Of && !c.Vg && zt(c) : (a.preventDefault(),
            yt(c)))
        }, !0);
        q(S, function(a) {
            c.Va.href = "/next/" + a.h.i + "/";
            c.Hb.href = "/next/" + a.h.i + "/?next_in=15";
            a.h.Ad !== c.Jn && (c.Jn = a.h.Ad,
            c.bf = new xi([Ei(c.Jn)],1));
            c.Yc.push(a.h.i);
            c.He = a.h;
            a = eb();
            "next_in"in a && (a = parseInt(a.next_in),
            isFinite(a) && !isNaN(a) && (1 <= a && (c.ci = 1E3 * a),
            At(c),
            Bt(c),
            c.Of = !0))
        });
        y("mousedown", document, function(a) {
            var b;
            if (!(b = c.Vg)) {
                a: {
                    b = a.target;
                    if (c.u.M instanceof Fn)
                        for (var d = l(c.u.a.getElementsByClassName("vjs-volume-panel")), h = d.next(); !h.done; h = d.next()) {
                            h = h.value;
                            if (b === h) {
                                b = !0;
                                break a
                            }
                            h = l(h.getElementsByTagNameNS("*", "*"));
                            for (var g = h.next(); !g.done; g = h.next())
                                if (b === g.value) {
                                    b = !0;
                                    break a
                                }
                        }
                    b = !1
                }
                if (!(b = b || (c.u.M instanceof Dn ? a.target === c.u.M.L : !1)))
                    a: {
                        b = a.target;
                        d = l(c.u.R.Qa.a.children);
                        for (h = d.next(); !h.done; h = d.next())
                            if (b === h.value) {
                                b = !0;
                                break a
                            }
                        b = b === c.u.R.Ha || b === c.u.R.Qa.a || b === c.u.R.Qa.handle
                    }
                b || (b = a.target,
                b = b === c.Hb || b === c.Ug);
                if (!b)
                    a: if (b = a.target,
                    c.u.M instanceof kn && c.u.M.pb instanceof Xm) {
                        d = l(c.u.M.pb.Qa.a.children);
                        for (h = d.next(); !h.done; h = d.next())
                            if (b === h.value) {
                                b = !0;
                                break a
                            }
                        b = b === c.u.M.pb.Ha
                    } else
                        b = !1
            }
            b || c.Of && a.target !== c.Va && zt(c)
        }, !1)
    }
    m(wt, v);
    function xt(a) {
        a.Jq = setTimeout(function() {
            a.Vg = !0;
            clearInterval(a.gn);
            Ct(a)
        }, 108E5)
    }
    function Ct(a) {
        Ki("Are you still watching?", function() {
            xt(a);
            Bt(a);
            a.Vg = !1
        }, function() {
            zt(a);
            a.ef = a.ci
        })
    }
    function Bt(a) {
        a.ef = a.Vg ? a.ef : a.ci;
        a.gn = window.setInterval(function() {
            a.ef -= 1E3;
            0 >= a.ef / 1E3 && (a.ef = a.ci,
            yt(a));
            a.Ug.innerText = lg + " (" + a.ef / 1E3 + "s)"
        }, 1E3);
        a.Ug.innerText = lg + " (" + a.ef / 1E3 + "s)";
        a.Va.innerText = mg + " (any key)"
    }
    function At(a) {
        clearInterval(a.gn);
        a.Ug.innerText = jg;
        a.Va.innerText = kg + " (CTRL-/)"
    }
    function yt(a) {
        function b(b) {
            a.jn = b[0];
            N("LoadRoomFromRoomList", {
                username: a.jn.username
            });
            u(mh, a.jn.username)
        }
        "key"in a.bf.wd ? a.bf.next().then(function(a) {
            b(a)
        })["catch"](function(b) {
            "Session Not Found" === b.trim() ? (delete a.bf.wd.key,
            yt(a)) : w("Error calling roomIterator.next: " + b)
        }) : a.bf.start(a.Yc).then(function(a) {
            b(a)
        })["catch"](function(a) {
            w("Error calling roomIterator.start: " + a)
        })
    }
    function zt(a) {
        if (a.Of) {
            clearTimeout(a.Jq);
            At(a);
            a.Of = !1;
            a.Vg = !1;
            var b = eb();
            delete b.next_in;
            b = db(b);
            var c = "";
            "" !== b && (c = "?" + b);
            window.history.replaceState(a.He.i, a.He.bb, "./" + c)
        } else
            w("Attempted to stop cam scanning when cams were not scanning")
    }
    wt.prototype.C = function() {
        var a = this.Tq.getBoundingClientRect()
          , b = this.a.getBoundingClientRect()
          , c = 0;
        null !== this.a.style.top && 2 <= this.a.style.top.length && (c = parseInt(this.a.style.top.split("px")[0]));
        b = b.bottom - c;
        this.a.style.top = Math.round(a.bottom < b ? a.bottom - b : b - a.bottom) + "px"
    }
    ;
    function Dt(a, b) {
        var c = this;
        this.J = a;
        this.Eb = b;
        this.rj = !1;
        this.ao = !0;
        this.S = "default";
        setTimeout(function() {
            c.ao = !1;
            Et(c)
        }, 5E3);
        this.Eb.a.onmouseenter = function() {
            Ft(c)
        }
        ;
        q(this.J.bl, function(a) {
            a !== c.J.X ? Et(c) : Ft(c)
        });
        q(this.J.Om, function(a) {
            a === c.J.X ? Ft(c) : Et(c)
        });
        q(ym, function() {
            Ft(c)
        });
        q(uh, function() {
            Et(c)
        });
        q(Nk, function(a) {
            c.S = a.Ya;
            Et(c)
        })
    }
    function Et(a) {
        "theater" !== a.S && "fullscreen" !== a.S || a.ao || (a.rj ? a.J.X.ia.$a() !== a.J.X.ia.ca || a.J.Lb === a.J.X || a.J.lj === a.J.X || kl(a.J.X.ia.ca.w) || (a.Eb.a.style.display = "block",
        a.rj = !1,
        Xj(a.J.X.ia, a.J.X.ia.ca),
        a.J.X.ia.ca.w.Oa.scrollLeft = 0,
        a.J.X.ia.ql = !0,
        a.J.X.A(),
        ui(a.J.X),
        a.Eb.clear(),
        ml(a.J.X.ia.ca.w),
        setTimeout(function() {
            a.Eb.pa()
        }, 0)) : a.Eb.a.style.display = "block")
    }
    function Ft(a) {
        a.rj || (a.rj = !0,
        a.J.X.ia.ql = !1,
        a.J.X.show(),
        si(a.J.X),
        a.Eb.a.style.display = "none",
        a.J.X.ia.ca.w.pa())
    }
    ;function Gt(a) {
        a.style.cursor = "pointer";
        a.onmouseenter = function() {
            a.style.textDecoration = "underline"
        }
        ;
        a.onmouseleave = function() {
            a.style.textDecoration = ""
        }
    }
    function Ht(a) {
        a.stopImmediatePropagation()
    }
    function It(a) {
        function b(a, b, c, e) {
            var f = document.createElement("input")
              , g = document.createElement("label");
            g.style.color = R.po;
            f.type = c;
            f.style.boxSizing = "border-box";
            f.style.width = "checkbox" === c ? "18px" : "310px";
            f.id = e;
            f.name = e;
            f.style.border = "1px solid #b1b1b1";
            f.style.padding = "2px 4px";
            f.style.margin = "1px 0";
            f.style.lineHeight = "18px";
            f.style.height = "checkbox" === c ? "18px" : "26px";
            f.style.borderRadius = "4px";
            f.style.color = "#000";
            f.style.font = "100% Arial,Helvetica,sans-serif";
            y("keydown", f, function(a) {
                13 === a.keyCode && d.tb.submit()
            });
            g.innerText = b;
            g.htmlFor = e;
            a.appendChild(g);
            a.appendChild(f);
            return f
        }
        function c(a) {
            var b = document.createElement("div");
            b.style.display = "block";
            b.style.margin = "10px 0 0 0";
            a.appendChild(b);
            return b
        }
        rk.call(this);
        var d = this;
        this.X = a;
        this.sb = !1;
        this.a.style.position = "fixed";
        this.a.style.top = "0";
        this.a.style.fontSize = "12px";
        this.a.style.color = R.Ba;
        this.a.onclick = function() {
            d.A()
        }
        ;
        this.O.style.background = "black";
        this.tb = document.createElement("form");
        this.tb.style.position = "absolute";
        this.tb.style.margin = "auto";
        this.tb.style.top = "15%";
        this.tb.style.background = "white";
        this.tb.style.border = "5px solid #006B94";
        this.tb.style.borderRadius = "10px";
        this.tb.style.padding = "10px 40px 15px 40px";
        this.tb.style.width = "315px";
        this.tb.action = "/auth/login/?next=" + window.location.pathname + encodeURIComponent(window.location.search);
        this.tb.method = "POST";
        this.tb.onclick = function(a) {
            a.stopPropagation()
        }
        ;
        a = c(this.tb);
        var e = c(this.tb);
        this.Ej = document.createElement("input");
        this.vh = document.createElement("input");
        this.Ej.type = this.vh.type = "hidden";
        this.Ej.name = "next";
        this.vh.name = "csrfmiddlewaretoken";
        this.vh.value = gb("csrftoken");
        e.appendChild(this.Ej);
        e.appendChild(this.vh);
        e = c(a);
        e.style.position = "absolute";
        e.style.top = "0";
        e.style.right = "0";
        e.style.margin = "0";
        var f = new Image;
        f.src = "https://ssl-ccstatic.highwebmedia.com/images/close_icon.gif";
        f.style.padding = "4px";
        f.style.cursor = "pointer";
        f.onclick = function() {
            d.A()
        }
        ;
        e.appendChild(f);
        var h = c(a);
        e = document.createElement("h1");
        f = document.createElement("small");
        h.appendChild(e);
        h.appendChild(f);
        e.style.color = "#FF6300";
        e.style.margin = "0";
        e.style.font = "26px ubuntubold,Arial,Helvetica,serif";
        h = document.createElement("span");
        h.innerText = R.rm + " " + jc;
        e.appendChild(h);
        f.innerText = uf;
        f.style.font = "12px ubuntu,Arial,Helvetica,serif";
        a = c(a);
        var g = c(a)
          , p = c(a);
        h = c(a);
        e = c(a);
        f = c(a);
        a = c(a);
        this.Wp = b(g, vf + ":", "text", "username");
        this.Zq = b(p, wf + ":", "password", "password");
        g = b(h, xf + ":", "checkbox", "rememberme");
        g.style.verticalAlign = "middle";
        g.style.marginLeft = "2px";
        h.style.marginTop = "5px";
        h = document.createElement("div");
        g = document.createElement("span");
        h.style.background = "#FF6300";
        h.style.cursor = "pointer";
        h.style.height = "25px";
        h.style.padding = "5px 13px 7px 15px";
        h.style.marginRight = "5px";
        h.style.border = "1px solid #b1b1b1";
        h.style.borderRadius = "5px";
        h.style.display = "inline";
        g.innerHTML = kc + " &#9656;";
        g.style.color = "white";
        g.style.fontWeight = "bold";
        g.style.font = "1.16em/1.0em 'UbuntuMedium',Arial,Helvetica,sans-serif";
        g.onclick = function() {
            d.tb.submit()
        }
        ;
        h.appendChild(g);
        e.appendChild(h);
        e.style.textAlign = "right";
        Gt(g);
        f.style.borderTop = "1px solid black";
        f.style.position = "relative";
        f.style.width = "100%";
        f.style.margin = "26px 0 12px 0";
        e = document.createElement("a");
        e.innerText = yf;
        e.style.display = "block";
        e.href = "/auth/password_reset/";
        e.style.marginRight = "66%";
        e.style.color = R.lc;
        f = document.createElement("a");
        f.innerText = zf;
        f.style.display = "block";
        f.href = "/accounts/register/?src=login_gate";
        f.style.marginRight = "60%";
        f.style.color = R.lc;
        f.style.marginBottom = "3px";
        Gt(e);
        Gt(f);
        a.appendChild(f);
        a.appendChild(e);
        q(S, function() {
            d.Ej.value = "" + window.location.pathname + window.location.search
        });
        this.a.appendChild(this.tb);
        this.A();
        q(this.bc, function() {
            d.A()
        });
        q(sh, function() {
            d.show()
        });
        Mi = function() {
            u(sh, void 0)
        }
    }
    m(It, rk);
    It.prototype.C = function() {
        this.tb.style.left = (window.innerWidth - this.tb.offsetWidth) / 2 + "px"
    }
    ;
    It.prototype.show = function() {
        this.vh.value = gb("csrftoken");
        this.a.style.display = "";
        this.tc();
        this.C();
        this.sb = !0;
        this.X.nm = !0;
        this.Wp.focus();
        y("keydown", this.a, Ht)
    }
    ;
    It.prototype.A = function() {
        var a = this.sb;
        this.a.style.display = "none";
        this.$b();
        this.sb = !1;
        this.Wp.blur();
        this.Zq.blur();
        this.X.nm = !1;
        a && cb("keydown", this.a, Ht)
    }
    ;
    function Jt(a) {
        if (void 0 !== window.history && void 0 !== window.history.pushState)
            if ("theatermode" === window.location.pathname.replace(/\//g, "")) {
                var b = eb();
                b.b = a.i;
                b.from_tube !== a.i && delete b.from_tube;
                Kt && delete b.join_overlay;
                b = db(b);
                Kt ? window.history.pushState(a.i, a.bb, "./?" + b) : window.history.replaceState(a.i, a.bb, "./?" + b)
            } else {
                b = eb();
                b.from_tube !== a.i && delete b.from_tube;
                Kt && delete b.join_overlay;
                b = db(b);
                var c = "";
                "" !== b && (c = "?" + b);
                Kt ? window.history.pushState(a.i, a.bb, "/" + a.i + "/" + c) : window.history.replaceState(a.i, a.bb, "/" + a.i + "/" + c)
            }
    }
    var Kt = !1;
    function Lt(a, b) {
        function c() {
            Kt && ("default" === d.S ? d.H.ia.ca.w.pa() : "theater" === d.S && d.Eb.pa(),
            setTimeout(function() {
                var a = d.u.a.getBoundingClientRect();
                if (d.H.Ab.getBoundingClientRect().bottom > document.documentElement.clientHeight || a.bottom < .6 * a.height)
                    Ya().scrollTop = d.Z.a.offsetHeight + d.yp.a.offsetHeight;
                d.Y()
            }, 0))
        }
        v.call(this);
        var d = this;
        this.S = "default";
        this.vg = new Pp;
        this.Wg = new vt;
        this.Fa = new Aa;
        document.body.style.backgroundColor = R.Si;
        document.body.style.color = R.Kd;
        document.body.appendChild(this.a);
        var e = new lq;
        document.body.appendChild(e.a);
        q(e.Ak, function() {
            document.body.removeChild(e.a)
        });
        this.a.style.position = "static";
        this.a.style.minWidth = "1000px";
        this.a.style.boxSizing = "border-box";
        this.a.focus();
        var f = new Bq;
        this.J = new Sm("theater",this.Wg);
        this.J.a.style.left = "0";
        this.J.X.a.style.zIndex = "";
        this.J.a.style.position = "fixed";
        this.J.a.style.width = "100%";
        this.Eb = new nt(this.J.X.ia.ca.w);
        new Dt(this.J,this.Eb);
        this.Z = this.N(new Eq(f));
        this.yp = this.N(new st);
        q(this.yp.so, function() {
            d.C()
        });
        this.Ke = this.N(new Uq(a));
        this.N(new ut);
        this.u = new kt(b,this.Ke.a);
        this.J.a.onclick = function() {
            d.u.R.toggle();
            d.J.Lb === d.J.X && d.J.lj !== d.J.X && qi(d.J, void 0)
        }
        ;
        this.Ke.N(new wt(this.Ke.a,this.u));
        this.ee = this.N(new Xs(f));
        this.ee.a.style.cssFloat = "right";
        this.H = this.N(new Us(this.u,this.J,this.Eb));
        this.u.H = this.H;
        this.u.R.Gn = this.H.Pa.a;
        this.H.Pa.N(this.Eb);
        this.J.a.style.visibility = "hidden";
        this.H.Ab.style.position = "fixed";
        this.H.Ab.style.top = "999999px";
        this.Nl = this.N(new tt);
        this.zi = this.N(new Ys(f));
        this.N(new Zs);
        var h;
        q(tk, function(a) {
            void 0 === h && (h = new uk);
            h.show(a)
        });
        var g;
        q(mq, function(a) {
            void 0 === g && (g = new pq);
            g.show(a)
        });
        f = new It(this.J.X);
        rt(this.J, this.H.ia, f);
        var p = this.N(new mt("theater",this.u,this.H.Ab,this.H.Pa.D));
        q(rh, function(a) {
            p.sb ? p.A() : p.show(a);
            p.Y()
        });
        q(p.jf, function() {
            p.A()
        });
        q(Nk, function(a) {
            d.S = a.Ya;
            c();
            switch (d.S) {
            case "default":
                void 0 !== d.J.fc && u(lh, {});
                void 0 !== d.J.Md && u(nh, void 0);
                d.u.a.appendChild(d.u.qg.O);
                d.u.a.appendChild(d.u.qg.a);
                d.Wg.Xm(p);
                d.u.M.Gd();
                break;
            case "theater":
                void 0 !== d.J.Md && u(nh, void 0);
                d.J.a.appendChild(d.u.qg.O);
                d.J.a.appendChild(d.u.qg.a);
                d.u.M.ae();
                break;
            case "fullscreen":
                d.J.a.appendChild(d.u.qg.O);
                d.J.a.appendChild(d.u.qg.a);
                d.u.M.ae();
                p.A();
                break;
            default:
                w("Unexpected VideoMode: " + d.S, {}, "VIDEO_")
            }
            d.Y()
        });
        u(Nk, {
            Yf: "default",
            Ya: this.S
        });
        var t = new ci(function() {
            d.Y()
        }
        ,{
            ph: 20,
            Rl: !0
        })
          , C = new ci(function() {
            u(uh, void 0)
        }
        ,{
            ph: 250,
            Rl: !0
        });
        window.onresize = function() {
            di(C);
            di(t)
        }
        ;
        window.onorientationchange = function() {
            Ih();
            d.Y()
        }
        ;
        y("scroll", window, function() {
            d.Eb.Y()
        }, !1, !0);
        y("popstate", window, function(a) {
            void 0 !== a.state && null !== a.state && Mt(d, a.state, !1)
        });
        q(mh, function(a) {
            Mt(d, a, !0)
        });
        q(S, function(a) {
            p.A();
            document.title = zb(a.h.i);
            d.H.a.style.display = "block";
            a.h.Se || a.h.Ig || "" === a.h.Ai.header ? (d.ee.a.style.display = "none",
            d.zi.a.style.display = "none") : (d.ee.a.style.display = "block",
            d.zi.a.style.display = "block");
            d.Ae = a.h.qa.Ae;
            if (d.u.M instanceof kn) {
                var b = function() {
                    void 0 === window.xmovie.SetTipVol ? setTimeout(function() {
                        b()
                    }, 100) : window.xmovie.SetTipVol(d.Ae)
                };
                b()
            }
            d.Y();
            Kt || (Kt = !0);
            c()
        });
        q(qj, function(a) {
            d.H.Pa.sa.innerText = "" + a.Sb;
            d.H.Pa.mi.innerText = " " + Ab(a.Sb, !1);
            d.Z.tk.rf(a.Sb)
        });
        this.J.a.ontouchstart = function() {
            d.$h = !0;
            setTimeout(function() {
                d.$h = !1
            }, 500)
        }
        ;
        this.J.a.onmousemove = function() {
            d.$h || d.u.R.$d()
        }
        ;
        cb("click", this.J.X.ia.ca.w.hf, gl);
        this.J.X.ia.ca.w.hf.onclick = function(a) {
            a.stopPropagation();
            a.preventDefault();
            u(lh, {})
        }
        ;
        this.u.M instanceof pn && (this.u.M.a.ontouchstart = function() {
            "default" === d.S && (d.$h = !0,
            setTimeout(function() {
                d.$h = !1
            }, 500))
        }
        ,
        this.u.M.a.onmousemove = function() {
            "default" === d.S && (d.$h || d.u.R.$d())
        }
        );
        Mt(this, a, !0);
        this.Y();
        this.oh();
        q(th, function(a) {
            d.Ae = a.Ae
        });
        Qp(this.vg, "huge", "https://ssl-ccstatic.highwebmedia.com/theatermodeassets/sounds/huge.mp3");
        Qp(this.vg, "large", "https://ssl-ccstatic.highwebmedia.com/theatermodeassets/sounds/large.mp3");
        Qp(this.vg, "medium", "https://ssl-ccstatic.highwebmedia.com/theatermodeassets/sounds/medium.mp3");
        Qp(this.vg, "small", "https://ssl-ccstatic.highwebmedia.com/theatermodeassets/sounds/small.mp3");
        Qp(this.vg, "tiny", "https://ssl-ccstatic.highwebmedia.com/theatermodeassets/sounds/tiny.mp3");
        this.u.M instanceof kn && q(pm.Rp, function(a) {
            void 0 !== window.xmovie.SetTipVol && window.xmovie.SetTipVol(a)
        })
    }
    m(Lt, v);
    Lt.prototype.C = function() {
        switch (this.S) {
        case "default":
            var a = this.a.clientWidth - 32 - 12 - 2 * this.H.gg;
            "none" !== this.ee.a.style.display && (a = a - this.ee.a.offsetWidth - 6);
            this.H.a.style.width = a + "px";
            this.J.a.style.top = "0";
            break;
        case "theater":
            a = this.a.clientWidth - 8 - 2 * this.H.gg;
            "none" !== this.ee.a.style.display && (a = a - this.ee.a.offsetWidth - 18);
            this.H.a.style.width = a + "px";
            Wr(this.H.fg, a);
            this.J.a.style.top = "0";
            this.u.Xp = this.H.wc.offsetHeight + this.H.Dl.offsetHeight + this.H.yh.a.offsetHeight + this.H.hr.dc.offsetHeight + this.Ke.a.offsetHeight;
            break;
        case "fullscreen":
            break;
        default:
            w("Unexpected VideoMode: " + this.S, {}, "VIDEO_")
        }
    }
    ;
    function Mt(a, b, c) {
        b !== a.Pq && (a.Pq = b,
        (Kt ? im(b) : hm(window.initialRoomDossier)).then(function(b) {
            N("LoadRoom", {
                version: 2
            });
            void 0 !== a.en && a.en();
            var d = b.mq ? new Do(b) : new Gp(b);
            a.Wg.kq(d);
            a.u.M instanceof kn ? q(d.event.re, function(a) {
                window.xmovie.PlayBeep(a)
            }) : q(d.event.re, function(b) {
                a.vg.re(b, a.Ae * a.u.M.Cg() / 100)
            });
            document.title = zb(b.i);
            void 0 !== b.Ec ? lt(a.u, b.Ec) : w("isWidescreen is not defined.", {}, "VIDEO_");
            var f = {
                h: b,
                j: d
            }, h, g;
            a.en = function() {
                clearInterval(h);
                window.clearTimeout(g);
                d.disconnect();
                a.u.M.stop();
                u(vh, void 0)
            }
            ;
            a.u.M.ej(f);
            c && Jt(b);
            "offline" === f.h.Xa ? (a.H.Ab.style.position = "fixed",
            a.H.Ab.style.top = "999999px",
            a.H.yh.a.style.display = "none",
            a.H.wc.style.display = "none",
            a.H.Dl.style.display = "none",
            a.H.vd.style.display = "block",
            a.H.vd.insertAdjacentElement("afterend", a.H.Pa.D)) : (a.H.Ab.style.position = "",
            a.H.Ab.style.top = "",
            a.H.Ab.style.display = "block",
            a.H.yh.a.style.display = "block",
            a.H.wc.style.display = "block",
            a.H.Dl.style.display = "block",
            a.H.vd.style.display = "none",
            a.H.Pa.jm.insertAdjacentElement("afterend", a.H.Pa.D));
            u(S, f);
            a.Nl.A();
            a.Fa.add(q(d.event.kb, function(b) {
                "notconnected" === b.Vc && (h = setInterval(function() {
                    d.Hm()
                }, 3E4));
                var c = ["privatewatching", "groupwatching"];
                c = 0 <= c.indexOf(b.Vc) || 0 <= c.indexOf(b.Aa);
                if ("notconnected" === b.Vc || c)
                    g = setTimeout(function() {
                        d.Hm()
                    }, 2E3);
                switch (b.Aa) {
                case "passwordprotected":
                    a.Nl.show(d.i()),
                    a.H.a.style.display = "none",
                    a.ee.a.style.display = "none",
                    a.zi.a.style.display = "none",
                    Xq(a.Ke, d.i()),
                    a.u.M.stop()
                }
            }));
            a.Fa.add(q(th, function(a) {
                d.Op(a.zd, a.Bd)
            }));
            a.Fa.add(q(vh, function() {
                Da(a.Fa)
            }));
            a.Y()
        })["catch"](function(c) {
            if (void 0 !== c.fd && void 0 !== c.fd.responseText && "" !== c.fd.responseText)
                if ("application/json" !== c.fd.getResponseHeader("Content-Type"))
                    w("Error reading room dossier error", {
                        room: b,
                        error: c.fd.responseText
                    });
                else {
                    var d = new D(c.fd.responseText);
                    switch (G(d, "code", !1)) {
                    case "access-denied":
                        U("Access Denied for room: " + b + "\n\n" + H(d, "detail"));
                        break;
                    case "unauthorized":
                    case "password-required":
                        Jt({
                            i: b,
                            bb: "Chat with " + b
                        });
                        a.Nl.show(b);
                        a.H.a.style.display = "none";
                        a.ee.a.style.display = "none";
                        a.zi.a.style.display = "none";
                        Xq(a.Ke, b);
                        break;
                    default:
                        w("Error parsing room dossier error", {
                            room: b,
                            error: c.fd.responseText
                        })
                    }
                }
            else
                w("Error occurred while processing room dossier: " + c, {
                    room: b
                })
        }),
        kb())
    }
    ;void 0 !== ta && (ta.config("https://041ea488280542a29047b1d3511a37e3@sentry.io/277797", {
        sampleRate: .005,
        release: "5f4168c972f9f6b75b5f6c9bf1f0d355d9f88b75",
        Vr: "production",
        ks: {
            product: "theater"
        }
    }).install(),
    va("product", "theater"));
    function Nt() {
        function a(a) {
            document.title = zb(a);
            bq().then(function(b) {
                new Lt(a,b)
            })
        }
        function b(c) {
            var e = c[d].username;
            im(e).then(function(f) {
                4 > d && "public" !== f.Xa ? (d += 1,
                b(c)) : a(e)
            })["catch"](function(a) {
                w("Error occurred while processing room dossier for " + e + ". xhr " + a + " ResponseText: " + a.responseText)
            })
        }
        var c = eb().b;
        void 0 === c && (c = "");
        var d = 0
          , e = window.location.pathname.replace(/\//g, "");
        "theatermode" === e ? 0 <= ["", "f", "m", "c", "t"].indexOf(c) ? (new xi([c],5)).start([]).then(function(a) {
            b(a)
        })["catch"](function(a) {
            w("Error Loading Rooms: " + a)
        }) : a(c) : a(e);
        q(S, function(a) {
            ga("set", "page", a.h.i);
            ga("send", "pageview");
            $p(a.j)
        });
        Wp()
    }
    void 0 === ta ? Nt() : ta.context(Nt);
    var Ot = window.newrelic
      , Pt = window.Raven;
    function Qt(a) {
        try {
            void 0 !== Ot && Ot.noticeError(a)
        } catch (b) {
            w("New Relic Error in reportError: " + b.toString())
        }
    }
    function Rt(a, b, c, d) {
        try {
            void 0 !== Pt && Pt.captureMessage(b, {
                level: a,
                extra: c,
                tags: {
                    subsystem: d
                }
            })
        } catch (e) {
            w("Sentry error in reportError: " + e.toString())
        }
    }
    function Xh(a) {
        void 0 !== window.console && console.info(a, "")
    }
    function r(a, b, c) {
        c = void 0 === c ? "" : c;
        void 0 !== window.console && console.warn(c.concat("WARN "), a, void 0 === b ? "" : b);
        try {
            var d = "object" === typeof a ? JSON.stringify(a) : a.toString()
        } catch (e) {
            d = "" + a
        }
        Qt(Error(d));
        Rt("warning", d, b, c)
    }
    function w(a, b, c, d) {
        c = void 0 === c ? "" : c;
        d = void 0 === d ? !1 : d;
        void 0 !== window.console && console.error(c.concat("ERROR "), a, void 0 === b ? "" : b);
        try {
            var e = "object" === typeof a ? JSON.stringify(a) : a.toString()
        } catch (f) {
            e = "" + a
        }
        Qt(Error(e));
        d || Rt("error", e, b, c)
    }
    function Fo() {}
    window.onerror = function(a, b, c, d, e) {
        try {
            var f = JSON.stringify(e)
        } catch (h) {
            f = "" + e
        }
        w(['"Message: " ' + a, '"URL: "  ' + b, '"Line: " ' + c, '"Column: " ' + d, '"Error object: " ' + f].join("\n"), {}, "", !0);
        return !1
    }
    ;
}
).call(this);
