import multiprocessing
from .config import *
import csv
import random, string
from .createuser import CreateUser

def create_users():
    #create a list of names
    last_names = ["".join([random.choice(string.ascii_letters+ string.digits) \
                           for y in range(6)]) for x in range(number_of_users *2 )]

    with open(first_namescsv, 'r') as f1:
        names_list = [name[0] for name in csv.reader(f1)]

    usernames = set()
    while len(usernames) <= number_of_users:
        username = "{}{}".format(random.choice(names_list), random.choice(last_names))
        usernames.add(username)

    #get proxy list
    with open(proxiescsv, 'r') as f2:
        proxy_list = [x for x in csv.DictReader(f2)]

    # create multiprocessing queues and pool
    queue = multiprocessing.Queue()

    #create processes
    with open('created_users.csv', 'a+') as results:
        writer = csv.DictWriter(results, fieldnames=['username', 'password', 'gender',
                                                     'birthday_month', 'birthday_day',
                                                     'birthday_year'], extrasaction='ignore')
        counter = 0
        processes =[]
        for username in usernames:
            counter += 1
            if counter <= 6:
                processes.append(multiprocessing.Process(target=CreateUser, args=[queue, url, username, proxy_list, data_sitekey]))
                processes[-1].start()
            else:
                for proc in processes:
                    proc.join()
                processes = []
                counter = 0
            while not queue.empty():
                rec = queue.get_nowait()
                writer.writerow(rec)
        for proc in processes:
            proc.join()
        while not queue.empty():
            rec = queue.get_nowait()
            writer.writerow(rec)

