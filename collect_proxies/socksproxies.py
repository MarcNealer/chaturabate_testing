import requests
from bs4 import BeautifulSoup
import csv


def CollectSocksProxies():
    url_list = ["https://www.socks-proxy.net"]
    session = requests.session()
    with open('create_users/socks_proxies.csv', 'w') as fl:
        print('file open')
        proxy_writer = csv.writer(fl)
        proxy_writer.writerow(['ip', 'port', 'c_code', 'country', 'type', 'last_checked'])
        for url in url_list:
            resp = session.get(url)
            soup = BeautifulSoup(resp.content, features='lxml')
            table = soup.table.tbody.find_all('tr')
            for row in table:
                cells = row.find_all('td')
                new_row = [cells[0].string, cells[1].string, cells[2].string,
                                  cells[3].string, cells[4].string, cells[7].string]
                print('writing')
                proxy_writer.writerow(new_row)
