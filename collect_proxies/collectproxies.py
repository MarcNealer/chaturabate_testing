import requests
from bs4 import BeautifulSoup
import csv
import multiprocessing
import copy

def CollectProxies():
    queue = multiprocessing.Queue()
    url_list = ["https://free-proxy-list.net", "https://www.us-proxy.org",
                "https://free-proxy-list.net/uk-proxy.html",
                "https://www.socks-proxy.net", "https://free-proxy-list.net/anonymous-proxy.html"]
    session = requests.session()
    with open('create_users/proxies.csv', 'w') as fl:
        proxy_writer = csv.writer(fl)
        proxy_writer.writerow(['ip', 'port', 'c_code', 'country', 'type', 'last_checked'])
        for url in url_list:
            resp = session.get(url)
            soup = BeautifulSoup(resp.content, features='lxml')
            table = soup.table.tbody.find_all('tr')
            counter = 0
            processes=[]
            for row in table:
                counter +=1
                if counter <= 5:
                    processes.append(multiprocessing.Process(target=test_proxy, args=[str(row), queue]))
                    processes[-1].start()
                else:
                    for proc in processes:
                        proc.join()
                    processes=[]
                    counter = 0
                while not queue.empty():
                    print(queue.qsize())
                    rec = queue.get_nowait()
                    proxy_writer.writerow(rec)
            for proc in processes:
                proc.join()
        while not queue.empty():
            rec = queue.get_nowait()
            proxy_writer.writerow(rec)

def test_proxy(row, queue):
    row = BeautifulSoup(row, features='lxml')
    session = requests.Session()
    cells = row.find_all('td')
    proxy = {'https': 'https://{}:{}'.format(cells[0].string, cells[1].string)}
    if cells[6].string.lower() == 'yes':
        print('testing {}'.format(proxy))
        try:
            resp = session.get("https://chaturbate.com/", proxies=proxy, timeout=(20,30))
            if resp.status_code == 200:
                queue.put_nowait([cells[0].string, cells[1].string, cells[2].string,
                                  cells[3].string, cells[4].string, cells[7].string])
            else:
                pass
        except Exception as e:
            pass