import multiprocessing
import csv
from .userroombot import user_room_bot
from .get_rooms import get_rooms
from create_users.config import *
import time
from .config import *

def controller():
    #get proxy list
    with open(proxiescsv, 'r') as f2:
        proxy_list = [x for x in csv.DictReader(f2)]

    # create multiprocessing queues and pool
    room_queue = multiprocessing.Queue()
    print('get rooms started')
    proc_rooms = multiprocessing.Process(target=get_rooms, args=[room_queue, proxy_list, cycles])
    proc_rooms.start()

    counter = 0
    processes =[]
    while room_queue.qsize()  < 1:
        time.sleep(5)
    while room_queue.qsize() > 0:
        counter += 1
        if counter <= bot_count:
            processes.append(multiprocessing.Process(target=user_room_bot,
                                                     args=[room_queue, proxy_list, rooms, mintime, maxtime, headless]))
            processes[-1].start()
        else:
            for proc in processes:
                proc.join()
                processes = []
                counter = 0
    proc_rooms.join()
