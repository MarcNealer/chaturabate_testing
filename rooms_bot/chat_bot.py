from seleniumwire import webdriver
from websocket import create_connection, WebSocket
from selenium.webdriver.firefox.options import Options
import multiprocessing
import csv
import ssl
import random

def bot_process():
    with open('create_users/proxies.csv','r') as f:
        proxies = [x for x in csv.DictReader(f)]
    with open('create_users/socks_proxies.csv','r') as f:
        socks_proxies = [x for x in csv.DictReader(f)]
    proc_count = 4
    proc_list = []
    for x in range(proc_count):
        proc_list.append(multiprocessing.Process(target=bot, args=[proxies, socks_proxies]))
        proc_list[-1].start()


class MySocket(WebSocket):
    def recv_frame(self):
        frame = super().recv_frame()
        print(frame)
        return frame


def bot(proxies, socks_proxies):

    socks = []
    for x in range(300):
        for y in range(20):
            try:
                selected_proxy = random.choice(proxies)
                proxy = {'https': 'https://{}:{}'.format(selected_proxy['ip'], selected_proxy['port'])}
                options = Options()
                options.headless = True
                dv = webdriver.Firefox(options=options, proxy=proxy)
                ws = ''
                dv.get('https://chaturbate.com/dirty_bears2/')
                break
            except Exception as e:
                print(e)
        for item in dv.requests:
            if '/websocket' in item.path:
                ws = item.path.replace('https', 'wss')
        if ws:
            print(ws)
            try:
                con =create_connection(ws, class_=MySocket, sslopt={"cert_reqs": ssl.CERT_NONE})
                socks.append(con)
                print(con.getstatus())
            except Exception as e:
                print(e)
        dv.close()
