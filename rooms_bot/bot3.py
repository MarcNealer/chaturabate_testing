from selenium import webdriver
import random
import time
import csv
import threading
from selenium.webdriver.firefox.options import Options

users = [{'username': 'janefx10', 'password': 'testtestfx10'},
         {'username': 'johnfx10', 'password': 'testtestfx10'},
         {'username': 'jillfx10', 'password': 'testtestfx10'},
         {'username': 'jackfx11', 'password': 'testtestfx11'},
         {'username': 'alfiefx10', 'password': 'testtestfx10'},
         {'username': 'angiefx10', 'password': 'testtestfx10'},
         {'username': 'janefx11', 'password': 'testtestfx11'},
         {'username': 'johnfx11', 'password': 'testtestfx11'},
         {'username': 'jillfx11', 'password': 'testtextfx11'},
         {'username': 'alfiefx11', 'password': 'testtestfx11'},
         {'username': 'angiefx11', 'password': 'testtestfx11'},
         {'username': 'lindafx10', 'password': 'testtestfx10'},
         {'username': 'lindafx11', 'password': 'testtestfx11'},
         {'username': 'peterfx10', 'password': 'testtestfx10'},
         {'username': 'peterfx11', 'password': 'testtestfx11'}]
rooms = ['cassielove101']

def controller():
    with open('create_users/proxies.csv', 'r') as f:
        proxy_list = [x for x in csv.DictReader(f)]
        counter = 0
        threads = []
        for user in users:
            threads.append(threading.Thread(target=room_bot, args=(rooms[0], user, proxy_list[0])))
            threads[-1].start()
            time.sleep(75)
            counter +=1
        for thread in threads:
            thread.join()

def room_bot(room, user, selected_proxy):
    for x in range(20):
        try:
            print('joining user {}'.format(user['username']))
            proxy = {'https': 'https://{}:{}'.format(selected_proxy['ip'], selected_proxy['port'])}
            options = Options()
            options.headless = True
            dv = webdriver.Firefox(options=options, proxy=proxy)
            dv.get('https://chaturbate.com/')
            click = dv.find_element_by_link_text('I AGREE')
            click.click()
            login_link = dv.find_element_by_class_name('login-link')
            login_link.click()
            box = dv.find_element_by_id('login_inputs')
            username = box.find_element_by_id('id_username')
            username.send_keys(user['username'])
            password = box.find_element_by_id('id_password')
            password.send_keys(user['password'])
            click = box.find_element_by_class_name('button')
            click.click()
            print('logged in ')
            dv.get('https://chaturbate.com/{}/'.format(room))
            print('entering room')
            time.sleep(random.randint(600, 900))
            dv.close()
            break
        except Exception as e:
            try:
                dv.close()
            except:
                pass
            print(e)


