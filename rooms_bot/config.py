rooms = 3 ## number of rooms visited per instance of firefox
headless=False ## run firefox headless or not
mintime=40 ## minimum time to spend in the room
maxtime=130 ## maximum time to spend in the room
bot_count = 3 ## number of instances of the firefox bot to run at the same trme
cycles = 1 ## the number of times to cycles through the lists of rooms and add them to the queue