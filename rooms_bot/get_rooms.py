import requests
from bs4 import BeautifulSoup
import random
import time

def get_rooms(room_queue, proxies, cycles=2):
    session = requests.session()
    got_page = False
    get_counter = 0
    next = '/?page=1'
    current_cycle = 0
    while not got_page and get_counter < 6:
        try:
            selected_proxy = random.choice(proxies)
            proxy = {'https': 'https://{}:{}'.format(selected_proxy['ip'], selected_proxy['port'])}
            resp = session.get('https://chaturbate.com/auth/login/', proxies=proxy, timeout=(20,30))
            while next:
                next_page_counter = 0
                while True:
                    try:
                        url = 'http://chaturbate.com{}'.format(next)
                        resp = session.get(url, proxies=proxy, timeout=(20,30))
                        break
                    except:
                        next_page_counter +=1
                        if next_page_counter > 6:
                            break

                soup = BeautifulSoup(resp.content, 'lxml')
                rooms = soup.find_all('li', {'class':'room_list_room'})
                room_counter = 0
                for room in rooms:
                    room_counter +=1
                    link = room.find('a').get('href')
                    room_queue.put_nowait(link)
                time.sleep(20)
                next = soup.find('a', {'class':'next'}).get('href')
                if next == '#':
                    current_cycle +=1
                    if current_cycle > cycles:
                        break
                    else:
                        time.sleep(200)
            got_page=True
        except Exception as e:
            print(e)
            get_counter +=1


    pass