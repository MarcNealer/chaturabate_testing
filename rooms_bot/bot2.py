import requests
import m3u8
from bs4 import BeautifulSoup
import multiprocessing
from threading import Thread
import csv
import random
import time


def bot_process(room):
    with open('create_users/proxies.csv','r') as f:
        proxies = [x for x in csv.DictReader(f)]
    proc_count = 4
    proc_list = []
    for x in range(proc_count):
        proc_list.append(multiprocessing.Process(target=bot_threads, args=[room, proxies]))
        proc_list[-1].start()

def bot_threads(room, proxies):
    thread_count = 50
    threads_list=[]
    for x in range(thread_count):
        threads_list.append(Thread(target=bot2, args=[room, proxies]))
        threads_list[-1].start()
    for thread in threads_list:
        thread.join()
        print('thread ended')


def bot2(room, proxies):
    media_gets = 300
    max_sessions = 8
    sessions = []
    for x in range(100):
        try:
            jar = requests.cookies.RequestsCookieJar()
            jar.set('agreeterms', 1, domain='www.chaturbate.com', path='/')
            s = requests.Session()
            selected_proxy = random.choice(proxies)
            proxy = {'https': 'https://{}:{}'.format(selected_proxy['ip'], selected_proxy['port'])}
            url = 'https://chaturbate.com/{}/'.format(room)
            main_page = s.get(url, timeout=(20,5),
                              cookies=jar, proxies=proxy)
            sessions.append([s, proxy])
        except Exception as e:
            pass
        if len(sessions) >= max_sessions:
            break
    if len(sessions) == 0:
        print('no working sessions. Ending')
        return
    soup = BeautifulSoup(main_page.content, 'lxml')
    scripts = soup.find_all('script')
    found_content = ''
    for script in scripts:
        contents = script.text
        if 'window.initialRoomDossier' in contents:
            found_content = contents
    if found_content:
        new_url = found_content.split('\n')[2].split(' = ')[1].split('hls_source')[1].split('m3u8')[0]
        new_url = new_url.replace('\\u0022', '').replace('\\u002D', '-')[2:]+'m3u8'
        base_url = new_url.split('playlist.m3u8')[0]
        for s in sessions:
            try:
                initial_playlist = s[0].get(new_url, proxies=s[1], timeout=20)
            except Exception as e:
                return
            m3u8_data = m3u8.loads(initial_playlist.text)
            s.append(m3u8_data)
        for x in range(media_gets):
            for s in sessions:
                try:
                    if s[2].playlists:
                        playlist = '{}{}'.format(base_url, s[2].playlists[0].uri)
                        try:
                            playlist_data = s[0].get(playlist, proxies=s[1], timeout=20)
                            m3u8_media = m3u8.loads(playlist_data.text)
                            if m3u8_media.segments:
                                get_uri = '{}{}'.format(base_url, m3u8_media.segments[-1].uri)
                                print(get_uri)
                                time.sleep(1)
                                s[0].get(get_uri, proxies=s[1], timeout=20)
                                time.sleep(1)
                                s[0].get('https://chaturbate.com/api/panel_contest/{}/'.format(room), proxies=s[1], timeout=20)
                                time.sleep(1)
                        except:
                            pass
                    else:
                        print('no Playlists. streaming has stopped')
                        return
                except:
                    pass
    else:
        print('streaming has stopped')
        return

