import random
from selenium import webdriver
import time
from selenium.webdriver.firefox.options import Options

def user_room_bot(room,proxies, mintime=40, maxtime=150, headless=True):
    got_page = False
    get_counter = 0
    while not got_page and get_counter < 6:
        try:
            selected_proxy = random.choice(proxies)
            proxy = {'https': 'https://{}:{}'.format(selected_proxy['ip'], selected_proxy['port'])}
            print('starting firefox')
            if headless:
                options = Options()
                options.headless = True
                driver = webdriver.Firefox(options=options, proxy=proxy)
            else:
                driver = webdriver.Firefox(proxy=proxy)
            url = 'https://chaturbate.com{}'.format(room)
            time_wait=random.randint(mintime,maxtime)
            print('showing {} with wait {} seconds'.format(url, time_wait))
           driver.get(url)
           try:
               el=driver.find_element_by_link_text('I AGREE')
               el.click()
           except:
               pass
           driver.close()
        except Exception as e:
            print(e)



